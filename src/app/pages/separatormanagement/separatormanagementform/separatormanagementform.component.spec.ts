import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeparatormanagementformComponent } from './separatormanagementform.component';

describe('SeparatormanagementformComponent', () => {
  let component: SeparatormanagementformComponent;
  let fixture: ComponentFixture<SeparatormanagementformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeparatormanagementformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeparatormanagementformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

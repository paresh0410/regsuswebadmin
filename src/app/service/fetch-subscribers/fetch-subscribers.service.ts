import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class FetchSubscribersService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchSubUrl = this.baseUrl + webApi.apiUrl.fetchSubscribers;
  validdata: any = [];
  invaliddata: any = [];
  constructor(public http: HttpClient) { }

  public getvalidUser(){
    return this.validdata;
  }
  public setvalidUser(vliData){
    this.validdata = vliData;
  }
  public getinvalidUser(){
    return this.invaliddata;
  }
  public setinvalidUser(invliData){
    this.invaliddata = invliData;
  }
  public resetUser(){
    this.validdata = [];
    this.invaliddata = [];
  }

  public fetchSubscriber(subscData: any): Promise<any> {

    return this.http.post(this.fetchSubUrl, subscData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

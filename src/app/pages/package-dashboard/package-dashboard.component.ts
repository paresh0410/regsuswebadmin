import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { BrowserInfoService } from '../../service/browser-info.service';
import { ExcelService } from '../../service/excel.service';
import { DashboardService } from '../../service/dashboard/dashboard.service';
import { PassServService } from '../../service/pass-serv.service';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';


export const MY_CUSTOM_FORMATS = {
  parseInput: 'DD-MM-YYYY',
  fullPickerInput: 'DD-MM-YYYY',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};
@Component({
  selector: 'ngx-package-dashboard',
  templateUrl: './package-dashboard.component.html',
  styleUrls: ['./package-dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    // `MomentDateTimeAdapter` can be automatically provided by importing
    // `OwlMomentDateTimeModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },

    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
  ],
})
export class PackageDashboardComponent implements OnInit {
  registerForm: any;
  minDate: any = new Date();
  maxDate: any = new Date();
  packageLists: any = [];
  packageList: any = [
    {
      name: 'Default',
      revenue: '',
      renewed: '',
      expired: '',
      subcriber: 200,
    },
    {
      name: 'Gold',
      revenue: '3000',
      renewed: 23,
      expired: 40,
      subcriber: 200,
    },
    {
      name: 'Gold',
      revenue: '3000',
      renewed: 23,
      expired: 40,
      subcriber: 200,
    },
    {
      name: 'Gold',
      revenue: '3000',
      renewed: 23,
      expired: 40,
      subcriber: 200,
    },
    {
      name: 'Gold',
      revenue: '3000',
      renewed: 23,
      expired: 40,
      subcriber: 200,
    },
    {
      name: 'Gold',
      revenue: '3000',
      renewed: 23,
      expired: 40,
      subcriber: 200,
    },
    {
      name: 'Gold',
      revenue: '3000',
      renewed: 23,
      expired: 40,
      subcriber: 200,
    },
    {
      name: 'Gold',
      revenue: '3000',
      renewed: 23,
      expired: 40,
      subcriber: 200,
    },
    {
      name: 'Gold',
      revenue: '3000',
      renewed: 23,
      expired: 40,
      subcriber: 200,
    },
    {
      name: 'Gold',
      revenue: '3000',
      renewed: 23,
      expired: 40,
      subcriber: 200,
    },

  ]
  dataArrayforExcel: any = [];
  dataObjforExcel: any = {};
  startDate: any;
  endDate: any;
  submitted: boolean = false
  p: any
  disableAll: any
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private dashboard: DashboardService,
    private browserInfoService: BrowserInfoService,
    private excelService: ExcelService,
    private passService: PassServService, public cdf: ChangeDetectorRef
  ) { }



  ngOnInit() {

    const today = new Date();
    const end = this.convert(today);
    const start = this.convert(today.setDate(today.getDate() - 7));

    console.log('this.packageList', this.packageList);
    this.registerForm = this.formBuilder.group({
      startDt: [start, [Validators.required]],
      endDt: [end, [Validators.required]],
      // status: ['1', [Validators.required]],
    });

    this.startDate = this.convert(this.registerForm.value.startDt);
    this.endDate = this.convert(this.registerForm.value.endDt);

    this.fetchActivity(this.startDate, this.endDate);
  }

  get f() {
    return this.registerForm.controls;
  }
  onSubmit() {
    // this.submitted = true;
    console.log(this.registerForm.value);
    if (this.registerForm.invalid) {
      return;
    } else {
      // this.spinner.show();
      this.startDate = this.convert(this.registerForm.value.startDt);
      this.endDate = this.convert(this.registerForm.value.endDt);
      const params = {
        FrmDate: this.startDate,
        ToDate: this.endDate,
      };
      console.log('params', params);
      this.fetchActivity(this.startDate, this.endDate);
    }
  }

  convert(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  fetchActivity(start, end) {
    this.spinner.show();
    if (!window.navigator.onLine) {
      this.spinner.hide();
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        FrmDate: start,
        ToDate: end,
      };
      this.dashboard.getDashboardReportPackage(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              this.packageLists = res.data[0];
              this.cdf.detectChanges();

              console.log('this.packageLists', this.packageLists);
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
    }
  }

  viewDetail(item) {
    console.log("item", item);
    this.passService.packId = item.pkgId;
    this.passService.startdate = this.startDate;
    this.passService.enddate = this.endDate;
    this.passService.pkgName = item.pkgName;


    this.router.navigate(['pages/package-dashboard/package-detail']);

  }

  exportExcelCorp() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.packageLists.length; i++) {
      this.dataObjforExcel = {
        'NAME': this.packageLists[i].pkgName,
        'SuBSCRIBERS': this.packageLists[i].SubscribersCount,
        'REVENUES-SUM': this.packageLists[i].RevenuesSum,
        'RENEWED': this.packageLists[i].RenewedCount,
        'EXPIRED': this.packageLists[i].ExpiredCount,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSXS();
  }

  exportAsXLSXS(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Package Details');
  }

}

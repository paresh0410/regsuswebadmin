import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageLibraryPopupComponent } from './image-library-popup.component';

describe('ImageLibraryPopupComponent', () => {
  let component: ImageLibraryPopupComponent;
  let fixture: ComponentFixture<ImageLibraryPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageLibraryPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageLibraryPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

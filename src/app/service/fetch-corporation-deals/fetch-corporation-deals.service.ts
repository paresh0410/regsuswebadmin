import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class FetchCorporationDealsService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchDealsUrl = this.baseUrl + webApi.apiUrl.fetchCorporationDeals;
  constructor(public http: HttpClient) { }
  public fetchCorpDeals(dealsData: any): Promise<any> {

    return this.http.post(this.fetchDealsUrl, dealsData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditInterestsComponent } from './add-edit-interests.component';

describe('AddEditInterestsComponent', () => {
  let component: AddEditInterestsComponent;
  let fixture: ComponentFixture<AddEditInterestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditInterestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditInterestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

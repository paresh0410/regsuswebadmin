import { TestBed } from '@angular/core/testing';

import { FetchCorporationsService } from './fetch-corporations.service';

describe('FetchCorporationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchCorporationsService = TestBed.get(FetchCorporationsService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { FetchForumsService } from './fetch-forums.service';

describe('FetchForumsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchForumsService = TestBed.get(FetchForumsService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { AddEditNewsOpinionsService } from './add-edit-news-opinions.service';

describe('AddEditNewsOpinionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditNewsOpinionsService = TestBed.get(AddEditNewsOpinionsService);
    expect(service).toBeTruthy();
  });
});

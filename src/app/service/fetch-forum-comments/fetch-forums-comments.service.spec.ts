import { TestBed } from '@angular/core/testing';

import { FetchForumsCommentsService } from './fetch-forums-comments.service';

describe('FetchForumsCommentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchForumsCommentsService = TestBed.get(FetchForumsCommentsService);
    expect(service).toBeTruthy();
  });
});

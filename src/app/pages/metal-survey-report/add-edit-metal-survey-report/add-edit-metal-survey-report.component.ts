import { Component, OnInit } from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormControl
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { PassServService } from "../../../service/pass-serv.service";
import { UploadMsReportFileService } from "../../../service/upload-ms-report-file/upload-ms-report-file.service";
import { AddEditMsReportService } from "../../../service/add-edit-ms-report/add-edit-ms-report.service";
import {
    DateTimeAdapter,
    OWL_DATE_TIME_FORMATS,
    OWL_DATE_TIME_LOCALE,
    OwlDateTimeComponent,
    OwlDateTimeFormats
} from "ng-pick-datetime";
import { MomentDateTimeAdapter } from "ng-pick-datetime-moment";
import * as _moment from "moment";
import { Moment } from "moment";
import { BrowserInfoService } from "../../../service/browser-info.service";

const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_MOMENT_DATE_TIME_FORMATS: OwlDateTimeFormats = {
    parseInput: "MM/YYYY",
    fullPickerInput: "l LT",
    datePickerInput: "MM/YYYY",
    timePickerInput: "LT",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY"
};

@Component({
    selector: "ngx-add-edit-metal-survey-report",
    templateUrl: "./add-edit-metal-survey-report.component.html",
    styleUrls: ["./add-edit-metal-survey-report.component.scss"],
    providers: [
        // `MomentDateTimeAdapter` and `OWL_MOMENT_DATE_TIME_FORMATS` can be automatically provided by importing
        // `OwlMomentDateTimeModule` in your applications root module. We provide it at the component level
        // here, due to limitations of our example generation script.
        {
            provide: DateTimeAdapter,
            useClass: MomentDateTimeAdapter,
            deps: [OWL_DATE_TIME_LOCALE]
        },

        { provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_DATE_TIME_FORMATS }
    ]
})
export class AddEditMetalSurveyReportComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    public name1: String = "Choose image or video";
    public str1: String = "";
    message: string;
    public imagePath;
    public filePath;
    imgURL: any;
    isImg: any;
    isFile: any;
    reportData: any = [];
    test: any;
    nsopImage: any;
    public showlbl: any;
    disableAll: any = false;
    year: any = new Date();
    udata: any;
    bowserInfo: any;
    month: any = new Date();
    minDate: any = new Date();
    monthList: any = [
        { monthcode: "Jan" },
        { monthcode: "Feb" },
        { monthcode: "Mar" },
        { monthcode: "Apr" },
        { monthcode: "May" },
        { monthcode: "Jun" },
        { monthcode: "Jul" },
        { monthcode: "Aug" },
        { monthcode: "Sep" },
        { monthcode: "Oct" },
        { monthcode: "Nov" },
        { monthcode: "Dec" }
    ];
    errorImsg: boolean = false;
    public dateTime = new FormControl(moment());

    constructor(
        private spinner: NgxSpinnerService,
        private formBuilder: FormBuilder,
        private router: Router,
        private routes: ActivatedRoute,
        private toastr: ToastrService,
        private passSer: PassServService,
        private upldSer: UploadMsReportFileService,
        private addEditSer: AddEditMsReportService,
        private browserInfoService: BrowserInfoService
    ) {
        this.reportData = this.passSer.editMS;
    }

    ngOnInit() {
        const yr = this.year.getFullYear();
        this.udata = JSON.parse(localStorage.getItem("UserData"));
        this.bowserInfo = this.browserInfoService.get_browser();
        for (let i = 0; i < this.monthList.length; i++) {
            this.monthList[i].monthcode = this.monthList[i].monthcode + "-" + yr;
        }
        console.log(this.monthList);
        if (this.reportData[0]) {
            this.disableAll = true;
            this.imgURL = this.reportData[0].msreportFile;
            this.month = new Date(this.reportData[0].monthFor);
            this.dateTime.setValue(this.month);
            this.registerForm = this.formBuilder.group({
                rname: [this.reportData[0].repName, [Validators.required]],
                rmonth: [
                    { value: new Date(this.reportData[0].repMonth), disabled: true },
                    [Validators.required]
                ],
                status: [this.reportData[0].isActive, [Validators.required]]
            });
            if (this.reportData[0].msreportFile) {
                this.name = this.reportData[0].msreportFile;
                const str12: any[] = this.name.split("getMsReportFile");
                console.log(str12);
                this.name = this.str = str12[str12.length - 1];
                const str123: any[] = this.name.split("/msreportFiles/");
                const lbl = str123[str123.length - 1];
                this.showname(lbl);
            }
            if (this.reportData[0].rpMonth) {
                this.reportData[0].rpMonth = new Date(this.reportData[0].rpMonth);
            }
        } else {
            this.registerForm = this.formBuilder.group({
                rname: ["", [Validators.required]],
                rmonth: [this.year, [Validators.required]],
                status: ["1", [Validators.required]]
            });
        }
        console.log(this.registerForm.value);
    }

    get f() {
        return this.registerForm.controls;
    }

    back() {
        this.router.navigate(["pages/ms-report"]);
    }

    removeFile() {
        this.name = "Choose file";
        this.typeRef = true;
        this.str = "";
    }

    // choose file
    public name: String = "Choose file";
    public str: String = "";
    typeRef: any = true;

    changeLabelName(file) {
        this.name = this.str;
        var size = file[0].size;
        if (file[0].type !== "application/pdf") {
            this.str = "";
            this.isFile = "Only pdf type is supported.";
            return;
        } else {
            // for (var i = 0; i < file.length; i++) {
            //   var nameF = file[i].name;
            //   var size =  Math.round(file[i].size / 1024);
            // }
            if (size < 102400  || size > 5120000) {
                this.errorImsg = true;
                this.str = "";
            } else {
                this.errorImsg = false;
                this.filePath = file;
                this.name = this.str;
                const str12: any[] = this.name.split("\\");
                this.name = str12[str12.length - 1];
                this.showname(this.name);
            }
        }
    }
    showname(getname) {
        this.showlbl = getname;
        this.typeRef = false;
    }

    checkVal(event) {
        console.log(this.registerForm.value);

        console.log("event", event);
        if (event.target.placeholder === "Report Name") {
            if (!event.target.value) {
                this.toastr.error("Report Name is required");
            }
        }
        // if (event.target.placeholder === 'Report Month') {
        //   if (!event.target.value) {
        //     this.toastr.error('Report Month is required');
        //   }
        // }
    }

    chosenYearHandler(normalizedYear: Moment) {
        const ctrlValue = this.dateTime.value;
        ctrlValue.year(normalizedYear.year());
        this.dateTime.setValue(ctrlValue);
        this.registerForm.controls.rmonth.setValue(this.dateTime.value);
    }

    chosenMonthHandler(
        normalizedMonth: Moment,
        datepicker: OwlDateTimeComponent<Moment>
    ) {
        const ctrlValue = this.dateTime.value;
        ctrlValue.month(normalizedMonth.month());
        this.dateTime.setValue(ctrlValue);
        this.registerForm.controls.rmonth.setValue(this.dateTime.value);
        datepicker.close();
    }

    formatDateNewDash(date) {
        var d = new Date(date),
            month = "" + (d.getMonth() + 1),
            day = "" + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = "0" + month;
        if (day.length < 2) day = "0" + day;
        return [year, month].join("-");
    }

    onSubmit() {
        this.submitted = true;
        let formData = new FormData();
        console.log(this.registerForm.value);
        if (new Date(this.registerForm.controls.rmonth.value) > new Date()) {
            this.toastr.warning("Please select past months", "", {
                timeOut: 6000
            });
            return;
        }
        if (this.registerForm.invalid) {
            if (!this.str) {
                this.isFile = "Please select file";
            }
            return;
        } else if (!this.str) {
            this.isFile = "Please select file";
            return;
        } else {
            const content = {
                rpId: 0,
                rpName: this.registerForm.value.rname,
                rpMonth: this.formatDateNewDash(
                    this.registerForm.controls.rmonth.value
                ),
                rfile: this.filePath ? null : this.name,
                isAct: this.registerForm.value.status,
                uid: this.udata.userid,
                uname: this.udata.subname,
                bname: this.bowserInfo.name,
                bversion: this.bowserInfo.version
            };

            if (this.reportData[0]) {
                if (this.reportData[0].repId) {
                    content.rpId = this.reportData[0].repId;
                }
            }
            if (this.filePath) {
                formData.append("file", this.filePath[0], this.filePath[0].name);
            }
            this.spinner.show();
            console.log("formData", formData);

            console.log("content", content);

            // service for uploading category images
            if (this.filePath) {
                this.upldSer.uploadMsReportFile(formData).then(
                    res => {
                        
                        try {
                            if (res.error_code === 0) {
                                console.log("res", res);
                                content.rfile = res.path;

                                // service for add edit category
                                this.addEditSer.addEditMsReports(content).then(
                                    res => {
                                        this.spinner.hide();
                                        try {
                                            if (res.type === true && res.status_code === 200) {
                                                console.log("res", res);
                                                try {
                                                    if (res.data) {
                                                        if (res.data.error) {
                                                            this.toastr.error(res.data.error);
                                                        } else if (res.data.updated) {
                                                            this.router.navigate(["pages/ms-report"]);
                                                            this.toastr.success(res.data.updated);
                                                        } else if (res.data.saved) {
                                                            this.router.navigate(["pages/ms-report"]);
                                                            this.toastr.success(res.data.saved);
                                                        }
                                                    }
                                                } catch (e) {
                                                    console.log(e);
                                                }
                                            }
                                            if (res.type === false) {
                                                this.spinner.hide();
                                                console.log("res", res);
                                            }
                                        } catch (e) {
                                            console.log(e);
                                        }
                                    },
                                    err => {
                                        console.log(err);
                                        this.spinner.hide();
                                        this.toastr.error("Server Error");
                                    }
                                );
                            }
                            if (res.error_code === 1) {
                                this.spinner.hide();
                                console.log("res", res);
                                this.toastr.error(res.err_desc);
                            }
                        } catch (e) {
                            console.log(e);
                            this.spinner.hide();
                        }
                    },
                    err => {
                        console.log(err);
                        this.spinner.hide();
                        this.toastr.error("Server Error");
                    }
                );
            } else {
                this.addEditSer.addEditMsReports(content).then(
                    res => {
                        this.spinner.hide();
                        try {
                            if (res.type === true && res.status_code === 200) {
                                console.log("res", res);
                                try {
                                    if (res.data) {
                                        if (res.data.error) {
                                            this.toastr.error(res.data.error);
                                        } else if (res.data.updated) {
                                            this.router.navigate(["pages/ms-report"]);
                                            this.toastr.success(res.data.updated);
                                        } else if (res.data.saved) {
                                            this.router.navigate(["pages/ms-report"]);
                                            this.toastr.success(res.data.saved);
                                        }
                                    }
                                } catch (e) {
                                    console.log(e);
                                }
                            }
                            if (res.type === false) {
                                this.spinner.hide();
                                console.log("res", res);
                            }
                        } catch (e) {
                            this.spinner.hide();
                            console.log(e);
                        }
                    },
                    err => {
                        console.log(err);
                        this.spinner.hide();
                        this.toastr.error("Server Error");
                    }
                );
            }
        }
    }
}

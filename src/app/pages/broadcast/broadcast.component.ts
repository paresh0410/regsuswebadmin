import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { webApi } from '../../../config';
import { BroadcastService } from '../../service/broadcast/broadcast.service';
import { BrowserInfoService } from '../../service/browser-info.service';
import { CommonService } from '../../service/common.service';
import { ExcelService } from '../../service/excel.service';

@Component({
  selector: 'ngx-broadcast',
  templateUrl: './broadcast.component.html',
  styleUrls: ['./broadcast.component.scss']
})
export class BroadcastComponent implements OnInit {
  tableList: any = []
  search = {
    title: '',
    type: '',
    category: '',
    publishDate: '',
    expiryDate: '',
    status: ''
  };

  column: any
  isDesc: boolean = false
  arrow = false;
  userData: any
  bowserInfo: any
  dataArrayforExcel: any = [];
  flag: any;
  dataObjforExcel: any = []
  p: any
  setveractive: boolean = false
  constructor(private router: Router, public commonService: CommonService, public browserInfoService: BrowserInfoService,
    private broadcastService: BroadcastService, private excelService: ExcelService) { }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
    this.fetchBroadcast()
  }

  addEditBrodcast(item, val) {
    this.broadcastService.broadcastData = item
    this.router.navigate(['/pages/broadcast/add-edit-broadcast'])
  }


  clearBtn() {
    this.search.title = ''
    this.search.type = ''
    this.search.category = ''
    this.search.publishDate = ''
    this.search.expiryDate = ''
    this.search.status = ''
  }

  myFunction(event) {
    // if (event.target.value == 1){
    //   this.search.status = 'Active'
    // } else {
    //   this.search.status = 'InActive'

    // }
    this.search.status = event.target.value;
    console.log('value', event);
  }


  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.tableList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  fetchBroadcast() {
    const url = webApi.baseUrl + webApi.apiUrl.fetchBroadcast
    const param = {
      bname: this.bowserInfo.name,
      bversion: this.bowserInfo.version,
      uid: this.userData.userid,
      uname: this.userData.subname
    }
    this.commonService.httpPostRequest(url, param).then((res: any) => {
      console.log(res)
      try {
        if (res.type) {
          this.tableList = res.data
          console.log(this.tableList)
        }
      } catch (error) {

      }
    })
  }


  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.tableList.length; i++) {
      // if (this.tableList[i].isActive === 1) {
      //   this.flag = 'Active';
      // }
      // if (this.tableList[i].isActive === 2) {
      //   this.flag = 'Inactive';
      // }
      this.dataObjforExcel = {
        'TITLE': this.tableList[i].title,
        'TYPE': this.tableList[i].type,
        'RECIPENT': this.tableList[i].category,
        'PUBLISH DATE': this.tableList[i].publishDate,
        'EXPIRY DATE': this.tableList[i].expiryDate,
        'STATUS': this.tableList[i].status == 1 ? 'Active' : 'Inactive',
      }
      // console.log(this.dataObjforExcel)
      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Broadcast Details');
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class FetchInterestsService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchInterestsUrl = this.baseUrl + webApi.apiUrl.fetchInterests;
  constructor(public http: HttpClient) { }
  public fetchInterests(data: any): Promise<any> {

    return this.http.post(this.fetchInterestsUrl, data, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

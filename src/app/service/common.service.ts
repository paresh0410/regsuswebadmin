import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { webApi } from '../../config';
import { BrowserInfoService } from './browser-info.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  userData: any
  bowserInfo: any

  constructor(public http: HttpClient, private browserInfoService: BrowserInfoService) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  httpPostRequest(url: any, parameters: any) {
    return new Promise((resolve, reject) => {
      this.http.post(url, parameters).subscribe((success) => {
        resolve(success);
      }, (error) => {
        reject(error);
      });
    });
  }

  formattime(time) {
    var d = new Date(time),
      hr = '' + d.getHours(),
      mm = '' + d.getMinutes(),
      ss = '' + d.getSeconds(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (hr.length < 2) hr = '0' + hr;
    if (mm.length < 2) mm = '0' + mm;
    if (ss.length < 2) ss = '0' + ss;
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var da = [year, month, day].join('-');
    var ti = [hr, mm, ss].join(':');
    return [da, ti].join(' ');
  }


  getDateDiff(date) {
    var today = new Date();
    var selectedDate = new Date(date);
    var age = selectedDate.getDate() - today.getDate();
    return age;
  }

  datediff(first, second) {
    return Math.round((second - first) / (1000 * 60 * 60 * 24));
  }

  dataURItoBlob(dataURI) {
    var binary = atob(dataURI.split(',')[1]);
    var array = [];
    for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
      type: 'image/jpg'
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { FetchMsReportService } from '../../service/fetch-MsReport/fetch-ms-report.service';
import { BrowserInfoService } from '../../service/browser-info.service';
@Component({
  selector: 'ngx-metal-survey-report',
  templateUrl: './metal-survey-report.component.html',
  styleUrls: ['./metal-survey-report.component.scss']
})
export class MetalSurveyReportComponent implements OnInit {
  reportList: any = [];
  p: number = 1;
  // fileName: any;
  fileUrl: any;
  fileName: any = 'No file chosen';
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  checkPay: any;
  search = {
    repName: '',
    repMonth: '',
    createdDate: '',
    isActive: '',
  };
  featureLength: boolean = true;
  setveractive: any = false;
  udata: any;
  bowserInfo:any;
  baseUrl: any = webApi.baseUrl;
  getDownload = this.baseUrl + webApi.apiUrl.getMsReportFile;
  getDownloadMS = this.baseUrl + webApi.apiUrl.downLoadMsReport;

  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private fetchMSRepServ: FetchMsReportService,
    private browserInfoService: BrowserInfoService,
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.udata = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
    console.log(this.udata);
    this.fetchFeatures();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.reportList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.repName = '';
    this.search.repMonth = '';
    this.search.createdDate = '';
    this.search.isActive = '';
  }

  myFunction(event) {
    this.search.isActive = event.target.value;
    console.log('value', event);
  }

    // Export To Excel
    exportExcel() {
      this.dataArrayforExcel = [];
      for (let i = 0; i < this.reportList.length; i++) {
        if (this.reportList[i].isActive === 1) {
          this.flag = 'Active';
        }
        if (this.reportList[i].isActive === 2) {
          this.flag = 'Inactive';
        }
        this.dataObjforExcel = {
          'REPORT NAME': this.reportList[i].repName,
          'REPORT MONTH': this.reportList[i].repMonth,
          'REPORT DATE': this.reportList[i].createdDate,
          'STATUS': this.flag,
        }
        // console.log('this.dataArray',this.dataObjforExcel)
  
        this.dataArrayforExcel.push(this.dataObjforExcel);
        //  for export to excel END //
      }
      this.exportAsXLSX();
    }
  
    exportAsXLSX(): void {
      this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'MS Report Details');
    }

    addMS(){
      this.passSer.editMS = [];
      this.router.navigate(['pages/ms-report/add-edit-ms-report']);
    }

    editMS(item) {
      this.passSer.editMS[0] = (item);
      this.router.navigate(['pages/ms-report/add-edit-ms-report']);
    }

    DownloadPdf(file) {
      // this.reportList[i].msreportFile = this.getDownload + this.reportList[i].msreportFile;
      if (!window.navigator.onLine){
        this.toastr.error('Please check your internet connection');
      } else {
        window.open(file, '_self');
      }
    }

  fetchFeatures() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      const params = {
        uid : this.udata.userid,
        uname : this.udata.username,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      }
      this.spinner.show();
      this.fetchMSRepServ.fetchMsReport(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
            // let userD = [];
            this.reportList = res.data;
            console.log('this.featureList',this.reportList);
            if(this.reportList.length === 0){
              this.featureLength = false;
            } else if(this.reportList.length > 0){
              this.featureLength = true;
              for (let i = 0; i < this.reportList.length; i++) {
                this.reportList[i].download = '?path=' + this.reportList[i].msreportFile;
              }
              for (let i = 0; i < this.reportList.length; i++) {
                this.reportList[i].msreportFile = this.getDownload + this.reportList[i].msreportFile;
                this.reportList[i].download = this.getDownloadMS + this.reportList[i].download;
              }
            }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.setveractive = true;
        this.reportList = [];
        this.toastr.error('Server Error');
      });
    }
  }
}

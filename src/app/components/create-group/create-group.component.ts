import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.scss']
})
export class CreateGroupComponent implements OnInit {
  public name1: String = 'Choose image';
  public str1: String = '';
  message: string;
  public imagePath;
  public filePath;
  imgURL: any;
  isImg: any;
  isFile: any;
  grpName: any = '';
  @Input() participants: any = [];
  @Input() image: any;
  @Input() interests: any = [];
  @Input() corporations: any = [];
  selectedItems: any = [];
  settings = { 
    text:'Select Participants',
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    showCheckbox: true,
    badgeShowLimit: 4,
  };
  selectedIns: any = [];
  dropdownSettings: any = {
    singleSelection: false,
    idField: 'interestId',
    textField: 'iname',
    itemsShowLimit: 1,
    allowSearchFilter: true,
  };
  selectedCorps: any = [];
  dropdownCorpSettings: any = {
    singleSelection: false,
    idField: 'corpId',
    textField: 'corname',
    itemsShowLimit: 1,
    allowSearchFilter: true,
  };
  openCreateGrp: any = true;
  constructor() { }

  ngOnInit() {
  }
  // image preview
  preview(files) {
    this.name1 = this.str1;
    const str11: any[] = this.name1.split('\\');
    this.name1 = str11[str11.length - 1];

    if (files.length === 0)
      return;

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.name1 = 'Choose image';
      this.message = 'Only images are supported.';
      return;
    } else {
      this.message = '';
      const reader = new FileReader();
      this.imagePath = files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.imgURL = reader.result;
      };
    }
  }
  removeImage() {
    this.str1 = '';
    this.name1 = 'Choose Image';
    this.imgURL = '';
  }
  onItemSelect(item: any) {
    console.log("Event Select",item);
    console.log(this.selectedItems);
    //this.selectedItems=item;
  }
  OnItemDeSelect(item: any) {
    console.log("Event",item);
    console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onItemDeSelect(itemsall){
    
  }
  createGroup() {
    this.openCreateGrp = !this.openCreateGrp;
  }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { PassServService } from '../../../../service/pass-serv.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AddEditDealService } from '../../../../service/add-edit-deal/add-edit-deal.service';
import { BrowserInfoService } from '../../../../service/browser-info.service';
import { invalid } from '@angular/compiler/src/render3/view/util';
import { CommonService } from '../../../../service/common.service';

@Component({
  selector: 'ngx-add-edit-sub-deal',
  templateUrl: './add-edit-sub-deal.component.html',
  styleUrls: ['./add-edit-sub-deal.component.scss']
})
export class AddEditSubDealComponent implements OnInit, OnDestroy {
  dropdownList: any = [];
  durationList: any = [];
  packageList: any = [];
  dealsData: any = [];
  registerForm: FormGroup;
  submitted = false;
  trkId: any = Math.random().toString(36).substr(2, 8).toUpperCase();
  checkAdd: any = false;
  selectedItems: any = [];
  dropdownList1: any = [
    // {subId: 1, subname: "Dwayne Johnson"},
    // {subId: 2, subname: "Renolds"},
    // {subId: 3, subname: "Tom"},
    // {subId: 4, subname: "testUser1"},
    // {subId: 5, subname: "testUser2"}
  ];
  selectedItems1: any = [
    // {subId: 1, subname: "Dwayne Johnson"},
    // {subId: 4, subname: "testUser1"},
  ];
  dropdownSettings1: any = {
    singleSelection: false,
    idField: 'subId',
    textField: 'subname',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };
  isCheck: any;
  isCheck1: any;
  todayDate: any = new Date();
  fromDate: any = new Date();
  frmdt: any = new Date();
  prdt: any;
  payrecdt: any;
  todt: any;
  gracePir: any;
  duration: any;
  maxDate: any = new Date();
  feaCostArray: any = [];
  featuresCost: any = 0;
  discount: any = 0;
  perCount: any = 0;
  gst: any = 18;
  corpData: any = [];
  baseAmt: any = [];
  fidList: any = [];
  tempCost: any = 0;
  disablebtn: any = false;
  hidePopup: boolean = true;
  isdis: boolean = true;
  singlePkg: any;
  tempfeature: any = [];
  pkgPrice: any;
  feaAdd: any = false;
  uAdd: any = false;
  graceAdd: any = true;
  disDcnt: any = false;
  renewList: any = [];
  userData: any;
  bowserInfo: any;
  confirmList: any = {
    countUser: '',
    amountBase: '',
    discountPer: '',
    gstNo: '',
    totalAmount: '',
    baseAmt: '',
    accprice: '',
    pkg: '',
  };
  cgst: any = 0;
  sgst: any = 0;
  igst: any = 0;
  gtypeid: any = 0;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private dealSer: AddEditDealService,
    private browserInfoService: BrowserInfoService,
    private commonService: CommonService
  ) {
    this.corpData = this.passSer.editCorp;
    this.dealsData = this.passSer.editDeals;
    console.log(' this.dealsData', this.dealsData)
    this.dropdownList = this.passSer.featureList;
    this.durationList = this.passSer.durationList;
    this.packageList = this.passSer.packageList;
    this.renewList = this.passSer.renewDeals;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  disbaleformdate = false;
  statusDropdown = false;

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    if (this.dealsData[0]) {

      // this.checkPackage1(this.dealsData[0].selPckage.pkgId)
      this.selectedItems = this.dealsData[0].featureList;
      for (let i = 0; i < this.selectedItems.length; i++) {
        this.tempfeature.push(this.selectedItems[i].featureId);
      }
      // const tempObj = {
      //   featureId: 2, featurename: "Polls",
      //   // featureId: 3, featurename: "Forums"
      // };
      // this.selectedItems.push(tempObj)
      this.feaCostArray = [];
      for (let i = 0; i < this.dropdownList.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (this.selectedItems[j].featureId === this.dropdownList[i].featureId) {
            this.feaCostArray.push(this.dropdownList[i]);
          }
        }
      }
      console.log('this.featuresCost', this.feaCostArray);
      this.pkgPrice = this.dealsData[0].selPckage.price;
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }
      this.registerForm = this.formBuilder.group({
        trackId: [{ value: this.dealsData[0].trackId, disabled: true }, [Validators.required]],
        duration: [this.dealsData[0].durationId, [Validators.required]],
        purDate: [{ value: this.formatDateNew(this.dealsData[0].purDate), disabled: true }, [Validators.required]],
        fromDate: [this.formatDateNew(this.dealsData[0].fromDate), [Validators.required]],
        toDate: [{ value: this.formatDateNew(this.dealsData[0].toDate), disabled: false }, [Validators.required]],
        discountPercent: [this.dealsData[0].discountPercent, [Validators.required, Validators.pattern('^([0-9]|([1-9][0-9])|100)$')]],
        // userCount: [this.dealsData[0].userCount, [Validators.required, Validators.pattern('[0-9]*')]],
        totalAmt: [{ value: this.dealsData[0].totalAmt, disabled: true }, [Validators.required]],
        dealstaname: [this.dealsData[0].dealstaId, [Validators.required]],
        status: [this.dealsData[0].isActive, [Validators.required]],
        paytype: [this.dealsData[0].payType],
        gross: [this.dealsData[0].gross],
        deducted: [this.dealsData[0].deducted],
        netAmount: [this.dealsData[0].netAmount],
        recptno: [this.dealsData[0].receiptNo],
        paydate: [this.dealsData[0].payDate],
        pkg: [this.dealsData[0].selPckage.pkgId, [Validators.required]],
        grcPir: [this.dealsData[0].gracePeriod],
        payRecDate: [this.dealsData[0].payRecDate],
      });
      // this.todt = this.formatDate(this.dealsData[0].toDate);
      this.maxDate = new Date(this.dealsData[0].purDate);
      this.frmdt = new Date(this.dealsData[0].fromDate);
      this.duration = parseInt(this.dealsData[0].durationId);
      this.discount = this.dealsData[0].discountPercent;
      this.perCount = this.dealsData[0].userCount;
      this.baseAmt = this.dealsData[0].baseAmt;
      this.prdt = new Date(this.dealsData[0].purDate);
      let payRecDat = null;
      // console.log('this.dealsData[0].payRecDate',this.dealsData[0].payRecDate.value)
      if (this.dealsData[0].payRecDate == 'Invalid Date') {
        this.dealsData[0].payRecDate = null
      }
      if (this.dealsData[0].payRecDate) {
        payRecDat = new Date(this.dealsData[0].payRecDate);
      } else {
        payRecDat = null;
      }

      if (this.dealsData[0].paydate == 'Invalid Date') {
        this.dealsData[0].paydate = null
      }

      this.payrecdt = payRecDat;
      this.todt = new Date(this.dealsData[0].toDate);
      this.tempCost = this.pkgPrice * this.perCount;

      this.cgst = this.dealsData[0].cgst;
      this.sgst = this.dealsData[0].sgst;
      this.igst = this.dealsData[0].igst;
      this.gst = this.dealsData[0].gst;
      this.gtypeid = this.dealsData[0].gtypeid;
      // this.todt = new Date(preDate.toLocaleDateString());
      // console.log('this.dealsData[0].status', this.dealsData[0].status.value)
      // && this.dealsData[0].status === '1'
      console.log('debuggggg++++++++++++++++++++++>')

      if (this.dealsData[0].dealstaname === 'Running') {

        console.log('debuggggg++++++++++++++++++++++>')
        this.statusDropdown = false;
        this.disablebtn = true;
        this.disbaleformdate = false;
        this.graceAdd = false;
        // this.checkAdd = true;
        // this.registerForm.controls['fromDate'].disable();
        this.registerForm.controls['duration'].disable();
        this.registerForm.controls['discountPercent'].disable();
        // this.registerForm.controls['dealstaname'].disable();
        // this.registerForm.controls['userCount'].disable();
        // this.registerForm.controls['status'].disable();
        this.registerForm.controls['paytype'].disable();
        this.registerForm.controls['recptno'].disable();
        this.registerForm.controls['paydate'].disable();
        this.registerForm.controls['pkg'].disable();
      } else if (this.dealsData[0].dealstaname === 'Expired') {
        this.disablebtn = true;
        this.disbaleformdate = true;
        this.statusDropdown = true;

        this.uAdd = true;
        // this.checkAdd = true;
        this.registerForm.controls['fromDate'].disable();
        this.registerForm.controls['duration'].disable();
        this.registerForm.controls['discountPercent'].disable();
        this.registerForm.controls['dealstaname'].disable();
        // this.registerForm.controls['userCount'].disable();
        this.registerForm.controls['status'].disable();
        this.registerForm.controls['paytype'].disable();
        this.registerForm.controls['recptno'].disable();
        this.registerForm.controls['paydate'].disable();
        this.registerForm.controls['pkg'].disable();
        this.registerForm.controls['grcPir'].disable();
      } else if (this.dealsData[0].dealstaname === 'Pending' && this.dealsData[0].isActive == 2) {
        this.disablebtn = true;
        this.disbaleformdate = true;
        this.statusDropdown = true;

        this.uAdd = true;
        // this.checkAdd = true;
        this.registerForm.controls['fromDate'].disable();
        this.registerForm.controls['duration'].disable();
        this.registerForm.controls['discountPercent'].disable();
        this.registerForm.controls['dealstaname'].disable();
        // this.registerForm.controls['userCount'].disable();
        this.registerForm.controls['status'].disable();
        this.registerForm.controls['paytype'].disable();
        this.registerForm.controls['recptno'].disable();
        this.registerForm.controls['paydate'].disable();
        this.registerForm.controls['pkg'].disable();
        this.registerForm.controls['grcPir'].disable();
      } else if (this.dealsData[0].dealstaname === 'Pending' && this.dealsData[0].isActive == 1) {
        this.disablebtn = true;
        this.disbaleformdate = false;
        this.statusDropdown = false;

        this.graceAdd = false;
        // this.checkAdd = true;
        // this.registerForm.controls['fromDate'].disable();
        this.registerForm.controls['duration'].disable();
        this.registerForm.controls['discountPercent'].disable();
        // this.registerForm.controls['dealstaname'].disable();
        // this.registerForm.controls['userCount'].disable();
        // this.registerForm.controls['status'].disable();
        this.registerForm.controls['paytype'].disable();
        this.registerForm.controls['recptno'].disable();
        this.registerForm.controls['paydate'].disable();
        this.registerForm.controls['pkg'].disable();
      } else {
        this.graceAdd = false;
        this.disbaleformdate = false;

        // this.registerForm.controls['grcPir'].disable();
      }

    } else if (this.renewList[0]) {
      // this.checkPackage1(this.renewList[0].selPckage.pkgId)
      this.disDcnt = true;
      this.selectedItems = this.renewList[0].featureList;
      for (let i = 0; i < this.selectedItems.length; i++) {
        this.tempfeature.push(this.selectedItems[i].featureId);
      }
      // const tempObj = {
      //   featureId: 2, featurename: "Polls",
      //   // featureId: 3, featurename: "Forums"
      // };
      // this.selectedItems.push(tempObj)
      this.feaCostArray = [];
      for (let i = 0; i < this.dropdownList.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (this.selectedItems[j].featureId === this.dropdownList[i].featureId) {
            this.feaCostArray.push(this.dropdownList[i]);
          }
        }
      }
      console.log('this.featuresCost', this.feaCostArray);
      this.pkgPrice = this.renewList[0].selPckage.price;
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }

      // this.renewList[0].toDate = new Date('2022-11-27')
      var dates = new Date()
      const checkdateDiff = this.commonService.datediff(dates, this.renewList[0].toDate)
      console.log('checkdateDiff', checkdateDiff)
      if (checkdateDiff >= 0) {
      } else {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        this.renewList[0].toDate = date
        // this.renewList[0].toDate = new Date()
      }
      let preDate = new Date(this.renewList[0].toDate);
      preDate.setDate(preDate.getDate() + 1);
      preDate.setMonth(preDate.getMonth() + this.renewList[0].durationId);
      const lastdate = preDate.toLocaleDateString();

      this.registerForm = this.formBuilder.group({
        trackId: [{ value: this.trkId, disabled: true }, [Validators.required]],
        duration: [this.renewList[0].durationId, [Validators.required]],
        purDate: [{ value: this.todayDate, disabled: true }, [Validators.required]],
        fromDate: [this.formatDateNew(this.renewList[0].toDate), [Validators.required]],
        toDate: [{ value: this.formatDateNew(lastdate), disabled: false }, [Validators.required]],
        discountPercent: [this.renewList[0].discountPercent, [Validators.required, Validators.pattern('^([0-9]|([1-9][0-9])|100)$')]],
        // userCount: [this.renewList[0].userCount, [Validators.required, Validators.pattern('[0-9]*')]],
        totalAmt: [{ value: this.renewList[0].totalAmt, disabled: true }, [Validators.required]],
        dealstaname: ['1', [Validators.required]],
        status: ['1', [Validators.required]],
        paytype: [''],
        recptno: [''],
        paydate: [''],
        payRecDate: [''],
        pkg: [this.renewList[0].selPckage.pkgId, [Validators.required]],
        grcPir: [''],
        gross: [this.renewList[0].gross],
        deducted: [this.renewList[0].deducted],
        netAmount: [this.renewList[0].netAmount],
      });
      // this.todt = this.formatDate(this.dealsData[0].toDate);
      this.duration = parseInt(this.renewList[0].durationId);
      this.discount = this.renewList[0].discountPercent;
      this.perCount = this.renewList[0].userCount;
      this.baseAmt = this.renewList[0].baseAmt;
      this.prdt = new Date();

      this.frmdt = new Date(this.renewList[0].toDate);
      this.frmdt.setDate(this.frmdt.getDate() + 1);
      this.todt = new Date(lastdate);
      this.cgst = this.renewList[0].cgst;
      this.sgst = this.renewList[0].sgst;
      this.igst = this.renewList[0].igst;
      this.gst = this.renewList[0].gst;
      this.gtypeid = this.renewList[0].gtypeid;

      this.tempCost = this.pkgPrice * this.perCount;
      this.graceAdd = false;
      this.uAdd = true;
      this.registerForm.controls['duration'].disable();
      // this.registerForm.controls['userCount'].disable();
      this.registerForm.controls['pkg'].disable();
      // this.registerForm.controls['grcPir'].disable();

    }



    // this.fetchSubscribers();
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);

    if (this.registerForm.controls.paydate.value == 'Invalid Date') {

      this.registerForm.patchValue({
        paydate: null

      });
    }
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.hidePopup = false;
      this.confirmList.countUser = 1;
      this.confirmList.amountBase = this.baseAmt;
      this.confirmList.discountPer = this.registerForm.controls.discountPercent.value;
      this.confirmList.gstNo = 18;
      this.confirmList.totalAmount = this.registerForm.controls.totalAmt.value;
      this.confirmList.baseAmt = this.tempCost;
      for (let i = 0; i < this.packageList.length; i++) {

        this.confirmList.pkg = this.packageList[i];

      }
      // this.confirmList.pkg = this.singlePkg;
    }
  }

  get f() {
    return this.registerForm.controls;
  }

  back() {
    this.router.navigate(['pages/user/subscriber-deal']);
  }

  onItemSelect(event) {
    if (event.featureId) {
      this.feaCostArray = [];
      console.log('select', this.selectedItems);
      this.isCheck = '';
      for (let i = 0; i < this.dropdownList.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (this.selectedItems[j].featureId === this.dropdownList[i].featureId) {
            this.feaCostArray.push(this.dropdownList[i]);
          }
        }
      }
      console.log('this.featuresCost', this.feaCostArray);
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }
      console.log('this.featuresCost', this.featuresCost);
      this.calculate();
    }
    if (event.subId) {
      console.log('select1', this.selectedItems1);
      this.isCheck1 = '';
    }
  }
  onItemDeSelect(des) {
    if (des.featureId) {
      this.feaCostArray = [];
      for (let i = 0; i < this.selectedItems.length; i++) {
        if (des.featureId === this.selectedItems[i].featureId) {
          this.selectedItems.splice(i, 1);
        }
      }
      console.log('deselect', this.selectedItems);
      if (this.selectedItems.length === 0) {
        this.isCheck = 'Please select features';
      }
      for (let i = 0; i < this.dropdownList.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (this.selectedItems[j].featureId === this.dropdownList[i].featureId) {
            this.feaCostArray.push(this.dropdownList[i]);
          }
        }
      }
      console.log('this.featuresCost', this.feaCostArray);
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }
      this.calculate();
    }
    if (des.subId) {
      for (let i = 0; i < this.selectedItems1.length; i++) {
        if (des.subId === this.selectedItems1[i].featureId) {
          this.selectedItems1.splice(i, 1);
        }
      }
      console.log('deselect1', this.selectedItems1);
      if (this.selectedItems1.length === 0) {
        this.isCheck1 = 'Please select users';
      }
    }
  }
  onSelectAll(allSelect) {
    if (allSelect.featureId) {
      this.feaCostArray = [];
      // if (this.selectedItems.length === 0) {
      //   this.selectedItems = allSelect;
      // } else {
      //   this.selectedItems = [];
      // }
      this.isCheck = '';
      this.selectedItems = allSelect;
      console.log('selectAll', this.selectedItems);
      for (let i = 0; i < this.dropdownList.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (this.selectedItems[j].featureId === this.dropdownList[i].featureId) {
            this.feaCostArray.push(this.dropdownList[i]);
          }
        }
      }
      console.log('this.featuresCost', this.feaCostArray);
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }
      this.calculate();
    }
    if (allSelect.subId) {
      this.isCheck1 = '';
      this.selectedItems1 = allSelect;
      console.log('selectAll', this.selectedItems1);
    }
  }


  changeToDate(date) {
    console.log(date.target.value);
    let pastDate = new Date(date.target.value);
    this.todt = new Date(pastDate.toLocaleDateString());
    this.registerForm.controls.grcPir.setValue(pastDate.toLocaleDateString());
    this.registerForm.controls.grcPir.setValue(pastDate.toLocaleDateString());

    this.gracePir = new Date(pastDate.toLocaleDateString());
    let today = new Date()
    if (this.formatDateNewDash(today) > this.formatDateNewDash(pastDate)) {
      this.registerForm.controls.dealstaname.setValue('3');
    } else {
      this.registerForm.controls.dealstaname.setValue(this.dealsData[0].dealstaId)
    }

  }



  checkPackage(pkg) {
    if (pkg) {
      if (pkg.target) {
        if (pkg.target.value) {
          for (let i = 0; i < this.packageList.length; i++) {
            if (parseInt(pkg.target.value, 10) === this.packageList[i].pkgId) {
              this.singlePkg = this.packageList[i];
            }
          }
          if (this.singlePkg) {
            this.duration = this.singlePkg.duration;
            this.getToDate(this.registerForm.controls.fromDate.value);
            this.tempfeature = this.singlePkg.featureId.split('|').map(Number);
            this.selectedItems = [];
            if (this.tempfeature.length > 0) {
              for (let i = 0; i < this.dropdownList.length; i++) {
                for (let j = 0; j < this.tempfeature.length; j++) {
                  if (this.tempfeature[j] === this.dropdownList[i].featureId) {
                    // let fetObj = {
                    //   featureId : this.dropdownList[i].featureId,
                    //   featurename : this.dropdownList[i].featurename,
                    // }
                    // this.selectedItems.push(fetObj);
                    this.selectedItems.push(this.dropdownList[i]);
                    this.feaAdd = false;
                  }
                }
              }
            }
          }
        }
      }
    }
    // this.selectedItems = this.selectedItems.concat(this.selectedItems);
    console.log('slected items', this.selectedItems);
    this.calculate();
  }

  checkPackage1(data) {

    if (data) {
      for (let i = 0; i < this.packageList.length; i++) {
        if (parseInt(data, 10) === this.packageList[i].pkgId) {
          this.singlePkg = this.packageList[i];
        }
      }
      if (this.singlePkg) {
        this.duration = this.singlePkg.duration;
        this.getToDate(this.registerForm.controls.fromDate.value);
        this.tempfeature = this.singlePkg.featureId.split('|').map(Number);
        this.selectedItems = [];
        if (this.tempfeature.length > 0) {
          for (let i = 0; i < this.dropdownList.length; i++) {
            for (let j = 0; j < this.tempfeature.length; j++) {
              if (this.tempfeature[j] === this.dropdownList[i].featureId) {
                // let fetObj = {
                //   featureId : this.dropdownList[i].featureId,
                //   featurename : this.dropdownList[i].featurename,
                // }
                // this.selectedItems.push(fetObj);
                this.selectedItems.push(this.dropdownList[i]);
                this.feaAdd = false;
              }
            }
          }
        }
      }
    }

    // this.selectedItems = this.selectedItems.concat(this.selectedItems);
    console.log('slected items', this.selectedItems);
    this.calculate();
  }

  setDisc(disc) {
    if (!disc.target.value) {
      this.toastr.error('Discount is required');
    } else {
      this.discount = parseInt(disc.target.value);
      this.calculate();
    }
  }

  setPerson(Per) {
    if (!Per.target.value) {
      this.toastr.error('No. of Persons is required');
    } else {
      this.perCount = parseInt(Per.target.value);
      this.calculate();
    }
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [month, day, year].join('/');
  }

  formatDateNew(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join('/');
  }

  formatDateNewDash(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  checkDuration(duration) {
    this.duration = parseInt(duration.target.value);
    this.getToDate(this.registerForm.controls.fromDate.value);
    this.calculate();
  }

  getToDate(date) {

    let pastDate;
    if (this.duration) {
      if (date.target) {
        if (!date.target.value) {
          this.toastr.error('From Date is required');
        } else {
          pastDate = new Date(date.target.value);
        }
      } else {
        pastDate = new Date(date);
      }
      pastDate.setMonth(pastDate.getMonth() + this.duration);
      console.log(pastDate.toLocaleDateString());
      this.registerForm.controls.toDate.setValue(pastDate.toLocaleDateString());
      this.todt = new Date(pastDate.toLocaleDateString());
    }
  }


  calculate() {
    // if ( this.selectedItems.length === 0 ) {
    //   // this.toastr.warning('Please select features');
    // } else
    if (!this.duration) {
      // this.toastr.warning('Please select duration');
    } else if (this.discount === '' || this.discount === null || this.discount === undefined) {
      // this.toastr.error('Discount is required');
    } else if (this.perCount === '' || this.perCount === null || this.perCount === undefined) {
      // this.toastr.error('No. of persons is required');
    } else {
      // let tempCost;
      let grossCost;
      let totalCost;
      // tempCost = ((this.featuresCost * this.perCount) * this.duration );
      // grossCost = ((tempCost) - (tempCost * (this.discount / 100)));
      // this.baseAmt = tempCost;
      //  ? this.singlePkg.price : this.pkgPrice;
      if (this.singlePkg) {
        this.tempCost = this.singlePkg.price ? this.singlePkg.price : 0;
        this.tempCost = this.tempCost * this.perCount;
      } else {
        this.tempCost = this.pkgPrice * this.perCount;
      }
      grossCost = ((this.tempCost) - (this.tempCost * (this.discount / 100)));
      this.baseAmt = this.tempCost;
      totalCost = ((grossCost) + (grossCost * (this.gst / 100)));
      totalCost = totalCost.toFixed(0);
      this.registerForm.controls.totalAmt.setValue(totalCost);
    }
  }

  hideinvoice() {
    this.hidePopup = true;
  }

  featuretotal(price) {
    return (price * this.confirmList.countUser).toFixed(0);
  }
  percent(baseAmount, percent) {
    return ((baseAmount / 100) * percent).toFixed(0);
  }

  gstpercent() {
    let discount = (this.confirmList.baseAmt / 100) * this.confirmList.discountPer;
    let accprice = this.confirmList.baseAmt - discount;
    this.confirmList.accprice = accprice;
    this.confirmList.gstvalue = (accprice / 100) * this.confirmList.gstNo;
    return ((accprice / 100) * this.confirmList.gstNo).toFixed(0);
  }

  GrandTotal() {
    return (this.confirmList.accprice + this.confirmList.gstvalue).toFixed(0);
  }

  confirmSubmit() {
    this.spinner.show();
    var uList = [];
    if (this.renewList[0]) {
      this.dealsData[0] = this.renewList[0]
    }
    uList.push(this.dealsData[0].subId);
    let payRecDate: any;
    let paydate: any;
    // console.log('this.dealsData[0].payRecDate',this.dealsData[0].payRecDate.value)
    if (this.dealsData[0].payRecDate == 'Invalid Date') {
      this.dealsData[0].payRecDate = null
    }
    if (this.registerForm.controls.payRecDate.value) {
      payRecDate = this.formatDateNewDash(this.registerForm.controls.payRecDate.value)
    } else {
      payRecDate = null
    }

    // console.log('this.dealsData[0].paydate',this.dealsData[0].paydate.value)
    if (this.dealsData[0].paydate == 'Invalid Date') {
      this.dealsData[0].paydate = null
    }
    if (this.registerForm.controls.paydate.value) {
      paydate = this.formatDateNewDash(this.registerForm.controls.paydate.value)
    } else {
      paydate = null
    }
    const params = {
      tid: 0,
      trkid: this.registerForm.controls.trackId.value,
      corpid: null,
      corcod: null,
      sid: this.dealsData[0].subId,
      purcdate: this.formatDateNewDash(this.registerForm.controls.purDate.value),
      fomdate: this.formatDateNewDash(this.registerForm.controls.fromDate.value),
      todate: this.formatDateNewDash(this.registerForm.controls.toDate.value),
      graceDate: this.registerForm.controls.grcPir.value ? this.formatDateNewDash(this.registerForm.controls.grcPir.value) : this.formatDateNewDash(this.registerForm.controls.toDate.value),
      duraid: this.duration,
      noPerson: 1,
      baAmt: this.baseAmt,
      discpre: this.registerForm.controls.discountPercent.value,
      gstno: 18,
      totamt: this.registerForm.controls.totalAmt.value,
      paytype: this.registerForm.controls.paytype.value,
      receiptno: this.registerForm.controls.recptno.value,
      paydate: paydate,
      payRecDate: payRecDate,
      isAct: this.registerForm.controls.status.value,
      deastId: this.registerForm.controls.dealstaname.value,
      // feaIds : this.fidList.join('|'),
      // fealen : this.fidList.length,
      // feaIds: this.tempfeature.join('|'),
      // fealen: this.tempfeature.length,
      pkgid: parseInt(this.registerForm.controls.pkg.value, 10),
      subIds: uList.join('|'),
      sublen: uList.length,
      uid: this.userData.userid,
      uname: this.userData.subname,
      bname: this.bowserInfo.name,
      bversion: this.bowserInfo.version,
      gross: this.registerForm.controls.gross.value,
      deducted: this.registerForm.controls.deducted.value,
      netAmount: this.registerForm.controls.netAmount.value,
      gtypeid: this.gtypeid,
      sgst: this.sgst,
      cgst: this.cgst,
      igst: this.igst,
    };
    if (this.dealsData[0]) {
      if (this.dealsData[0].tmId) {
        params.tid = this.dealsData[0].tmId;
      }
    }
    if (this.renewList[0]) {
      if (this.renewList[0].tmId) {
        params.tid = 0
      }
    }
    // params.fomdate = this.formatDate(this.registerForm.controls.fromDate.value);
    // for (let i = 0; i < this.durationList.length ; i++) {
    //   if (parseInt(this.registerForm.value.duration) === this.durationList[i].duration) {
    //     params.duraid = this.durationList[i].durationId;
    //   }
    // }
    console.log('params', params);
    this.dealSer.addEditDeals(params).then(res => {
      this.spinner.hide();
      try {
        console.log('res', res);
        if (res.type === true && res.status_code === 200) {
          console.log('res', res);
          try {
            if (res.data[0]) {
              if (res.data[0].updated) {
                this.router.navigate(['pages/user/subscriber-deal']);
                this.toastr.success(res.data[0].updated);
              }
              else if (res.data[0].saved) {
                this.router.navigate(['pages/user/subscriber-deal']);
                this.toastr.success(res.data[0].saved);
              }
            }
          } catch (e) {
            console.log(e);
          }
        }
        if (res.type === false) {
          this.spinner.hide();
          console.log('res', res);
          this.toastr.error('Server Error');
        }
      } catch (e) {
        console.log(e);
      }
    },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
  }

  ngOnDestroy(): void {
    this.dealsData = ''
    this.renewList = ''
  }
}
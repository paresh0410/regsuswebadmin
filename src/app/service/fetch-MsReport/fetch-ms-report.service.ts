import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class FetchMsReportService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchMSReportUrl = this.baseUrl + webApi.apiUrl.fetchMSReport;
  constructor(public http: HttpClient) { }
  public fetchMsReport(msfData: any): Promise<any> {

    return this.http.post(this.fetchMSReportUrl, msfData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

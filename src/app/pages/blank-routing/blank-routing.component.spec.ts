import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlankRoutingComponent } from './blank-routing.component';

describe('BlankRoutingComponent', () => {
  let component: BlankRoutingComponent;
  let fixture: ComponentFixture<BlankRoutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlankRoutingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlankRoutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

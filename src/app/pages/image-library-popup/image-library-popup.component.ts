import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { CommonService } from '../../service/common.service';

@Component({
  selector: 'ngx-image-library-popup',
  templateUrl: './image-library-popup.component.html',
  styleUrls: ['./image-library-popup.component.scss']
})
export class ImageLibraryPopupComponent implements OnInit, OnDestroy {
  @Output() closeEvent = new EventEmitter();
  data: any
  imageList: any = [
    { id: 1, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 2, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 3, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Excel', checked: 0 },
    { id: 4, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 5, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Pdf', checked: 0 },
    { id: 6, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 7, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 8, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Excel', checked: 0 },
    { id: 9, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 10, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Pdf', checked: 0 },
    { id: 11, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 12, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 13, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Excel', checked: 0 },
    { id: 14, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 15, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Pdf', checked: 0 },
    { id: 16, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 17, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 18, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Excel', checked: 0 },
    { id: 19, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image', checked: 0 },
    { id: 20, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Pdf', checked: 0 }
  ]
  fileTypeList: any = [
    { id: 1, name: 'All' },
    { id: 2, name: 'Image' },
    { id: 3, name: 'Excel' },
    { id: 4, name: 'Pdf' },
  ]
  searchText: any = ''
  selectedItems = []
  currentPage: any = 1
  pageLimit: any = 20
  totalPages: any
  categoryTypeList: any = []
  imageLibraryData: any = []
  imgUrl = webApi.baseUrl + webApi.apiUrl.getImg
  imageplaceholder: any = 'assets/images/IamgePlaceholder.png'
  pdfplaceholder: any = 'assets/images/pdfPlaceholder.png'
  excelplaceholder: any = 'assets/images/excelPlaceholder.png'
  @ViewChild('fileInput') myInputVariable: ElementRef;
  filetype: any = 4
  sortOrder: any = 2
  selectedImgDetails: any
  constructor(protected ref: NbDialogRef<ImageLibraryPopupComponent>, public commonService: CommonService,
    private toastr: ToastrService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    console.log(this.data)
    if (this.data) {
      if (this.data.id == 1) {
        this.filetype = '4,5'
      }
    }
    this.fetchImageData(this.currentPage)
    window.addEventListener('scroll', this.scroll, true);
  }

  scroll = (event: any): void => {
    // console.log(event);
    // console.log(event.target.scrollHeight - event.target.scrollTop - event.target.clientHeight)
    if (event.target.scrollHeight - event.target.scrollTop - event.target.clientHeight < 5) {
      // console.log('true')
      this.currentPage++;
      if (this.currentPage <= this.totalPages) {
        this.fetchImageData(this.currentPage);
      }
    } else {
      // console.log('false')
    }
  };

  uploadImage(event) {
    console.log('uploadImage', event)
    const files = event.target.files;
    if (files.length === 0)
      return;

    if (files[0].name) {
      const nameSplit = files[0].name.split('.')
      if (nameSplit.length > 2) {
        this.toastr.error('Please upload file with proper name');
        return
      }
      const name = nameSplit[0]
      console.log(name)
      // this.checkWhiteSpace(name)

      const outputforSpace = this.hasWhiteSpace(name)
      console.log('outputforSpace', outputforSpace)
      if (outputforSpace) {
        this.toastr.error('Please upload file with proper name');
        return
      }

      // this.checkSpecialCharacters(name)
      const output = name.match(/[~!@#$%^&*(\\\/\-['`;=+\]),.?":{}|<>_]/g);
      console.log(output);
      if (output) {
        this.toastr.error('Please upload file with proper name');
        return
      }
    }
    const reader = new FileReader();
    this.selectedImgDetails = files
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      // this.previewImg = reader.result;
    }
    // this.addFile()
    this.uploadImg()
  }


  checkSpecialCharacters(name) {
    const output = name.match(/[~!@#$%^&*(\\\/\-['`;=+\]),.?":{}|<>_]/g);
    console.log(output);
    if (output) {
      this.toastr.error('Please upload file with proper name');
      return
    }
  }

  hasWhiteSpace(s) {
    const whitespaceChars = [' ', '\t', '\n'];
    return whitespaceChars.some(char => s.includes(char));
  }

  uploadImg() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      this.spinner.show()
      if (this.selectedImgDetails) {
        let formData: any = new FormData();
        if (this.selectedImgDetails.length > 0) {
          for (let i = 0; i < this.selectedImgDetails.length; i++) {
            formData.append('file', this.selectedImgDetails[i], this.selectedImgDetails[i].name);
          }
        }
        formData.append('bname', this.commonService.bowserInfo.name);
        formData.append('bversion', this.commonService.bowserInfo.version);
        formData.append('uid', this.commonService.userData.userid);
        formData.append('uname', this.commonService.userData.subname);
        console.log(formData)
        const url: any = webApi.baseUrl + webApi.apiUrl.uploadImages;
        this.commonService.httpPostRequest(url, formData).then((res: any) => {
          console.log(res)
          try {
            if (res.type) {
              if (res.error_code == 0) {
                this.toastr.success(res.err_desc)
                this.imageLibraryData = []
                this.currentPage = 1
                this.fetchImageData(this.currentPage)
                this.spinner.hide()

              } else {
                this.toastr.error(res.err_desc)
                this.spinner.hide()

              }
            }
          } catch (error) {
            this.spinner.hide()
          }
        })
      } else {
        // this.imgErrors.requiredText = "Please upload your AOF Form before processing."
        // this.imgErrors.isValid = true
      }
    }
  }
  fetchImageData(currentPage) {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      this.spinner.show();
      const param = {
        bname: this.commonService.bowserInfo.name,
        bversion: this.commonService.bowserInfo.version,
        uid: this.commonService.userData.userid,
        uname: this.commonService.userData.subname,
        pNo: currentPage,
        datalimit: this.pageLimit,
        searchStr: this.searchText,
        filetype: this.filetype ? this.filetype : null,
        orderby: this.sortOrder
      }
      console.log(param)
      const url = webApi.baseUrl + webApi.apiUrl.fetchImageLibrary
      this.commonService.httpPostRequest(url, param).then((res: any) => {
        console.log(res)
        try {
          if (res.type) {
            this.totalPages = res.totalcount[0].totalPages
            this.categoryTypeList = res.filetypedropdown
            if (this.imageLibraryData.length > 0 && currentPage > 1) {
              this.imageLibraryData = this.imageLibraryData.concat(res.data);
            } else {
              this.imageLibraryData = res.data
            }
            // this.imageLibraryData = res.data
            // this.myInputVariable.nativeElement.value = "";
            this.spinner.hide();

          }
        } catch (error) {
          this.spinner.hide();

        }
      })
    }
  }
  close() {
    this.selectedItems = []
    this.ref.close();
    this.closeEvent.emit(this.selectedItems)
  }

  selectItems(item) {
    console.log(item)
    this.selectedItems = []
    if (this.data) {
      if (!this.data.multiImgs) {
        this.imageLibraryData.map(a => a.checked = 0)
      }
      item.checked == 0 ? this.imageLibraryData.find(a => a.id == item.id).checked = 1 : this.imageLibraryData.find(a => a.id == item.id).checked = 0
      this.selectedItems.push(this.imageLibraryData.filter(a => a.checked == 1))
    } else {
      this.imageLibraryData.map(a => a.checked = 0)
      item.checked == 0 ? this.imageLibraryData.find(a => a.id == item.id).checked = 1 : this.imageLibraryData.find(a => a.id == item.id).checked = 0
      this.selectedItems.push(this.imageLibraryData.filter(a => a.checked == 1))

    }

  }

  uploadData() {
    console.log(this.selectedItems)
    this.ref.close(this.selectedItems);
  }

  search() {
    console.log(this.searchText)
    if (this.searchText !== "") {
      if (this.searchText.length > 3) {
        let searchValue = this.searchText.toLocaleLowerCase();
        this.imageLibraryData = this.imageLibraryData.filter(contact => {
          return contact.filename.toLocaleLowerCase().match(searchValue) ||
            contact.filetype.toLocaleLowerCase().match(searchValue);
          // you can keep on adding object properties here   
        });
        this.fetchImageData(this.currentPage)
      }

    } else {  // if(this.searchText== ""){ you don't need this if
      this.ngOnInit();
    }
  }
  searchKeyup() {
    console.log(this.searchText)
    if (this.searchText == "") {
      this.ngOnInit();
    }
  }

  onCategoryChange(event) {
    console.log(event.target.value)
    this.filetype = event.target.value
    this.currentPage = 1
    this.imageLibraryData = []
    this.fetchImageData(this.currentPage)
  }

  ngOnDestroy(): void {
    console.log('destroy')
    // window.addEventListener('scroll', this.scroll, false);

    this.scroll = (event: any) => {
      event = ''
    }
  }

}

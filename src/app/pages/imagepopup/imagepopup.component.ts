import { Component, OnInit } from '@angular/core';
import { NbDialogRef, NbDialogService } from '@nebular/theme';
import { webApi } from '../../../config';

@Component({
  selector: 'ngx-imagepopup',
  templateUrl: './imagepopup.component.html',
  styleUrls: ['./imagepopup.component.scss']
})
export class ImagepopupComponent implements OnInit {
  data: any
  imgUrl = webApi.baseUrl + webApi.apiUrl.getImg

  constructor(protected ref: NbDialogRef<ImagepopupComponent>) { }

  ngOnInit() {
  }

  close() {
    this.ref.close();
  }

}

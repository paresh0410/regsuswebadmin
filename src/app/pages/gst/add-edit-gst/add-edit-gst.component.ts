import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';

@Component({
  selector: 'ngx-add-edit-gst',
  templateUrl: './add-edit-gst.component.html',
  styleUrls: ['./add-edit-gst.component.scss'],
})
export class AddEditGstComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  gstData: any = [];
  cgstVal: any = new RegExp('^([0-9]|([1-4][0-9])|50)$');
  gstVal: any = new RegExp('^([0-9]|([1-9][0-9])|100)$');
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private passSer: PassServService,
  ) {
    this.gstData = this.passSer.editGST;
  }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    if (this.gstData[0]) {
      this.registerForm = this.formBuilder.group({
        cgst: [this.gstData[0].cgst, [Validators.required, Validators.pattern('^([0-9]|([1-9][0-9])|50)$')]],
        sgst: [this.gstData[0].sgst, [Validators.required, Validators.pattern('^([0-9]|([1-9][0-9])|50)$')]],
        gst: [this.gstData[0].gst, [Validators.required, Validators.pattern('^([0-9]|([1-9][0-9])|100)$')]],
        status: [this.gstData[0].status, [Validators.required]],
      });
    } else {
      this.registerForm = this.formBuilder.group({
        cgst: ['', [Validators.required, Validators.pattern('^([0-9]|([1-9][0-9])|50)$')]],
        sgst: ['', [Validators.required, Validators.pattern('^([0-9]|([1-9][0-9])|50)$')]],
        gst: ['', [Validators.required, Validators.pattern('^([0-9]|([1-9][0-9])|100)$')]],
        status: ['', [Validators.required]],
      });
    }
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.router.navigate(['pages/duration']);
      this.toastr.success('Data saved successfully');
    }
  }

  get f() {
    return this.registerForm.controls;
  }

  back() {
    this.router.navigate(['pages/gst']);
  }

  Validation(event) {
    console.log('event', event);
    if (event.target.placeholder === 'CGST') {
      let cgstVal = parseInt(this.registerForm.value.cgst);
      let sgstVal = parseInt(this.registerForm.value.sgst);
      console.log('values', cgstVal, sgstVal);
      if (!event.target.value) {
        this.toastr.error('CGST is required');
        if (!isNaN(sgstVal)) {
          this.registerForm.controls.gst.setValue(sgstVal);
        } else {
          this.registerForm.controls.gst.reset();
        }
      } else if (!this.cgstVal.test(event.target.value)) {
        this.toastr.error('CGST is invalid');
        this.registerForm.controls.gst.reset();
      }
      else if (((isNaN(cgstVal)) ? (cgstVal = 0) : cgstVal) + ((isNaN(sgstVal)) ? (sgstVal = 0) : sgstVal) < 100) {
        this.registerForm.controls.gst.setValue(cgstVal + sgstVal);
      }
       else if (((isNaN(cgstVal)) ? (cgstVal = 0) : cgstVal) + ((isNaN(sgstVal)) ? (sgstVal = 0) : sgstVal) > 100) {
        this.toastr.error('CGST & SGST are invalid');
        this.registerForm.controls.gst.reset();
      }
    }
    if (event.target.placeholder === 'SGST') {
      let cgstVal = parseInt(this.registerForm.value.cgst);
      let sgstVal = parseInt(this.registerForm.value.sgst);
      if (!event.target.value) {
        this.toastr.error('SGST is required');
        if (!isNaN(cgstVal)) {
          this.registerForm.controls.gst.setValue(cgstVal);
        } else {
          this.registerForm.controls.gst.reset();
        }
      } else if (!this.cgstVal.test(event.target.value)) {
        this.toastr.error('SGST is invalid');
        this.registerForm.controls.gst.reset();
      }
      else if (((isNaN(cgstVal)) ? (cgstVal = 0) : cgstVal) + ((isNaN(sgstVal)) ? (sgstVal = 0) : sgstVal) < 100) {
        this.registerForm.controls.gst.setValue(cgstVal + sgstVal);
      }
       else if (((isNaN(cgstVal)) ? (cgstVal = 0) : cgstVal) + ((isNaN(sgstVal)) ? (sgstVal = 0) : sgstVal) > 100) {
        this.toastr.error('CGST & SGST are invalid');
        this.registerForm.controls.gst.reset();
      }
    }
    // if (event.target.placeholder === 'Discount') {
    //   if (!event.target.value) {
    //     this.toastr.error('Discount is required');
    //   } else if (!this.disVal.test(event.target.value)) {
    //     this.toastr.error('Discount is invalid');
    //   }
    // }
  }
}

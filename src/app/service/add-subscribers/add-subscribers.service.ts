import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class AddSubscribersService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addSubscribersUrl = this.baseUrl + webApi.apiUrl.addSubcribers;
  constructor(public http: HttpClient) { }
  public addSubscribers(subData: any): Promise<any> {

    return this.http.post(this.addSubscribersUrl, subData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

import { Component, OnInit, ViewContainerRef, ComponentRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../service/pass-serv.service';
import { ModalDialogService, IModalDialog, IModalDialogButton, IModalDialogOptions } from 'ngx-modal-dialog';
import { PollResultsComponent } from '../polls/poll-results/poll-results.component';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FetchPollsService } from '../../service/fetch-polls/fetch-polls.service';
import { SearchNewsPollsService } from '../../service/search-polls/search-news-polls.service';
import { BrowserInfoService } from '../../service/browser-info.service';
import { ExcelService } from '../../service/excel.service';
// import { ChartType, ChartOptions } from 'chart.js';
// import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
// // import * as jspdf from 'jspdf';
// import html2canvas from 'html2canvas';
// import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'ngx-polls',
  templateUrl: './polls.component.html',
  styleUrls: ['./polls.component.scss']
})
export class PollsComponent implements OnInit , IModalDialog {
  // graphVisible: boolean = true;
  // pollsData: any = [];  
  percent: any = [];
  graphData: any;
  actionButtons: IModalDialogButton[];
  p: number = 1;
  q: number = this.p;
  showhide = true;
  title: any;
  today: any = new Date();
  disableAll: any = true;
  polls: any = [ ];
  search = {
    pollText: '',
    startDate: '',
    endDate: '',
    pollsta: '',
    viewCount: '',
    attemptCount: '',
    isActive: '',
  };

  hidePopup: boolean = true;
  detailView: any = [];
  search1 = {
    fromdate: '',
    todate: '',
  };
  arrow = false;
  isDesc: any = [];
  column: any = [];
  display = 'none';
  showDiv: boolean = true;
  checkFlag: any = false;
  featureLength: any = true;
  setveractive: any = false;
  interestList: any = [];
  searchCount: any = 0;
  userData :any;
  bowserInfo:any;
  sort1: any = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  flag: any;

  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private toastr: ToastrService,
    private passService: PassServService,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef,
    private fetchPollSer: FetchPollsService,
    private searchPollsSer: SearchNewsPollsService,
    private browserInfoService: BrowserInfoService,
    private excelService: ExcelService,
  ) {
    this.actionButtons = [
      { text: 'Close', onAction: () => true  }, // no special processing here
    ];
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
   }

  ngOnInit() {
    this.spinner.show();
    this.fetchPolls();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    if ( new Date(this.polls.endDate) < this.today ) {
      this.disableAll = true;
    }
  }

  openModalDialog() {
    this.display = 'block';
    this.showDiv = false;
    // console.log(this.showDiv);
  }

  closeModalDialog() {
    this.display = 'none';
    this.showDiv = true;
  }

  // clear button
  clearBtn() {
    this.search.pollText = '';
    this.search.startDate = '';
    this.search.endDate = '';
    this.search.pollsta = '';
    this.search.viewCount = '';
    this.search.attemptCount = '';
    this.search.isActive = '';
  }
  clearBtn1() {
    // this.search1.fromdate = '';
    // this.search1.todate = '';
    if (this.searchCount > 0) {
      this.fetchPolls();
      this.closeModalDialog();
      this.searchCount= 0;
    }
  }

   // sort 
   invert() {
    this.arrow = !this.arrow;
    this.sort1 = true;
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.polls.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }


  addPolls() {
    this.passService.editPoll = [];
    this.passService.interestList = this.interestList;
    this.router.navigate(['pages/polls/add-edit-polls']);
  }
  editPolls(data) {
    this.passService.editPoll[0] = (data);
    // this.pollsData = this.passService.editPoll;
    this.passService.interestList = this.interestList;
    this.router.navigate(['pages/polls/add-edit-polls']);
  }
  openNewDialog(dataP) {
    this.checkFlag = true;
    this.passService.editPoll[0] = (dataP);
    this.passService.editPoll[1] = (this.checkFlag);
    this.modalService.openDialog(this.viewRef, {
      title: 'X',
      childComponent: PollResultsComponent,
      actionButtons: this.actionButtons,
      // onClose: () => true,
    });
  }

  dialogInit(reference: ComponentRef <IModalDialog>, options: Partial<IModalDialogOptions<any>>) {
    // no processing needed
  }

  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.polls.length; i++) {
      if (this.polls[i].isActive === 1) {
        this.flag = 'Active';
      }
      if (this.polls[i].isActive === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'TITLE': this.polls[i].pollText,
        'START DATE': this.polls[i].startDate,
        'END DATE': this.polls[i].endDate,
        'STATE': this.polls[i].sta,
        'VIEWS': this.polls[i].viewCount,
        'ATTEMPTS': this.polls[i].attemptCount,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Polls Details');
  }

  myFunction(event) {
    this.search.isActive = event.target.value;
  }

  myFunction1(event1) {
    this.search.pollsta = event1.target.value;
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  fetchPolls() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      this.fetchPollSer.fetchPolls(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
            // let userD = [];
            this.polls = res.data;
            this.interestList = res.interest;
            if(this.polls.length === 0) {
              this.featureLength = false;
            } else if(this.polls.length > 0) {
              this.featureLength = true;
              // for (let i = 0; i < this.polls.length; i++) {
              // // this.polls[i].startDate = new Date(this.polls[i].startDate);
              // // this.polls[i].endDate = new Date(this.polls[i].endDate);
              // }
            }

            // this.featureLength = this.polls.length === 0 ? false :  true;
            console.log('this.categoryList',this.polls);
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.polls = [];
        this.setveractive = true;
        this.toastr.error('Server Error');
      });
    }
  }

  searchPost() {
    if (new Date(this.search1.fromdate) > new Date(this.search1.todate)) {
      this.toastr.warning('To date should not be greater than From date');
      return;
    }
    const content = {
      FrmDate: this.formatDate(this.search1.fromdate),
      ToDate: this.formatDate(this.search1.todate),
      uid: this.userData.userid,
      uname: this.userData.subname,
      bname: this.bowserInfo.name,
      bversion: this.bowserInfo.version,
    };
    console.log('content',content);
    this.spinner.show();
      this.searchPollsSer.searchPoll(content).then(res => {
        if (res.type === true) {
          console.log('res', res);
          // res.data = this.posts;
          this.polls = res.data;
          if(this.polls.length === 0) {
            this.featureLength = false;
            this.closeModalDialog();
          } else if(this.polls.length > 0) {
            this.featureLength = true;
            this.closeModalDialog();
          }
          this.spinner.hide();
          this.searchCount++;
        }
        if (res.type === false) {
          this.spinner.hide();
          console.log('res', res);
        }
      }, err => {
        this.spinner.hide();
        // console.log(err);
        this.polls = [];
        this.setveractive = true;
        this.toastr.error('Server Error');
      });
    }

    // graph
    // showGraph() {
    //   this.graphVisible = false;
    //   console.log('show pie chart', this.graphVisible);
    // }

    // public captureScreen() {
    //   // only pdf download
    //   // var data = document.getElementById('contentToConvert');
    //   // html2canvas(data).then(canvas => {  
    //   //   // Few necessary setting options  
    //   //   var imgWidth = 208;   
    //   //   var pageHeight = 485;    
    //   //   var imgHeight = canvas.height * imgWidth / canvas.width;  
    //   //   var heightLeft = imgHeight;    
    //   //   const contentDataURL = canvas.toDataURL('image/png')  
    //   //   let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
    //   //   var position = 0;  
    //   //   pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
    //   //   pdf.save('Polls.pdf');  
    //   // });
  
    //   // only image download
    //   // var element = <HTMLElement>document.querySelector("#pollCanvasImg");
    //   var data = document.getElementById('pollCanvasImg');
    //   // data.setAttribute("style","width:500px");
    //   html2canvas(data).then(function(canvas) {
    //     // data.setAttribute("style","width:500px");
    //       // Convert the canvas to blob
    //       canvas.toBlob(function(blob){
    //         // To download directly on browser 
    //         let link = document.createElement("a");
    //         link.download = "Polls.png";
    //         link.href = URL.createObjectURL(blob);
    //         link.click();
    //         // To save manually somewhere in file explorer
    //         // window.saveAs(blob, 'image.png');
    //       },'image/png');
    //   });
    // }
   

    showViewDetails(item) {
      console.log('item',item);
      if(item.viewCount == 0) {
        return;
      }
  
  
      this.hidePopup = false;
      this.spinner.show();
  
      let data = {
        pollId : item.pollId
      }
      this.fetchPollSer.fetchPollsViewDetail(data).then(res => {
        this.spinner.hide();
          try {
            console.log('detail Res', res.data);
            this.detailView = res.data;
          } catch (e) {
            console.log(e);
          }
        },
        err => {
          this.spinner.hide();
          console.log('err', err);
          this.toastr.error('Server Error');
      });
      // this.cardDetails = item;
      // console.log("carddetails ==", this.cardDetails);
    
  }
  
  hideViewDetails() {
    this.hidePopup = true;
  }


  

searchView = {
  subname: '',
  subemail: '',
  phoneno: '',
  corpname: '',
}

clearViewDetail() {
  this.searchView.subname = '';
  this.searchView.subemail = '';
  this.searchView.phoneno = '';
  this.searchView.corpname= '';
}

  
  
sortView(property) {
  this.isDesc = !this.isDesc; // change the direction
  this.column = property;
  const direction = this.isDesc ? 1 : -1;
  this.detailView.sort(function (a, b) {
    if (a[property] < b[property]) {
      return -1 * direction;
    } else if (a[property] > b[property]) {
      return 1 * direction;
    } else {
      return 0;
    }
  });
}
}

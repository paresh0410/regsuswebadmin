import { TestBed } from '@angular/core/testing';

import { BrowserInfoService } from './browser-info.service';

describe('BrowserInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BrowserInfoService = TestBed.get(BrowserInfoService);
    expect(service).toBeTruthy();
  });
});

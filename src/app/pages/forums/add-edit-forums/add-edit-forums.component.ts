import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { AddEditForumService } from '../../../service/add-edit-forum/add-edit-forum.service';
import { BrowserInfoService } from '../../../service/browser-info.service';

@Component({
  selector: 'ngx-add-edit-forums',
  templateUrl: './add-edit-forums.component.html',
  styleUrls: ['./add-edit-forums.component.scss']
})
export class AddEditForumsComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  public name1: String = 'Choose image';
  public str1: String = '';
  message: string;
  public imagePath;
  public filePath;
  imgURL: any;
  isImg: any = '';
  isFile: any;
  PostData: any = [];
  minDate: any = new Date();
  newoption: any = false;
  optionsArray: any = [];
  inValidArray: any = [];
  isInvalid: any = false;
  forumData: any = [];
  questionData: any = [];
  getIt: any = false;
  today: any = new Date();
  disableAll: any = false;
  checkQnEnd: any = new Date();
  checkstd: any = new Date();
  checkend: any = new Date();
  dropdownList: any = [];
  selectedItems: any = [];
  dropdownSettings: any = {
    singleSelection: false,
    idField: 'interestId',
    textField: 'intersetName',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };
  isCheck1: any;
  disableOption: any = false;
  stopAdd: any = false;
  userData :any;
  bowserInfo:any;
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private passSer: PassServService,
    private addEditFSer: AddEditForumService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.forumData = this.passSer.editForum;
    this.dropdownList = this.passSer.interestList;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
    console.log('this.pollsData' , this.forumData);
   }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    console.log('this.forumData',this.forumData);
    if (this.forumData[0]) {
      this.selectedItems = this.forumData[0].intersetLit;
      for (let i = 0; i < this.selectedItems.length; i++) {
        this.selectedItems[i].interestId = parseInt(this.selectedItems[i].interestId);
      }
      if ( this.forumData[0].forumsta === 3) {
        this.disableAll = true;
      }
      if ( this.forumData[0].forumsta === 2) {
        this.disableOption = true;
        this.disableAll = true;
      }
      this.registerForm = this.formBuilder.group({
        pname: [{value : this.forumData[0].forumTitle, disabled : this.disableAll } , [Validators.required]],
        descri: [{value : this.forumData[0].forumDescription , disabled : this.disableAll }, [Validators.required]],
        status: [{value : this.forumData[0].isActive , disabled : this.disableAll }, [Validators.required]],
        startDt: [{value : new Date(this.forumData[0].forumStartDate),
                 disabled : this.disableAll },
                 [Validators.required]],
        endDt: [ {value : new Date(this.forumData[0].forumEndDate),
                  disabled : this.disableAll },
                  [Validators.required]],
        QuestionEnd: [{value: new Date(this.forumData[0].questionEndDate),
                     disabled : this.disableAll},
                    [Validators.required]],
      });
      this.minDate = new Date(this.forumData[0].forumStartDate);
    } else {
      this.registerForm = this.formBuilder.group({
        pname: ['' , [Validators.required]],
        descri: ['' , [Validators.required]],
        status: ['1', [Validators.required]],
        startDt: ['', [Validators.required]],
        endDt: ['', [Validators.required]],
        QuestionEnd: ['', [Validators.required]],
      });
      this.checkstd = this.registerForm.value.startDt;
      this.checkend = this.registerForm.value.endDt;
      this.checkQnEnd = this.registerForm.value.QuestionEnd;
    }
    console.log('disd',this.disableAll);
  }

  get f() {
    return this.registerForm.controls;
  }

  onItemSelect(item) {
    console.log('select', this.selectedItems);
    this.isCheck1 = '';
}
onSelectAll(items) {
  // console.log(items);
  if (this.selectedItems.length === this.dropdownList.length) {
    this.selectedItems = [];
    this.isCheck1 = 'Please select interests';
  } else {
    this.selectedItems = items;
    this.isCheck1 = '';
  }
  console.log('selectAll', this.selectedItems);
}
onItemDeSelect(des) {
  for (let i = 0; i < this.selectedItems.length; i++) {
    if (des.interestId === this.selectedItems[i].interestId) {
      this.selectedItems.splice(i, 1);
    }
  }
  if (this.selectedItems.length === 0) {
    this.isCheck1 = 'Please select interests';
  }
   console.log('deselect', this.selectedItems);
}

onSubmit() {
  this.submitted = true;
  console.log(this.registerForm.value);
  if (this.selectedItems.length === 0) {
    this.isCheck1 = 'Please select interests';
  console.log('this.isCheck1', this.isCheck1);
    return;
  }
  if (this.registerForm.value.startDt && this.registerForm.value.endDt && this.registerForm.value.QuestionEnd) {
    let tod = new Date(this.registerForm.value.endDt);
    let qtd = new Date(this.registerForm.value.QuestionEnd);
    let hrd = new Date(this.registerForm.value.endDt);
    let compDate;
    // hrd.setHours(tod.getHours() - 1);
    hrd.setMinutes(tod.getMinutes() - 60);
    // compDate = new Date(hrd.toLocaleDateString());
    console.log('hrd',hrd);
    if (new Date(this.registerForm.value.startDt).setSeconds(0,0) ===
    new Date(this.registerForm.value.endDt).setSeconds(0,0)) {
      this.toastr.warning('End date and time should not be same as start date and time','', {
        timeOut: 6000
      });
      return;
    }
    if (new Date(this.registerForm.value.startDt) > new Date(this.registerForm.value.endDt)) {
      this.toastr.warning('End date and time should not be greater than start date and time','', {
        timeOut: 6000
      });
      return;
    }
    if (new Date(this.registerForm.value.QuestionEnd) > new Date(this.registerForm.value.endDt)) {
      this.toastr.warning('Question end date and time should not be greater than end date and time','', {
        timeOut: 6000
      });
      return;
    }
    if (new Date(this.registerForm.value.QuestionEnd) < new Date(this.registerForm.value.startDt)) {
      this.toastr.warning('Question end date and time should not be lesser than start date and time','', {
        timeOut: 6000
      });
      return;
    }
    if (qtd > hrd) {
      this.toastr.warning('Question end date and time should be 1 hour lesser than end date and time','', {
        timeOut: 6000
      });
      return;
    }
  }
  // stop here if form is invalid
  if (this.registerForm.invalid) {
  //   if (this.selectedItems.length === 0) {
  //     this.isCheck1 = 'Please select interests';
  // console.log('this.isCheck1', this.isCheck1);
  //   }
    return;
    } else {
      const intrId = [];
      for (let i = 0; i < this.selectedItems.length; i++) {
        intrId.push(this.selectedItems[i].interestId);
      }
      const content = {
        f_forumId : 0,
        f_forTitle : this.registerForm.value.pname,
        f_forDesc : this.registerForm.value.descri,
        f_forStaDate : this.formattime(this.registerForm.value.startDt),
        f_forEndDate : this.formattime(this.registerForm.value.endDt),
        f_quesEndDate : this.formattime(this.registerForm.value.QuestionEnd),
        f_act : this.registerForm.value.status,
        f_inters : intrId.join('|'),
        f_interslen : intrId.length,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      }

      if (this.forumData[0]) {
        if (this.forumData[0].forumId) {
          content.f_forumId = this.forumData[0].forumId;
        }
      }
      console.log('content' , content);
      this.addEditFSer.addEditForum(content).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.data) {
                if (res.data.error) {
                  this.toastr.error(res.data.error);
                } else if (res.data.updated) {
                  this.router.navigate(['pages/forums']);
                  this.toastr.success(res.data.updated);
              } else if (res.data.saved) {
                this.router.navigate(['pages/forums']);
                this.toastr.success(res.data.saved);
            }
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
      // this.toastr.success('Data Saved Successfully');
      // this.router.navigate(['pages/forums']);
   }
 }

 checkVal(event) {
  console.log('event', event);
  if (event.target.placeholder === 'Forum Name') {
    if (!event.target.value) {
      this.toastr.error('Forum Name is required');
    }
  }
  if (event.target.placeholder === 'Forum Description') {
    if (!event.target.value) {
      this.toastr.error('Forum Description is required');
    }
  }
  // if (event.target.placeholder === 'Start Date') {
  //   if (!event.target.value) {
  //     this.toastr.error('Start Date is required');
  //   }
  // }
  // if (event.target.placeholder === 'End Date') {
  //   if (!event.target.value) {
  //     this.toastr.error('End Date is required');
  //   }
  // }
}
back() {
  this.router.navigate(['pages/forums']);
}

 // image preview
 preview(files) {
  this.name1 = this.str1;
  const str11: any[] = this.name1.split('\\');
  this.name1 = str11[str11.length - 1];

  if (files.length === 0)
    return;

  const mimeType = files[0].type;
  if (mimeType.match(/image\/*/) == null) {
    this.name1 = 'Choose image';
    this.message = 'Only images are supported.';
    return;
  } else {
    this.message = '';
    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
  }
}
removeImage() {
  this.str1 = '';
  this.name1 = 'Choose Image';
  this.imgURL = '';
}

formattime(time) {
  var d = new Date(time),
    hr = '' + d.getHours(),
    mm = '' + d.getMinutes(),
    ss = '' + d.getSeconds(),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (hr.length < 2) hr = '0' + hr;
  if (mm.length < 2) mm = '0' + mm;
  if (ss.length < 2) ss = '0' + ss;
  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  var da = [year, month, day].join('-');
  var ti = [hr, mm, ss].join(':');
  return [da, ti].join(' ');
}
}

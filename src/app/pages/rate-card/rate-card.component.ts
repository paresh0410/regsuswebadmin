import { Component, OnInit, ElementRef } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { FetchRateCardService } from '../../service/fetch-rate-card/fetch-rate-card.service';
import { BrowserInfoService } from '../../service/browser-info.service';

@Component({
  selector: 'ngx-rate-card',
  templateUrl: './rate-card.component.html',
  styleUrls: ['./rate-card.component.scss']
})
export class RateCardComponent implements OnInit {
  rateList: any = [
    // {
    //   rid : 1,
    //   fid : 1,
    //   fname : 'News & Opinions',
    //   frate: 200,
    //   status: 1,
    // },
    // {
    //   rid : 2,
    //   fid : 2,
    //   fname : 'Polls',
    //   frate: 0,
    //   status: 1,
    // },
    // {
    //   rid : 3,
    //   fid : 3,
    //   fname : 'Forums',
    //   frate: 600,
    //   status: 1,
    // },
    // {
    //   rid : 4,
    //   fid : 4,
    //   fname : 'Metal Survey Reports',
    //   frate: 0,
    //   status: 1,
    // },
    // {
    //   rid : 5,
    //   fid : 5,
    //   fname : 'LME Data',
    //   frate: 1000,
    //   status: 1,
    // },
    // {
    //   rid : 6,
    //   fid : 6,
    //   fname : 'Personal Chat',
    //   frate: 1200,
    //   status: 1,
    // },
  ];
  p: number = 1;
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  search = {
    fname : '',
    frate: '',
    status: '',
  };
  featureLength: any = true;
  setveractive: any = false;
  userData :any;
  bowserInfo:any;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private fetchRateSer: FetchRateCardService,
    private browserInfoService: BrowserInfoService,
  ) { 
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.spinner.show();
    this.fetchRateCard();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.rateList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.fname = '',
    this.search.frate = '',
    this.search.status = '';
  }

  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }

  // Export To Excel
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.rateList.length; i++) {
      if (this.rateList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.rateList[i].status === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'FEATURE NAME': this.rateList[i].fname,
        'Base Rate(₹)': this.rateList[i].frate,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Rate Card Details');
  }

  addRate() {
    this.passSer.editRate = [];
    this.router.navigate(['pages/rate-card/add-edit-rate-card']);
  }

  editRate(editdata) {
    this.passSer.editRate[0] = (editdata);
    this.router.navigate(['pages/rate-card/add-edit-rate-card']);
  }

  fetchRateCard() {
    if(!window.navigator.onLine){
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      this.fetchRateSer.fetchRateCard(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
            // let userD = [];
            this.rateList = res.data[0];
            this.passSer.featureList = res.data[1];
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            if (this.rateList.length === 0) {
              this.featureLength = false;
            } else if (this.rateList.length > 0) {
              this.featureLength = true;
            }
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.rateList = [];
        this.setveractive = true;
        this.toastr.error('Server Error');
      });
    }
  }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PassServService {
  orglActivityname: any;
  flag: any;
  actflag: any;
  packId: any;
  startdate: any;
  enddate: any;
  pkgName: any;
  name: any;
  editUser: any = [];
  editFeature: any = [];
  editSubscription: any = [];
  editCategory: any = [];
  AuthorList: any = [];
  editPost: any = [];
  editAction: any
  editPoll: any = [];
  editET: any = [];
  editPkg: any = [];
  editCorp: any = [];
  editDuration: any = [];
  editRate: any = [];
  disableComp: any = false;
  featureList: any = [];
  durationList: any = [];
  companyList: any = [];
  selectedFeatures: any = [];
  durId: any;
  editGST: any = [];
  addDeals: any = [];
  editDeals: any = [];
  renewDeals: any = [];
  trackerID: any;
  editInterests: any = [];
  CategoryList: any = [];
  interestList: any = [];
  editForum: any = [];
  packageList: any = [];
  gstList: any = [];
  editPromo: any = [];
  editMS: any = [];
  imageflag: any;
  constructor() { }
}

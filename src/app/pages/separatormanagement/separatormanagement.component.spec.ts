import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeparatormanagementComponent } from './separatormanagement.component';

describe('SeparatormanagementComponent', () => {
  let component: SeparatormanagementComponent;
  let fixture: ComponentFixture<SeparatormanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeparatormanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeparatormanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MannualSubscribersComponent } from './mannual-subscribers.component';

describe('MannualSubscribersComponent', () => {
  let component: MannualSubscribersComponent;
  let fixture: ComponentFixture<MannualSubscribersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MannualSubscribersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MannualSubscribersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

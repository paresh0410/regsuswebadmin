import { Component, OnInit, ElementRef } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { XlsxToJsonService } from '../../service/xlsx-to-json-service';
import { ViewChild } from '@angular/core';
import { FetchCategoriesService } from '../../service/fetch-categories/fetch-categories.service';
import { BrowserInfoService } from '../../service/browser-info.service';

@Component({
  selector: 'ngx-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  categoryList: any = [];
  p: number = 1;
  // fileName: any;
  fileReaded: any;
  enableUpload: boolean;
  bulkUploadAssetData: any;
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  fileUrl: any;
  fileName: any = 'No file chosen';
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  userData :any;
  bowserInfo:any;
  flag: any;
  search = {
    cat: '',
    catIcon: '',
    status: '',
  };
  featureLength: any = true;
  setveractive: any = false;
  baseUrl: any = webApi.baseUrl;
  getDownload = this.baseUrl + webApi.apiUrl.getCatImage;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private fetchCatSer: FetchCategoriesService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
   }

  ngOnInit() {
    this.spinner.show();
    this.fetchCategories();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.categoryList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.cat = '';
    this.search.catIcon = '';
    this.search.status = '';
  }

  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }
  editCat(editData) {
    this.passSer.editCategory[0] = (editData);
    this.router.navigate(['pages/categories/edit-categories']);
  }
  addCat() {
    this.passSer.editCategory = [];
    this.router.navigate(['pages/categories/edit-categories']);
  }
  // Export To Excel
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.categoryList.length; i++) {
      if (this.categoryList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.categoryList[i].status === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'CATEGORY NAME': this.categoryList[i].cat,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Categories Details');
  }

  fetchCategories() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      this.fetchCatSer.fetchCategories(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
            // let userD = [];
            this.categoryList = res.data;
            if(this.categoryList.length === 0) {
              this.featureLength = false;
            } else if(this.categoryList.length > 0) {
              this.featureLength = true;
              for (let i = 0; i < this.categoryList.length; i++) {
                if( this.categoryList[i].catIcon != null) {
                  this.categoryList[i].catIcon = this.getDownload + this.categoryList[i].catIcon;
                }
              }
            }
            console.log('this.categoryList',this.categoryList);
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.categoryList = [];
        this.setveractive = true;
        this.toastr.error('Server Error');
      });
    }
  }
}

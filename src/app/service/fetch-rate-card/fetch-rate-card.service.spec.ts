import { TestBed } from '@angular/core/testing';

import { FetchRateCardService } from './fetch-rate-card.service';

describe('FetchRateCardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchRateCardService = TestBed.get(FetchRateCardService);
    expect(service).toBeTruthy();
  });
});

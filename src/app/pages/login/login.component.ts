import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { EventsService } from 'angular4-events';
import { LoginService } from '../../service/login/login.service';
import { FcmService } from '../../service/fcm/fcm.service';
@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  registerForm: FormGroup;
  recoverForm: FormGroup;
  submitted = false;
  userD: any = [];
  public type2 = 'password';
  public showPass = false;
  loginScreen = true;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private pubsub: EventsService,
    private loginSer: LoginService,
    private fcmservice: FcmService) {
    console.log('login');
    var id = document.getElementById('nb-global-spinner');
    id.style.display = "none";
  }


  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],

    });

    this.recoverForm = this.formBuilder.group({
      email: ['', Validators.required],
      username: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  get r() { return this.recoverForm.controls; }

  forgot() {
    this.loginScreen = false;
  }

  signIn() {
    this.loginScreen = true;

  }
  onSubmit() {
    if (this.loginScreen) {


      this.submitted = true;
      console.log(this.registerForm.value);
      // stop here if form is invalid
      if (this.registerForm.invalid) {
        // this.spinner.hide();
        if (!this.registerForm.value.username || !this.registerForm.value.password) {
          this.toastr.error('Username and Password is required');
        }
        return;
      } else if (!window.navigator.onLine) {
        this.toastr.error('Please check your internet connection');
      } else {
        this.spinner.show();
        const params = {
          usn: this.registerForm.value.username,
          pass: this.registerForm.value.password,
        }
        this.loginSer.webLogin(params).then(res => {
          try {
            if (res.type === true && res.status_code === 200) {
              console.log('res', res);
              try {
                this.userD = res.data[0];
                localStorage.setItem('UserName', JSON.stringify(this.userD.username));
                localStorage.setItem('UserData', JSON.stringify(this.userD));
                localStorage.setItem('userid', this.userD.userid);
                this.fcmservice.saveUserTokenInfo(this.userD.userid, 'login', (response) => {

                  this.spinner.hide();

                  console.log('registerUserTokenInfo', response);
                });
                this.router.navigate(['../pages']);
                this.toastr.success(res.message);


                // setTimeout(() => {
                //   /** spinner ends after 5 seconds */
                //   this.toastr.success(res.message);
                // }, 1000);
              } catch (e) {
                console.log(e);
              }
            }
            if (res.type === false) {
              this.spinner.hide();
              console.log('res', res);
              this.toastr.error(res.message);
            }
          } catch (e) {
            console.log(e);
          }
        },
          err => {
            console.log(err);
            this.spinner.hide();
            this.toastr.error('Server Error');
          });
        // if (this.registerForm.value.username === 'admin' && this.registerForm.value.password === 'admin') {

        //   this.router.navigate(['../pages']);
        //   this.spinner.hide();
        //   setTimeout(() => {
        //     /** spinner ends after 5 seconds */
        //     this.toastr.success('Login Successfull');
        //   }, 2000);
        // }
        // else {
        //   this.spinner.hide();
        //   this.toastr.error('Invalid username or password');
        // }
      }

    } else {
      console.log(this.recoverForm.value);

      if (this.recoverForm.invalid) {
        // this.spinner.hide();
        if (!this.recoverForm.value.email) {
          this.toastr.error('email is required');
        }
        return;
      } else if (!window.navigator.onLine) {
        this.toastr.error('Please check your internet connection');
      } else {
        this.spinner.show();
        const params = {
          user: this.recoverForm.value.username,
          email: this.recoverForm.value.email,
        }
        this.loginSer.forgotLogin(params).then(res => {
          this.spinner.hide();

          try {
            if (res.type === true) {
              console.log('res', res);
              try {


                if (res.usercount == 0) {
                  this.toastr.success('Invalid credentails');
                  console.log('Invalid credentails');

                } else {
                  this.toastr.success('New password sent to ' + params.email);
                  console.log('New password sent to ' + params.email);

                 

                }





                // setTimeout(() => {
                //   /** spinner ends after 5 seconds */
                //   this.toastr.success(res.message);
                // }, 1000);
              } catch (e) {
                console.log(e);
              }
            }
            if (res.type === false) {
              this.spinner.hide();
              console.log('res', res);
              this.toastr.error(res.message);
            }
          } catch (e) {
            console.log(e);
          }
        },
          err => {
            console.log(err);
            this.spinner.hide();
            this.toastr.error('Server Error');
          });

      }


      console.log('Forgot Paassword')
    }
  }

  // loginUser(){
  //   this.submitted = true;
  //   console.log(this.registerForm.value);
  //       // stop here if form is invalid
  //     if (this.registerForm.invalid) {
  //         return;
  //     }
  //     else{
  //       // this.router.navigate(['../pages']);
  //       this.spinner.show();
  //       setTimeout(() => {
  //         /** spinner ends after 5 seconds */
  //         this.spinner.hide();
  //     }, 1000);
  //       this.loginSer.webLogin(this.registerForm.value).then(res => {
  //         if (res.type === true) {
  //           console.log('res', res);
  //           this.userD = res.data[0];
  //           let details = {
  //             address: this.userD.address,
  //             city: this.userD.city,
  //             country: this.userD.country,
  //             createddate: this.userD.createddate,
  //             deleted: this.userD.deleted,
  //             dob: this.userD.dob,
  //             email: this.userD.email,
  //             firstlogin: this.userD.firstlogin,
  //             firstname: this.userD.firstname,
  //             gender: this.userD.gender,
  //             isnotification: this.userD.isnotification,
  //             language: this.userD.language,
  //             lastlogin: this.userD.lastlogin,
  //             lastname: this.userD.lastname,
  //             modifieddate: this.userD.modifieddate,
  //             otpverified: this.userD.otpverified,
  //             phone: this.userD.phone,
  //             picture: this.userD.picture,
  //             pincode: this.userD.pincode,
  //             policyagreed: this.userD.policyagreed,
  //             roleName: this.userD.roleName,
  //             state: this.userD.state,
  //             suspended: this.userD.suspended,
  //             url: this.userD.url,
  //             userid: this.userD.userid,
  //             username: this.userD.username,
  //             verifieddate: this.userD.verifieddate
  //           }
  //           localStorage.setItem('UserDetails',JSON.stringify(details));
  //           // this.spinner.hide();
  //           // this.toastr.success('Login Successfull',' ', {timeOut: 4000});
  //           this.router.navigate(['../pages']);
  //           this.pubsub.publish('login');
  //           this.toastr.success('Login Successfull');
  //         }
  //         if (res.type === false) {
  //           console.log('res', res);
  //           if (res.data1 == 'Invalid Username') {
  //             // this.spinner.hide();
  //             this.toastr.error('Invalid Username');
  //         }
  //         else if (res.data1 == 'Invalid Password') {
  //           this.spinner.hide();
  //           this.toastr.error('Invalid Password');
  //         }
  //         }
  //       }, err => {
  //         console.log(err);
  //         this.spinner.hide();
  //         this.toastr.error('Server Error');
  //     });
  //     }
  // }
  forgotPassPage() {
    this.router.navigate(['../forgot-password']);
  }

  showPassword() {
    this.showPass = !this.showPass;
    if (this.showPass) {
      this.type2 = 'text';
    } else {
      this.type2 = 'password';
    }
  }

}

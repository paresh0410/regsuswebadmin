import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class AddEditDuartionService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addeditDurationUrl = this.baseUrl + webApi.apiUrl.addEditDuration;
  constructor(public http: HttpClient) { }
  public addeditDuration(durationData: any): Promise<any> {

    return this.http.post(this.addeditDurationUrl, durationData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

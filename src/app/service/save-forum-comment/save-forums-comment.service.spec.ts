import { TestBed } from '@angular/core/testing';

import { SaveForumsCommentService } from './save-forums-comment.service';

describe('SaveForumsCommentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveForumsCommentService = TestBed.get(SaveForumsCommentService);
    expect(service).toBeTruthy();
  });
});

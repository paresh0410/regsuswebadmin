import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeTrailRequestComponent } from './free-trail-request.component';

describe('FreeTrailRequestComponent', () => {
  let component: FreeTrailRequestComponent;
  let fixture: ComponentFixture<FreeTrailRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeTrailRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeTrailRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Component, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';

import { FilterPipeModule } from 'ngx-filter-pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { BlankRoutingComponent } from './blank-routing/blank-routing.component';
import { TruncateModule } from 'ng2-truncate';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ModalDialogModule } from 'ngx-modal-dialog';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NbTabsetModule, NbButtonModule, NbBadgeModule } from '@nebular/theme';
import { UserComponent } from './user/user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { FeaturesComponent } from './features/features.component';
import { AddEditFeaturesComponent } from './features/add-edit-features/add-edit-features.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import { AddEditSubscriptionComponent } from './subscription/add-edit-subscription/add-edit-subscription.component';
import { CategoriesComponent } from './categories/categories.component';
import { EditCategoriesComponent } from './categories/edit-categories/edit-categories.component';
import { PostsComponent } from './posts/posts.component';
import { AddEditPostsComponent } from './posts/add-edit-posts/add-edit-posts.component';
import { PollsComponent } from './polls/polls.component';
import { AddEditPollsComponent } from './polls/add-edit-polls/add-edit-polls.component';
import { EmailTemplatesComponent } from './email-templates/email-templates.component';
import { EditEmailTemplatesComponent } from './email-templates/edit-email-templates/edit-email-templates.component';
import { PollResultsComponent } from './polls/poll-results/poll-results.component';
// import { D3BarComponent } from '../pages/charts/d3/d3-bar.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PackagesComponent } from './packages/packages.component';
import { AddEditPackagesComponent } from './packages/add-edit-packages/add-edit-packages.component';
import { CorporationComponent } from './corporation/corporation.component';
import { AddEditCorporationComponent } from './corporation/add-edit-corporation/add-edit-corporation.component';
import { DurationComponent } from './duration/duration.component';
import { AddEditDurationComponent } from './duration/add-edit-duration/add-edit-duration.component';
import { RateCardComponent } from './rate-card/rate-card.component';
import { AddEditRateCardComponent } from './rate-card/add-edit-rate-card/add-edit-rate-card.component';
import { SubscriberValidationComponent } from './user/subscriber-validation/subscriber-validation.component';
import { GstComponent } from './gst/gst.component';
import { AddEditGstComponent } from './gst/add-edit-gst/add-edit-gst.component';
import { DealsComponent } from './corporation/deals/deals.component';
import { AddEditDealsComponent } from './corporation/deals/add-edit-deals/add-edit-deals.component';
import { InterestsComponent } from './interests/interests.component';
import { AddEditInterestsComponent } from './interests/add-edit-interests/add-edit-interests.component';
import { ForumsComponent } from './forums/forums.component';
import { AddEditForumsComponent } from './forums/add-edit-forums/add-edit-forums.component';
import { ViewForumsComponent } from './forums/view-forums/view-forums.component';
import { ChatsComponent } from './chats/chats.component';
import { AvatarModule } from 'ngx-avatar';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { NgxInfiniteScrollerModule } from 'ngx-infinite-scroller';
import { ComponentsModule } from '../components/components.module';
import { Dropdown } from '../../app/directive/dropdown.directive';
import { SubDealComponent } from './user/sub-deal/sub-deal.component';
import { AddEditSubDealComponent } from './user/sub-deal/add-edit-sub-deal/add-edit-sub-deal.component';
import { ChartsModule } from 'ng2-charts';
// import { ChartsModule } from 'angular-bootstrap-md';

// import { PackageComponent } from './package/package.component';
import { PromotionComponent } from './promotion/promotion.component';
import { AddEditPromotionComponent } from './promotion/add-edit-promotion/add-edit-promotion.component';
import { MetalSurveyReportComponent } from './metal-survey-report/metal-survey-report.component';
import { AddEditMetalSurveyReportComponent } from './metal-survey-report/add-edit-metal-survey-report/add-edit-metal-survey-report.component';
import { NumberOnlyDirective } from '../directive/numbers-only.directive';
import { AlphabetOnlyDirective } from '../directive/alphabet-only.directive';
import { MannualSubscribersComponent } from './user/mannual-subscribers/mannual-subscribers.component';
import { AppSubscribersComponent } from './user/app-subscribers/app-subscribers.component';
import { ActivityReportComponent } from './activity-report/activity-report.component';
import { PackageDashboardComponent } from './package-dashboard/package-dashboard.component';
import { TrendReportComponent } from './trend-report/trend-report.component';
import { ActivityDetailsComponent } from './activity-report/activity-details/activity-details.component';
import { PackageDetailsComponent } from './package-dashboard/package-details/package-details.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SectionmanagementComponent } from './sectionmanagement/sectionmanagement.component';
import { AddEditSectionsComponent } from './sectionmanagement/add-edit-sections/add-edit-sections.component';
import { SetOrderComponent } from './sectionmanagement/set-order/set-order.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { BroadcastComponent } from './broadcast/broadcast.component';
import { AddEditBroadcastComponent } from './broadcast/add-edit-broadcast/add-edit-broadcast.component';
import { ImageLibraryComponent } from './image-library/image-library.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgSelect2Module } from 'ng-select2';
import { OrderByPipe } from '../pipes/sort.pipe';
// import { CKEditorModule } from 'ng2-ckeditor';
import { CKEditorModule } from 'ckeditor4-angular';
import { LiveFeedComponent } from './live-feed/live-feed.component';
import { AddEditLiveFeedComponent } from './live-feed/add-edit-live-feed/add-edit-live-feed.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SeparatormanagementComponent } from './separatormanagement/separatormanagement.component';
import { SeparatormanagementformComponent } from './separatormanagement/separatormanagementform/separatormanagementform.component';
import { FreeTrailRequestComponent } from './free-trail-request/free-trail-request.component';
import { FreeTrailPopupComponent } from '../components/free-trail-popup/free-trail-popup.component';



const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    ChartsModule,
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    FilterPipeModule,
    NgxPaginationModule,
    TruncateModule,
    OwlDateTimeModule, OwlNativeDateTimeModule,
    NbTabsetModule,
    NbButtonModule,
    NbBadgeModule,
    NgMultiSelectDropDownModule.forRoot(),
    ModalDialogModule.forRoot(),
    NgxChartsModule,
    AvatarModule,
    UiSwitchModule,
    // InfiniteScrollModule,
    NgxInfiniteScrollerModule,
    ComponentsModule,
    ColorPickerModule,
    DragDropModule,
    NgSelect2Module,
    CKEditorModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    

  ],
  declarations: [
    ...PAGES_COMPONENTS,
    BlankRoutingComponent,
    UserComponent,
    EditUserComponent,
    FeaturesComponent,
    NumberOnlyDirective,
    AlphabetOnlyDirective,
    AddEditFeaturesComponent,
    SubscriptionComponent,
    AddEditSubscriptionComponent,
    CategoriesComponent,
    EditCategoriesComponent,
    PostsComponent,
    AddEditPostsComponent,
    PollsComponent,
    AddEditPollsComponent,
    EmailTemplatesComponent,
    EditEmailTemplatesComponent,
    PollResultsComponent,
    // D3BarComponent,
    PackagesComponent,
    AddEditPackagesComponent,
    CorporationComponent,
    AddEditCorporationComponent,
    DurationComponent,
    AddEditDurationComponent,
    RateCardComponent,
    AddEditRateCardComponent,
    SubscriberValidationComponent,
    GstComponent,
    AddEditGstComponent,
    DealsComponent,
    AddEditDealsComponent,
    InterestsComponent,
    AddEditInterestsComponent,
    ForumsComponent,
    AddEditForumsComponent,
    ViewForumsComponent,
    ChatsComponent,
    Dropdown,
    SubDealComponent,
    AddEditSubDealComponent,
    // PackageComponent,
    PromotionComponent,
    AddEditPromotionComponent,
    MetalSurveyReportComponent,
    AddEditMetalSurveyReportComponent,
    MannualSubscribersComponent,
    AppSubscribersComponent,
    ActivityReportComponent,
    PackageDashboardComponent,
    TrendReportComponent,
    ActivityDetailsComponent,
    PackageDetailsComponent,
    ChangePasswordComponent,
    SectionmanagementComponent,
    AddEditSectionsComponent,
    SetOrderComponent,
    BroadcastComponent,
    AddEditBroadcastComponent,
    ImageLibraryComponent,
    SeparatormanagementComponent,
     SeparatormanagementformComponent,
    OrderByPipe,
    LiveFeedComponent,
    AddEditLiveFeedComponent,
    FreeTrailRequestComponent,
    
    // LoginComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ],
})
export class PagesModule {
}

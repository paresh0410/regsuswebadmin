import { SeparatorsService } from './../../service/separators/separators.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../service/common.service';
import { ToastrService } from 'ngx-toastr';
import { SectionsService } from '../../service/sections/sections.service';
import { ExcelService } from '../../service/excel.service';

@Component({
  selector: 'ngx-separatormanagement',
  templateUrl: './separatormanagement.component.html',
  styleUrls: ['./separatormanagement.component.scss']
})
export class SeparatormanagementComponent implements OnInit {

  sectionList: any = []
  tableList: any = []

  column: any = [];
  isDesc: any = [];
  search = {
    position: '',
    html: '',
    link: '',
    linkType: ''
  };
  dataArrayforExcel: any = []
  dataObjforExcel: any = []
  setveractive: boolean = false
  constructor(private router: Router, private commonService: CommonService, private toastr: ToastrService,
    private sectionService: SectionsService,private separatorservice:SeparatorsService, private excelService: ExcelService) { }

  ngOnInit() {
    this.fetchSectionData()
  }

  setOrder() {
    this.router.navigate(['pages/sectionmanagement/set-order']);

  }
  addEditSection(item, val) {
    console.log(item)
    this.separatorservice.sepratordata = item
    this.router.navigate(['pages/separatormanagement/separatormanagementform']);
  }

  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.tableList.length; i++) {
      this.dataObjforExcel = {
        'NAME': this.tableList[i].name,
        'DISCRIPTION': this.tableList[i].description,
        'MANDATARY SECTION': this.tableList[i].ismandatory == 1 ? 'Yes' : 'No',
        'STATUS': this.tableList[i].status == 1 ? 'Active' : 'Inactive',
      }
      // console.log(this.dataObjforExcel)
      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Section Details');
  }
  sort(val) {

  }
  invert() { }
  myFunction(event) {
    // this.search.status = event.target.value;
    // console.log('value', event);
  }
  myFunction1(event) {
    // this.search.ismandatory = event.target.value;
    // console.log('value', event);
  }
  clearBtn() {
    // this.search.name = '',
    //   this.search.description = '',
    //   this.search.ismandatory = '',
    //   this.search.status = ''
  }

  fetchSectionData() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      this.separatorservice.fetchSectionList().then((res: any) => {
        try {
          if (res.type) {
            this.tableList = res.data
            console.log('this.tablelist', this.tableList)
          } else {

          }
        } catch (error) {
          console.log(error);
        }
      })
    }
  }
}

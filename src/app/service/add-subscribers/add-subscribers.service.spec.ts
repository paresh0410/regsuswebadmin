import { TestBed } from '@angular/core/testing';

import { AddSubscribersService } from './add-subscribers.service';

describe('AddSubscribersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddSubscribersService = TestBed.get(AddSubscribersService);
    expect(service).toBeTruthy();
  });
});

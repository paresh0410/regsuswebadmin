import { Component, OnInit, ElementRef } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { FetchEmailTemplateService } from '../../service/fetch-email-template/fetch-email-template.service';
import { BrowserInfoService } from '../../service/browser-info.service';

@Component({
  selector: 'ngx-email-templates',
  templateUrl: './email-templates.component.html',
  styleUrls: ['./email-templates.component.scss']
})
export class EmailTemplatesComponent implements OnInit {
  templateList: any = [
    // {
    //   id : 1,
    //   label : 'Registration',
    //   subject : 'Regsus App Registration',
    //   content : `<p>Hi User,</p>
    //             <p>Welcome to Regsus.You have succesfully completed your registration process.Please log in with your mobile number to continue.</p>
    //             <p>Yours truly,</p>
    //             <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 1,
    //   triggerName : 'Registration Completion',
    //   status : 1,
    // },
    // {
    //   id : 2,
    //   label : 'Trial Subscription',
    //   subject : 'Trial Subscription Period',
    //   content : `<p>Hi User,</p>
    //             <p>You have benifited with trial subscription period from 12.06.2019 to 11.07.2019.</p>
    //             <p>Yours truly,</p>
    //             <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 1,
    //   triggerName : 'Registration Completion',
    //   status : 1,
    // },
    // {
    //   id : 3,
    //   label : 'Trial Subscription Notify',
    //   subject : 'Trial Subscription Period Warning',
    //   content : `<p>Hi User,</p>
    //           <p>Your trial subscription will be expired on 11.07.2019.Please renew the subscription to avoid the stoppage in services.</p>
    //           <p>Yours truly,</p>
    //           <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 2,
    //   triggerName : 'Before 10 day of free expiry',
    //   status : 1,
    // },
    // {
    //   id : 4,
    //   label : 'Trial Subscription End',
    //   subject : 'Trial Subscription Period Expired',
    //   content : `<p>Hi User,</p>
    //           <p>Your trial subscription period is expired today.Please renew the subscription to continue.</p>
    //           <p>Yours truly,</p>
    //           <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 3,
    //   triggerName : 'After free expiry',
    //   status : 1,
    // },
    // {
    //   id : 5,
    //   label : 'Subscription Purchase',
    //   subject : 'New Subscription',
    //   content : `<p>Hi User,</p>
    //           <p>Thank you for purchasing the new subscription.</p>
    //           <p>Your subscription plan will be valid till 20.08.2019.</p>
    //           <p>Regards,</p>
    //           <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 4,
    //   triggerName : 'After purchase',
    //   status : 1,
    // },
    // {
    //   id : 6,
    //   label : 'Subscription Notify',
    //   subject : 'Subscription Period Warning',
    //   content : `<p>Hi User,</p>
    //           <p>Your subscription will be expired on 20.08.2019.Please renew the subscription to avoid the stoppage in services.</p>
    //           <p>Yours truly,</p>
    //           <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 5,
    //   triggerName : 'Before 10 day of expiry',
    //   status : 1,
    // },
    // {
    //   id : 7,
    //   label : 'Subscription End',
    //   subject : 'Subscription Period Expired',
    //   content : `<p>Hi User,</p>
    //           <p>Your subscription period is expired today.Please renew the subscription to continue.</p>
    //           <p>Yours truly,</p>
    //           <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 6,
    //   triggerName : 'After expiry',
    //   status : 1,
    // },
    // {
    //   id : 8,
    //   label : 'Subcription Change',
    //   subject : 'Switching to Another Subscription',
    //   content : `<p>Hi User,</p>
    //             <p>Your previous subscription has been replaced with the new subscription.</p>
    //             <p>Your subscription plan will be valid till 20.08.2019.</p>
    //             <p>Please contact to administrator for quries and further details.</p>
    //             <p>Regards,</p>
    //             <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 7,
    //   triggerName : 'update user',
    //   status : 1,
    // },
    // {
    //   id : 9,
    //   label : 'Poll',
    //   subject : 'New Poll Added',
    //   content : `<p>Hi User,</p>
    //           <p>New poll has been scheduled on 30.09.2019 and will end on 01.08.2019.</p>
    //           <p>Regards,</p>
    //           <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 8,
    //   triggerName : 'add poll',
    //   status : 1,
    // },
    // {
    //   id : 10,
    //   label : 'Start Poll',
    //   subject : 'Starting New Poll',
    //   content : `<p>Hi User,</p>
    //             <p>New poll has been started and will end on 01.08.2019.Please attend the poll and provide your feedback.</p>
    //             <p>Regards,</p>
    //             <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 9,
    //   triggerName : 'start poll',
    //   status : 1,
    // },
    // {
    //   id : 11,
    //   label : 'End Poll',
    //   subject : 'Poll Ended',
    //   content : `<p>Hi User,</p>
    //           <p>This poll has been ended.Thank you for your response.</p>
    //           <p>Regards,</p>
    //           <p>Regsus</p>`,
    //   attachment: '',
    //   fromEmail : 'support@test.com',
    //   footer : '',
    //   trigId : 10,
    //   triggerName : 'end poll',
    //   status : 1,
    // },
  ];
  p: number = 1;
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  userData :any;
  bowserInfo:any;
  isDesc: any = [];
  column: any = [];
  flag: any;
  search = {
    subject: '',
    triggerName: '',
    status: '',
  };
  featureLength: any = true;
  setveractive:any = false;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private emailTempSer: FetchEmailTemplateService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
   }
  ngOnInit() {
    this.spinner.show();
    this.fetchTemplate();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.templateList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.subject = '';
    this.search.triggerName = '';
    this.search.status = '';
  }

  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }

  editFeature(editData) {
    this.passSer.editET[0] = (editData);
    this.router.navigate(['pages/email-templates/edit-email-templates']);
  }

  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.templateList.length; i++) {
      if (this.templateList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.templateList[i].status === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'SUBJECT': this.templateList[i].subject,
        'TRIGGER NAME': this.templateList[i].triggerName,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Email Template Details');
  }

  fetchTemplate() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      this.emailTempSer.fetchEmailTemplates(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
            // let userD = [];
            this.templateList = res.data;
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            if(this.templateList.length === 0) {
              this.featureLength = false;
            } else if(this.templateList.length > 0){
              this.featureLength = true;
            }
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.templateList = [];
        this.setveractive = true;
        this.toastr.error('Server Error');
      });
    }
  }
}

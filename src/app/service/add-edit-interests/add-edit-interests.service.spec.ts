import { TestBed } from '@angular/core/testing';

import { AddEditInterestsService } from './add-edit-interests.service';

describe('AddEditInterestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditInterestsService = TestBed.get(AddEditInterestsService);
    expect(service).toBeTruthy();
  });
});

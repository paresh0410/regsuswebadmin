import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { AddEditRateCardService } from '../../../service/add-edit-rate-card/add-edit-rate-card.service';
import { BrowserInfoService } from '../../../service/browser-info.service';

@Component({
  selector: 'ngx-add-edit-rate-card',
  templateUrl: './add-edit-rate-card.component.html',
  styleUrls: ['./add-edit-rate-card.component.scss']
})
export class AddEditRateCardComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  rateData: any = [];
  isedit: any = false;
  featuresList: any = [];
  durVal: any = new RegExp('[0-9]');
  userData :any;
  bowserInfo:any;
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private passSer: PassServService,
    private addEditRateSer: AddEditRateCardService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.rateData = this.passSer.editRate;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
   }

   ngOnInit() {
    this.spinner.show();
    this.featuresList = this.passSer.featureList;
    console.log('this.featuresList' , this.featuresList);
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    if(this.rateData[0]) {
      this.isedit = true;
      this.registerForm = this.formBuilder.group({
        fname : [ this.rateData[0].featureId , [Validators.required]],
        frate: [this.rateData[0].frate , [Validators.required , Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]],
        status: [this.rateData[0].status , [Validators.required]],
      });
    } else {
      this.registerForm = this.formBuilder.group({
        fname : ['' , [Validators.required]],
        frate: ['' , [Validators.required, Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')]],
        status: ['', [Validators.required]],
      });
    }
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.spinner.show();
      const params = { 
        r_id : 0,
        r_fid : this.registerForm.value.fname,
        r_brate : this.registerForm.value.frate,
        r_status : this.registerForm.value.status,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      console.log('params',params)
      if (this.rateData[0]) {
        if (this.rateData[0].ratecardId) {
          params.r_id = this.rateData[0].ratecardId;
        }
      }
      console.log('params', params);
      this.addEditRateSer.addeditRateCard(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.data) {
                if (res.data.error) {
                  this.toastr.error(res.data.error);
                } else if (res.data.updated) {
                  this.router.navigate(['pages/rate-card']);
                  this.toastr.success(res.data.updated);
              } else if (res.data.saved) {
                this.router.navigate(['pages/rate-card']);
                this.toastr.success(res.data.saved);
            } else if (res.data.free) {
              // this.router.navigate(['pages/rate-card']);
              this.toastr.warning(res.data.free);
          } else if (res.data.paid) {
            // this.router.navigate(['pages/rate-card']);
            this.toastr.warning(res.data.paid);
        }
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
      // console.log('success');
      // this.router.navigate(['pages/rate-card']);
      // this.toastr.success('Data Saved Successfully');
    }
  }

  get f() {
    return this.registerForm.controls;
  }

  back() {
    this.router.navigate(['pages/rate-card']);
  }

  Validation(event) {
    console.log('event', event);
    if (event.target.placeholder === 'Feature Name') {
      if (!event.target.value) {
        this.toastr.error('Feature Name is required');
      }
    }
    if (event.target.placeholder === 'Base Rate') {
      if (!event.target.value) {
        this.toastr.error('Base Rate is required');
      } else if (!this.durVal.test(event.target.value)) {
        this.toastr.error('Base Rate is invalid');
      }
    }
  }
}

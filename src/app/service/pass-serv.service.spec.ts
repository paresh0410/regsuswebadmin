import { TestBed } from '@angular/core/testing';

import { PassServService } from './pass-serv.service';

describe('PassServService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PassServService = TestBed.get(PassServService);
    expect(service).toBeTruthy();
  });
});

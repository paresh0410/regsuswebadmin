import { TestBed } from '@angular/core/testing';

import { AddEditRateCardService } from './add-edit-rate-card.service';

describe('AddEditRateCardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditRateCardService = TestBed.get(AddEditRateCardService);
    expect(service).toBeTruthy();
  });
});

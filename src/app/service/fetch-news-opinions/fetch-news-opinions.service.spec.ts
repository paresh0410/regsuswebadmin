import { TestBed } from '@angular/core/testing';

import { FetchNewsOpinionsService } from './fetch-news-opinions.service';

describe('FetchNewsOpinionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchNewsOpinionsService = TestBed.get(FetchNewsOpinionsService);
    expect(service).toBeTruthy();
  });
});

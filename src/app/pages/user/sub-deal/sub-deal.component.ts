import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from '../../../service/excel.service';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { FetchSubDealsService } from '../../../service/fetch-sub-deals/fetch-sub-deals.service';
import { BrowserInfoService } from '../../../service/browser-info.service';

@Component({
  selector: 'ngx-sub-deal',
  templateUrl: './sub-deal.component.html',
  styleUrls: ['./sub-deal.component.scss']
})
export class SubDealComponent implements OnInit {
  dealsList: any = [];

  invoice: any = [];
  tooggleshowHidw = false;
  p: number = 1;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  search = {
    trackId: '',
    pkgName: '',
    purDate: '',
    fromDate: '',
    toDate: '',
    modifiedDate: '',
    felenth: '',
    discountPercent: '',
    durationId: '',
    totalAmt: '',
    payType: '',
    receiptNo: '',
    payDate: '',
    dealstaId: '',
    isActive: '',
  };
  display: any = 'none';
  showDiv: boolean = true;
  display1: any = 'none';
  showDiv1: boolean = true;
  arrow: any = false;
  featureLength: any = true;
  setveractive: any = false;
  tempData: any = [];
  subID: any;
  corpName: any;
  featuresData: any;
  durationData: any;
  packageData: any = [];
  hidePopup: boolean = true;
  cardDetails: any;
  makenot: any = true;
  showOptions: any = false;
  validdata: any = [];
  invaliddata: any = [];
  todayDate: any = new Date();
  userData: any;
  bowserInfo: any;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private toastr: ToastrService,
    private excelService: ExcelService,
    private fetchSubDealsSer: FetchSubDealsService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.tempData = this.passSer.editUser;
    if (this.tempData[0]) {
      this.subID = this.tempData[0].subId;
      this.corpName = this.tempData[0].name;
    }
    this.spinner.show();
    this.fetchDeals();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }


  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.dealsList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  clearBtn() {
    this.search.trackId = '';
    this.search.pkgName = '';
    this.search.purDate = '';
    this.search.fromDate = '';
    this.search.toDate = '';
    this.search.modifiedDate = '';
    this.search.felenth = '';
    this.search.discountPercent = '';
    this.search.durationId = '';
    this.search.totalAmt = '';
    this.search.payType = '';
    this.search.receiptNo = '';
    this.search.payDate = '';
    this.search.dealstaId = '';
    this.search.isActive = '';
  }

  myFunction(event) {
    this.search.isActive = event.target.value;
  }

  changeState(state) {
    this.search.dealstaId = state.target.value;
  }

  back() {
    let back = this.passSer.flag;
    if (back == 'manual') {
      this.router.navigate(['pages/user/mannual-subscribers']);
    } else {
      this.router.navigate(['pages/user/app-subscribers']);
    }
    //this.router.navigate(['pages/user']);
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join('-');
  }

  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.dealsList.length; i++) {
      if (this.dealsList[i].isActive === 1) {
        this.flag = 'Active';
      }
      if (this.dealsList[i].isActive === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'TRACKER ID': this.dealsList[i].trackId,
        'PACKAGE': this.dealsList[i].selPckage.pkgName,
        'PURCHASE DATE': this.formatDate(this.dealsList[i].purDate),
        'FROM DATE': this.formatDate(this.dealsList[i].fromDate),
        'TO DATE': this.formatDate(this.dealsList[i].toDate),
        'MODIFIED DATE': this.formatDate(this.dealsList[i].modifiedDate),
        'NO. OF FEATURES': this.dealsList[i].felenth,
        'DISCOUNT(%)': this.dealsList[i].discountPercent,
        'DURATION(MONTHS) ': this.dealsList[i].durationId,
        // 'NO. OF PERSONS': this.dealsList[i].userCount,
        'TOTAL PRICE': this.dealsList[i].totalAmt,
        'PAYMENT TYPE': this.dealsList[i].payType,
        'RECEIPT NO.': this.dealsList[i].receiptNo,
        'PAYMENT DATE': this.formatDate(this.dealsList[i].payDate),
        'STATE': this.dealsList[i].dealstaname,
        'STATUS': this.flag,
      },
        // console.log('this.dataArray',this.dataObjforExcel)

        this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Deals Details');
  }

  showinvoice(item) {
    this.hidePopup = false;
    this.cardDetails = item;
    console.log("carddetails ==", this.cardDetails);
  }

  hideinvoice() {
    this.hidePopup = true;
  }

  featuretotal(price, unit) {
    return (price * unit).toFixed(0);
  }

  userTotal(tprice, user) {
    return (tprice * user).toFixed(0);
  }

  percent(baseAmount, percent) {
    return ((baseAmount / 100) * percent).toFixed(0);
  }

  gstpercent(baseAmount, percent, gst) {
    let discount = (baseAmount / 100) * percent;
    let accprice = baseAmount - discount;
    this.cardDetails.accprice = accprice;
    this.cardDetails.gstvalue = (accprice / 100) * gst;
    return ((accprice / 100) * gst).toFixed(0);
  }
  acGrandTotal() {
    if (this.cardDetails.gst) {
      var gval = (this.cardDetails.accprice / 100) * this.cardDetails.gst;
      return (this.cardDetails.accprice + gval).toFixed(0);
    } else {
      let discount = (this.cardDetails.baseAmt * this.cardDetails.discountPercent) / 100;
      this.cardDetails.accprice = this.cardDetails.baseAmt - discount;
      return this.cardDetails.accprice
    }
  }

  showMore() {
    this.showOptions = !this.showOptions;
  }

  acGrandTotalW() {
    // const converter = require('number-to-words');
    // const wvalue = (this.cardDetails.accprice + this.cardDetails.gstvalue).toFixed(0);
    // const main = converter.toWords(wvalue);
    // return main;
    var gval = (this.cardDetails.accprice / 100) * this.cardDetails.gst;
    const writtenNumber = require('written-number');
    const main = parseInt((this.cardDetails.accprice + gval).toFixed(0), 10);
    let word = writtenNumber(main);
    word = word.replace(/-/g, ' ');
    return word;
  }

  public captureScreen() {
    var data = document.getElementById('contentToConvert');
    // var name = this.corpName;
    var currentPosition = document.getElementById("contentToConvert").scrollTop;
    var w = document.getElementById("contentToConvert").offsetWidth;
    var h = document.getElementById("contentToConvert").offsetHeight;
    document.getElementById("contentToConvert").style.height = "auto";
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save(this.corpName + '.pdf'); // Generated PDF
    });
    document.getElementById("contentToConvert").style.height = "100px";
    document.getElementById("contentToConvert").scrollTop = currentPosition;
    this.hideinvoice();
  }

  editDeals(editData) {
    this.passSer.editDeals[0] = (editData);
    this.passSer.featureList = this.featuresData;
    this.passSer.durationList = this.durationData;
    this.passSer.packageList = this.packageData;
    this.router.navigate(['pages/user/subscriber-deal/add-edit-subscriber-deal']);
  }

  renewDeals(newData) {
    this.passSer.renewDeals[0] = (newData);
    this.passSer.featureList = this.featuresData;
    this.passSer.durationList = this.durationData;
    this.passSer.packageList = this.packageData;
    this.router.navigate(['pages/user/subscriber-deal/add-edit-subscriber-deal']);
  }



  fetchDeals() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    }
    else {
      const params = {
        subId: this.subID,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      }
      this.fetchSubDealsSer.fetchSubDeals(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              let userD = [];
              userD = res.data
              this.dealsList = res.data;
              console.log('this first list', userD)
              for (let i = 0; i < this.dealsList.length; i++) {
                this.dealsList[i].purDate = new Date(this.dealsList[i].purDate);
                this.dealsList[i].fromDate = new Date(this.dealsList[i].fromDate);
                this.dealsList[i].toDate = new Date(this.dealsList[i].toDate);
                this.dealsList[i].modifiedDate = new Date(this.dealsList[i].modifiedDate);


                if (this.dealsList[i].payDate) {
                  this.dealsList[i].payDate = new Date(this.dealsList[i].payDate);
                } else {
                  this.dealsList[i].payDate = '';

                }
                if (this.dealsList[i].payRecDate) {

                  this.dealsList[i].payRecDate = new Date(this.dealsList[i].payRecDate);
                } else {
                  this.dealsList[i].payRecDate = null;
                }
                this.dealsList[i].pkgName = this.dealsList[i].selPckage.pkgName;
                let preDate = new Date(this.dealsList[i].toDate);
                preDate.setMonth(preDate.getMonth() - 2);
                const lastdate = preDate.toLocaleDateString();
                // console.log(preDate.toLocaleDateString());
                if (new Date(lastdate) < this.todayDate) {
                  this.dealsList[i].activeInvoice = true;
                } else {
                  this.dealsList[i].activeInvoice = false;
                }
              }
              this.featuresData = res.featureList;
              this.durationData = res.durationList;
              this.packageData = res.packageList;
              if (this.dealsList.length === 0) {
                this.featureLength = false;
              } else if (this.dealsList.length > 0) {
                this.featureLength = true;
              }
              console.log('this.dealsList', this.dealsList, this.featuresData, this.durationData);
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            if (this.dealsList.length === 0) {
              this.featureLength = false;
            } else if (this.dealsList.length > 0) {
              this.featureLength = true;
            }
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.dealsList = [];
          this.setveractive = true;
          this.toastr.error('Server Error');
        });
    }
  }
}

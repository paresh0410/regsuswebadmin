import { TestBed } from '@angular/core/testing';

import { AddEditDealService } from './add-edit-deal.service';

describe('AddEditDealService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditDealService = TestBed.get(AddEditDealService);
    expect(service).toBeTruthy();
  });
});

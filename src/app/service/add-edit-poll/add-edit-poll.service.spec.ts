import { TestBed } from '@angular/core/testing';

import { AddEditPollService } from './add-edit-poll.service';

describe('AddEditPollService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditPollService = TestBed.get(AddEditPollService);
    expect(service).toBeTruthy();
  });
});

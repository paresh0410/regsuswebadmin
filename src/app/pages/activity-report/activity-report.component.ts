import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../service/pass-serv.service';
import { BrowserInfoService } from '../../service/browser-info.service';
import { ExcelService } from '../../service/excel.service';
import { DashboardService } from '../../service/dashboard/dashboard.service';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';


export const MY_CUSTOM_FORMATS = {
  parseInput: 'DD-MM-YYYY',
  fullPickerInput: 'DD-MM-YYYY',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};

@Component({
  selector: 'ngx-activity-report',
  templateUrl: './activity-report.component.html',
  styleUrls: ['./activity-report.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    // `MomentDateTimeAdapter` can be automatically provided by importing
    // `OwlMomentDateTimeModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },

    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
  ],
})
export class ActivityReportComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private dashboard: DashboardService,
    private passService: PassServService,
    private browserInfoService: BrowserInfoService,
    private excelService: ExcelService, public cdf: ChangeDetectorRef) { }
  registerForm: any;
  gstList: any = [];

  dataArrayforExcel: any = [];
  dataObjforExcel: any = {};
  maxDate: any = new Date();
  // minDate: any = new Date();
  trailList: any = [];
  startDate: any;
  endDate: any;
  activityLists: any = [];
  activityList: any = [
    {
      name: 'Total',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    }, {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    }, {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    }, {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    }, {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    },
    {
      name: 'Total registrations from',
      corp: 'Bulk',
      indvidual: 'Non-Bulk',
      sum: 'Total',
      corpCount: 30,
      indCount: 20,
      total: 50,
    }
  ]
  ngOnInit() {
    const today = new Date();
    const end = this.convert(today);
    const start = this.convert(today.setDate(today.getDate() - 7));


    if (this.passService.actflag == 4) {
      this.registerForm = this.formBuilder.group({
        startDt: [this.passService.startdate, [Validators.required]],
        endDt: [this.passService.enddate, [Validators.required]],
      });
    } else {
      this.registerForm = this.formBuilder.group({
        startDt: [start, [Validators.required]],
        endDt: [end, [Validators.required]],
      });
    }


    if (this.passService.actflag == 4) {
      this.startDate = this.passService.startdate;
      this.endDate = this.passService.enddate;
    } else {
      this.startDate = this.convert(this.registerForm.value.startDt);
      this.endDate = this.convert(this.registerForm.value.endDt);
    }

    this.fetchActivity(this.startDate, this.endDate);

    // console.log('activityList',this.activityList);
  }

  // ngOnDestroy(): void {
  //   this.passService.actflag = null;
  // }

  checkVal(event) {
    console.log('event', event);

  }

  viewDetails(flag, name, count) {
    console.log(name)

    if (count == 0) {
      this.toastr.error('Choose count greater than 0');
      return;
    }
    this.passService.orglActivityname = name;
    this.passService.actflag = flag;
    this.passService.startdate = this.startDate;
    this.passService.enddate = this.endDate;
    if (name == 'New Subscriptions') {
      name = 'ReportReportNewSubscrip';
    } else if (name == 'Total Registrations') {
      name = 'ReportTotalRegis';
    } else if (name == 'Total Active Subscribers ') {
      name = 'ReportTotalActRegis';
    } else if (name == 'Renewals') {
      name = 'ReportRenewalsSubscrip';
    } else if (name == 'Upgrades') {
      name = 'ReportUpgradesSubscrip';
    } else if (name == 'New Subscription Revenues') {
      name = 'ReportNewSubscripReven';
    } else if (name == 'Renewal Revenues') {
      name = 'ReportRenewalsSubscripReven';
    } else if (name == 'Upgrade Revenues') {
      name = 'ReportUpgradesSubscripReven';
    } else if (name == 'TDS') {
      name = 'ReportTDS';
    } else if (name == 'GST') {
      name = 'ReportGST';
    } else if (name == 'Expiring Contract') {
      name = 'ReportExpiringContract';
    } else if (name == 'New Subscription Revenues for Overseas') {
      name = 'ReportNewSubscripRevenforoverseas';
    } else if (name == 'Renewal Revenues for Overseas') {
      name = 'ReportRenewalsSubscripRevenforoverseas';
    } else if (name == 'Upgrade Revenues for Overseas') {
      name = 'ReportUpgradesSubscripRevenforoverseas';
    } else if (name == 'Total New Subscription Revenues') {
      name = 'ReportNewSubscripRevenforTotal';
    } else if (name == 'Total Renewal Revenues') {
      name = 'ReportRenewalsSubscripRevenTotal';
    } else if (name == 'Total Upgrade Revenues') {
      name = 'ReportUpgradesSubscripRevenforTotal';
    } else {
      name = 'ReportExpired';
    }
    this.passService.name = name;

    console.log(this.passService.actflag, this.passService.startdate, this.passService.enddate, this.passService.name);

    this.router.navigate(['pages/activity/activity-detail']);
  }

  // ReportTotalRegis
  // ReportTotalActRegis
  // ReportReportNewSubscrip
  // ReportRenewalsSubscrip
  // ReportUpgradesSubscrip
  // ReportNewSubscripReven
  // ReportRenewalsSubscripReven
  // ReportUpgradesSubscripReven

  onSubmit() {
    // this.submitted = true;
    console.log(this.registerForm.value);
    if (this.registerForm.invalid) {
      return;
    } else {
      // this.spinner.show();
      this.startDate = this.convert(this.registerForm.value.startDt);
      this.endDate = this.convert(this.registerForm.value.endDt);
      const params = {
        FrmDate: this.startDate,
        ToDate: this.endDate,
      };
      console.log('params', params);
      this.fetchActivity(this.startDate, this.endDate);
    }
  }

  convert(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  exportExcelCorp() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.activityLists.length; i++) {
      this.dataObjforExcel = {
        'NAME': this.activityLists[i].name,
        'BULK': this.activityLists[i].bulk,
        'NON-BULK': this.activityLists[i].nonBulk,
        'SUM': this.activityLists[i].sum,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSXS();
  }

  exportAsXLSXS(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Activity Report');
  }


  fetchActivity(start, end) {
    this.spinner.show();
    if (!window.navigator.onLine) {
      this.spinner.hide();
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        FrmDate: start,
        ToDate: end,
      };
      this.dashboard.getDashboardActivity(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              this.activityLists = [];
              this.gstList = [];
              this.trailList = res.data;

              for (let i = 0; i < this.trailList.length; i++) {
                if (i < 3) {
                  if (i == 0) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 1) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[0].nonBulk = value
                      }
                    }
                  } else {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[0].sum = value;
                        this.activityLists[0].name = 'Total Registrations'
                      }
                    }
                  }
                }

                // if (i > 2 && i < 6) {
                //   if (i == 3) {
                //     for (let key in this.trailList[i][0]) {
                //       if (this.trailList[i][0].hasOwnProperty(key)) {
                //         var value = this.trailList[i][0][key];
                //         this.activityLists.push({
                //           bulk: value
                //         })
                //       }
                //     }
                //   } else if (i == 4) {
                //     for (let key in this.trailList[i][0]) {
                //       if (this.trailList[i][0].hasOwnProperty(key)) {
                //         var value = this.trailList[i][0][key];
                //         this.activityLists[1].nonBulk = value;
                //       }
                //     }
                //   } else {
                //     for (let key in this.trailList[i][0]) {
                //       if (this.trailList[i][0].hasOwnProperty(key)) {
                //         var value = this.trailList[i][0][key];
                //         this.activityLists[1].sum = value;
                //         this.activityLists[1].name = 'Total Active Subscribers '
                //       }
                //     }
                //   }
                // }
                if (i > 5 && i < 9) {
                  if (i == 6) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 7) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[1].nonBulk = value
                      }
                    }
                  } else {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[1].sum = value;
                        this.activityLists[1].name = 'New Subscriptions'
                      }
                    }
                  }
                }
                if (i > 8 && i < 12) {
                  if (i == 9) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 10) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[2].nonBulk = value
                      }
                    }
                  } else {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[2].sum = value;
                        this.activityLists[2].name = 'Renewals'
                      }
                    }
                  }
                }
                if (i > 11 && i < 15) {
                  if (i == 12) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 13) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[3].nonBulk = value
                      }
                    }
                  } else {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[3].sum = value;
                        this.activityLists[3].name = 'Upgrades'
                      }
                    }
                  }
                }
                if (i > 14 && i < 18) {
                  if (i == 15) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 16) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[4].nonBulk = value
                      }
                    }
                  } else {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[4].sum = value;
                        this.activityLists[4].name = 'New Subscription Revenues'
                      }
                    }
                  }
                }
                if (i > 17 && i < 21) {
                  if (i == 18) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 19) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[5].nonBulk = value
                      }
                    }
                  } else {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[5].sum = value;
                        this.activityLists[5].name = 'Renewal Revenues'
                      }
                    }
                  }
                }
                if (i > 20 && i < 24) {
                  if (i == 21) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 22) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[6].nonBulk = value;
                      }
                    }
                  } else {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[6].sum = value;
                        this.activityLists[6].name = 'Upgrade Revenues'
                      }
                    }
                  }
                }

                if (i > 23 && i < 27) {
                  if (i == 24) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 25) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[7].nonBulk = value;
                      }
                    }
                  } else {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[7].sum = value;
                        this.activityLists[7].name = 'Expired & Not-Renewed';
                      }
                    }
                  }
                }

                if (i > 26 && i < 30) {
                  if (i == 27) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 28) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[8].nonBulk = value;
                      }
                    }
                  } else if (i == 29) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[8].sum = value;
                        this.activityLists[8].name = 'TDS';
                      }
                    }
                  }
                }
                if (i > 29 && i < 33) {
                  if (i == 30) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 31) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[9].nonBulk = value;
                      }
                    }
                  } else if (i == 32) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[9].sum = value;
                        this.activityLists[9].name = 'Expiring Contract';
                      }
                    }
                  }
                } else if (i > 33 && i < 37) {
                  if (i == 34) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 35) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[10].nonBulk = value;
                      }
                    }
                  } else if (i == 36) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[10].sum = value;
                        this.activityLists[10].name = 'New Subscription Revenues for Overseas';
                      }
                    }
                  }
                } else if (i > 36 && i < 40) {
                  if (i == 37) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 38) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[11].nonBulk = value;
                      }
                    }
                  } else if (i == 39) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[11].sum = value;
                        this.activityLists[11].name = 'Renewal Revenues for Overseas';
                      }
                    }
                  }
                } else if (i > 39 && i < 43) {
                  if (i == 40) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 41) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[12].nonBulk = value;
                      }
                    }
                  } else if (i == 42) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[12].sum = value;
                        this.activityLists[12].name = 'Upgrade Revenues for Overseas';
                      }
                    }
                  }
                } else if (i > 42 && i < 46) {
                  if (i == 43) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 44) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[13].nonBulk = value;
                      }
                    }
                  } else if (i == 45) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[13].sum = value;
                        this.activityLists[13].name = 'Total New Subscription Revenues';
                      }
                    }
                  }
                } else if (i > 45 && i < 49) {
                  if (i == 46) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 47) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[14].nonBulk = value;
                      }
                    }
                  } else if (i == 48) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[14].sum = value;
                        this.activityLists[14].name = 'Total Renewal Revenues';
                      }
                    }
                  }
                } else if (i > 48 && i < 52) {
                  if (i == 49) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists.push({
                          bulk: value
                        })
                      }
                    }
                  } else if (i == 50) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[15].nonBulk = value;
                      }
                    }
                  } else if (i == 51) {
                    for (let key in this.trailList[i][0]) {
                      if (this.trailList[i][0].hasOwnProperty(key)) {
                        var value = this.trailList[i][0][key];
                        this.activityLists[15].sum = value;
                        this.activityLists[15].name = 'Total Upgrade Revenues';
                      }
                    }
                  }
                }


                if (i == 33) {


                  var sgst = this.trailList[i][0].SGST;
                  this.gstList.push({
                    SGST: sgst
                  })

                  var cgst = this.trailList[i][0].CGST;
                  this.gstList[0].CGST = cgst;

                  var igst = this.trailList[i][0].IGST;
                  this.gstList[0].IGST = igst;

                  var gst = this.trailList[i][0].GST;
                  this.gstList[0].GST = gst;
                }




              }
              this.cdf.detectChanges();
              console.log('this.trailList', this.trailList);
              console.log('this.activityLists', this.activityLists);
              console.log('this.gstList', this.gstList);

            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
    }
  }





}

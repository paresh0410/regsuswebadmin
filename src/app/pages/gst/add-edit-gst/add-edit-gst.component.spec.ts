import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditGstComponent } from './add-edit-gst.component';

describe('AddEditGstComponent', () => {
  let component: AddEditGstComponent;
  let fixture: ComponentFixture<AddEditGstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditGstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditGstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ExcelService } from '../../../service/excel.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { DashboardService } from '../../../service/dashboard/dashboard.service';
import { PassServService } from '../../../service/pass-serv.service';
import { letterSpacing } from '../../../../../node_modules/html2canvas/dist/types/src/css/property-descriptors/letter-spacing';

@Component({
  selector: 'ngx-activity-details',
  templateUrl: './activity-details.component.html',
  styleUrls: ['./activity-details.component.scss']
})
export class ActivityDetailsComponent implements OnInit {


  activityDetail: any = [];
  TDSDetail: any = [];
  GSTDetail: any = [];
  type: any;
  category: any;
  dataObjforExcel: any;
  dataArrayforExcel: any = [];

  searchManual = {
    subname: '',
    corpname: '',
    phoneno: '',
    createdDate: '',
    fromDate: '',
    gracePeriod: '',
    pkgName: '',
    amount: ''
  };
  searchTDS = {
    month: '',
    name: '',
    patment_mode: '',
    payment_sec: '',
    panNo: '',
    transaction_date: '',
    totalAmt: '',
    tdsrate: '',
    tdsamount: '',
  };
  searchGST = {
    invoice_number: '',
    invoice_date: '',
    hsn_code: '',
    name: '',
    gstid: '',
    package_name: '',
    payment_mode: '',
    from_date: '',
    to_date: '',
    taxable_vale: '',
    cgst: '',
    sgst: '',
    igst: '',
    total_tax: '',
    total_invoice_value: '',

  }
  showTdsTabe = false;
  showGSTTabe = false;
  showNorTable = true;
  total: any;
  constructor(private excelService: ExcelService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private dashboard: DashboardService,
    private passService: PassServService,
    public cdf: ChangeDetectorRef) { }

  ngOnInit() {

    let flag = this.passService.actflag;
    const startDate = this.passService.startdate;
    const endDate = this.passService.enddate;
    const name = this.passService.name;
    console.log(name)
    if (name === 'ReportTDS') {
      this.showTdsTabe = true;
      this.showNorTable = false;
      flag = 3;
    }

    if (name === 'ReportGST') {
      this.showGSTTabe = true;
      this.showNorTable = false;

    }






    this.category = this.passService.orglActivityname;
    if (this.passService.actflag == 1) {
      this.type = 'Bulk';
    } else if (this.passService.actflag == 2) {
      this.type = 'Non-Bulk';
    } else {
      this.type = 'Total';
    }

    // console.log(flag, startDate, endDate, name);
    this.fetchActivityDetail(flag, startDate, endDate, name);
  }

  goBack() {

    this.passService.actflag = 4;
    window.history.back();
  }

  clearBtnManual() {
    this.searchManual.subname = '';
    this.searchManual.corpname = '';
    this.searchManual.phoneno = '';
    this.searchManual.createdDate = '';
    this.searchManual.fromDate = '';
    this.searchManual.gracePeriod = '';
    this.searchManual.pkgName = '';
    this.searchManual.amount = ''
  }

  clearTDSManual() {
    this.searchTDS.month = '';
    this.searchTDS.name = '';
    this.searchTDS.patment_mode = '';
    this.searchTDS.payment_sec = '';
    this.searchTDS.panNo = '';
    this.searchTDS.transaction_date = '';
    this.searchTDS.totalAmt = '';
    this.searchTDS.tdsrate = '';
    this.searchTDS.tdsamount = '';
  }

  clearGSTManual() {
    this.searchGST.invoice_number = '';
    this.searchGST.invoice_date = '';
    this.searchGST.hsn_code = '';
    this.searchGST.name = '';
    this.searchGST.gstid = '';
    this.searchGST.package_name = '';
    this.searchGST.payment_mode = '';
    this.searchGST.from_date = '';
    this.searchGST.to_date = '';
    this.searchGST.taxable_vale = '';
    this.searchGST.cgst = '';
    this.searchGST.sgst = '';
    this.searchGST.igst = '';
    this.searchGST.total_tax = '';
    this.searchGST.total_invoice_value = '';
  }
  exportExcelCorpGST() {
    let title = 'Activity GST Details'
    let heading = ['INVOICE NUMBER',
      'INVOICE DATE',
      'HSN CODE',
      'COMPANY/NAME',
      'GST ID',
      'PACKAGE NAME',
      'PAYMENT MODE',
      'BILLING PERIOD FROM',
      'BILLING PERIOD TO',
      'TAXABLE VALUE',
      'CGST',
      'SGST',
      'IGST',
      'TOTAL TAX', 'TOTAL INVOICE VALUE'];
    let totalcount = Object.values(this.total);
    for (let i = 0; i < 9; i++) {
      totalcount.unshift("");
    }
    let data: any = [];

    let arangeData: any = [];
    let dataObjforExcelGst: any;
    for (let i = 0; i < this.GSTDetail.length; i++) {

      dataObjforExcelGst = {
        'INVOICE NUMBER': this.GSTDetail[i].invoice_number,
        'INVOICE DATE': this.GSTDetail[i].invoice_date,
        'HSN CODE': this.GSTDetail[i].hsn_code,
        'COMPANY/NAME': this.GSTDetail[i].name,
        'GST ID': this.GSTDetail[i].gstid,
        'PACKAGE NAME': this.GSTDetail[i].package_name,
        'PAYMENT MODE': this.GSTDetail[i].payment_mode,
        'BILLING PERIOD TO': this.GSTDetail[i].to_date,
        'BILLING PERIOD FROM': this.GSTDetail[i].from_date,
        'TAXABLE VALUE': this.GSTDetail[i].taxable_vale,
        'CGST': this.GSTDetail[i].cgst,
        'SGST': this.GSTDetail[i].sgst,
        'IGST': this.GSTDetail[i].igst,
        'TOTAL TAX': this.GSTDetail[i].total_tax,
        'TOTAL INVOICE VALUE': this.GSTDetail[i].total_invoice_value,


      }


      arangeData.push(dataObjforExcelGst);
    }
    // data = Object.values(arangeData);
    for (let i = 0; i < arangeData.length; i++) {
      const propertyValues: any = Object.values(arangeData[i]);
      data.push(propertyValues);
    }
    const totalLab = ["", "", "", "", "", "", "", "", "", "(total)", "(total)", "(total)", "(total)", "(total)", "(total)"]

    this.excelService.generateExcel(title, totalcount, heading, data, totalLab);
  }


  exportExcelCorpTDS() {
    let title = 'Activity TDS Details'
    let heading = [
      'MONTH',
      'COMAPNY',
      'PAYMNET MODE',
      'PAYMNET SECTION',
      'PAN NO',
      'TRANSACTION DATE',
      'AMOUNT CREDITED',
      'TDS RATE',
      'TDS DEDUCTED'
    ];
    let totalcount = Object.values(this.total);
    for (let i = 0; i < 6; i++) {
      totalcount.unshift("");
    }
    totalcount.splice(7, 0, "");

    let data: any = [];

    let arangeData: any = [];
    let dataObjforExcelGst: any;
    for (let i = 0; i < this.TDSDetail.length; i++) {

      dataObjforExcelGst = {
        'MONTH': this.TDSDetail[i].month,
        'COMAPNY': this.TDSDetail[i].name,
        'PAYMNET MODE': this.TDSDetail[i].patment_mode,
        'PAYMNET SECTION': this.TDSDetail[i].payment_sec,
        'PAN NO': this.TDSDetail[i].panNo,
        'TRANSACTION DATE': this.TDSDetail[i].transaction_date,
        'AMOUNT CREDITED': this.TDSDetail[i].totalAmt,
        ' TDS RATE': this.TDSDetail[i].tdsrate,
        'TDS DEDUCTED': this.TDSDetail[i].tdsamount,

      }
      arangeData.push(dataObjforExcelGst);
    }
    // data = Object.values(arangeData);
    for (let i = 0; i < arangeData.length; i++) {
      const propertyValues: any = Object.values(arangeData[i]);
      data.push(propertyValues);
    }
    const totalLab = ["", "", "", "", "", "", "(total)", "", "(total)"]

    this.excelService.generateExcel(title, totalcount, heading, data, totalLab);
  }



  exportExcelCorp() {
    this.dataArrayforExcel = [];
    if (this.showNorTable) {
      for (let i = 0; i < this.activityDetail.length; i++) {
        let currency
        if (this.activityDetail[i].currency == 1) {
          currency = '₹'
        } else if (this.activityDetail[i].currency == 2) {
          currency = '$'
        } else {
          currency = ''
        }
        this.dataObjforExcel = {
          'NAME': this.activityDetail[i].subname,
          'COMAPNY': this.activityDetail[i].corpname,
          'MOB NO': this.activityDetail[i].phoneno,
          'REGISTER DATE': this.activityDetail[i].createdDate,
          'SUBSCRIPTION START DATE': this.activityDetail[i].fromDate,
          'SUBSCRIPTION END DATE': this.activityDetail[i].gracePeriod,
          'PACKAGE': this.activityDetail[i].pkgName,
          'AMOUNT': currency + this.activityDetail[i].amount,
        }
        // console.log('this.dataArray',this.dataObjforExcel)

        this.dataArrayforExcel.push(this.dataObjforExcel);
        //  for export to excel END //
      }
      this.exportAsXLSXS();
    } else if (this.showTdsTabe) {
      // for (let i = 0; i < this.TDSDetail.length; i++) {
      //   this.dataObjforExcel = {
      //     'MONTH': this.TDSDetail[i].month,
      //     'COMAPNY': this.TDSDetail[i].name,
      //     'PAYMNET MODE': this.TDSDetail[i].patment_mode,
      //     'PAN NO': this.TDSDetail[i].panNo,
      //     'TRANSACTION DATE': this.TDSDetail[i].transaction_date,
      //     'AMOUNT CREDITED': this.TDSDetail[i].totalAmt,
      //     ' TDS RATE': this.TDSDetail[i].tdsrate,
      //     'TDS DEDUCTED': this.TDSDetail[i].tdsamount,
      //   }

      //   this.dataArrayforExcel.push(this.dataObjforExcel);
      // }
      // this.exportAsXLSXS();
      this.exportExcelCorpTDS();
    } else {
      // console.log('debug')
      // for (let i = 0; i <= this.GSTDetail.length; i++) {
      //   if(i < this.GSTDetail.length) {
      //   this.dataObjforExcel = {
      //     'INVOICE NUMBER': this.GSTDetail[i].invoice_number,
      //     'INVOICE DATE': this.GSTDetail[i].invoice_date,
      //     'HSN CODE': this.GSTDetail[i].hsn_code,
      //     'COMPANY/NAME': this.GSTDetail[i].name,
      //     'GST ID': this.GSTDetail[i].gstid,
      //     'PACKAGE NAME': this.GSTDetail[i].package_name,
      //     'PAYMENT MODE': this.GSTDetail[i].payment_mode,
      //     'BILLING PERIOD TO': this.GSTDetail[i].to_date,
      //     'BILLING PERIOD FROM': this.GSTDetail[i].from_date,
      //     'TAXABLE VALUE': this.GSTDetail[i].taxable_vale,
      //     'CGST': this.GSTDetail[i].cgst,
      //     'SGST': this.GSTDetail[i].sgst,
      //     'IGST': this.GSTDetail[i].igst,
      //     'TOTAL TAX': this.GSTDetail[i].total_tax,
      //     'TOTAL INVOICE VALUE': this.GSTDetail[i].total_invoice_value,

      //     }
      //   }

      //   if(i == this.GSTDetail.length) {
      //     this.dataObjforExcel = {
      //       'INVOICE NUMBER': '',
      //       'INVOICE DATE': '',
      //       'HSN CODE': '',
      //       'COMPANY/NAME':'',
      //       'GST ID': '',
      //       'PACKAGE NAME': '',
      //       'PAYMENT MODE': '',
      //       'BILLING PERIOD TO': '',
      //       'BILLING PERIOD FROM': '',
      //       'TAXABLE VALUE': this.total.Total_taxable_vale,
      //       'CGST': this.total.Total_cgst,
      //       'SGST': this.total.Total_sgst,
      //       'IGST': this.total.Total_igst,
      //       'TOTAL TAX': this.total.Total_total_tax,
      //       'TOTAL INVOICE VALUE': this.total.Total_total_invoice_value,

      //     }
      //   }
      //   this.dataArrayforExcel.push(this.dataObjforExcel);      
      // } 
      // this.exportAsXLSXS();
      this.exportExcelCorpGST();
    }
  }

  exportAsXLSXS(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Activity User Details');
  }

  isDesc: any = [];
  column: any = [];


  sortTds(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.TDSDetail.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  sortGST(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.GSTDetail.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.activityDetail.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }


  fetchActivityDetail(flag, startDate, endDate, name) {
    this.spinner.show()
    if (!window.navigator.onLine) {
      this.spinner.hide();
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        FrmDate: startDate,
        ToDate: endDate,
        flag: flag,
        report: name
      };
      this.dashboard.getDashboardActivityDetails(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            // console.log('res', res);
            try {
              if (name == 'ReportTDS') {
                this.TDSDetail = res.data[0];
                this.total = res.data[1][0];
                console.log('this.TDSDetail', this.TDSDetail);
                console.log('whole', this.total);


              } else if (name == 'ReportGST') {
                this.GSTDetail = res.data[0];
                this.total = res.data[1][0]
                console.log('this.GSTDetail', this.GSTDetail);
                console.log('whole', this.total);

              } else {
                let templateVisitAll: any = res.data[0];
                if (res.data[1]) {
                  for (let i = 0; i < res.data[1].length; i++) {
                    templateVisitAll.push(res.data[1][i])
                  }
                }
                this.activityDetail = templateVisitAll;
                console.log('this.activityDetail', res);
                console.log('this.activityDetail', this.activityDetail);
                this.cdf.detectChanges();
              }
              this.cdf.detectChanges();
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
    }
  }

}

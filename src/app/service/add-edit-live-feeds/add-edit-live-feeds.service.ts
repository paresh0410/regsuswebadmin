import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { webApi } from '../../../config';

@Injectable({
  providedIn: 'root'
})
export class AddEditLiveFeedsService {

  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addeditLiveFeedsUrl = this.baseUrl + webApi.apiUrl.addEditLiveFeed;
  getDynamiclink = this.baseUrl + webApi.apiUrl.getDynamiclink;

  constructor(public http: HttpClient) { }
  public addeditLiveFeed(liveFeedData: any): Promise<any> {

    return this.http.post(this.addeditLiveFeedsUrl, liveFeedData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public getNewsOpinionDynamicLink(nwopData: any): Promise<any> {

    return this.http.post(this.getDynamiclink, nwopData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

import { TestBed } from '@angular/core/testing';

import { SearchNewsOpinionsService } from './search-news-opinions.service';

describe('SearchNewsOpinionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchNewsOpinionsService = TestBed.get(SearchNewsOpinionsService);
    expect(service).toBeTruthy();
  });
});

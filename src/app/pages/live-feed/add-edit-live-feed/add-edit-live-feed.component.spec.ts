import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditLiveFeedComponent } from './add-edit-live-feed.component';

describe('AddEditLiveFeedComponent', () => {
  let component: AddEditLiveFeedComponent;
  let fixture: ComponentFixture<AddEditLiveFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditLiveFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditLiveFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

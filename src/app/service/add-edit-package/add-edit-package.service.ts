import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class AddEditPackageService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addeditPackageUrl = this.baseUrl + webApi.apiUrl.addEditPackages_new;

  constructor(public http: HttpClient) { }

  public addeditPackage(PackageData: any): Promise<any> {

    return this.http.post(this.addeditPackageUrl, PackageData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

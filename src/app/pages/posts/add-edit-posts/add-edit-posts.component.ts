import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { DatePipe } from '@angular/common';
import { UploadNewsOpinionsImageService } from '../../../service/upload-news-opinions-image/upload-news-opinions-image.service';
import { AddEditNewsOpinionsService } from '../../../service/add-edit-news-opinions/add-edit-news-opinions.service';
import { BrowserInfoService } from '../../../service/browser-info.service';
import { webApi } from '../../../../config';
import { Select2OptionData } from 'ng-select2';
import { NbDialogService } from '@nebular/theme';
import { ImageLibraryComponent } from '../../image-library/image-library.component';
import { ImageLibraryPopupComponent } from '../../image-library-popup/image-library-popup.component';
import { CommonService } from '../../../service/common.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import ImageInsert from '@ckeditor/ckeditor5-image/src/imageinsert';
// import { CKEditorComponent } from 'ng2-ckeditor';


// import * as jquery from 'jquery';
// var $: any
@Component({
  selector: 'ngx-add-edit-posts',
  templateUrl: './add-edit-posts.component.html',
  styleUrls: ['./add-edit-posts.component.scss']
})
export class AddEditPostsComponent implements OnInit {

  progress: number = 0;
  registerForm: FormGroup;
  submitted = false;
  selectRole: any;
  editUserData: any = [];
  userData: any;
  bowserInfo: any;
  categoryType: any = [];
  userStatus: any;
  userFlag: any;
  dropdownList: any = [];
  selectedItems: any = [];
  dropdownSettings: any = {
    singleSelection: false,
    idField: 'interestId',
    textField: 'intersetName',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };
  nameVal: any = new RegExp('^[A-Za-z -]+$');
  spaceval: any = new RegExp(/^\S*$/);
  disableCompany: any = false;
  public name1: String = 'Choose image or video';
  public str1: String = '';
  message: string;
  public imagePath;
  public filePath;
  imgURL: any;
  minDate: any = new Date();
  isImg: any;
  isFile: any;
  PostData: any = [];
  isCheck: string;
  catData: any = [];
  categories: any = [];
  categoryList: any = [];
  authorList: any = [];
  splitArray: any = [];
  test: any;
  not: any;
  emailnot: any;
  nsopImage: any;
  multipleurl: any = [];
  getMultipleurl: any = [];
  splitMultiIm: any = [];
  geturl = webApi.baseUrl + webApi.apiUrl.getNewsOpinionImage;
  isSpel: any;
  notispe: any;
  emailnotispe: any;
  // image preview
  displayVideo: boolean;
  displayImage: boolean;
  videoType: any;
  errorImsg: boolean = false;
  errorVmsg: boolean = false;
  DynamicLink: any;
  imgFlag: any;
  textBg: any
  imgBg: any
  selectedImgDetails: any;
  fruit: any
  public exampleData: Array<Select2OptionData>;
  public options: any;
  // public options: Select2Options;

  public _value: string[] = [];
  public current: string;

  isChecked = false;

  toggleSwitch() {
    this.isChecked = !this.isChecked;
  }

  uploadData: any = [
    {
      id: 1, label: 'Home Image', btnText: 'Browse', multiImgs: false, show: true, class: 'align-items-center', isError: false, data: []
    },
    {
      id: 2, label: 'Upload Video Thumbnail', btnText: 'Browse', multiImgs: false, show: false, class: 'align-items-center', isError: false, data: []
    },
    // {
    //   id: 2, label: 'Data Explorer Image', btnText: 'Browse', multiImgs: false, show: false, class: 'align-items-baseline', data: []
    //   // [{ id: 1, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image' }]
    // },
    {
      id: 3, label: 'Multiple Image', btnText: 'Browse', multiImgs: true, show: true, class: 'align-items-baseline', isError: false, data:
        [
          // { id: 1, imgUrl: '../../../../assets/images/Blue_bg.jpg', type: 'Image' },
          // { id: 2, imgUrl: '../../../../assets/images/Blue_bg.jpg', type: 'Image' },
          // { id: 3, imgUrl: '../../../../assets/images/Blue_bg.jpg', type: 'Excel' },
        ]
    },
  ]
  interestddlList: any = []
  public tagsddlList: Array<Select2OptionData>
  tagsBindList: any = []
  selectedTagsList: any = []
  imgUrl = webApi.baseUrl + webApi.apiUrl.getImg
  imageplaceholder: any = 'assets/images/IamgePlaceholder.png'
  public Editor = ClassicEditor;

  descriptionEditor: any = 'test'
  public config = {
    toolbar: ['heading', '|',
      'fontfamily', 'fontsize',
      'alignment',
      'fontColor', 'fontBackgroundColor', '|',
      'bold', 'italic', 'custombutton', 'strikethrough', 'underline', 'subscript', 'superscript', '|',
      'link', '|',
      'outdent', 'indent', '|',
      'bulletedList', 'numberedList', '|',
      'code', 'codeBlock', '|',
      'insertTable', '|',
      'imageUpload', 'onlyInsertImage', '|', 'blockQuote', '|',
      'undo', 'redo', '|',
      'youtube',
      'mediaEmbed'
    ],
    // plugins: [OnlyInsertImage]
  }
  mycontent: string;
  // @ViewChild('myckeditor') ckeditor: CKEditorComponent;
  ckeConfig: CKEDITOR.config;
  public minPublishDate = new Date()
  editorData: any = ''
  dataAction: any
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private passSer: PassServService,
    private datePipe: DatePipe,
    private uploadImgSer: UploadNewsOpinionsImageService,
    private addEditNwOpSer: AddEditNewsOpinionsService,

    private browserInfoService: BrowserInfoService,
    private dialogService: NbDialogService,
    private commonService: CommonService
  ) {
    this.ckeConfig = {
      allowedContent: false,
      extraPlugins: 'divarea',
      forcePasteAsPlainText: true,
      removePlugins: 'exportpdf',
      height: 100
    };
    this.ckeConfig.easyimage_styles = {
      full: {
        // Changes just the class name, icon label remains unchanged.
        attributes: {
          'class': 'my-custom-full-class'
        }
      },
    }

    this.ckeConfig.image_previewText = ''
    console.log('this.ckeConfig', this.ckeConfig)
    this.PostData = this.passSer.editPost;

    this.imgFlag = this.passSer.imageflag;
    this.dataAction = this.passSer.editAction
    console.log("this.PostData", this.PostData);
    // this.categoryList = this.passSer.CategoryList;
    // this.authorList = this.passSer.AuthorList;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();

    // this.categoryType = [
    //   {
    //     categoryId: 1,
    //     categoryName: 'News',
    //   },
    //   {
    //     categoryId: 2,
    //     categoryName: 'Opinion',
    //   },
    // ];

    this.interestddlList = this.passSer.interestList;
    for (let i = 0; i < this.interestddlList.length; i++) {
      if (this.PostData.length > 0) {
        this.selectedItems = this.PostData[0].intersetLit;
        for (let i = 0; i < this.selectedItems.length; i++) {
          this.selectedItems[i].interestId = parseInt(this.selectedItems[i].interestId);
        }
      }
    }


  }


  ngOnInit() {
    // jquery('select').select2();    
    // this.value = ['1', '2'];
    this.options = {
      multiple: true,
      tags: true
    }
    // this.current = this._value.join(' | ');
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
    }, 1000);
    this.DynamicLink = null;

    this.registerForm = this.formBuilder.group({
      pname: ['', [Validators.required]],
      // descri: ['', [Validators.required]],
      catTy: ['', [Validators.required]],
      catg: ['', [Validators.required]],
      tags: [''],
      authN: ['', [Validators.required]],
      publishDt: ['', [Validators.required]],
      url: ['', [Validators.pattern(/^\S*$/)]],
      comment: [''],
      status: ['1', [Validators.required]],
      textBg: [''],
      imgBg: [''],
      highL: [''],
      noti: [''],
      emailNoti: [''],
      premium: [0],
      // homeImage: ['']

      // image:   ['', [Validators.required, requiredFileType('png')]]],
    });
    this.getDdlList()


    // this.registerForm.controls.homeImage.setValue()
  }
  get f() {
    return this.registerForm.controls;
  }
  get value(): string[] {
    return this._value;
  }
  set value(value: string[]) {
    if (value && value.length > 0) {
      let lastElement = value[value.length - 1];
      if (Number(lastElement)) {
        console.log('number', lastElement)
      } else {
        console.log('text', lastElement)
        const obj: any = {
          id: '0',
          text: lastElement
        }
        this.tagsddlList.push(obj)
      }
      this._value = value;
      console.log('Set value: ' + value, this._value);
    } else {
      this._value = []
    }
  }
  editedData() {
    if (this.PostData[0]) {
      this.test = this.PostData[0].isSpecial // === 2 ? 0 : this.PostData[0].isSpecial;
      this.not = this.PostData[0].isNotify // === 2 ? 0 : this.PostData[0].isNotify;
      if (this.PostData[0].isEmail == null) {
        this.emailnot = 0;

      } else if (this.PostData[0].isEmail == 2) {
        this.emailnot = 0
      } else {
        this.emailnot = this.PostData[0].isEmail

      }
      if (this.PostData[0].tag) {
        const tags = this.PostData[0].tag.split(',')
        tags.forEach((el: any) => {
          this._value.push(el.split('|')[0])
          // tags.find(a => a).split('|')[0]
        });
        this.value = this._value
        console.log(tags, this._value)
      }


      if (this.PostData[0].neOpinimage) {
        // this.uploadData[0].data[0].imgUrl = this.PostData[0].neOpinimage
        const obj = {
          fileurl: this.PostData[0].neOpinimage,
          filetypeid: this.PostData[0].neOpinimage.split('.')[1] == 'mp4' ? 5 : 4
          // type: this.PostData[0].mimeType
        }
        this.uploadData[0].data.push(obj)
        if (obj.filetypeid == 5) {
          this.uploadData[1].show = true
        }
        console.log(this.uploadData)
      }
      if (this.PostData[0].videoThumbnail) {
        const obj = {
          fileurl: this.PostData[0].videoThumbnail,
          filetypeid: 4
          // type: this.PostData[0].mimeType
        }
        this.uploadData[1].data.push(obj)

      }
      if (this.PostData[0].neOpImages) {
        const arrimages = this.PostData[0].neOpImages.split('|')
        const objArr = []
        arrimages.forEach((el: any) => {
          var obj = {
            fileurl: el,
            filetypeid: 4
          }
          objArr.push(obj)
        });

        this.uploadData[2].data = objArr
      }

      // if (this.PostData[0].mainTypeName == 'Data') {
      //   this.uploadData[1].show = true
      // } else {
      //   this.uploadData[1].show = false
      // }


    } else {
      this.test = 0;
      this.not = 0;
      this.emailnot = 0;
    }
    console.log('this.test', this.test);
    if (this.PostData[0]) {
      if (this.PostData[0].neOpinimage) {
        this.imgURL = this.PostData[0].neOpinimage;
        this.splitArray = this.PostData[0].neOpinimage.split('path=');
        this.nsopImage = this.splitArray[this.splitArray.length - 1];
        if (this.imgFlag == 1) {
          // this.imagePath = this.PostData[0].neOpinimage;
          if (this.nsopImage == "null") {
            this.removeImage();
          }
        }
      }


      // multiple images
      this.splitMultiIm = this.PostData[0].neOpImages ? this.PostData[0].neOpImages.split('|') : this.PostData[0].neOpImages;
      if (this.splitMultiIm) {
        for (let i = 0; this.splitMultiIm.length - 1 > i; i++) {
          // let checkUrl = this.splitArray[i].split('path=');
          this.getMultipleurl.push({ name: this.splitMultiIm[i] });
          console.log(this.splitMultiIm)
          // if(checkUrl[0] == this.geturl) {
          //   this.getMultipleurl.push({name: this.splitMultiIm[i] })
          // } else {
          //   this.getMultipleurl.push({ name: this.geturl + this.splitMultiIm[i] });
          // }
        }
      }

      this.registerForm = this.formBuilder.group({
        pname: [this.PostData[0].neOpinName, [Validators.required]],
        // descri: [this.PostData[0].neOpindesc, [Validators.required]],
        catTy: [this.PostData[0].mainType, [Validators.required]],
        catg: [this.PostData[0].categoryId, [Validators.required]],
        tags: [this._value],
        authN: [this.PostData[0].authorid, [Validators.required]],
        publishDt: [new Date(this.PostData[0].publishDate), [Validators.required]],
        // url: [this.PostData[0].neOpinURL, [Validators.pattern(/^\S*$/)]],
        comment: [this.PostData[0].neOpincomment],
        status: [this.PostData[0].isActive, [Validators.required]],
        textBg: [this.PostData[0].textBackground],
        imgBg: [this.PostData[0].imageBackground],
        highL: [this.PostData[0].isSpecial],
        noti: [this.PostData[0].isNotify],
        emailNoti: [this.emailnot],
        premium: [this.PostData[0].ispremium == 1 ? 1 : 0],
        // homeImage: ['']
      });

      this.editorData = this.PostData[0].neOpindesc
      // this.registerForm.controls.descri.setValue(this.PostData[0].neOpindesc)
      this.minPublishDate = new Date(this.PostData[0].publishDate)
      this.textBg = this.PostData[0].textBackground
      this.imgBg = this.PostData[0].imageBackground

      if (this.PostData[0].mimeType) {
        this.videoType = this.PostData[0].mimeType;
        // console.log(' this.videoType', this.videoType);
        const typeMime = this.videoType.split('/');
        if (typeMime[0] === 'video') {
          this.displayVideo = true;
        } else {
          this.displayImage = true;
        }
      }

      if (!this.dataAction) {
        this.registerForm.controls.publishDt.setValue(null)
        this.registerForm.controls.status.setValue(2)
      }

      this.minDate = new Date(this.PostData[0].publishDate);
      const content = {
        event: 'new and opinion',
        id: this.PostData[0].newsOpinionId,
        secid: this.PostData[0].mainType
      };
      this.addEditNwOpSer.getNewsOpinionDynamicLink(content).then(res => {
        this.spinner.hide();
        try {
          console.log('Dynamic Link Res', res);
          this.DynamicLink = res.data.shortLink;
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          this.spinner.hide();
          console.log('err', err);
          this.toastr.error('Server Error');
        });
    }
  }

  changed(data: { value: string[] }) {
    console.log('changed')
    if (data.value) {
      this.current = data.value.join(' | ');
    }

  }
  // category selection
  onItemSelect(item) {
    // console.log('select', this.selectedItems);
    this.isCheck = '';
  }
  onSelectAll(items) {
    // console.log(items);
    if (this.selectedItems.length === this.interestddlList.length) {
      this.selectedItems = [];
      this.isCheck = 'Please select interests';
    } else {
      this.selectedItems = items;
      this.isCheck = '';
    }
    console.log('selectAll', this.selectedItems);
  }
  onItemDeSelect(des) {
    for (let i = 0; i < this.selectedItems.length; i++) {
      if (des.interestId === this.selectedItems[i].interestId) {
        this.selectedItems.splice(i, 1);
      }
    }
    if (this.selectedItems.length === 0) {
      this.isCheck = 'Please select interests';
    }
    console.log('deselect', this.selectedItems);
  }


  preview(files) {
    this.name1 = this.str1;
    const str11: any[] = this.name1.split('\\');
    this.name1 = str11[str11.length - 1];
    this.videoType = files[0].type;
    if (files.length === 0) {
      return;
    }
    // console.log('this.imagePath', files);
    // console.log('this.imagePath', files[0].type);
    //this.mimeTypeData = mimeType;

    const mimeType = files[0].type;
    var res = mimeType.split('/');
    var size = files[0].size;
    // const fileSize = files[0].size > 5266468? this.errormsg = true : this.displayImage = true;

    console.log('this.imagePath', files);
    // console.log('mimeName', res[0]);
    // console.log('size', size);

    if (res[0] === 'video' || res[0] === 'image') {
      this.message = '';
      const reader = new FileReader();
      this.imagePath = files;

      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.imgURL = reader.result;

        if (res[0] === 'video') {
          // this.displayVideo = true;
          if (size < 52621162 && size > 1266468) {
            this.errorImsg = false;

            this.errorVmsg = false;
            this.displayVideo = true;

          } else {
            this.errorVmsg = true;
            this.imgURL = '';
            this.message = "size alowed 1mb to 15mb"

            // this.errorVmsg = false;
            // this.displayVideo = true;
          }
        } else {

          if (res[1] == "jpg" || res[1] == "jpeg" || res[1] == "png") {
            if (size < 5266468) {
              this.errorVmsg = false;

              this.errorImsg = false;
              this.displayImage = true;

            } else {
              this.errorImsg = true;
              this.imgURL = '';
              this.message = "Image size alowed  upto 5mb"

            }
          } else {
            // alert("Only jpg/jpeg, png files are allowed!");
            this.message = "Only jpg/jpeg, png files are allowed!"
            this.errorImsg = true;
            this.imgURL = '';
          }


        }
        console.log(' this.displayVideo', this.displayVideo);
        console.log(' this.displayImage', this.displayImage);
      };
      this.isImg = '';

    } else {
      this.isImg = '';
      this.name1 = 'Choose image or video';
      this.message = 'Only images or videoare supported.';
      return;
    }
  }
  removeImage() {
    this.str1 = '';
    this.name1 = 'Choose Image or video';
    this.imgURL = '';
    this.displayVideo = false;
    this.displayImage = false;
    this.nsopImage = null;
    this.videoType = null;
  }
  multipleImage(event) {
    if (event.target.files.length > 10) {
      this.toastr.error('More than 10 image is not allowed at once');
      return;
    }
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        this.multipleurl.push(event.target.files[i]);
        console.log("this.multipleurl", this.multipleurl);
      }
    }
  }
  back() {
    this.router.navigate(['pages/posts']);
  }

  onSubmit() {
    const tagslist = this._value
    this.tagsBindList = []
    for (let i = 0; i < tagslist.length; i++) {
      if (Number(tagslist[i])) {
        const a = this.tagsddlList.find(a => a.id == tagslist[i])
        console.log(a)
        this.tagsBindList.push(a)
        console.log('number', tagslist[i])
      } else {
        console.log('text', tagslist[i])
        const obj = {
          id: '0',
          text: tagslist[i]
        }
        this.tagsBindList.push(obj)
      }
    }

    const tagIds = this.tagsBindList.map(a => a.id)
    const tagTexts = this.tagsBindList.map(a => a.text)


    this.submitted = true;
    let formData = new FormData();
    let formDatamulti = new FormData();

    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.selectedItems.length === 0) {
      this.isCheck = 'Please select interests';
      return;
    }
    if (this.errorImsg || this.errorVmsg) {
      return;
    }
    if (this.errorImsg || this.errorVmsg) {
      return;
    }

    if (this.uploadData[0].data.length > 0) {
      if (this.uploadData[0].data[0].filetypeid == 5) {
        if (this.uploadData[1].data.length > 0) {
          this.uploadData[1].isError = false
        } else {
          this.uploadData[1].isError = true
          return;
        }
      }
    }

    // if (!this.imgURL) {
    //   this.isImg = 'Please select image';
    //   return;
    // }
    if (this.registerForm.invalid) {
      return;
    } else {
      const intrId = [];
      const intrName = [];
      for (let i = 0; i < this.selectedItems.length; i++) {
        intrId.push(this.selectedItems[i].interestId);
        intrName.push(this.selectedItems[i].intersetName);
      }
      if (this.registerForm.value.highL === true || this.test === 1) {
        this.isSpel = 1;
        if (this.isSpel = 1) {
          // if (!this.imgURL) {
          //   this.toastr.error('image mandatory');
          //   this.isImg = 'Please select image';
          //   return;
          // }
          if (this.uploadData[0].data.length == 0) {
            this.toastr.error('Home image mandatory');
            this.isImg = 'Please select image';
            return;
          }
        }
      } else {
        this.isSpel = 2;
      }
      if (this.registerForm.value.noti === true || this.not === 1) {
        this.notispe = 1;
      } else {
        this.notispe = 2;
      }

      if (this.registerForm.value.emailNoti === true || this.emailnot === 1) {
        this.emailnotispe = 1;
      } else {
        this.emailnotispe = 2;
      }

      const mulitimages = ''
      if (this.uploadData[0].data.length == 0) {
        // this.toastr.error('image mandatory');

        // this.isImg = 'Please select image';
        // return
      }

      const content = {
        neopId: 0,
        neonName: this.registerForm.value.pname,
        mainT: this.registerForm.value.catTy,
        pDate: this.formattime(this.registerForm.value.publishDt),
        authId: this.registerForm.value.authN,
        catId: this.registerForm.value.catg,
        // neopdes: this.registerForm.value.descri,
        neopdes: this.editorData,
        neopComm: this.registerForm.value.comment,
        // neopUrl: this.registerForm.value.url,
        // neopImg: this.imagePath ? null : this.nsopImage,
        neopImg: this.uploadData[0].data.length > 0 ? this.uploadData[0].data[0].fileurl : null,
        Inters: intrId.join('|'),
        Interslen: intrId.length,
        isAct: this.registerForm.value.status == 1 ? 1 : 2,
        mimeT: this.videoType ? this.videoType : null,
        //isSpe : this.registerForm.value.highL,
        isSpe: this.isSpel,
        notification: this.notispe,
        isEmail: this.emailnotispe,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
        multiImage: this.uploadData[2].data.map(a => a.fileurl).join('|'),
        isPremi: this.registerForm.value.premium == 1 ? 1 : 2,
        textBrg: this.textBg ? this.textBg : null,
        imgBrg: this.imgBg ? this.imgBg : null,
        dataExpimage: null,
        // dataExpimage: this.uploadData[1].data.length > 0 ? this.uploadData[1].data[0].fileurl : null,
        tag: tagIds.join('|'),
        taglen: tagslist.length,
        tagname: tagTexts.join('|'),
        videothumbnail: this.uploadData[1].data.length > 0 ? this.uploadData[1].data[0].fileurl : null
      }

      if (this.PostData[0]) {
        if (this.dataAction) {
          if (this.PostData[0].newsOpinionId) {
            content.neopId = this.PostData[0].newsOpinionId;
          }
        } else {
          content.neopId = 0;
          // content.isAct = 2
          // content.pDate = this.formattime(new Date())
          // content.pDate = null
        }
      }
      // if (this.imagePath) {
      //   formData.append('file', this.imagePath[0], this.imagePath[0].name);
      // }
      // if (this.multipleurl.length > 0) {
      //   for (let i = 0; i < this.multipleurl.length; i++) {
      //     formDatamulti.append('file', this.multipleurl[i], this.multipleurl[i].name);
      //   }
      // }
      console.log('formDatamulti', formDatamulti);
      console.log('content', content);
      this.spinner.show();

      this.addEditNwOpSer.addEditNewsOpinion(content).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            // console.log('res', res);
            try {
              if (res.data) {
                if (res.data.error) {
                  this.toastr.error(res.data.error);
                } else if (res.data.updated) {
                  this.router.navigate(['pages/posts']);
                  this.toastr.success(res.data.updated);
                } else if (res.data.saved) {
                  this.router.navigate(['pages/posts']);
                  this.toastr.success(res.data.saved);
                }
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
      // service for uploading category images
      // if (this.imagePath) {
      // this.uploadImgSer.uploadNewsOpinionImg(formData).then(res => {
      //   try {
      //     if (res.error_code === 0) {
      //       content.neopImg = res.path;
      //       // multiple image upload
      //       if (this.multipleurl.length > 0) {
      //         this.uploadImgSer.uploadMultiImg(formDatamulti).then(res => {
      //           this.spinner.hide();
      //           try {
      //             console.log('multiImage res', res);
      //             if (res.error_code === 0) {
      //               content.multiImage = res.multipath.join("|");
      //               // service for add edit category

      //             }
      //             if (res.error_code === 1) {
      //               this.spinner.hide();
      //               this.toastr.error(res.err_desc);
      //             }
      //           } catch (e) {
      //             console.log(e);
      //           }
      //         },
      //           err => {
      //             console.log(err);
      //             this.spinner.hide();
      //             this.toastr.error('Server Error');
      //           });
      //       }
      //       else {
      //         var stale = [];
      //         if (this.getMultipleurl.length > 0) {
      //           for (var i = 0; this.getMultipleurl.length > i; i++) {
      //             stale.push(this.getMultipleurl[i].name);
      //           }
      //           // content.neopImg = stale.join("|");
      //           content.multiImage = stale.join("|");
      //         }
      //         // this.addEditNwOpSer.addEditNewsOpinion(content).then(res => {
      //         //   this.spinner.hide();
      //         //   try {
      //         //     if (res.type === true && res.status_code === 200) {
      //         //       // console.log('res', res);
      //         //       try {
      //         //         if (res.data) {
      //         //           if (res.data.error) {
      //         //             this.toastr.error(res.data.error);
      //         //           } else if (res.data.updated) {
      //         //             this.router.navigate(['pages/posts']);
      //         //             this.toastr.success(res.data.updated);
      //         //           } else if (res.data.saved) {
      //         //             this.router.navigate(['pages/posts']);
      //         //             this.toastr.success(res.data.saved);
      //         //           }
      //         //         }
      //         //       } catch (e) {
      //         //         console.log(e);
      //         //       }
      //         //     }
      //         //     if (res.type === false) {
      //         //       this.spinner.hide();
      //         //       console.log('res', res);
      //         //     }
      //         //   } catch (e) {
      //         //     console.log(e);
      //         //   }
      //         // },
      //         //   err => {
      //         //     console.log(err);
      //         //     this.spinner.hide();
      //         //     this.toastr.error('Server Error');
      //         //   });
      //       }
      //     }
      //     if (res.error_code === 1) {
      //       this.spinner.hide();
      //       console.log('res', res);
      //       this.toastr.error(res.err_desc);
      //     }
      //   } catch (e) {
      //     console.log(e);
      //   }
      // },
      //   err => {
      //     console.log(err);
      //     this.spinner.hide();
      //     this.toastr.error('Server Error');
      //   });
      // } else {
      // if (this.multipleurl.length > 0) {
      //   // content.multiImage = `/newsopinimage/ + ${this.multipleurl.name} + ${this.splitMultiIm.join("|")}`;
      //   // var hege = this.multipleurl;
      //   // for (let i = 0; i < this.multipleurl.length; i++) {
      //   //   // formDatamulti.append('file', this.multipleurl[i], this.multipleurl[i].name);
      //   //   var hege = this.multipleurl[i].name
      //   // }
      //   // var stale = this.splitMultiIm.join("|");
      //   // content.multiImage = hege.concat(stale);
      //   // console.log('hege images', hege);

      //   this.uploadImgSer.uploadMultiImg(formDatamulti).then(res => {
      //     this.spinner.hide();
      //     try {
      //       console.log('multiImage res', res);
      //       if (res.error_code === 0) {
      //         var path = '';
      //         var editImage = res.multipath.join("|");
      //         if (this.splitMultiIm) {
      //           var stale = this.splitMultiIm.join("|");
      //           path = editImage + '|' + stale;
      //         } else {
      //           path = editImage;
      //         }
      //         content.multiImage = path;
      //         console.log('editImage res', content.multiImage);

      //         // this.addEditNwOpSer.addEditNewsOpinion(content).then(res => {
      //         //   this.spinner.hide();
      //         //   try {
      //         //     if (res.type === true && res.status_code === 200) {
      //         //       // console.log('res', res);
      //         //       try {
      //         //         if (res.data) {
      //         //           if (res.data.error) {
      //         //             this.toastr.error(res.data.error);
      //         //           } else if (res.data.updated) {
      //         //             this.router.navigate(['pages/posts']);
      //         //             this.toastr.success(res.data.updated);
      //         //           } else if (res.data.saved) {
      //         //             this.router.navigate(['pages/posts']);
      //         //             this.toastr.success(res.data.saved);
      //         //           }
      //         //         }
      //         //       } catch (e) {
      //         //         console.log(e);
      //         //       }
      //         //     }
      //         //     if (res.type === false) {
      //         //       this.spinner.hide();
      //         //       console.log('res', res);
      //         //     }
      //         //   } catch (e) {
      //         //     console.log(e);
      //         //   }
      //         // },
      //         //   err => {
      //         //     console.log(err);
      //         //     this.spinner.hide();
      //         //     this.toastr.error('Server Error');
      //         //   });
      //       }
      //       if (res.error_code === 1) {
      //         this.spinner.hide();
      //         this.toastr.error(res.err_desc);
      //       }
      //     } catch (e) {
      //       console.log(e);
      //     }
      //   },
      //     err => {
      //       console.log(err);
      //       this.spinner.hide();
      //       this.toastr.error('multi image error');
      //     });
      // } else {
      //   var stale = [];
      //   if (this.getMultipleurl.length > 0) {
      //     for (var i = 0; this.getMultipleurl.length > i; i++) {
      //       stale.push(this.getMultipleurl[i].name);
      //     }
      //     // content.neopImg = stale.join("|");
      //     content.multiImage = stale.join("|");
      //   }
      //   // this.addEditNwOpSer.addEditNewsOpinion(content).then(res => {
      //   //   this.spinner.hide();
      //   //   try {
      //   //     if (res.type === true && res.status_code === 200) {
      //   //       // console.log('res', res);
      //   //       try {
      //   //         if (res.data) {
      //   //           if (res.data.error) {
      //   //             this.toastr.error(res.data.error);
      //   //           } else if (res.data.updated) {
      //   //             this.router.navigate(['pages/posts']);
      //   //             this.toastr.success(res.data.updated);
      //   //           } else if (res.data.saved) {
      //   //             this.router.navigate(['pages/posts']);
      //   //             this.toastr.success(res.data.saved);
      //   //           }
      //   //         }
      //   //       } catch (e) {
      //   //         console.log(e);
      //   //       }
      //   //     }
      //   //     if (res.type === false) {
      //   //       this.spinner.hide();
      //   //       console.log('res', res);
      //   //     }
      //   //   } catch (e) {
      //   //     console.log(e);
      //   //   }
      //   // },
      //   //   err => {
      //   //     console.log(err);
      //   //     this.spinner.hide();
      //   //     this.toastr.error('Server Error');
      //   //   });
      // }
      // }
    }
  }


  checkVal(event) {
    // console.log('event', event);
    if (event.target.placeholder === 'News & Opinion Name') {
      if (!event.target.value) {
        this.toastr.error('News & Opinion Name is required');
      }
    }
    if (event.target.placeholder === 'Comment') {
      if (!event.target.value) {
        this.toastr.error('Comment is required');
      }
    }
    if (event.target.placeholder === 'Url') {
      if (!this.spaceval.test(event.target.value)) {
        this.toastr.error('Blank spaces are not allowed');
      }
    }
    if (event.target.placeholder === 'News & Opinion Description') {
      if (!event.target.value) {
        this.toastr.error('News & Opinion Description is required');
      }
    }
  }

  formattime(time) {
    var d = new Date(time),
      hr = '' + d.getHours(),
      mm = '' + d.getMinutes(),
      ss = '' + d.getSeconds(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (hr.length < 2) hr = '0' + hr;
    if (mm.length < 2) mm = '0' + mm;
    if (ss.length < 2) ss = '0' + ss;
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var da = [year, month, day].join('-');
    var ti = [hr, mm, ss].join(':');
    return [da, ti].join(' ');
  }

  special: boolean;
  onToggle(value) {
    console.log('highlight value', value);
    if (value === 1) {
      this.test = 0;
    } else if (value === 0) {
      this.test = 1;
    }

    console.log('this.test', this.test);
  }

  notification(value) {
    console.log('notification', value);
    if (value === 1) {
      this.not = 0;
    } else if (value === 0) {
      this.not = 1;
    }
    console.log('notification value', this.not);
  }

  notificationEmail(value) {
    console.log('email notification', value);
    if (value === 1) {
      this.emailnot = 2;
    } else if (value === 2) {
      this.emailnot = 1;
    }
    console.log('Email notification value', this.emailnot);
  }

  statusChange(event) {
    if (event.currentTarget.checked) {
      this.registerForm.controls.status.setValue(1)
    } else {
      this.registerForm.controls.status.setValue(0)
    }
    console.log(this.registerForm.controls.status.value)
  }

  premiumChange(event) {
    if (event.currentTarget.checked) {
      this.registerForm.controls.premium.setValue(1)
    } else {
      this.registerForm.controls.premium.setValue(0)
    }
    console.log(this.registerForm.controls.premium.value)
  }
  removeImgUrl(item, items) {
    console.log(item)
    console.log(items)
    if (!item.multiImgs) {
      item.data = []
    } else {
      const findIndex = item.data.findIndex(a => a.id === items.id)
      findIndex !== -1 && item.data.splice(findIndex, 1)
      console.log(item.data)
    }
  }

  copyText(textToCopy: any) {
    console.log(textToCopy)
    textToCopy.select();
    document.execCommand('copy');
    textToCopy.setSelectionRange(0, 0);
  }

  uploadImage(event) {
    console.log('uploadImage', event)
    const files = event.target.files;
    if (files.length === 0)
      return;
    const reader = new FileReader();
    this.selectedImgDetails = files[0]
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      // this.previewImg = reader.result;
    }
  }

  open(item): void {
    console.log('open popup')
    let dialogRef = this.dialogService.open(ImageLibraryPopupComponent, {
      context: {
        data: item
      },
      hasBackdrop: true,
      hasScroll: true
    })

    dialogRef.onClose.subscribe(result => {
      if (result) {
        if (item.id == 1) {
          this.uploadData[0].data = []
          this.uploadData[0].data = result[0]
          if (result[0][0].filetypeid == 5) {
            this.uploadData[0].class = 'align-items-center'
            this.uploadData[1].show = true
          } else {
            this.uploadData[1].show = false
          }
        } else if (item.id == 2) {
          this.uploadData[1].data = []
          this.uploadData[1].data = result[0]
          this.uploadData[1].isError = false
        } else {
          this.uploadData[2].data = []
          this.uploadData[2].data = result[0]
        }
      }

      console.log(result, 'The dialog was closed');
      console.log(this.uploadData, 'uploadData');
      // this.animal = result;
    });
  }

  closeEvent(event) {
    console.log(event)
  }

  getDdlList() {
    const url = webApi.baseUrl + webApi.apiUrl.newsopinionDropdown
    const param = {
      bname: this.commonService.bowserInfo.name,
      bversion: this.commonService.bowserInfo.version,
      uid: this.commonService.userData.userid,
      uname: this.commonService.userData.subname
    }
    this.commonService.httpPostRequest(url, param).then((res: any) => {
      console.log(res)
      try {
        if (res.type) {
          this.categoryType = res.section
          this.interestddlList = res.Interest
          this.categoryList = res.category
          this.tagsddlList = res.tags
          this.authorList = res.author
          this.editedData()
          this.spinner.hide();
        }
      } catch (error) {
        this.spinner.hide();
      }
    })
  }

  changeSection(event) {
    console.log(event)
    // const sectionname = this.categoryType.find(a => a.sectionId == event.target.value).sectionName
    // if (sectionname == 'Data') {
    //   this.uploadData[1].show = true
    // } else {
    //   this.uploadData[1].show = false
    // }
  }

}
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddParticipantsComponent } from './add-participants/add-participants.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CreateGroupComponent } from './create-group/create-group.component';
import { SubscribersComponent } from './subscribers/subscribers.component';
import { YearPickerComponent } from './year-select/year-picker.component';

// import { ChartBarComponent } from './chart-bar/chart-bar.component';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { NgxPaginationModule } from 'ngx-pagination'
import { NbTabsetModule, NbButtonModule, NbBadgeModule } from '@nebular/theme';
import { MonthPicker } from './month-picker/month-picker.component';
import { ImgpopupComponent } from './imgpopup/imgpopup.component';

@NgModule({
  declarations: [AddParticipantsComponent, CreateGroupComponent, SubscribersComponent, YearPickerComponent, MonthPicker, ImgpopupComponent],
  exports: [AddParticipantsComponent, CreateGroupComponent, SubscribersComponent, YearPickerComponent, MonthPicker],
  entryComponents: [AddParticipantsComponent, CreateGroupComponent, SubscribersComponent, YearPickerComponent, MonthPicker, ImgpopupComponent],

  imports: [
    CommonModule,
    AngularMultiSelectModule,
    FormsModule,
    NgMultiSelectDropDownModule,
    FilterPipeModule,
    NgxPaginationModule,
    NbTabsetModule, NbButtonModule, NbBadgeModule
  ],
  // schemas: [
  //   CUSTOM_ELEMENTS_SCHEMA,
  //   NO_ERRORS_SCHEMA,
  // ]
})
export class ComponentsModule { }

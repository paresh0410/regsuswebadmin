import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class SearchForumsService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  searchForumsUrl = this.baseUrl + webApi.apiUrl.searchForums;
  constructor(public http: HttpClient) { }
  public searchForums(srchFData: any): Promise<any> {

    return this.http.post(this.searchForumsUrl, srchFData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

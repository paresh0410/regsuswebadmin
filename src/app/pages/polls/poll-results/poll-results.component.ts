import { Component, OnInit } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PassServService } from '../../../service/pass-serv.service';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
// import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'ngx-poll-results',
  templateUrl: './poll-results.component.html',
  styleUrls: ['./poll-results.component.scss']
})
export class PollResultsComponent implements OnInit {
  // graphVisible: boolean = true;
  // Pie chart
  public pieChartOptions: ChartOptions = {
    // responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          // let labelValue: any = [];
          // // const label = ctx.chart.data.labels[ctx.dataIndex];
          // for (var i = 0; i < ctx.chart.data.datasets[0].data.length; i++) {
          //   labelValue.push(ctx.chart.data.datasets[0].data[i]);
          // }
          // console.log('lable', labelValue);
          // // return label;

          // let sum = 0;
          // for (var i = 0; i < ctx.chart.data.datasets[0].data.length; i++) {
          //   sum = sum + labelValue[i];
          // }
          // console.log('sum', sum);
        },
      },
    }
  }; 
  // public pieChartLabels: Label[] = [['Download', 'Sales'], ['In', 'Store', 'Sales'], 'Mail Sales'];
  // public pieChartData: SingleDataSet = [300, 500, 100];

  public pieChartLabels: Label[];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  // ===========================================================
  graphText = [];
  graphValue = [];
  graphPercentValue = [];
  sum: number = 0;

  view: any[] = [500, 400];
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Options';
  showYAxisLabel = true;
  yAxisLabel = '';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#76A7FA', '#a664c4'],
  };
  pollsData: any = [];
  title: any;
  optionDate: any = [];
  flag: any = true;
  constructor(
    private passSer: PassServService,
  ) {
    this.pollsData = this.passSer.editPoll;
    console.log('this.pollsData' , this.pollsData);
   
    this.flag = this.pollsData[1];
    console.log('this.flag',this.flag);
   
    this.title = this.pollsData[0] ? this.pollsData[0].pollText : '';
    this.optionDate = this.pollsData[0] ? this.pollsData[0].ChoiceGraph:  '';
    
    for (let i = 0; i < this.optionDate.length ; i++) {
      let pollObj = {
        // id : this.optionDate[i].pollchoiceId,
        // name : this.optionDate[i].choiceText,
        value : this.optionDate[i].choiseValues,
      }
      this.graphText.push(this.optionDate[i].choiceText);

      // if(this.optionDate[i].choiseValues > 0) { // 0 push in array
        this.graphValue.push(pollObj);
        // if (this.optionDate[i].choiseValues = 0) { }
      // }
    }

    for (let j = 0; j < this.graphValue.length; j++ ) {
      this.sum = this.sum + this.graphValue[j].value;
    }

    for (let k = 0; k < this.graphValue.length; k++ ) {
      let  tempValue: number = 0;
      tempValue = (this.graphValue[k].value / this.sum) * 100;
      let actulaValue = tempValue.toFixed(2);
      this.graphPercentValue.push(actulaValue);
    }

    this.pieChartLabels = this.graphText;
    this.pieChartData = this.graphPercentValue;
    console.log('this.pieChartLabels',this.pieChartLabels);
    console.log('this.pieChartData',this.pieChartData);
    
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit () { }

  // onSelect(event) {
  //   console.log(event);
  // }

  // chartOptions = {
  //   responsive: true
  // };

  // chartData = [
  //   { data: [330, 600, 260, 700], label: 'Account A' },
  //   { data: [120, 455, 100, 340], label: 'Account B' },
  //   { data: [45, 67, 800, 500], label: 'Account C' }
  // ];

  // chartLabels = ['January', 'February', 'Mars', 'April'];

  // onChartClick(event) {
  //   console.log(event);
  // }
  

  public captureScreen() {
    // only pdf download
    // var data = document.getElementById('contentToConvert');
    // html2canvas(data).then(canvas => {  
    //   // Few necessary setting options  
    //   var imgWidth = 208;   
    //   var pageHeight = 485;    
    //   var imgHeight = canvas.height * imgWidth / canvas.width;  
    //   var heightLeft = imgHeight;    
    //   const contentDataURL = canvas.toDataURL('image/png')  
    //   let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
    //   var position = 0;  
    //   pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
    //   pdf.save('Polls.pdf');  
    // });

    // only image download
    // var element = <HTMLElement>document.querySelector("#pollCanvasImg");
    var data = document.getElementById('pollCanvasImg');
    // data.setAttribute("style","width:500px");
    html2canvas(data).then(function(canvas) {
      // data.setAttribute("style","width:500px");
      // Convert the canvas to blob
      canvas.toBlob(function(blob){
        // To download directly on browser 
        let link = document.createElement("a");
        link.download = "Polls.png";
        link.href = URL.createObjectURL(blob);
        link.click();
        // To save manually somewhere in file explorer
        // window.saveAs(blob, 'image.png');
      },'image/png');
    });
  }

}

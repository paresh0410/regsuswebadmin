import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BroadcastService {
  broadcastData: any
  typeList: any = [
    { id: 1, name: 'News' },
    { id: 2, name: 'Offers' },
  ]
  recipentList: any = [
    { id: 1, name: 'All' },
    { id: 2, name: 'Subscribers' },
    { id: 3, name: 'Non-Subscribers' },
  ]
  constructor() { }
}

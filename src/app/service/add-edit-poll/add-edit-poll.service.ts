import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class AddEditPollService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addEditPollUrl = this.baseUrl + webApi.apiUrl.addEditPoll;
  constructor(public http: HttpClient) { }
  public addEditPolls(pollData: any): Promise<any> {

    return this.http.post(this.addEditPollUrl, pollData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

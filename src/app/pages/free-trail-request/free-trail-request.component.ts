import { Component, HostListener, OnInit } from '@angular/core';
import { Select2OptionData } from 'ng-select2';


@Component({
  selector: 'ngx-free-trail-request',
  templateUrl: './free-trail-request.component.html',
  styleUrls: ['./free-trail-request.component.scss'],
  

})
export class FreeTrailRequestComponent implements OnInit {

  isPopupVisible: boolean = false;

  tableList: any = [
    {
      name:'rohit',
      email:'rohit12@gmail.com',
      mobile_number:98877789,
      statusId:'1'
    },
    {
      name:'rohit',
      email:'rohit12@gmail.com',
      mobile_number:98877789,
      statusId:'2'
    },
    {
      name:'rohit',
      email:'rohit12@gmail.com',
      mobile_number:98877789,
      statusId:'1'
    }
  ];

  
  
  search = {
    name: '',
    email: '',
    mobile_number: '',
    status:''
    // accept_bt: '',
    // reject_bt:''
  };

  setveractive: boolean = false;

  public tagsddlList: Array<Select2OptionData>

  public options: any;

  public current: string;

  show_accept_data:boolean=false;

  constructor() { }

  ngOnInit() {
    console.log(this.tableList);
  }

  openAcceptPopup() {
    this.isPopupVisible = true;
    this.show_accept_data = true;
  }

  openRejectPopup(){
    this.isPopupVisible = true;
    this.show_accept_data = false;
  }

  // onPopupContentClick(event: Event) {
  //   this.isPopupVisible = false;
  //   event.stopPropagation();
    
  // }

  closePopup() {
    this.isPopupVisible = false;
  }

  save(){
    this.isPopupVisible = false;
  }

  // @HostListener('document:click', ['$event'])
  // onDocumentClick(event: Event): void {
  //   if (this.isPopupVisible) {
  //     this.isPopupVisible = false;
  //   }
  // }
  
  changed(data: { value: string[] }) {
    console.log('changed')
    if (data.value) {
      this.current = data.value.join(' | ');
    }

  }
  

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditFeaturesComponent } from './add-edit-features.component';

describe('AddEditFeaturesComponent', () => {
  let component: AddEditFeaturesComponent;
  let fixture: ComponentFixture<AddEditFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

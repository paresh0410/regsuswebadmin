import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class FetchPromotionService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchPromotionUrl = this.baseUrl + webApi.apiUrl.fetchPromotion;
  
  constructor(public http: HttpClient) { }

  public fetchPromotion(data : any): Promise<any> {
    return this.http.post(this.fetchPromotionUrl, data, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { FetchSubscribersService } from '../../../service/fetch-subscribers/fetch-subscribers.service';
import { AddSubscribersService } from '../../../service/add-subscribers/add-subscribers.service';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { ExcelService } from '../../../service/excel.service';
import { UploadSubscriberDealsService } from '../../../service/upload-subcriber-deals/upload-subscriber-deals.service';
import { BrowserInfoService } from '../../../service/browser-info.service';

@Component({
  selector: 'ngx-subscriber-validation',
  templateUrl: './subscriber-validation.component.html',
  styleUrls: ['./subscriber-validation.component.scss']
})
export class SubscriberValidationComponent implements OnInit {
  validdata: any = [];
  invaliddata: any = [];
  showResult: boolean = false;
  resultData: any = [];
  showUpload: any = false;
  allData: any = [];
  featuresList: any = [];
  fIds: any = [];
  Durid: any;
  trkId: any;
  validUserDate: any = [];
  compDetails: any = [];
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  validcount: any = [];
  invalidcount: any = [];
  dontupload: any = false;
  hidePopup: any = false;
  isback: any = false;
  userData: any;
  bowserInfo: any;
  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private fetchSer: FetchSubscribersService,
    private addSubSer: AddSubscribersService,
    private toastr: ToastrService,
    private passSer: PassServService,
    private excelService: ExcelService,
    private uploadPenDealSer: UploadSubscriberDealsService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.compDetails = this.passSer.editCorp;
    console.log('this.compDetails', this.compDetails);
    this.validdata = this.fetchSer.getvalidUser();
    this.invaliddata = this.fetchSer.getinvalidUser();
    console.log(this.validdata, this.invaliddata);
    for (let i = 0; i < this.validdata.length; i++) {
      const userObj = {
        v_subname: this.validdata[i].Name,
        v_subemail: this.validdata[i].Email,
        v_phoneno: this.validdata[i].Mobile,
        v_subaddress: this.validdata[i].Address,
        v_gst: this.validdata[i].GST,
        v_designation: this.validdata[i].Designation,
        v_corpcode: this.validdata[i].CorporateCode,
        v_country: this.validdata[i].Country_Code,
      };
      this.validUserDate.push(userObj);
    }
    // this.Durid = this.passSer.durId;
    // this.featuresList = this.passSer.selectedFeatures;
    // for (let i = 0; i < this.featuresList.length; i++) {
    //   // let fIds = this.featuresList[i].featureId)
    //   this.fIds.push(this.featuresList[i].featureId);
    // }
    // for (let i = 0; i < this.validdata.length; i++) {
    //   this.validUserDate[i].v_durationId = JSON.parse(this.Durid);
    //   this.validUserDate[i].v_trackId = this.trkId;
    //   this.validUserDate[i].v_len = this.fIds.length;
    //   this.validUserDate[i].v_features = this.fIds.join('|');
    // }
    // this.allData.push(this.validUserDate);
    // this.allData.push(this.fIds.join('|'));
    console.log('validUserDate', this.validUserDate);
    this.trkId = this.passSer.trackerID;
  }

  back() {
    // if(this.isback === true) {
    //   this.hidePopup = true;
    // } else {
    this.router.navigate(['pages/corporations']);
    // }
  }

  upload() {
    this.spinner.show();
    const params = {
      fileData: this.validUserDate,
      corpId: this.compDetails.corpId,
      corpn: this.compDetails.corpName,
    };
    console.log('params', params);
    this.addSubSer.addSubscribers(params).then(res => {
      console.log('res', res);
      try {
        if (res.type === true && res.status_code === 200) {
          console.log('res', res);
          try {
            this.spinner.hide();
            this.resultData = res.ExcelData;
            for (let i = 0; i < this.resultData.length; i++) {
              if (this.resultData[i].flag === 0) {
                this.invalidcount.push(this.resultData[i]);
                this.dontupload = true;
              } else if (this.resultData[i].flag === 1) {
                this.validcount.push(this.resultData[i]);
                this.isback = true;
              }
            }
            this.showResult = true;
            this.showUpload = false;
          } catch (e) {
            console.log(e);
          }
        }
        if (res.type === false) {
          this.spinner.hide();
          console.log('res', res);
          // this.toastr.error(res.data[0].error);
        }
      } catch (e) {
        console.log(e);
      }
    },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
  }

  selectTab(event) {
    if (event.tabTitle === 'Valid') {
      this.showUpload = true;
    } else {
      this.showUpload = false;
    }
  }

  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.resultData.length; i++) {
      this.dataObjforExcel = {
        'NAME': this.resultData[i].v_subname,
        'EMAIL': this.resultData[i].v_subemail,
        'PHONE': this.resultData[i].v_phoneno,
        'ADDRESS': this.resultData[i].v_subaddress,
        'DESIGNATION': this.resultData[i].v_designation,
        'GST NO.': this.resultData[i].v_gst,
        'COUNTRY CODE.': this.resultData[i].v_country,
        'RESULT': this.resultData[i].result,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Upload Details');
  }

  uploadDeals() {
    this.spinner.show();
    const params = {
      finalData: this.validUserDate,
      trckId: this.trkId,
      uid: this.userData.userid,
      uname: this.userData.subname,
      bname: this.bowserInfo.name,
      bversion: this.bowserInfo.version,
    };
    console.log('params', params);
    this.uploadPenDealSer.uploadDealsForSubscriber(params).then(res => {
      this.spinner.hide();
      console.log('res', res);
      try {
        if (res.type === true && res.status_code === 200) {
          console.log('res', res);
          this.spinner.hide();
          this.toastr.success('Data saved successfully');
          this.router.navigate(['pages/user']);
        }
        if (res.type === false) {
          this.spinner.hide();
          console.log('res', res);
          // this.toastr.error(res.data[0].error);
        }
      } catch (e) {
        console.log(e);
      }
    },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
  }

  hideinvoice() {
    this.hidePopup = false;
  }

  confirmSubmit() {
    this.router.navigate(['pages/user']);
  }
}

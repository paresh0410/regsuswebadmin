import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class SaveForumsCommentService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  saveForumCommentUrl = this.baseUrl + webApi.apiUrl.saveForumComment;
  constructor(public http: HttpClient) { }
  public saveForumComments(commData: any): Promise<any> {

    return this.http.post(this.saveForumCommentUrl, commData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

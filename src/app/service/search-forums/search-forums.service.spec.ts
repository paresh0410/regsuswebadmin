import { TestBed } from '@angular/core/testing';

import { SearchForumsService } from './search-forums.service';

describe('SearchForumsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchForumsService = TestBed.get(SearchForumsService);
    expect(service).toBeTruthy();
  });
});

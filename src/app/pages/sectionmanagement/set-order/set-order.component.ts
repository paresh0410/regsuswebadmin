import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../../config';
import { CommonService } from '../../../service/common.service';
import { SectionsService } from '../../../service/sections/sections.service';

@Component({
  selector: 'ngx-set-order',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './set-order.component.html',
  styleUrls: ['./set-order.component.scss']
})
export class SetOrderComponent implements OnInit {
  listData: any = []
  webList: any = []
  appList: any = []

  constructor(private router: Router, private toastr: ToastrService, private commonService: CommonService,
    private sectionService: SectionsService, private cdf: ChangeDetectorRef, private http: HttpClient) {
    this.fetchSectionData()
  }

  ngOnInit() {
  }

  tabChange(event) {
    console.log(event)
  }
  onBack() {
    this.router.navigate(['pages/sectionmanagement']);
  }
  async onSave() {
    const paramWeb = {
      bname: this.commonService.bowserInfo.name,
      bversion: this.commonService.bowserInfo.version,
      uid: this.commonService.userData.userid,
      uname: this.commonService.userData.subname,
      ordertype: 'web',
      sectionid: this.webList.map(a => a.id).join('|'),
      setorder: this.webList.sort((a, b) => a.id - b.id).map(a => a.id).join('|'),
      len: this.listData.length
    }
    const paramApp = {
      bname: this.commonService.bowserInfo.name,
      bversion: this.commonService.bowserInfo.version,
      uid: this.commonService.userData.userid,
      uname: this.commonService.userData.subname,
      ordertype: 'app',
      sectionid: this.appList.map(a => a.id).join('|'),
      setorder: this.appList.sort((a, b) => a.id - b.id).map(a => a.id).join('|'),
      len: this.listData.length
    }
    await this.setSectionOrder(paramWeb)
    console.log('Web order set')
    await this.setSectionOrder(paramApp)
    console.log('App order set')
    this.fetchSectionData()
    this.cdf.detectChanges()
  }

  drop(event: CdkDragDrop<string[]>, val) {
    console.log('drop event')
    if (val == 'web') {
      moveItemInArray(this.webList, event.previousIndex, event.currentIndex);
    } else {
      moveItemInArray(this.appList, event.previousIndex, event.currentIndex);
    }
    console.log('webList', this.webList)
    console.log('appList', this.appList)
  }


  fetchSectionData() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      this.sectionService.fetchSectionList().then((res: any) => {
        try {
          if (res.type) {
            this.listData = res.data
            this.webList = []
            this.appList = []
            this.appList = this.listData.slice().sort((a, b) => a.appsortorder - b.appsortorder)
            this.webList = this.listData.slice().sort((a, b) => a.websortorder - b.websortorder)
            console.log('this.webList', this.webList)
            console.log('this.appList', this.appList, this.listData)
            this.cdf.detectChanges()
          } else {

          }
        } catch (error) {
          console.log(error);

        }
      })
    }
  }
  setSectionOrder(param) {
    return new Promise((resolve, reject) => {
      if (!window.navigator.onLine) {
        this.toastr.error('Please check your internet connection');
      } else {
        const url = webApi.baseUrl + webApi.apiUrl.setSectionOrder
        this.http.post(url, param).subscribe((success: any) => {
          console.log(success)
          this.toastr.success(success.data[0].msg)
          resolve({ err: false, data: success });
        }, (error) => {
          console.log(error)
          resolve({ err: true, data: error });
        });

      }
    })

  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { AddEditDuartionService } from '../../../service/add-edit-duration/add-edit-duartion.service';

@Component({
  selector: 'ngx-add-edit-duration',
  templateUrl: './add-edit-duration.component.html',
  styleUrls: ['./add-edit-duration.component.scss']
})
export class AddEditDurationComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  duratationData: any = [];
  durationVal: any = new RegExp('[0-9]');
  disVal: any = new RegExp('^([0-9]|([1-9][0-9])|100)$');
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private passSer: PassServService,
    private addeditDurationSer: AddEditDuartionService,
  ) { 
      this.duratationData = this.passSer.editDuration;
    }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    if(this.duratationData[0]) {
      this.registerForm = this.formBuilder.group({
        months : [this.duratationData[0].months , [Validators.required , Validators.pattern('[0-9]*')]],
        // perPrice: [this.duratationData[0].perPrice , [Validators.required]],
        discount : [this.duratationData[0].discount , [Validators.required,Validators.pattern('^([0-9]|([1-9][0-9])|100)$')]],
        status: [this.duratationData[0].status , [Validators.required]],
      });
    } else {
      this.registerForm = this.formBuilder.group({
        months : ['', [Validators.required , Validators.pattern('[0-9]*')]],
        // perPrice: ['', [Validators.required]],
        discount : ['', [Validators.required,Validators.pattern('^([0-9]|([1-9][0-9])|100)$')]],
        status: ['', [Validators.required]],
      });
    }
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.spinner.show();
      const params = {
        d_id : 0,
        d_duration : this.registerForm.value.months,
        // d_isPrice : this.registerForm.value.perPrice,
        d_discount : this.registerForm.value.discount,
        d_isActive : this.registerForm.value.status,
      };
      if (this.duratationData[0]) {
        if (this.duratationData[0].durationId) {
          params.d_id = this.duratationData[0].durationId;
        }
      }
      console.log('params', params);
      this.addeditDurationSer.addeditDuration(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.data) {
                if (res.data.error) {
                  this.toastr.error(res.data.error);
                } else if (res.data.updated) {
                  this.router.navigate(['pages/duration']);
                  this.toastr.success(res.data.updated);
              } else if (res.data.saved) {
                this.router.navigate(['pages/duration']);
                this.toastr.success(res.data.saved);
            }
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
      // console.log('success');
      // this.router.navigate(['pages/duration']);
      // this.toastr.success('Data Saved Successfully');
    }
  }

  get f() {
    return this.registerForm.controls;
  }

  back() {
    this.router.navigate(['pages/duration']);
  }

  Validation(event) {
    console.log('event', event);
    if (event.target.placeholder === 'Duration') {
      if (!event.target.value) {
        this.toastr.error('Duration is required');
      } else if (!this.durationVal.test(event.target.value)) {
        this.toastr.error('Duration is invalid');
      }
    }
    if (event.target.placeholder === 'Per Price') {
      if (!event.target.value) {
        this.toastr.error('Per Price is required');
      }
    }
    if (event.target.placeholder === 'Discount') {
      if (!event.target.value) {
        this.toastr.error('Discount is required');
      } else if (!this.disVal.test(event.target.value)) {
        this.toastr.error('Discount is invalid');
      }
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { AddEditInterestsService } from '../../../service/add-edit-interests/add-edit-interests.service';
import { BrowserInfoService } from '../../../service/browser-info.service';

@Component({
  selector: 'ngx-add-edit-interests',
  templateUrl: './add-edit-interests.component.html',
  styleUrls: ['./add-edit-interests.component.scss']
})
export class AddEditInterestsComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  interestData: any = [];
  isedit: any = false;
  userData :any;
  bowserInfo:any;
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private passSer: PassServService,
    private addEditIntSer: AddEditInterestsService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.interestData = this.passSer.editInterests;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
   }

   ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    if(this.interestData[0]) {
      this.isedit = true;
      this.registerForm = this.formBuilder.group({
        iname : [ this.interestData[0].iname , [Validators.required]],
        icode : [ this.interestData[0].icode , [Validators.required]],

        status: [this.interestData[0].status , [Validators.required]],
      });
    } else {
      this.registerForm = this.formBuilder.group({
        iname : ['' , [Validators.required]],
        icode : ['' , [Validators.required]],

        status: ['1', [Validators.required]],
      });
    }
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.spinner.show();
      const params = {
        i_id : 0,
        i_name : this.registerForm.value.iname,
        i_code : this.registerForm.value.icode,
        i_isActive : this.registerForm.value.status,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      if (this.interestData[0]) {
        if (this.interestData[0].interestId) {
          params.i_id = this.interestData[0].interestId;
        }
      }
      console.log('params', params);
      this.addEditIntSer.addeditInterest(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.data) {
                if (res.data.error) {
                  this.toastr.error(res.data.error);
                } else if (res.data.updated) {
                  this.router.navigate(['pages/interests']);
                  this.toastr.success(res.data.updated);
              } else if (res.data.saved) {
                this.router.navigate(['pages/interests']);
                this.toastr.success(res.data.saved);
            }
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
    }
  }

  get f() {
    return this.registerForm.controls;
  }

  back() {
    this.router.navigate(['pages/interests']);
  }

  Validation(event) {
    console.log('event', event);
    if (event.target.placeholder === 'Interest Name') {
      if (!event.target.value) {
        this.toastr.error('Interest Name is required');
      }
    }
  }
}

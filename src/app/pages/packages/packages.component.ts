import { Component, OnInit, ElementRef } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { XlsxToJsonService } from '../../service/xlsx-to-json-service';
import { ViewChild } from '@angular/core';
import { FetchPackageService } from '../../service/fetch-package/fetch-package.service';
import { BrowserInfoService } from '../../service/browser-info.service';

@Component({
  selector: 'ngx-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss']
})
export class PackagesComponent implements OnInit {
  packageList: any = [];
  features: any = [];
  p: number = 1;
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  userData: any;
  bowserInfo: any;
  search = {
    pkgName: '',
    packType: '',
    price: '',
    duration: '',
    discount: '',
    isActive: '',
    isSpecial: '',
    countryname: ''
  };
  newFeature: any = [];
  str: any;
  featureList: any;
  constructor(
    private passSer: PassServService,
    private fetchPackage: FetchPackageService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private browserInfoService: BrowserInfoService,
  ) {
    // for (let i = 0; i < this.packageList.length ; i++) {
    //   this.newFeature.push((this.packageList[i].features));
    // }
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.fetchPackages();
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  fetchPackages() {
    const data = {
      uid: this.userData.userid,
      uname: this.userData.subname,
      bname: this.bowserInfo.name,
      bversion: this.bowserInfo.version,
    };
    this.fetchPackage.fetchPackages(data).then(res => {
      this.packageList = res.data;
      this.features = res.feature;
      console.log('res', res);
      // console.log('Package features', this.features);
    });
  }

  sort(property) {
    console.log(property)
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.packageList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.pkgName = '';
    this.search.packType = '';
    this.search.price = '';
    this.search.duration = '';
    this.search.discount = '';
    this.search.isActive = '';
    this.search.isSpecial = '';
    this.search.countryname = ''
  }

  // status
  myFunction(event) {
    this.search.isActive = event.target.value;
    console.log('value', event);
  }
  myFunction1(event1) {
    this.search.packType = event1.target.value;
  }
  myFunction2(event1) {
    this.search.isSpecial = event1.target.value;
  }
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.packageList.length; i++) {
      // if (this.packageList[i].isActive === 1) {
      //   this.flag = 'Active';
      // }
      // if (this.packageList[i].isActive === 2) {
      //   this.flag = 'Inactive';
      // }

      this.dataObjforExcel = {
        'PACKAGE NAME': this.packageList[i].pkgName,
        'PACKAGE TYPE': this.packageList[i].packTypeName,
        'PRICE (₹)': this.packageList[i].price,
        'COUNTRY': this.packageList[i].countryname,
        'DURATION (MONTHS)': this.packageList[i].duration,
        'DISCOUNT(%)': this.packageList[i].discount,
        'STATUS': this.packageList[i].isActive == 1 ? 'Active' : 'Inactive',
        'SPECIAL': this.packageList[i].isSpecial == 1 ? 'Yes' : 'No'
      }
      console.log(this.dataObjforExcel)
      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();

  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Package Details');
  }
  joinArrayObjs(ar) {
    // let str ;
    // for (let i = 0, len = ar.length; i < len; i++) {
    return this.str += ar.name + ', ';
    // }
    //  str;
  }

  editSub(editData) {
    this.passSer.featureList = this.features;
    this.passSer.editPkg[0] = (editData);
    this.router.navigate(['pages/packages/add-edit-packages']);
  }
  AddSub() {
    this.passSer.featureList = this.features;
    this.passSer.editPkg = [];
    this.router.navigate(['pages/packages/add-edit-packages']);
  }

}

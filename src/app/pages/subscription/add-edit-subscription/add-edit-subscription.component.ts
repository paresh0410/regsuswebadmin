import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'ngx-add-edit-subscription',
  templateUrl: './add-edit-subscription.component.html',
  styleUrls: ['./add-edit-subscription.component.scss']
})
export class AddEditSubscriptionComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  isNotification: any = 0;
  isDeleted: any = 0;
  isSuspended: any = 0;
  isPolicyAgreed: any = 0;
  isotpVerified: any;
  roleArray: any = [];
  selectRole: any;
  editSubata: any = [];
  userStatus: any;
  userFlag: any;
  dropdownList: any = [
    { fetId: 1, feature: 'News & Opinions' },
    { fetId: 2, feature: 'Polls' },
    { fetId: 3, feature: 'Chat' },
  ];
  selectedItems: any = [];
  dropdownSettings: any = {
    singleSelection: false,
    idField: 'fetId',
    textField: 'feature',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };
  featuresData : any = [];
  nameVal: any = new RegExp('^[A-Za-z -]+$');
  isCheck: string;
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private passSer: PassServService,
    private datePipe: DatePipe,
  ) {
    console.log('this.passSer.editSubscription',this.passSer.editSubscription);
    this.editSubata = this.passSer.editSubscription;
    console.log('featuresData',this.featuresData);
    console.log('this.editSubata',this.editSubata);
    for (let i = 0; i < this.dropdownList.length; i++) {
      if(this.editSubata.length > 0) {
        this.featuresData = this.editSubata[0].features;
        for(let j = 0; j < this.featuresData.length; j++) {
          if (this.featuresData[j].id) {
            if (this.featuresData[j].id === this.dropdownList[i].fetId) {
              const subobj = {
                fetId: this.featuresData[j].id,
                feature: this.featuresData[j].name,
              };
              this.selectedItems.push(subobj);
            }
          }
        }
      }
    }
    console.log('this.selectedItems',this.selectedItems);
  }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    this.registerForm = this.formBuilder.group({
      subsc: [this.editSubata[0].subsc, [Validators.required]],
      // price: [this.editSubata[0].price, [Validators.required]],
      // period: [this.editSubata[0].period, [Validators.required]],
      status: [this.editSubata[0].status, [Validators.required]],
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if features will not be selected
    if (this.selectedItems.length === 0) {
      this.isCheck = 'Please select features';
    }
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.router.navigate(['pages/subscriptions']);
      this.toastr.success('Data Saved Sucessfully');
    }
  }
  back() {
    this.router.navigate(['pages/subscriptions']);
  }
  onItemSelect(item) {
      console.log('select', this.selectedItems);
      this.isCheck = '';
  }
  onSelectAll(items) {
    // console.log(items);
    if (this.selectedItems.length === 0) {
      // this.selectedItems = [];
      this.selectedItems = items;
      this.isCheck = '';
    } else {
      this.selectedItems = [];
      this.isCheck = 'Please select features';
    }
    console.log('selectAll', this.selectedItems);
  }
  onItemDeSelect(des) {
    for (let i = 0; i < this.selectedItems.length; i++) {
      if (des.fetId === this.selectedItems[i].fetId) {
        this.selectedItems.splice(i, 1);
      }
    }
    if (this.selectedItems.length === 0) {
      this.isCheck = 'Please select features';
    }
    //  console.log('deselect', this.selectedItems);
  }
  checkVal(event) {
    console.log('event', event);
    if (event.target.placeholder === 'Subscription Name') {
      if (!event.target.value) {
        this.toastr.error('Subscription is required');
      } 
      // else if (!this.nameVal.test(event.target.value)) {
      //   this.toastr.error('Numbers and Special characters are not allowed');
      // }
    }
    if (event.target.placeholder === 'Price') {
      if (!event.target.value) {
        this.toastr.error('Price is required');
      }
    }
    if (event.target.placeholder === 'Period') {
      if (!event.target.value) {
        this.toastr.error('Period is required');
      }
    }
  }
}

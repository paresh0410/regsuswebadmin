import { TestBed } from '@angular/core/testing';

import { FetchPackageService } from './fetch-package.service';

describe('FetchPackageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchPackageService = TestBed.get(FetchPackageService);
    expect(service).toBeTruthy();
  });
});

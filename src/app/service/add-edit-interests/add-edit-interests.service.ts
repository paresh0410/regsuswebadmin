import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class AddEditInterestsService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addeditInterestsUrl = this.baseUrl + webApi.apiUrl.addEditInterests;
  constructor(public http: HttpClient) { }
  public addeditInterest(interestData: any): Promise<any> {

    return this.http.post(this.addeditInterestsUrl, interestData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

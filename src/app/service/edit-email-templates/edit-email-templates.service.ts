import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class EditEmailTemplatesService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  editEmailTemplatesUrl = this.baseUrl + webApi.apiUrl.editEmailTemplate;
  constructor(public http: HttpClient) { }
  public editEmailTemplates(emailData: any): Promise<any> {

    return this.http.post(this.editEmailTemplatesUrl, emailData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

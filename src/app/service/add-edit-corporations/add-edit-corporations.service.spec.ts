import { TestBed } from '@angular/core/testing';

import { AddEditCorporationsService } from './add-edit-corporations.service';

describe('AddEditCorporationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditCorporationsService = TestBed.get(AddEditCorporationsService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { FetchInterestsService } from './fetch-interests.service';

describe('FetchInterestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchInterestsService = TestBed.get(FetchInterestsService);
    expect(service).toBeTruthy();
  });
});

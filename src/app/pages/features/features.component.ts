import { Component, OnInit, ElementRef } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { XlsxToJsonService } from '../../service/xlsx-to-json-service';
import { ViewChild } from '@angular/core';
import { FetchFeaturesService } from '../../service/fetch-features/fetch-features.service';

import { BrowserInfoService } from '../../service/browser-info.service';

@Component({
  selector: 'ngx-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent implements OnInit {
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  featureList: any = [];
  p: number = 1;
  // fileName: any;
  fileReaded: any;
  enableUpload: boolean;
  bulkUploadAssetData: any;
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  fileUrl: any;
  fileName: any = 'No file chosen';
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  checkPay: any;
  search = {
    fcode: '',
    featureName: '',
    paidType: '',
    status: '',
  };
  @ViewChild('customFileLangHTML') myInputVariable: ElementRef;
  featureLength: boolean = true;
  setveractive: any = false;
  userData :any;
  bowserInfo:any;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private fetchFeatureServ: FetchFeaturesService,
    private browserInfoService: BrowserInfoService,
  ) { 
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    // this.spinner.show();
    this.fetchFeatures();
    // setTimeout(() => {
    //   /* spinner ends after 1 seconds */
    //   this.spinner.hide();
    // }, 1000);
  }

  goChangePassword() {
    this.router.navigate(['pages/changepassword'])
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.featureList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.featureName = '';
    this.search.fcode = '';
    this.search.paidType = '';
    this.search.status = '';
  }

  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }

  checkPaid(event) {
    this.search.paidType = event.target.value;
    console.log('value', event);
  }

  editFeature(editData) {
    this.passSer.editFeature = editData;
    this.router.navigate(['pages/features/add-edit-features']);
  }

  // Export To Excel
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.featureList.length; i++) {
      if (this.featureList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.featureList[i].status === 2) {
        this.flag = 'Inactive';
      }
      if (this.featureList[i].paidType === 1) {
        this.checkPay = 'Paid';
      }
      if (this.featureList[i].paidType === 2) {
        this.checkPay = 'Free';
      }
      this.dataObjforExcel = {
        'FEATURE CODE': this.featureList[i].fcode,
        'FEATURE': this.featureList[i].featureName,
        'PAID TYPE': this.checkPay,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Feature Details');
  }

  fetchFeatures() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      this.spinner.show();
      const data = {
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      this.fetchFeatureServ.fetchFeatures(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
            // let userD = [];
            this.featureList = res.data;
            console.log('this.featureList',this.featureList);
            this.setveractive = false;
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            if(this.featureList.length === 0){
              this.featureLength = false;
            } else if(this.featureList.length > 0){
              this.featureLength = true;
            }
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.setveractive = true;
        this.featureList = [];
        this.toastr.error('Server Error');
      });
    }
  }
}

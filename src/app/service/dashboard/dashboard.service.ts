import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchDashboardActivity = this.baseUrl + webApi.apiUrl.fetchActivityReport;
  fetchDashboardDetail = this.baseUrl + webApi.apiUrl.fetchActivityDetail;
  fetchPackageDetail = this.baseUrl + webApi.apiUrl.fetchPackageDetail;

  fetchPackageReport = this.baseUrl + webApi.apiUrl.fetchDashboardPackage;
  fetchTrendReport = this.baseUrl + webApi.apiUrl.fetchTrendPackage;

  constructor(public http: HttpClient) { }
  public getDashboardActivity(dashboardData: any): Promise<any> {

    return this.http.post(this.fetchDashboardActivity, dashboardData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public getDashboardActivityDetails(dashboardData: any): Promise<any> {

    return this.http.post(this.fetchDashboardDetail, dashboardData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }
  

  public getDashboardPackageDetails(dashboardData: any): Promise<any> {

    return this.http.post(this.fetchPackageDetail, dashboardData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public getDashboardReportPackage(dashboardData: any): Promise<any> {
    return this.http.post(this.fetchPackageReport, dashboardData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public getDashboardTrendReport(dashboardData: any): Promise<any> {
    return this.http.post(this.fetchTrendReport, dashboardData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }


  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}


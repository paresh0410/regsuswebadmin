import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { FetchInterestsService } from '../../service/fetch-interests/fetch-interests.service';
import { BrowserInfoService } from '../../service/browser-info.service';

@Component({
  selector: 'ngx-interests',
  templateUrl: './interests.component.html',
  styleUrls: ['./interests.component.scss']
})
export class InterestsComponent implements OnInit {
  interestList: any = [];
  p: number = 1;
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  userData: any;
  bowserInfo: any;
  search = {
    iname: '',
    status: '',
    icode: ''
  };
  featureLength: any = true;
  setveractive: any = false;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private fetchInterets: FetchInterestsService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.spinner.show();
    this.fetchFeatures();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.interestList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.iname = '',
      this.search.status = '';
    this.search.icode = '';
  }

  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }

  // Export To Excel
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.interestList.length; i++) {
      if (this.interestList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.interestList[i].status === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'INTEREST NAME': this.interestList[i].iname,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)
      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }
  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Interests Details');
  }

  addIn() {
    this.passSer.editInterests = [];
    this.router.navigate(['pages/interests/add-edit-interests']);
  }

  editIn(item) {
    this.passSer.editInterests[0] = (item);
    this.router.navigate(['pages/interests/add-edit-interests']);
  }

  fetchFeatures() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      this.fetchInterets.fetchInterests(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              // let userD = [];
              this.interestList = res.data;
              if (this.interestList.length === 0) {
                this.featureLength = false;
              } else if (this.interestList.length > 0) {
                this.featureLength = true;
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.interestList = [];
          this.setveractive = true;
          this.toastr.error('Server Error');
        });
    }
  }
}

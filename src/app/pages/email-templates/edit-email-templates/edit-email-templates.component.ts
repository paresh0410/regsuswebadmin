import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { EditEmailTemplatesService } from '../../../service/edit-email-templates/edit-email-templates.service';
import { BrowserInfoService } from '../../../service/browser-info.service';

@Component({
  selector: 'ngx-edit-email-templates',
  templateUrl: './edit-email-templates.component.html',
  styleUrls: ['./edit-email-templates.component.scss']
})
export class EditEmailTemplatesComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  selectRole: any;
  editData: any = [];
  userStatus: any;
  userFlag: any;
  userData :any;
  bowserInfo:any;
  fileName: any = 'No file chosen';
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private passSer: PassServService,
    private etSer: EditEmailTemplatesService,
    private browserInfoService: BrowserInfoService,
  ) {
      this.editData = this.passSer.editET;
      console.log('this.editData' , this.editData);
      this.userData = JSON.parse(localStorage.getItem('UserData'));
      this.bowserInfo = this.browserInfoService.get_browser();
   }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    this.registerForm = this.formBuilder.group({
      label: [this.editData[0].label , [Validators.required]],
      subject: [this.editData[0].subject , [Validators.required]],
      content: [this.editData[0].content , [Validators.required]],
      fromEmail: [this.editData[0].fromEmail , [Validators.required]],
      footer: [this.editData[0].footer],
      triggerName: [this.editData[0].triggerName , [Validators.required]],
      status: [this.editData[0].status , [Validators.required]],
    });
  }
  get f() {
    return this.registerForm.controls;
  }
  back() {
    this.router.navigate(['pages/email-templates']);
  }
  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.spinner.show();
      const params = {
        et_id : this.editData[0].templateId,
        et_label : this.registerForm.value.label,
        et_subject : this.registerForm.value.subject,
        et_content : this.registerForm.value.content,
        et_attch : null,
        et_frmEml : this.registerForm.value.fromEmail,
        et_footer : this.registerForm.value.footer,
        et_tgrName : this.registerForm.value.triggerName,
        et_status : this.registerForm.value.status,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      console.log('params', params);
      this.etSer.editEmailTemplates(params).then(res => {
        this.spinner.hide();
        console.log('res', res);
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.data[0].updated) {
                this.router.navigate(['pages/email-templates']);
                this.toastr.success(res.data[0].updated);
              } else if (res.data[0].error) {
                this.toastr.error(res.data[0].error);
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.data[0].error);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
      // console.log('success');
      // this.router.navigate(['pages/email-templates']);
      // this.router.navigate(['pages/posts']);
    }
  }
  checkVal(event) {
    if (event.target.placeholder === 'Label') {
      if (!event.target.value) {
        this.toastr.error('Label is required');
      }
    }
    if (event.target.placeholder === 'Subject') {
      if (!event.target.value) {
        this.toastr.error('Subject is required');
      }
    }
    if (event.target.placeholder === 'Content') {
      if (!event.target.value) {
        this.toastr.error('Content is required');
      }
    }
    if (event.target.placeholder === 'Trigger Name') {
      if (!event.target.value) {
        this.toastr.error('Trigger Name is required');
      }
    }
  }
  readFileUrl(event: any) {
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (event.target.files && event.target.files[0]) {
      this.fileName = event.target.files[0].name;
    }
  }
}

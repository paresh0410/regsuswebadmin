import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class FetchDealsOfCorporationService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchPendingDealsUrl = this.baseUrl + webApi.apiUrl.fetchDealsOfCorp;
  constructor(public http: HttpClient) { }
  public fetchPendingDeals(dealsData: any): Promise<any> {

    return this.http.post(this.fetchPendingDealsUrl, dealsData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

import { TestBed } from '@angular/core/testing';

import { AddEditPromotionService } from './add-edit-promotion.service';

describe('AddEditPromotionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditPromotionService = TestBed.get(AddEditPromotionService);
    expect(service).toBeTruthy();
  });
});

import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';
// import { AppConfig } from '../../../app.module';
// import { webApi } from '../../../service/webApi';
import { webApi } from '../../config';
import { HttpClient } from "@angular/common/http";

@Injectable()

export class BulkUploadService {


    tenantId : any = 1;

    // baseUrl: any = webApi.baseUrl; this.baseUrl + webApi.apiUrl
    private _urlInsertAssetBulkUpload = '';
    private _urlInsertValidDataTemp = '';


    constructor(@Inject('APP_CONFIG_TOKEN') private _http: Http,
     private _httpClient: HttpClient) {
        // this.busy = this._http.get('...').toPromise();
    }


    insertAssetBulkUpload(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlInsertAssetBulkUpload, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    insertValidDataTemp(param) {
        return new Promise(resolve => {
            this._httpClient.post(this._urlInsertValidDataTemp, param)
                //.map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    _errorHandler(error: Response) {
        console.error(error);
        return Observable.throw(error || "Server Error")
    }


}

import { TestBed } from '@angular/core/testing';

import { AddEditLiveFeedsService } from './add-edit-live-feeds.service';

describe('AddEditLiveFeedsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditLiveFeedsService = TestBed.get(AddEditLiveFeedsService);
    expect(service).toBeTruthy();
  });
});

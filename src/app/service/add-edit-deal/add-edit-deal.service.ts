import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class AddEditDealService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addEditDealUrl = this.baseUrl + webApi.apiUrl.addEditDeal;
  constructor(public http: HttpClient) { }
  public addEditDeals(dealData: any): Promise<any> {

    return this.http.post(this.addEditDealUrl, dealData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

import { Component, OnInit, ElementRef } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { XlsxToJsonService } from '../../service/xlsx-to-json-service';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'ngx-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {
  subcriptionList: any = [
    {
      subscId: 1,
      subsc: 'Free Trial',
      features: [
        { id: 1 , name: 'News & Opinions'},
        { id: 2 , name: 'Polls'},
    ],
      price: 999,
      period: 44,
      status: 1,
    },
    {
      subscId: 2,
      subsc: 'Basic',
      features: [
        { id: 2 , name: 'Polls'},
    ],
      price: 599,
      period: 89,
      status: 1,
    },
    {
      subscId: 3,
      subsc: 'Silver',
      features: [
        { id: 2 , name: 'Polls'},
        { id: 4 , name: 'Chat'},
    ],
      price: 1299,
      period: 21,
      status: 1,
    },
    // {
    //   subscId: 4,
    //   subsc: 'colaba',
    //   features: [
    //     { id: 4 , name: 'Chat'},
    // ],
    //   price: 299,
    //   period: 34,
    //   status: 1,
    // },
    {
      subscId: 4,
      subsc: 'Gold',
      features: [
        { id: 1 , name: 'News & Opinions'},
        { id: 2 , name: 'Polls'},
        { id: 4 , name: 'Chat'},
    ],
      price: 6499,
      period: 15,
      status: 1,
    },
    // {
    //   subscId: 6,
    //   subsc: 'Edge',
    //   features: [
    //     { id: 1 , name: 'News & Opinions'},
    // ],
    //   price: 99,
    //   period: 30,
    //   status: 1,
    // },
    // {
    //   subscId: 7,
    //   subsc: 'development',
    //   features: [
    //     { id: 2 , name: 'Polls'},
    //     { id: 4 , name: 'Chat'},
    // ],
    //   price: 799,
    //   period: 18,
    //   status: 1,
    // },
    // {
    //   subscId: 8,
    //   subsc: 'design',
    //   features: [
    //     { id: 4 , name: 'Chat'},
    // ],
    //   price: 399,
    //   period: 10,
    //   status: 1,
    // },
    // {
    //   subscId: 9,
    //   subsc: 'old',
    //   features: [
    //     { id: 1 , name: 'News & Opinions'},
    //     { id: 4 , name: 'Chat'},
    // ],
    //   price: 4599,
    //   period: 68,
    //   status: 1,
    // },
    // {
    //   subscId: 10,
    //   subsc: 'nashik',
    //   features: [
    //     { id: 1 , name: 'News & Opinions'},
    //     { id: 2 , name: 'Polls'},
    //     { id: 4 , name: 'Chat'},
    // ],
    //   price: 9999,
    //   period: 123,
    //   status: 1,
    // },
    // {
    //   subscId: 11,
    //   subsc: 'nashik',
    //   features: [
    //     { id: 2 , name: 'Polls'},
    // ],
    //   price: 1099,
    //   period: 38,
    //   status: 1,
    // },
    // {
    //   subscId: 12,
    //   subsc: 'Admin and HR',
    //   features: [
    //     { id: 2 , name: 'Polls'},
    //     { id: 4 , name: 'Chat'},
    // ],
    //   price: 699,
    //   period: 45,
    //   status: 1,
    // },
  ];
  p: number = 1;
  // fileName: any;
  fileReaded: any;
  enableUpload: boolean;
  bulkUploadAssetData: any;
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  fileUrl: any;
  fileName: any = 'No file chosen';
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  search = {
    subsc: '',
    // features: {
    //   id: '',
    //   name: '',
    // },
    features: '',
    price: '',
    period: '',
    status: '',
  };
  str: any;
  newFeature: any = [];
  @ViewChild('customFileLangHTML') myInputVariable: ElementRef;
  featureList: any;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
  ) {
    for(let i = 0; i < this.subcriptionList.length ; i++) {
      this.newFeature.push((this.subcriptionList[i].features));
    }
    // console.log('this.newFeature',this.newFeature);
   }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }
  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.subcriptionList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.subsc = '';
    // this.search.features.name = '';
    this.search.features = '';
    this.search.price = '';
    this.search.period = '';
    this.search.status = '';
  }

  changePeriod(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }
  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.subcriptionList.length; i++) {
      this.subcriptionList[i]['fname'] = [];
      if (this.subcriptionList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.subcriptionList[i].status === 2) {
        this.flag = 'Inactive';
      }

      for (let k = 0; k < this.subcriptionList[i].features.length; k++) {
        this.featureList = {
          name: this.subcriptionList[i].features[k].name,
        };
        console.log('this.featureList  ',this.featureList)
        this.subcriptionList[i].fname.push(this.featureList);
        console.log('this.subscription[i].fname  ',this.subcriptionList[i].fname);
      }
      let feature = '';
      this.str = '';
      for (let j = 0; j < this.subcriptionList[i].fname.length; j++){
        feature = this.joinArrayObjs(this.subcriptionList[i].fname[j]);
        // console.log('feature ', feature);
      }
      let newStr = '';
      newStr = feature.replace(/,\s*$/, '');
      console.log('this.subcriptionList[i].fname  ', this.subcriptionList[i].fname);
      this.dataObjforExcel = {
        'SUBSCRIPTION': this.subcriptionList[i].subsc,
        'FEATURES': newStr,
        // 'PRICE (IN ₹)': this.subcriptionList[i].price,
        // 'PERIOD (IN DAYS)': this.subcriptionList[i].period,
        'STATUS': this.flag,
      }
      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    console.log('this.newFeature', this.newFeature);
    console.log('this.dataArray',this.dataObjforExcel);
    console.log('this.subscriptionList ', this.subcriptionList);
    console.log('this.dataArrayforExcel ', this.dataArrayforExcel);
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Subscription Details');
  }
  editSub(editData) {
    this.passSer.editSubscription[0] = (editData);
    this.router.navigate(['pages/subscriptions/add-edit-subscriptions']);
  }
  AddSub() {
    this.passSer.editSubscription = [] ;
    this.router.navigate(['pages/subscriptions/add-edit-subscriptions']);
  }

  joinArrayObjs(ar) {
    // let str ;
    // for (let i = 0, len = ar.length; i < len; i++) {
      return  this.str += ar.name + ', ';
    // }
    //  str;
}
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class UploadCategoryIconService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  uploadCatIconUrl = this.baseUrl + webApi.apiUrl.uploadCatIcon;
  constructor(public http: HttpClient) { }
  public uploadCatIcon(catIconData: any): Promise<any> {

    return this.http.post(this.uploadCatIconUrl, catIconData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

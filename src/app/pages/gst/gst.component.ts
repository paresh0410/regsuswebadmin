import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-gst',
  templateUrl: './gst.component.html',
  styleUrls: ['./gst.component.scss']
})
export class GstComponent implements OnInit {
  gstList: any = [
    {
      gstid : 1,
      cgst : 6,
      sgst : 6,
      gst : 12,
      createdDate : '2019-04-11',
      status : 2,
    },
    {
      gstid : 2,
      cgst : 9,
      sgst : 9,
      gst : 18,
      createdDate : '2019-08-01',
      status : 1,
    },
  ];
  p: number = 1;
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  search = {
    cgst : '',
    sgst: '',
    gst : '',
    createdDate : '',
    status: '',
  };
  featureLength: any = true;
  setveractive: any = false;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
  ) { }
  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.gstList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.cgst = '',
    this.search.sgst = '',
    this.search.gst = '',
    this.search.createdDate = '',
    this.search.status = '';
  }

  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }

  // Export To Excel
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.gstList.length; i++) {
      if (this.gstList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.gstList[i].status === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'CGST(%)': this.gstList[i].cgst,
        'SGST(%)': this.gstList[i].sgst,
        'GST(%)': this.gstList[i].gst,
        'CREATED DATE': this.gstList[i].createdDate,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'GST Details');
  }

  addGst() {
    this.passSer.editGST = [];
    this.router.navigate(['pages/gst/add-edit-gst']);
  }

  editGst(editdata) {
    this.passSer.editGST[0] = (editdata);
    this.router.navigate(['pages/gst/add-edit-gst']);
  }
}

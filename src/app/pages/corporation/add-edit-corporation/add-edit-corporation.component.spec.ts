import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditCorporationComponent } from './add-edit-corporation.component';

describe('AddEditCorporationComponent', () => {
  let component: AddEditCorporationComponent;
  let fixture: ComponentFixture<AddEditCorporationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditCorporationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditCorporationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

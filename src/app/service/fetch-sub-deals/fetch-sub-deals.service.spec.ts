import { TestBed } from '@angular/core/testing';

import { FetchSubDealsService } from './fetch-sub-deals.service';

describe('FetchSubDealsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchSubDealsService = TestBed.get(FetchSubDealsService);
    expect(service).toBeTruthy();
  });
});

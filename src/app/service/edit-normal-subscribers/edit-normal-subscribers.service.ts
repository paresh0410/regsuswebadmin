import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class EditNormalSubscribersService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  editNormalSubUrl = this.baseUrl + webApi.apiUrl.editNormalSubscribers;
  logoutSubUrl = this.baseUrl + webApi.apiUrl.logoutSubscribers;
  constructor(public http: HttpClient) { }
  public editNormalSub(subData: any): Promise<any> {

    return this.http.post(this.editNormalSubUrl, subData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public logoutSub(subData: any): Promise<any> {

    return this.http.post(this.logoutSubUrl, subData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }


  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

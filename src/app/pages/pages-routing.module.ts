import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

import { BlankRoutingComponent } from './blank-routing/blank-routing.component';
import { UserComponent } from './user/user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { FeaturesComponent } from './features/features.component';
import { AddEditFeaturesComponent } from './features/add-edit-features/add-edit-features.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import { AddEditSubscriptionComponent } from './subscription/add-edit-subscription/add-edit-subscription.component';
import { CategoriesComponent } from './categories/categories.component';
import { EditCategoriesComponent } from './categories/edit-categories/edit-categories.component';
import { PostsComponent } from './posts/posts.component';
import { AddEditPostsComponent } from './posts/add-edit-posts/add-edit-posts.component';
import { PollsComponent } from './polls/polls.component';
import { AddEditPollsComponent } from './polls/add-edit-polls/add-edit-polls.component';
import { EmailTemplatesComponent } from './email-templates/email-templates.component';
import { EditEmailTemplatesComponent } from './email-templates/edit-email-templates/edit-email-templates.component';
import { PollResultsComponent } from './polls/poll-results/poll-results.component';
import { PackagesComponent } from './packages/packages.component';
import { AddEditPackagesComponent } from './packages/add-edit-packages/add-edit-packages.component';
import { CorporationComponent } from './corporation/corporation.component';
import { AddEditCorporationComponent } from './corporation/add-edit-corporation/add-edit-corporation.component';
import { DurationComponent } from './duration/duration.component';
import { AddEditDurationComponent } from './duration/add-edit-duration/add-edit-duration.component';
import { RateCardComponent } from './rate-card/rate-card.component';
import { AddEditRateCardComponent } from './rate-card/add-edit-rate-card/add-edit-rate-card.component';
import { SubscriberValidationComponent } from './user/subscriber-validation/subscriber-validation.component';
import { GstComponent } from './gst/gst.component';
import { AddEditGstComponent } from './gst/add-edit-gst/add-edit-gst.component';
import { DealsComponent } from './corporation/deals/deals.component';
import { AddEditDealsComponent } from './corporation/deals/add-edit-deals/add-edit-deals.component';
import { InterestsComponent } from './interests/interests.component';
import { AddEditInterestsComponent } from './interests/add-edit-interests/add-edit-interests.component';
import { ForumsComponent } from './forums/forums.component';
import { AddEditForumsComponent } from './forums/add-edit-forums/add-edit-forums.component';
import { ViewForumsComponent } from './forums/view-forums/view-forums.component';
import { ChatsComponent } from './chats/chats.component';
import { SubDealComponent } from './user/sub-deal/sub-deal.component';
import { AddEditSubDealComponent } from './user/sub-deal/add-edit-sub-deal/add-edit-sub-deal.component';
import { PromotionComponent } from './promotion/promotion.component';
import { AddEditPromotionComponent } from './promotion/add-edit-promotion/add-edit-promotion.component';
import { MetalSurveyReportComponent } from './metal-survey-report/metal-survey-report.component';
import { AddEditMetalSurveyReportComponent } from './metal-survey-report/add-edit-metal-survey-report/add-edit-metal-survey-report.component';
import { MannualSubscribersComponent } from './user/mannual-subscribers/mannual-subscribers.component';
import { AppSubscribersComponent } from './user/app-subscribers/app-subscribers.component';
import { ActivityReportComponent } from './activity-report/activity-report.component';
import { TrendReportComponent } from './trend-report/trend-report.component';
import { PackageDashboardComponent } from './package-dashboard/package-dashboard.component';
import { ActivityDetailsComponent } from './activity-report/activity-details/activity-details.component';
import { PackageDetailsComponent } from './package-dashboard/package-details/package-details.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SectionmanagementComponent } from './sectionmanagement/sectionmanagement.component';
import { AddEditSectionsComponent } from './sectionmanagement/add-edit-sections/add-edit-sections.component';
import { SetOrderComponent } from './sectionmanagement/set-order/set-order.component';
import { BroadcastComponent } from './broadcast/broadcast.component';
import { AddEditBroadcastComponent } from './broadcast/add-edit-broadcast/add-edit-broadcast.component';
import { ImageLibraryComponent } from './image-library/image-library.component';
import { LiveFeedComponent } from './live-feed/live-feed.component';
import { AddEditLiveFeedComponent } from './live-feed/add-edit-live-feed/add-edit-live-feed.component';
import { SeparatormanagementformComponent } from './separatormanagement/separatormanagementform/separatormanagementform.component';
import { SeparatormanagementComponent } from './separatormanagement/separatormanagement.component';
import { FreeTrailRequestComponent } from './free-trail-request/free-trail-request.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: ECommerceComponent,
  },
  {
    path: '',
    redirectTo: 'posts',
    pathMatch: 'full',
  },
  {
    path: 'changepassword',
    component: ChangePasswordComponent,
  },
  {
    path: 'user',
    component: UserComponent,
  },
  {
    path: 'features',
    component: FeaturesComponent,
  },
  {
    path: 'subscriptions',
    component: SubscriptionComponent,
  },
  {
    path: 'categories',
    component: CategoriesComponent,
  },
  {
    path: 'posts',
    component: PostsComponent,
  },
  {
    path:'live-feed',
    component:LiveFeedComponent
  },
  {
    path: 'activity',
    component: ActivityReportComponent,
  },
  {
    path: 'package-dashboard',
    component: PackageDashboardComponent,
  },
  {
    path: 'trend-report',
    component: TrendReportComponent,
  },
  {
    path: 'polls',
    component: PollsComponent,
  },
  {
    path: 'email-templates',
    component: EmailTemplatesComponent,
  },
  {
    path: 'packages',
    component: PackagesComponent,
  },
  {
    path: 'corporations',
    component: CorporationComponent,
  },
  {
    path: 'duration',
    component: DurationComponent,
  },
  {
    path: 'rate-card',
    component: RateCardComponent,
  },
  {
    path: 'gst',
    component: GstComponent,
  },
  {
    path: 'interests',
    component: InterestsComponent,
  },
  {
    path: 'forums',
    component: ForumsComponent,
  },
  {
    path: 'chats',
    component: ChatsComponent,
  },
  {
    path: 'promotion',
    component: PromotionComponent,
  },
  {
    path: 'ms-report',
    component: MetalSurveyReportComponent,
  },
  {
    path: '',
    component: BlankRoutingComponent,
    children: [
      { path: 'user/edit-user', component: EditUserComponent },
      { path: 'user/subscriber-validation', component: SubscriberValidationComponent },
      { path: 'user/subscriber-deal', component: SubDealComponent },
      {path:'separatormanagement/separatormanagementform', component:SeparatormanagementformComponent},
      { path: 'user/subscriber-deal/add-edit-subscriber-deal', component: AddEditSubDealComponent },
      { path: 'features/add-edit-features', component: AddEditFeaturesComponent },
      { path: 'subscriptions/add-edit-subscriptions', component: AddEditSubscriptionComponent },
      { path: 'categories/edit-categories', component: EditCategoriesComponent },
      { path: 'posts/add-edit-posts', component: AddEditPostsComponent },
      { path: 'live-feed/add-edit-live-feed', component:AddEditLiveFeedComponent},
      { path: 'polls/add-edit-polls', component: AddEditPollsComponent },
      { path: 'email-templates/edit-email-templates', component: EditEmailTemplatesComponent },
      { path: 'polls/poll-results', component: PollResultsComponent },
      { path: 'packages/add-edit-packages', component: AddEditPackagesComponent },
      { path: 'corporations/add-edit-corporations', component: AddEditCorporationComponent },
      { path: 'corporations/deals', component: DealsComponent },
      { path: 'corporations/deals/add-edit-deals', component: AddEditDealsComponent },
      { path: 'duration/add-edit-duration', component: AddEditDurationComponent },
      { path: 'rate-card/add-edit-rate-card', component: AddEditRateCardComponent },
      { path: 'gst/add-edit-gst', component: AddEditGstComponent },
      { path: 'interests/add-edit-interests', component: AddEditInterestsComponent },
      { path: 'forums/add-edit-forums', component: AddEditForumsComponent },
      { path: 'forums/view-forums', component: ViewForumsComponent },
      { path: 'promotion/add-edit-promotion', component: AddEditPromotionComponent },
      { path: 'ms-report/add-edit-ms-report', component: AddEditMetalSurveyReportComponent },
      { path: 'user/mannual-subscribers', component: MannualSubscribersComponent },
      { path: 'user/app-subscribers', component: AppSubscribersComponent },
      { path: 'activity/activity-detail', component: ActivityDetailsComponent },
      { path: 'package-dashboard/package-detail', component: PackageDetailsComponent },
      { path: 'sectionmanagement/add-edit-sections', component: AddEditSectionsComponent },
      { path: 'sectionmanagement/set-order', component: SetOrderComponent },
      { path: 'broadcast/add-edit-broadcast', component: AddEditBroadcastComponent },
    ],
  },
  {
    path:'separatormanagement',
    component: SeparatormanagementComponent
  },
  {
    path: 'sectionmanagement',
    component: SectionmanagementComponent,
  },
  {
    path:'free-trail-request',
    component:FreeTrailRequestComponent
  },
  {
    path: 'broadcast',
    component: BroadcastComponent,
  },
  {
    path: 'image-library',
    component: ImageLibraryComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}

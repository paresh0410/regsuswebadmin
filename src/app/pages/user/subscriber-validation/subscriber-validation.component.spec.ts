import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriberValidationComponent } from './subscriber-validation.component';

describe('SubscriberValidationComponent', () => {
  let component: SubscriberValidationComponent;
  let fixture: ComponentFixture<SubscriberValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriberValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriberValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

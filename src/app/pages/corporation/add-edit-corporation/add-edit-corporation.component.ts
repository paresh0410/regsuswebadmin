import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { AddEditCorporationsService } from '../../../service/add-edit-corporations/add-edit-corporations.service';
import { BrowserInfoService } from '../../../service/browser-info.service';

@Component({
  selector: 'ngx-add-edit-corporation',
  templateUrl: './add-edit-corporation.component.html',
  styleUrls: ['./add-edit-corporation.component.scss']
})
export class AddEditCorporationComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  editUserData: any = [];
  emailVal: any = new RegExp('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$');
  phoneVal: any = new RegExp('^[0-9]{10}$');
  corpHide: any = false;
  userData :any;
  bowserInfo:any;
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private passSer: PassServService,
    private addEditCorpSer: AddEditCorporationsService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.editUserData = this.passSer.editCorp;
    console.log('this.editUserData', this.editUserData);
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
   }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    if(this.editUserData[0]) {
      this.registerForm = this.formBuilder.group({
        corpCode: [ { value : this.editUserData[0].corpCode , disabled: true } , [Validators.required]],
        corpName: [ this.editUserData[0].corpName , [Validators.required]],
        contactPer: [ this.editUserData[0].contactPer , [Validators.required]],
        email: [ this.editUserData[0].email ,
         [Validators.required , Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
        gst: [ this.editUserData[0].gst ],
        phone: [ this.editUserData[0].phone , [Validators.required , Validators.pattern('^[0-9]{10}$')]],
        address: [ this.editUserData[0].address , [Validators.required] ],
        status: [ this.editUserData[0].status , [Validators.required]],
      });
      this.registerForm.controls['corpCode'].disable();
      this.corpHide = true;
    } else {
      this.registerForm = this.formBuilder.group({
        corpCode: ['', [Validators.required]],
        corpName: ['', [Validators.required]],
        contactPer: ['', [Validators.required]],
        email: ['', [Validators.required , Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
        gst: [''],
        phone: ['', [Validators.required , Validators.pattern('^[0-9]{10}$')]],
        address: [ '' , [Validators.required] ],
        status: ['1', [Validators.required]],
      });
    }
    this.BrowserDetection();
  }

    // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  back() {
    this.router.navigate(['pages/corporations']);
  }

  BrowserDetection() {
    //Check if browser is IE
    if (navigator.userAgent.search("MSIE")) {
        // insert conditional IE code here
        console.log(navigator.userAgent.search("MSIE"));
    }
    //Check if browser is Chrome
    else if (navigator.userAgent.search("Chrome")) {
        // insert conditional Chrome code here
        console.log(navigator.userAgent.search("Chrome"));

    }
    //Check if browser is Firefox 
    else if (navigator.userAgent.search("Firefox")) {
        // insert conditional Firefox Code here
        console.log(navigator.userAgent.search("Firefox"));
    }
    //Check if browser is Safari
    else if (navigator.userAgent.search("Safari")) {
        // insert conditional Safari code here
        console.log(navigator.userAgent.search("Safari"));
    }
    //Check if browser is Opera
    else if (navigator.userAgent.search("Opera")) {
        // insert conditional Opera code here
        console.log(navigator.userAgent.search("Opera"));
    }
}
showAlert= false;
  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);

    if(this.registerForm.value.status == 2) {
      this.showAlert = true;
      return;
    }
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.spinner.show();
      const params = {
        corp_corpId : 0,
        corp_corpcode : this.registerForm.controls.corpCode.value,
        corp_corpname : this.registerForm.value.corpName,
        corp_corpperson : this.registerForm.value.contactPer,
        corp_email : this.registerForm.value.email,
        corp_phone : this.registerForm.value.phone,
        corp_gst : this.registerForm.value.gst,
        corp_addr : this.registerForm.value.address,
        corp_status : this.registerForm.value.status,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      if (this.editUserData[0]) {
        if (this.editUserData[0].corpId) {
          params.corp_corpId = this.editUserData[0].corpId;
        }
      }
      console.log('params', params);
      this.addEditCorpSer.addeditCorporation(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.data) {
                if (res.data.error) {
                  this.toastr.error(res.data.error);
                } else if (res.data.saved) {
                  this.router.navigate(['pages/corporations']);
                  this.toastr.success(res.data.saved);
              } else if (res.data.upadted) {
                this.router.navigate(['pages/corporations']);
                this.toastr.success(res.data.upadted);
            }
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
    //   // console.log('success');
    //   // this.router.navigate(['pages/corporations']);
    //   // this.toastr.success('Data Saved Succesfully');
    }
  }

  close() {
    this.showAlert = false;
  }



  onSubmitInactive() {
    this.submitted = true;
    console.log(this.registerForm.value);

    
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.spinner.show();
      const params = {
        corp_corpId : 0,
        corp_corpcode : this.registerForm.controls.corpCode.value,
        corp_corpname : this.registerForm.value.corpName,
        corp_corpperson : this.registerForm.value.contactPer,
        corp_email : this.registerForm.value.email,
        corp_phone : this.registerForm.value.phone,
        corp_gst : this.registerForm.value.gst,
        corp_addr : this.registerForm.value.address,
        corp_status : this.registerForm.value.status,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      if (this.editUserData[0]) {
        if (this.editUserData[0].corpId) {
          params.corp_corpId = this.editUserData[0].corpId;
        }
      }
      console.log('params', params);
      this.addEditCorpSer.addeditCorporation(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.data) {
                if (res.data.error) {
                  this.toastr.error(res.data.error);
                } else if (res.data.saved) {
                  this.router.navigate(['pages/corporations']);
                  this.toastr.success(res.data.saved);
              } else if (res.data.upadted) {
                this.router.navigate(['pages/corporations']);
                this.toastr.success(res.data.upadted);
            }
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
    //   // console.log('success');
    //   // this.router.navigate(['pages/corporations']);
    //   // this.toastr.success('Data Saved Succesfully');
    }
  }


  checkVal(event) {
    console.log('event', event);
    if (event.target.placeholder === 'Corpoartion Code') {
      if (!event.target.value) {
        this.toastr.error('Corpoartion code is required');
      }
    }
    if (event.target.placeholder === 'Corpoartion Name') {
      if (!event.target.value) {
        this.toastr.error('Corpoartion name is required');
      }
    }
    if (event.target.placeholder === 'Contact Person') {
      if (!event.target.value) {
        this.toastr.error('Contact person is required');
      }
    }
    if (event.target.placeholder === 'Email') {
      if (!event.target.value) {
        this.toastr.error('Email is required');
      } else if (!this.emailVal.test(event.target.value)) {
        this.toastr.error('Email is invalid');
      }
    }
    if (event.target.placeholder === 'Phone') {
      if (!event.target.value) {
        this.toastr.error('Phone is required');
      } else if (event.target.value.length !== 10 || !this.phoneVal.test(event.target.value)) {
        this.toastr.error('Phone is invalid');
      }
    }
    // if (event.target.placeholder === 'GST') {
    //   if (!event.target.value) {
    //     this.toastr.error('GST is required');
    //   } else if (event.target.value.length !== 15) {
    //     this.toastr.error('GST is invalid');
    //   }
    // }
    if (event.target.placeholder === 'Address') {
      if (!event.target.value) {
        this.toastr.error('Address is required');
      }
    }
  }
}

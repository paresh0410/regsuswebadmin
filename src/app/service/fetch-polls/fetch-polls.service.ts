import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class FetchPollsService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchPollsUrl = this.baseUrl + webApi.apiUrl.fetchpolls;
  fetchPollsViewDetailUrl = this.baseUrl + webApi.apiUrl.fetchPollsViewDetail;
  ;

  constructor(public http: HttpClient) { }
  public fetchPolls(data: any): Promise<any> {

    return this.http.post(this.fetchPollsUrl, data, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public fetchPollsViewDetail(data: any): Promise<any> {

    return this.http.post(this.fetchPollsViewDetailUrl, data, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }


  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

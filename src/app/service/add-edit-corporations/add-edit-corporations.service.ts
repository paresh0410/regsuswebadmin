import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class AddEditCorporationsService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addeditCorporationUrl = this.baseUrl + webApi.apiUrl.addEditCorporation;
  constructor(public http: HttpClient) { }
  public addeditCorporation(corporationData: any): Promise<any> {

    return this.http.post(this.addeditCorporationUrl, corporationData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

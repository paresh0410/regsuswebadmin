import { TestBed } from '@angular/core/testing';

import { FetchLiveFeedsService } from './fetch-live-feeds.service';

describe('FetchLiveFeedsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchLiveFeedsService = TestBed.get(FetchLiveFeedsService);
    expect(service).toBeTruthy();
  });
});

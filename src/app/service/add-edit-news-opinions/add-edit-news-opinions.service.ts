import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class AddEditNewsOpinionsService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addEditNwOpUrl = this.baseUrl + webApi.apiUrl.addeditNewsOpinion_new;
  getDynamiclink = this.baseUrl + webApi.apiUrl.getDynamiclink;
  constructor(public http: HttpClient) { }
  public addEditNewsOpinion(nwopData: any): Promise<any> {

    return this.http.post(this.addEditNwOpUrl, nwopData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public getNewsOpinionDynamicLink(nwopData: any): Promise<any> {

    return this.http.post(this.getDynamiclink, nwopData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { XlsxToJsonService } from '../../service/xlsx-to-json-service';
import { FetchSubscribersService } from '../../service/fetch-subscribers/fetch-subscribers.service';
import { FetchTemplateService } from '../../service/fetch-template/fetch-template.service';
import { FetchNormalSubscriberService } from '../../service/fetch-normal-subscriber/fetch-normal-subscriber.service';
import { FetchDealsOfCorporationService } from '../../service/fetch-deals-of-corporation/fetch-deals-of-corporation.service';
import { BrowserInfoService } from '../../service/browser-info.service';

@Component({
  selector: 'ngx-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  @ViewChild('nc') nc: ElementRef;
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  tableList: any = [];
  userList: any = [];
  p: number = 1;
  q: number = 1;
  // fileName: any;
  fileReaded: any;
  enableUpload: boolean;
  bulkUploadAssetData: any;
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  fileUrl: any;
  fileName: any = 'Select user template';
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  search = {
    name: '',
    phone: '',
    company: '',
    email: '',
    createdDate: '',
    // regDate: '',
    // subsc: '',
    designation : '',
    gst : '',
    // userType: '',
    // subscstart: '',
    // subscend: '',
    subcStarus: '',
    status: '',
  };
  display: any = 'none';
  showDiv: boolean = true;
  display1: any = 'none';
  showDiv1: boolean = true;
  checkcorp: any;
  @ViewChild('customFileLangHTML') myInputVariable: ElementRef;
  fileName1: any = 'Select renewal template';
  featureLength: any = true;
  setveractive: any = false;
  featureLength1: any = true;
  setveractive1: any = false;
  dropdownList: any = [];
  selectedItems: any = [];
  validdata: any = [];
  invaliddata: any = [];
  dropdownSettings: any = {
    singleSelection: false,
    idField: 'featureId',
    textField: 'featurename',
    itemsShowLimit: 2,
    allowSearchFilter: true,
  };
  emailVal: any = new RegExp('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$');
  mobVal: any = new RegExp('^[0-9]{10}$');
  gstVal: any = new RegExp('^[A-Z\d]{15}$');
  duration: any;
  durationList: any = [];
  baseUrl: any = webApi.baseUrl;
  getDownload = this.baseUrl + webApi.apiUrl.getTemplate;
  companyList: any = [];
  company:any = '';
  compDetail: any;
  hidePopup: boolean = true;
  activeDeals: any = [];
  previceDeals: any = []
  previewData: any = [];
  deal:any = '';
  dealList: any = [];
  iscorp: any = false;
  userData :any;
  bowserInfo:any;

  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private fetchSubSer: FetchSubscribersService,
    private downloadService: FetchTemplateService,
    private fetchNorSub: FetchNormalSubscriberService,
    private fetchPenDeals: FetchDealsOfCorporationService,
    private browserInfoService: BrowserInfoService,
  ) { 
    console.log('previceDeals',this.previceDeals);
    console.log('activeDeals',this.activeDeals);
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.tableList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
    this.userList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.name = '';
    this.search.phone = '';
    this.search.company = '';
    this.search.email = '';
    this.search.createdDate = '';
    // this.search.subsc = '';
    this.search.designation = '';
    this.search.gst = '';
    // this.search.userType = '';
    this.search.subcStarus = '';
    this.search.status = '';
  }

  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }
  subChange(sub) {
    this.search.subcStarus = sub.target.value;
    console.log('value', event);
  }
  // checkCorp(event) {
  //   this.search.userType = event.target.value;
  //   console.log('value', event);
  // }

  gotoDeals(editData) {
    this.passSer.editUser[0] = (editData);
    this.router.navigate(['pages/user/subscriber-deal']);
  }

  edituser(editData) {
    this.passSer.editUser[0] = (editData);
    this.router.navigate(['pages/user/edit-user']);
  }

  addUser() {
    this.passSer.editUser = [];
    this.router.navigate(['pages/user/edit-user']);
  }

  editCorpuser(editCorp) {
    this.passSer.editUser[0] = editCorp;
    this.passSer.companyList = this.companyList;
    this.router.navigate(['pages/user/edit-user']);
    this.passSer.disableComp = true;
  }
  showFile() {
    this.display = 'block';
    this.showDiv = false;
    this.display1 = 'none';
    this.showDiv1 = true;
    this.fileName1 = 'Select renewal template';
  }
  showFile1() {
    this.display1 = 'block';
    this.showDiv1 = false;
    this.display = 'none';
    this.showDiv = true;
    this.fileName = 'Select user template';
  }
  onItemSelect(event) {
    console.log('select', this.selectedItems);
  }
  onItemDeSelect(des) {
    for (let i = 0; i < this.selectedItems.length; i++) {
      if (des.featureId === this.selectedItems[i].featureId) {
        this.selectedItems.splice(i, 1);
      }
    }
    console.log('deselect', this.selectedItems);
  }
  onSelectAll(allSelect) {
    // if (this.selectedItems.length === 0) {
    //   this.selectedItems = allSelect;
    // } else {
    //   this.selectedItems = [];
    // }
    this.selectedItems = allSelect;
    console.log('selectAll', this.selectedItems);
  }
  remFile() {
    this.display = 'none';
    this.showDiv = true;
    // this.fileName = 'Select user template';
    // this.uploadedData = [];
    this.company = '';
    this.deal = '';
    // this.uploadedData = [];
    this.previewData = [];
    // this.myInputVariable.nativeElement.value = "";
  }
  remFile1() {
    this.display1 = 'none';
    this.showDiv1 = true;
    this.fileName1 = 'Select renewal template';
  }
  checkDuration(event) {
    this.duration = event.target.value;
  }
  checkCompany(comp) {
    this.previewData = [];
    if(comp) {
    this.company = parseInt(comp.target.value);
    this.fetchDeals();
    }
  }
  checkDeal(ddata) {
    this.previewData = [];
    if (ddata.target.value) {
      this.deal = ddata.target.value;
      for (let i = 0; i < this.dealList.length; i++) {
        if (this.deal === this.dealList[i].trackId) {
          this.previewData.push(this.dealList[i]);
        }
      }
    }
    console.log('this.previewData',this.previewData);
    if (this.previewData.length > 0) {
      for (let i = 0; i < this.previewData.length; i++) {
        this.previewData[i].purchaseDate = new Date(this.previewData[i].purchaseDate);
        this.previewData[i].fromDate = new Date(this.previewData[i].fromDate);
        this.previewData[i].toDate = new Date(this.previewData[i].toDate);
      }
    }
  }

  fetchDeals() {
    this.dealList = [];
    this.spinner.show();
      const params = {
        coid : this.company,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      console.log('params', params);
      this.fetchPenDeals.fetchPendingDeals(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              this.dealList = res.data;
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            // this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
  }

  readFileUrl(event: any) {
    // this.fileName = 'Select user template';
    const validExts = new Array('.xlsx', '.xls');
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.toastr.error('Please select the user template excel file');
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        this.bulkUploadAssetData = event.target.files[0];
        console.log('this.bulkUploadAssetDataFileRead',this.bulkUploadAssetData);

        this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          let sheetName = this.resultSheets[0];
          if(sheetName){
          this.result = data['sheets'][sheetName];
          } else {
            this.result = [];
          }
          console.log('dataSheet', data);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
            console.log(' this.uploadedData', this.uploadedData);
          } else {
            this.uploadedData = [];
          }
        });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }
  readFileUrl1(event: any) {
  //   const validExts1 = new Array('.xlsx', '.xls');
  //   let fileExt1 = event.target.files[0].name;
  //   fileExt1 = fileExt1.substring(fileExt1.lastIndexOf('.'));
  //   if (validExts1.indexOf(fileExt1) < 0) {
  //     this.toastr.error('Please select the user template excel file');
  //   } else {
  //     // return true;
  //     if (event.target.files && event.target.files[0]) {
  //       this.fileName1 = event.target.files[0].name;
  //       this.fileReaded = event.target.files[0];

  //       if (this.fileReaded) {
  //         this.enableUpload = true;
  //       }

  //       let file = event.target.files[0];
  //       this.bulkUploadAssetData = event.target.files[0];
  //       console.log('this.bulkUploadAssetDataFileRead',this.bulkUploadAssetData);

  //       this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
  //         this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
  //         console.log('File Property Names ', this.resultSheets);
  //         let sheetName = this.resultSheets[0];
  //         this.result = data['sheets'][sheetName];
  //         console.log('dataSheet', data);

  //         if (this.result.length > 0) {
  //           this.uploadedData = this.result;
  //           console.log(' this.uploadedData', this.uploadedData);
  //         } else {
  //           this.toastr.error('File is empty');
  //         }
  //       });
  //       var reader = new FileReader();
  //       reader.onload = (event: ProgressEvent) => {
  //       };
  //       reader.readAsDataURL(event.target.files[0]);
  //       this.validdata = [];
  //       this.invaliddata = [];
  //     }
  //   }
  }

  uploadSheet() {
    this.spinner.show();
    if (!this.company) {
      this.spinner.hide();
      this.toastr.error('Please select company');
    }
    //  else if (this.selectedItems.length === 0) {
    //   this.spinner.hide();
    //   this.toastr.error('Please select feature');
    // }
     else if (!this.deal) {
      this.spinner.hide();
      this.toastr.error('Please select deal');
    } else if (this.uploadedData.length === 0) {
      this.spinner.hide();
      this.toastr.error('Please select valid file');
    } else if (this.uploadedData.length !== this.previewData[0].pax) {
      this.spinner.hide();
      this.toastr.error('No. of users are not matching to No. of users in deal');
    } else if (this.uploadedData.length === this.previewData[0].pax) {
      console.log(this.uploadedData);
      // this.rows = this.uploadedData;
      for (let i = 0 ;i < this.companyList.length; i++) {
        if (this.company === this.companyList[i].corpId) {
          this.compDetail =  this.companyList[i];
        }
      }
     this.passSer.editCorp =  this.compDetail;
     this.passSer.trackerID = this.previewData[0].trackId;
      console.log('this.compDetail',this.compDetail);
      for (let i = 0; i < this.uploadedData.length; i++) {
        this.uploadedData[i].valid = 'Valid';
        this.uploadedData[i].reason = '';
        if (!this.uploadedData[i].Name) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason  += ' Name is required';
        }
        // else if (!this.fnameVal.test(this.uploadedData[i].FirstName)){
        //   this.uploadedData[i].valid = 'Invalid';
        //   this.uploadedData[i].reason  += ' First Name is invalid';
        // }
        if (!this.uploadedData[i].Email) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Email is required';
        } else if (!this.emailVal.test(this.uploadedData[i].Email)) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Email is invalid';
        }
        if (!this.uploadedData[i].Mobile) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Mobile no is required';
        } else if (!this.mobVal.test(this.uploadedData[i].Mobile)) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Mobile no is invalid';
        }
        // if (!this.uploadedData[i].CorporateCode) {
        //   this.uploadedData[i].valid = 'Invalid';
        //   this.uploadedData[i].reason += ' Corporate Code is required';
        // } else if (this.uploadedData[i].CorporateCode !== this.company) {
        //   this.uploadedData[i].valid = 'Invalid';
        //   this.uploadedData[i].reason += ' Corporate code is invalid';
        // }
        if (!this.uploadedData[i].Designation) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Designation is required';
        }
         if (!this.uploadedData[i].Address) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Address is required';
        } else if (this.uploadedData[i].Address !== this.compDetail.corpAddress) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Address is invalid';
        }
        if (!this.uploadedData[i].GST) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' GST is required';
        } else if (this.uploadedData[i].GST !== this.compDetail.GSTno) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' GST is invalid';
        }
        // else if (!this.gstVal.test(this.uploadedData[i].GST)) {
        //   this.uploadedData[i].valid = 'Invalid';
        //   this.uploadedData[i].reason += ' GST is invalid';
        // }
      }
      for (let i = 0; i < this.uploadedData.length; i++) {
        if (this.uploadedData[i].valid === 'Valid') {
          this.validdata.push(this.uploadedData[i]);
        } else {
          this.invaliddata.push(this.uploadedData[i]);
        }
      }
      this.spinner.hide();
      this.fetchSubSer.resetUser();
      this.fetchSubSer.setvalidUser(this.validdata);
      this.fetchSubSer.setinvalidUser(this.invaliddata);
      this.passSer.selectedFeatures = this.selectedItems;
      this.passSer.durId = this.duration;
      console.log('valid data:', this.validdata);
      console.log('invalid data:', this.invaliddata);
      this.router.navigate(['pages/user/subscriber-validation']);
    } else {
      this.spinner.hide();
      this.toastr.error('Please select a file');
    }
  }

  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.tableList.length; i++) {
      if (this.tableList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.tableList[i].status === 2) {
        this.flag = 'Inactive';
      }
      if (this.tableList[i].subcStarus === 1) {
        this.checkcorp = 'Inactive';
      }
      if (this.tableList[i].subcStarus === 2) {
        this.checkcorp = 'Active';
      }
      this.dataObjforExcel = {
        'NAME': this.tableList[i].name,
        'EMAIL': this.tableList[i].email,
        'PHONE': this.tableList[i].phone,
        'COMPANY': this.tableList[i].company,
        'DESIGNATION': this.tableList[i].designation,
        'GST NO.': this.tableList[i].gst,
        'REGISTRATION DATE': this.tableList[i].createdDate,
        'SUBSCRIPTION':  this.checkcorp,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }
  exportExcelCorp() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.userList.length; i++) {
      if (this.userList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.userList[i].status === 2) {
        this.flag = 'Inactive';
      }
      if (this.tableList[i].subcStarus === 1) {
        this.checkcorp = 'Inactive';
      }
      if (this.tableList[i].subcStarus === 2) {
        this.checkcorp = 'Active';
      }
      this.dataObjforExcel = {
        'NAME': this.userList[i].name,
        'EMAIL': this.userList[i].email,
        'PHONE': this.userList[i].phone,
        'COMPANY': this.userList[i].company,
        'DESIGNATION': this.userList[i].designation,
        'GST NO.': this.userList[i].gst,
        // 'IS CORPORATE': this.userList[i].checkcorp,
        'REGISTERED DATE': this.userList[i].createdDate,
        // 'SUBSCRIPTION': this.userList[i].subsc,
        'SUBSCRIPTION':  this.checkcorp,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSXS();
  }
  download() {
    // this.dataArrayforExcel = [];
    //   this.dataObjforExcel = {
    //     'NAME': '',
    //     'COMPANY': '',
    //     'EMAIL': '',
    //     'MOBILE NO.': '',
    //     'SUBSCRIPTION': '',
    //     'SUBSCRIPTION START DATE': '',
    //     'SUBSCRIPTION END DATE': '',
    //     'STATUS': '',
    //   },
    //   // console.log('this.dataArray',this.dataObjforExcel)

    //   this.dataArrayforExcel.push(this.dataObjforExcel);
    //   //  for export to excel END //
    // this.downloadUserTemp();
    if (!window.navigator.onLine){
      this.toastr.error('Please check your internet connection');
    } else {
      window.open(this.getDownload, '_self');
    }
  }
  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'User Details');
  }
  exportAsXLSXS(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Corporate User Details');
  }
  downloadUserTemp(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'User Template');
  }
  selectTab(event) {
    this.clearBtn();
    this.column = [];
    this.isDesc = [];
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else if (event.tabTitle === 'Non-Corporate') {
      this.iscorp = true;
    // console.log('event' , event);
    // let params = {
    // user_type : 2,
    // }
    // if (event.tabTitle === 'Corporate') {
    //   params.user_type = 1;
    // }
    const data = {
      uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
    }
    this.fetchNorSub.fetchNormalSub(data).then(res => {
      this.spinner.hide();
      try {
        if (res.type === true && res.status_code === 200) {
          console.log('res', res);
          try {
          let userD = [];
          this.tableList = res.data;
          // this.userList = res.data[0];
          // this.dropdownList = res.data[1];
          // this.durationList = res.data[2];
          // this.companyList = res.data[3];
          console.log('this.tableList', this.tableList);
          // console.log('this.user', this.userList);
          // console.log('this.dropdownList', this.dropdownList);
          this.setveractive1 = false;
          if (this.tableList.length === 0) {
            this.featureLength1 = false;
          } else if (this.tableList.length > 0) {
            this.featureLength1 = true;
            for (let i = 0; i < this.tableList.length; i++) {
              if (!this.tableList[i].subcStarus) {
                this.tableList[i].subcStarus = 1;
              }
            }
          }
          } catch (e) {
            console.log(e);
          }
        }
        if (res.type === false) {
          this.spinner.hide();
          console.log('res', res);
        }
      } catch (e) {
        console.log(e);
      }
    },
    err => {
      console.log(err);
      this.spinner.hide();
      this.tableList = [];
      this.userList = [];
      this.dropdownList = [];
      this.durationList = [];
      this.setveractive1 = true;
      this.toastr.error('Server Error');
    });
    } else if (event.tabTitle === 'Bulk Deal') {
      this.iscorp = false;
      let params = {
        user_type : 1,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
        };
      this.fetchSubSer.fetchSubscriber(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
            let userD = [];
            // this.tableList = res.data;
            this.userList = res.data[0];
            this.dropdownList = res.data[1];
            this.durationList = res.data[2];
            this.companyList = res.data[3];
            console.log('this.tableList', this.tableList);
            // console.log('this.user', this.userList);
            // console.log('this.dropdownList', this.dropdownList);
            this.setveractive = false;
            if (this.userList.length === 0) {
              this.featureLength = false;
            } else if (this.userList.length > 0) {
              this.featureLength = true;
            }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.tableList = [];
        this.userList = [];
        this.dropdownList = [];
        this.durationList = [];
        this.setveractive = true;
        this.toastr.error('Server Error');
      });
    }
  }

  closepopup() {
    this.hidePopup = true;
  }
  openpopup(subscrip) {
    this.activeDeals = [];
    this.hidePopup = false;
    if (subscrip) {
      if (subscrip.DList) {
        this.activeDeals = subscrip.DList;
      }
    }
    console.log('this.activeDeals' , this.activeDeals);
  }
}

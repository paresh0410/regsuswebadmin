import { TestBed } from '@angular/core/testing';

import { EditEmailTemplatesService } from './edit-email-templates.service';

describe('EditEmailTemplatesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditEmailTemplatesService = TestBed.get(EditEmailTemplatesService);
    expect(service).toBeTruthy();
  });
});

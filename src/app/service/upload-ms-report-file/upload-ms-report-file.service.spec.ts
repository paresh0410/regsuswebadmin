import { TestBed } from '@angular/core/testing';

import { UploadMsReportFileService } from './upload-ms-report-file.service';

describe('UploadMsReportFileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadMsReportFileService = TestBed.get(UploadMsReportFileService);
    expect(service).toBeTruthy();
  });
});

import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'fas fa-tachometer-alt',
    children: [
      {
        title: 'Activity report',
        icon: 'fas fa-tasks',
        link: '/pages/activity',
        home: true,
      },
      {
        title: 'Package dashboard',
        icon: 'fab fa-usps',
        link: '/pages/package-dashboard',
        home: true,
      },
      {
        title: 'Trend Report',
        icon: 'fas fa-poll',
        link: '/pages/trend-report',
        home: true,
      },
    ],
  },
  // {
  //   title: 'Contents',
  //   icon: 'fas fa-book-reader',
  //   link: '/pages/features',
  // },
  {
    title: 'Corporations',
    icon: 'fas fa-briefcase',
    link: '/pages/corporations',
  },
  // {
  //   title: 'Promotion',
  //   icon: 'fas fa-gift',
  //   link: '/pages/promotion',
  // },
  // {
  //   title: 'Subscribers',
  //   icon: 'fas fa-users',
  //   link: '/pages/user',
  // },
  {
    title: 'Subscribers',
    icon: 'fas fa-users',
    children: [
      {
        title: 'Corporate Subscribers',
        icon: 'fas fa-indent',
        link: '/pages/user/mannual-subscribers',
        home: true,
      },
      {
        title: 'Individual Subscribers',
        icon: 'fas fa-indent',
        link: '/pages/user/app-subscribers',
        home: true,
      },
    ],
  },
  {
    title: 'Package',
    // icon: 'fas fa-money-check-alt',
    icon: 'fas fa-project-diagram',
    link: '/pages/packages',

    // children: [
    // {
    //   title: 'Subscriptions',
    //   icon: 'fas fa-money-check-alt',
    //   link: '/pages/subscriptions',
    //   home : true,
    // },
    // {
    //   title: 'Packages',
    //   icon: 'fas fa-project-diagram',
    //   link: '/pages/packages',
    //   home: true,
    // },
    // {
    //     title: 'Duration',
    //     icon: 'far fa-calendar-alt',
    //     link: '/pages/duration',
    //     home : true,
    // },
    // {
    //   title: 'Rate Card',
    //   icon: 'fas fa-rupee-sign',
    //   link: '/pages/rate-card',
    //   home: true,
    // },
    // ],
  },
  {
    title: 'Interests',
    icon: 'fas fa-tags',
    link: '/pages/interests',
  },
  {
    title: 'Section Management',
    icon: 'far fa-newspaper',
    link: '/pages/sectionmanagement',
  },
  {
    title: 'Separator Management',
    icon: 'far fa-newspaper',
    link: '/pages/separatormanagement',
  },
  {
    title: 'Content',
    icon: 'far fa-newspaper',
    children: [
      {
        title: 'Content Category',
        icon: 'fas fa-indent',
        link: '/pages/categories',
        home: true,
      },
      {
        title: 'Content Management',
        icon: 'far fa-newspaper',
        link: '/pages/posts',
        home: true,
      },
      {
        title: 'Live Feed',
        icon: 'far fa-newspaper',
        link: '/pages/live-feed',
        home: true,
      }

    ],
  },
  {
    title: 'Free Trail Request',
    icon: 'far fa-newspaper',
    link: '/pages/free-trail-request',
  },
  {
    title: 'Image Library',
    icon: 'far fa-image',
    link: '/pages/image-library',
    // home: true,
  },
  {
    title: 'Broadcast',
    icon: 'far fa-newspaper',
    link: '/pages/broadcast',
  },

  {
    title: 'Polls',
    icon: 'far fa-chart-bar',
    link: '/pages/polls',
  },
  {
    title: 'Forums',
    icon: 'fab fa-stack-exchange',
    link: '/pages/forums',
  },
  {
    title: 'MS Report',
    icon: 'fas fa-file',
    link: '/pages/ms-report',
  },
  // {
  //   title: 'Chats',
  //   icon: 'fas fa-comments',
  //   link: '/pages/chats',
  // },
  {
    title: 'Email Templates',
    icon: 'fas fa-envelope',
    link: '/pages/email-templates',
  },
  // {
  //   title: 'GST',
  //   icon: 'fas fa-percent',
  //   link: '/pages/gst',
  // },
];

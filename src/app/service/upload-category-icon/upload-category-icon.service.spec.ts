import { TestBed } from '@angular/core/testing';

import { UploadCategoryIconService } from './upload-category-icon.service';

describe('UploadCategoryIconService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadCategoryIconService = TestBed.get(UploadCategoryIconService);
    expect(service).toBeTruthy();
  });
});

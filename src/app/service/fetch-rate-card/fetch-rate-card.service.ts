import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class FetchRateCardService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchRateCardUrl = this.baseUrl + webApi.apiUrl.fetchRateCard;
  constructor(public http: HttpClient) { }
  public fetchRateCard(data: any): Promise<any> {

    return this.http.post(this.fetchRateCardUrl, data, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

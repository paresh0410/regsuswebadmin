import { TestBed } from '@angular/core/testing';

import { AddEditMsReportService } from './add-edit-ms-report.service';

describe('AddEditMsReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditMsReportService = TestBed.get(AddEditMsReportService);
    expect(service).toBeTruthy();
  });
});

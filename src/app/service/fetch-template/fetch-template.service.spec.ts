import { TestBed } from '@angular/core/testing';

import { FetchTemplateService } from './fetch-template.service';

describe('FetchTemplateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchTemplateService = TestBed.get(FetchTemplateService);
    expect(service).toBeTruthy();
  });
});

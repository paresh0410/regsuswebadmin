import { TestBed } from '@angular/core/testing';

import { SearchNewsPollsService } from './search-news-polls.service';

describe('SearchNewsPollsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchNewsPollsService = TestBed.get(SearchNewsPollsService);
    expect(service).toBeTruthy();
  });
});

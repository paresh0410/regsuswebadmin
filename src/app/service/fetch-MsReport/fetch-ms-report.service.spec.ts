import { TestBed } from '@angular/core/testing';

import { FetchMsReportService } from './fetch-ms-report.service';

describe('FetchMsReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchMsReportService = TestBed.get(FetchMsReportService);
    expect(service).toBeTruthy();
  });
});

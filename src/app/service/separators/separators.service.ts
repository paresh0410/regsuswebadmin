import { HttpClient } from '@angular/common/http';
import { webApi } from '../../../config';
import { CommonService } from './../common.service';
import { Injectable } from '@angular/core';
import { position } from 'html2canvas/dist/types/css/property-descriptors/position';

@Injectable({
  providedIn: 'root'
})
export class SeparatorsService {
sepratordata:any
  constructor( private   commonService:CommonService, private http:HttpClient) { }


  fetchSectionList() {
    const param = {
      // bname: this.commonService.bowserInfo.name,
      // bversion: this.commonService.bowserInfo.version,
      // uid: this.commonService.userData.userid,
      // uname: this.commonService.userData.subname
      id:this.commonService.userData.id,
      position: this.commonService.userData.position,
      html:this.commonService.userData.html,
      link: this.commonService.userData.link,
      linkType: this.commonService.userData.linkType
    }
    const url = webApi.baseUrl + webApi.apiUrl.fetchSeparator
    return new Promise(resolve => {
      this.http.post(url, param)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });

  }
}

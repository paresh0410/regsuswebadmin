import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { DatePipe } from '@angular/common';
import { EditNormalSubscribersService } from '../../../service/edit-normal-subscribers/edit-normal-subscribers.service';
import { BrowserInfoService } from '../../../service/browser-info.service';
import { FetchSubDealsService } from '../../../service/fetch-sub-deals/fetch-sub-deals.service';
import { FetchNormalSubscriberService } from '../../../service/fetch-normal-subscriber/fetch-normal-subscriber.service';
import { webApi } from '../../../../config';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../../../service/common.service';

@Component({
  selector: 'ngx-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  registerForm: FormGroup;
  NumberChangeForm: FormGroup;
  corporateForm: FormGroup
  showChangeBtn = false;
  submitted = false;
  isNotification: any = 0;
  isDeleted: any = 0;
  isSuspended: any = 0;
  isPolicyAgreed: any = 0;
  isotpVerified: any;
  roleArray: any = [];
  selectRole: any;
  editUserData: any = [];
  userStatus: any;
  userFlag: any = false;
  showSecondForm = false;
  userData: any;
  bowserInfo: any;
  dropdownList: any = [
    { subscId: 1, subsc: 'test' },
    { subscId: 2, subsc: 'new' },
    { subscId: 3, subsc: 'ended' },
    { subscId: 4, subsc: 'colaba' },
    { subscId: 5, subsc: 'jollywell' },
    { subscId: 6, subsc: 'bajaj' },
    { subscId: 7, subsc: 'development' },
    { subscId: 8, subsc: 'design' },
    { subscId: 9, subsc: 'old' },
    { subscId: 10, subsc: 'nashik' },
    { subscId: 11, subsc: 'Pachora' },
    { subscId: 11, subsc: 'Admin and HR' },
  ];
  selectedItems: any = [];
  dropdownSettings: any = {
    singleSelection: true,
    idField: 'subscId',
    textField: 'subsc',
    itemsShowLimit: 1,
    allowSearchFilter: true,
  };
  nameVal: any = new RegExp('^[A-Za-z -]+$');
  mobVal: any = new RegExp('^[0-9]{10}$');
  emailVal: any = new RegExp('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$');
  disableCompany: any = false;
  flag: any;

  companyList: any = [];
  corporateList: any = []
  isCorporate: boolean = false
  selectedCorporate: any
  countryList: any = []
  currencyList: any = []
  currencyId: any;
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private passSer: PassServService,
    private datePipe: DatePipe,
    private editNorSubSer: EditNormalSubscribersService,
    private browserInfoService: BrowserInfoService,
    private subscriberService: FetchSubDealsService,
    private fetchnormalSub: FetchNormalSubscriberService,
    private http: HttpClient,
    private commonService: CommonService,
  ) {
    this.editUserData = this.passSer.editUser;
    this.disableCompany = this.passSer.disableComp;
    this.companyList = this.passSer.companyList;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
    // if(this.disableCompany) {
    //   this.registerForm.disabled.
    // }
    console.log('this.disableCompany', this.disableCompany);
    console.log('this.editUserData', this.editUserData);
    // this.userStatus = this.editUserData[0].userType;
    // for (let i = 0; i < this.dropdownList.length; i++) {
    //   if (this.editUserData[0].subscId) {
    //     if (this.editUserData[0].subscId === this.dropdownList[i].subscId) {
    //       const subobj = {
    //         subscId: this.editUserData[0].subscId,
    //         subsc: this.editUserData[0].subsc,
    //       };
    //       this.selectedItems.push(subobj);
    //     }
    //   }
    // }
    console.log('this.selectedItems', this.selectedItems);
    this.corporateList = this.fetchnormalSub.corporateList
    console.log(this.corporateList)
  }


  ngOnInit() {
    this.spinner.show();
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*')]],
      mob: ['', [Validators.required]],
      email: ['', [Validators.required,
      Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
      company: [''],
      regDate: [''],
      // subsc: [{value: '', disabled : this.editUserData[0].userType === 1}],
      days: [''],
      selectTag: [{ value: '1', disabled: true }, [Validators.required]],
      // isLogin: [ {value: '0'} , [Validators.required]],

      subscstart: [''],
      subscend: [''],
      designation: [''],
      gst: [''],
      address: [''],
      isemailone: [{ value: '1', disabled: false }],
      isemailall: [{ value: '1', disabled: false }],
      country: [''],
      currency: ['']
    });
    this.getDdlList()
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    this.flag = this.passSer.flag;

    this.setCorporateForm()
  }

  editedData() {
    if (this.editUserData[0]) {
      this.registerForm = this.formBuilder.group({
        name: [this.editUserData[0].name, [Validators.required, Validators.pattern('[a-zA-Z ]*')]],
        mob: [{ value: this.editUserData[0].phone, disabled: this.editUserData[0].userType === 1 }, [Validators.required]],
        email: [this.editUserData[0].email, [Validators.required,
        Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
        company: [{ value: this.editUserData[0].company, disabled: this.editUserData[0].userType === 1 }],
        regDate: [this.editUserData[0].regDate,],
        subsc: [{ value: this.editUserData[0].subscId, disabled: this.editUserData[0].userType === 1 }],
        days: [this.editUserData[0].days],
        selectTag: [{ value: this.editUserData[0].status, disabled: true }, [Validators.required]],
        // isLogin: [this.editUserData[0].isLogin , [Validators.required]],
        subscstart: [this.editUserData[0].subscstart],
        subscend: [this.editUserData[0].subscend],
        designation: [this.editUserData[0].designation],
        gst: [{ value: this.editUserData[0].gst, disabled: this.editUserData[0].userType === 1 }],
        address: [{ value: this.editUserData[0].address, disabled: this.editUserData[0].userType === 1 }],
        isemailone: [{ value: this.editUserData[0].isEmailOne, disabled: false }],
        isemailall: [{ value: this.editUserData[0].isEmailAll, disabled: false }],
        country: [this.editUserData[0].countryname ? this.countryList.find(a => a.country == this.editUserData[0].countryname).countryId : ''],
        currency: [this.editUserData[0].currency ? this.currencyList.find(a => a.currencyId == this.editUserData[0].currency).currencyId : '']
      });

      if (this.editUserData[0].userType === 1) {
        this.registerForm.get('company').setValidators([Validators.required]);
        this.registerForm.get('designation').setValidators([Validators.required]);
        this.registerForm.get('gst').setValidators([Validators.required]);
        this.registerForm.get('address').setValidators([Validators.required]);
        // this.registerForm.controls.company.disable()
        // this.registerForm.controls.mob.disable()
        // this.registerForm.controls.gst.disable()
        // this.registerForm.controls.address.disable()
        this.userFlag = true;
      }

      this.NumberChangeForm = this.formBuilder.group({
        mobChn: ['', [Validators.required, Validators.pattern('[0-9]*')]]
      });

      if (this.editUserData[0].userType == 2) {
        this.isCorporate = true
      }
      this.showSecondForm = true;
    } else {
      this.registerForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*')]],
        mob: ['', [Validators.required]],
        email: ['', [Validators.required,
        Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
        company: [''],
        regDate: [''],
        // subsc: [{value: '', disabled : this.editUserData[0].userType === 1}],
        days: [''],
        selectTag: [{ value: '1', disabled: true }, [Validators.required]],
        // isLogin: [ {value: '0'} , [Validators.required]],

        subscstart: [''],
        subscend: [''],
        designation: [''],
        gst: [''],
        address: [''],
        isemailone: [{ value: '1', disabled: false }],
        isemailall: [{ value: '1', disabled: false }],
        country: [''],
        currency: [''],
      });
      this.showSecondForm = false;

    }
  }

  setCorporateForm() {
    this.corporateForm = this.formBuilder.group({
      corporate: ['', [Validators.required]]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    // let nameLength: number;
    // nameLength = this.registerForm.value.name.length;
    // document.getElementById('name').innerHTML = '';
    // if (nameLength > 100) {
    //   document.getElementById('name').innerHTML = 'Please enter characters less than 100';
    // }
    return this.registerForm.controls;
  }

  get s() {
    return this.NumberChangeForm.controls;

  }

  onItemSelect(item) {
    console.log('select', this.selectedItems);
    // this.selectRole = '';
  }
  onItemDeSelect(des) {
    console.log(des);
    this.selectedItems = [];
    console.log('deselect', this.selectedItems);
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      // this.spinner.show();
      const params = {
        suId: 0,
        subn: this.registerForm.controls.name.value,
        sube: this.registerForm.controls.email.value,
        subp: this.registerForm.controls.mob.value,
        suba: this.registerForm.controls.address.value,
        gst: this.registerForm.controls.gst.value,
        subdes: this.registerForm.controls.designation.value,
        corpn: this.registerForm.controls.company.value,
        isCorp: 2,
        isAct: this.registerForm.controls.selectTag.value,
        // isLogin : this.registerForm.controls.isLogin.value,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
        isEmailOne: this.registerForm.controls.isemailone.value,
        isEmailAll: this.registerForm.controls.isemailall.value,
        countryId: this.registerForm.controls.country.value
      };
      if (this.editUserData[0]) {
        if (this.editUserData[0].subId) {
          params.suId = this.editUserData[0].subId;
        }
        if (this.editUserData[0].userType) {
          params.isCorp = this.editUserData[0].userType;
        }
      }
      console.log('params', params);
      this.editNorSubSer.editNormalSub(params).then(res => {
        this.spinner.hide();
        console.log('res', res);
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.data.updated) {
                if (this.flag == 'manual') {
                  this.router.navigate(['pages/user/mannual-subscribers']);
                  this.toastr.success(res.data.updated);
                } else {
                  this.router.navigate(['pages/user/app-subscribers']);
                  this.toastr.success(res.data.updated);
                }
                // this.router.navigate(['pages/user']);
                // this.toastr.success(res.data.updated);
              } else if (res.data.insert) {
                if (this.flag == 'manual') {
                  this.router.navigate(['pages/user/mannual-subscribers']);
                  this.toastr.success(res.data.insert);
                } else {
                  this.router.navigate(['pages/user/app-subscribers']);
                  this.toastr.success(res.data.insert);
                }
                // this.router.navigate(['pages/user']);
                // this.toastr.success(res.data.insert);
              } else if (res.data.error) {
                if (this.flag == 'manual') {
                  this.router.navigate(['pages/user/mannual-subscribers']);
                  this.toastr.success(res.data.error);
                } else {
                  this.router.navigate(['pages/user/app-subscribers']);
                  this.toastr.success(res.data.error);
                }
                //   this.router.navigate(['pages/user']);
                //   this.toastr.success(res.data.error);
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            // this.toastr.error(res.data[0].error);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
    }
  }

  onChangeSubmit() {
    // this.submitted = true;
    console.log(this.NumberChangeForm.value);
    // stop here if form is invalid
    if (this.NumberChangeForm.invalid) {
      return;
    } else {
      // this.spinner.show();
      const params = {
        sid: this.editUserData[0].subId,

        phno: this.NumberChangeForm.controls.mobChn.value,

      };

      console.log('params', params);
      this.subscriberService.changePhoneNo(params).then(res => {
        this.spinner.hide();
        console.log('res', res);
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.type == true) {
                this.toastr.success('Number has changed');
                if (this.flag == 'manual') {
                  this.router.navigate(['pages/user/mannual-subscribers']);
                } else {
                  this.router.navigate(['pages/user/app-subscribers']);
                }

              } else {
                this.toastr.error('Number not changed.');

              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error('Server Error');

            // this.toastr.error(res.data[0].error);
          }
        } catch (e) {
          console.log(e);
          this.toastr.error('Server Error');

        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
    }
  }

  checkAvail() {
    if (this.NumberChangeForm.invalid) {
      return;
    } else {
      // this.spinner.show();
      const params = {

        phno: this.NumberChangeForm.controls.mobChn.value,

      };

      console.log('params', params);
      this.subscriberService.checkPhoneAvailaiblity(params).then(res => {
        this.spinner.hide();
        console.log('res', res);
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            let result = res.data[0][0];
            try {
              if (res.data[0][0].count == 1) {
                this.toastr.error('Number is already regiter with username ' + result.subname);
                this.showChangeBtn = false;

              } else {
                this.toastr.success('You can save the number it is not been used');
                this.showChangeBtn = true;
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error('Server is down');

            // this.toastr.error(res.data[0].error);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
    }
  }
  back() {
    if (this.flag == 'manual') {
      this.router.navigate(['pages/user/mannual-subscribers']);
    } else {
      this.router.navigate(['pages/user/app-subscribers']);
    }

  }
  checkVal(event) {
    console.log('event', event);
    if (event.target.placeholder === 'Name') {
      if (!event.target.value) {
        this.toastr.error('Name is required');
      } else if (!this.nameVal.test(event.target.value)) {
        this.toastr.error('Numbers and Special characters are not allowed');
      }
    }

    if (event.target.placeholder === 'Email') {
      if (!event.target.value) {
        this.toastr.error('Email is required');
      } else if (!this.emailVal.test(event.target.value)) {
        this.toastr.error('Email is invalid');
      }
    }

    if (event.target.placeholder === 'Mobile No.') {
      if (!event.target.value) {
        this.toastr.error('Mobile No. is required');
      } else if (!this.mobVal.test(event.target.value)) {
        this.toastr.error('Mobile No. is invalid');
      }
    }
    if (this.editUserData[0]) {
      if (this.editUserData[0].userType === 1) {
        if (event.target.placeholder === 'Designation') {
          if (!event.target.value) {
            this.toastr.error('Designation is required');
          }
        }
        if (event.target.placeholder === 'GST No.') {
          if (!event.target.value) {
            this.toastr.error('GST is required');
          } else if (event.target.value.length !== 15) {
            this.toastr.error('GST is invalid');
          }
        }
        if (event.target.placeholder === 'Address') {
          if (!event.target.value) {
            this.toastr.error('Address is required');
          }
        }
        if (event.target.placeholder === 'Company') {
          if (!event.target.value) {
            this.toastr.error('Company is required');
          }
        }
      }
    }
  }
  changeDetails(comp) {
    console.log('comp', comp);
    for (let i = 0; i < this.companyList.length; i++) {
      if (JSON.parse(comp.target.value) === this.companyList[i].corpId) {
        this.registerForm.controls.gst.setValue(this.companyList[i].GSTno);
        this.registerForm.controls.address.setValue(this.companyList[i].corpAddress);
        // this.registerForm.value.address = this.companyList[i].corpAddress;
      }
    }
  }
  corporateChange(event) {
    console.log(event.target.value)
    this.selectedCorporate = this.corporateList.find(a => a.corpId == event.target.value)
  }

  changeCorporate() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      if (this.registerForm.invalid) {
        this.submitted = true
      } else {
        this.submitted = false
        const url = webApi.baseUrl + webApi.apiUrl.moveSubscribertoCorporation
        const param = {
          uid: this.userData.userid,
          uname: this.userData.subname,
          bname: this.bowserInfo.name,
          bversion: this.bowserInfo.version,
          subid: this.editUserData[0].subId,
          isCorporate: this.editUserData[0].userType == 2 ? 1 : 2,
          corpid: this.selectedCorporate.corpId,
          corpname: this.selectedCorporate.corpname
        }
        this.http.post(url, param).subscribe((res: any) => {
          console.log(res)
          if (res.type) {
            this.router.navigate(['pages/user/app-subscribers']);
          }
        }, (error) => {
          console.log(error)
        });
      }

    }
  }
  getDdlList() {
    const url = webApi.baseUrl + webApi.apiUrl.packageDropdown
    const param = {
      bname: this.commonService.bowserInfo.name,
      bversion: this.commonService.bowserInfo.version,
      uid: this.commonService.userData.userid,
      uname: this.commonService.userData.subname
    }
    this.commonService.httpPostRequest(url, param).then((res: any) => {
      console.log(res)
      try {
        if (res.type) {
          this.countryList = res.country
          this.currencyList = res.currency
          this.editedData()
        }
      } catch (error) {

      }
    })
  }

  onCountryChange(event: any) {
    console.log(event.target.value)
    const currId = this.countryList.find(a => a.countryId == event.target.value).currencyId
    this.currencyId = currId
    this.registerForm.controls.currency.setValue(currId)
  }
}

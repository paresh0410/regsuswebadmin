import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class FetchSubDealsService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchSubDealsUrl = this.baseUrl + webApi.apiUrl.fetchSubDeals;
  changePhoneNoUrl = this.baseUrl + webApi.apiUrl.changePhoneNo;
  checkPhoneAvailaiblityUrl = this.baseUrl + webApi.apiUrl.checkPhoneAvailaiblity;
  constructor(public http: HttpClient) { }
  public fetchSubDeals(sudealsData: any): Promise<any> {

    return this.http.post(this.fetchSubDealsUrl, sudealsData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public changePhoneNo(sudealsData: any): Promise<any> {

    return this.http.post(this.changePhoneNoUrl, sudealsData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public checkPhoneAvailaiblity(sudealsData: any): Promise<any> {

    return this.http.post(this.checkPhoneAvailaiblityUrl, sudealsData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }



  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

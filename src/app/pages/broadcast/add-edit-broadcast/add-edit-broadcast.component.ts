import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../../config';
import { BroadcastService } from '../../../service/broadcast/broadcast.service';
import { CommonService } from '../../../service/common.service';

@Component({
  selector: 'ngx-add-edit-broadcast',
  templateUrl: './add-edit-broadcast.component.html',
  styleUrls: ['./add-edit-broadcast.component.scss']
})
export class AddEditBroadcastComponent implements OnInit {
  broadcastForm: FormGroup
  submitted: boolean = false
  typeList: any = []
  recipentList: any = []

  broadcastData: any
  public minPublishDate = new Date()
  public minExpiryDate = new Date()
  constructor(private router: Router, private fb: FormBuilder, private toastr: ToastrService, private commonService: CommonService,
    private broadcastService: BroadcastService) { }

  ngOnInit() {
    this.setForm()
    this.typeList = this.broadcastService.typeList
    this.recipentList = this.broadcastService.recipentList
    this.broadcastData = this.broadcastService.broadcastData
    if (this.broadcastData) {
      this.prepareEditData(this.broadcastData)
    }
  }

  setForm() {
    this.broadcastForm = this.fb.group({
      title: ['', [Validators.required]],
      type: ['', [Validators.required]],
      publishDate: ['', [Validators.required]],
      expiryDate: ['', [Validators.required]],
      recipent: ['', [Validators.required]],
      status: ['1', [Validators.required]]
    });
  }
  get f() {
    return this.broadcastForm.controls;
  }

  prepareEditData(data: any) {
    this.broadcastForm.controls.title.setValue(data.title)
    this.broadcastForm.controls.type.setValue(this.broadcastService.typeList.find(a => a.name == data.type).id)
    this.broadcastForm.controls.publishDate.setValue(new Date(data.publishDate))
    this.broadcastForm.controls.expiryDate.setValue(new Date(data.expiryDate))
    this.broadcastForm.controls.recipent.setValue(this.broadcastService.recipentList.find(a => a.name == data.category).id)
    this.broadcastForm.controls.status.setValue(data.status == 1 ? 1 : 0)
    this.minPublishDate = new Date(data.publishDate)
    this.minExpiryDate = new Date(data.expiryDate)
  }

  onSubmit() {
    console.log('test')
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      if (this.broadcastForm.invalid) {
        this.submitted = true
        return
      } else {
        this.submitted = false
        this.addEditBroadcast()
        // this.http.post(url, param).subscribe((res: any) => {
        //   try {
        //     if (res.type) {
        // if (this.edited){
        //        this.toastr.success(res.data.updated);
        // } else {
        // this.toastr.success(res.data.saved);
        // }
        //     }
        //   } catch (error) {
        //     console.log(error)
        //   }
        // },
        // resUserError => {
        //   console.log(resUserError)
        // });
      }

    }
  }

  back() {
    this.router.navigate(['pages/broadcast'])
    this.broadcastForm.reset()
    this.broadcastService.broadcastData = ''
  }

  statusChange(event) {
    if (event.currentTarget.checked) {
      this.broadcastForm.controls.status.setValue(1)
    } else {
      this.broadcastForm.controls.status.setValue(0)
    }
    console.log(this.broadcastForm.controls.status.value)
  }


  addEditBroadcast() {
    const param = {
      bname: this.commonService.bowserInfo.name,
      bversion: this.commonService.bowserInfo.version,
      uid: this.commonService.userData.userid,
      uname: this.commonService.userData.subname,
      brodcastid: this.broadcastData ? this.broadcastData.broadcastId : 0,
      name: this.broadcastForm.controls.title.value,
      type: this.broadcastForm.controls.type.value,
      publishdate: this.commonService.formattime(this.broadcastForm.controls.publishDate.value),
      expirydate: this.commonService.formattime(this.broadcastForm.controls.expiryDate.value),
      recipent: this.broadcastForm.controls.recipent.value,
      isActive: this.broadcastForm.controls.status.value == 1 ? 1 : 2
    }
    console.log(param)
    const url = webApi.baseUrl + webApi.apiUrl.addEditBroadcast
    this.commonService.httpPostRequest(url, param).then((res: any) => {
      console.log(res)
      try {
        if (res.type) {
          if (res.data.saved) {
            this.toastr.success(res.data.saved);
          } else if (res.data.updted) {
            this.toastr.success(res.data.updted);
          }
          this.back()
        } else {
          this.toastr.error(res.data);
        }
      } catch (error) {

      }
    })
  }

  setExpirtyStartDate(event: any) {
    let updatedTIme = new Date(new Date(event).getTime() + 1 * 60 * 60 * 1000)
    console.log(updatedTIme)
    // new Date(new Date().getTime() + 2 * 60 * 60 * 1000)

    // console.log(this.broadcastForm.controls.publishDate.value)
    // const dateDiff = this.commonService.getDateDiff(event) + 1
    // console.log(dateDiff)
    // this.minExpiryDate = new Date(new Date().setDate(new Date().getDate() + dateDiff))
    this.minExpiryDate = updatedTIme
    this.broadcastForm.controls.expiryDate.setValue(null)
  }

  addHours(numOfHours) {
    const date = this.broadcastForm.controls.publishDate.value
    date.setTime(date.getTime() + numOfHours * 60 * 60 * 1000);
    return date;
  }

  onTypeChange(event) {
    console.log(event.target.value)
    this.broadcastForm.controls.type.setValue(event.target.value)
  }
}

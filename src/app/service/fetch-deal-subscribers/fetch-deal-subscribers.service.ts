import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class FetchDealSubscribersService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchSubscriberDealsUrl = this.baseUrl + webApi.apiUrl.fetchDealSubscribers;
  constructor(public http: HttpClient) { }
  public fetchDealSubscriber(dealsubData: any): Promise<any> {

    return this.http.post(this.fetchSubscriberDealsUrl, dealsubData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

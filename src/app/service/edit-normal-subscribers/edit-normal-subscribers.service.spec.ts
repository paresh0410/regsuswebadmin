import { TestBed } from '@angular/core/testing';

import { EditNormalSubscribersService } from './edit-normal-subscribers.service';

describe('EditNormalSubscribersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditNormalSubscribersService = TestBed.get(EditNormalSubscribersService);
    expect(service).toBeTruthy();
  });
});

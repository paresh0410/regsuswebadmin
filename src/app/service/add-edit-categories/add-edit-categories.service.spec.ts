import { TestBed } from '@angular/core/testing';

import { AddEditCategoriesService } from './add-edit-categories.service';

describe('AddEditCategoriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditCategoriesService = TestBed.get(AddEditCategoriesService);
    expect(service).toBeTruthy();
  });
});

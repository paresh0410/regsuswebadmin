import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../../config';
import { ImgpopupComponent } from '../../../components/imgpopup/imgpopup.component';
import { CommonService } from '../../../service/common.service';
import { SectionsService } from '../../../service/sections/sections.service';
import { ImageLibraryPopupComponent } from '../../image-library-popup/image-library-popup.component';
import { ImagepopupComponent } from '../../imagepopup/imagepopup.component';

@Component({
  selector: 'ngx-add-edit-sections',
  templateUrl: './add-edit-sections.component.html',
  styleUrls: ['./add-edit-sections.component.scss']
})
export class AddEditSectionsComponent implements OnInit {
  sectionForm: FormGroup
  submitted: boolean = false
  primaryList: any = []
  secondaryList: any = []
  webprimaryList: any = []
  websecondaryList: any = []
  // ddlList: any = {
  //   primaryList: [
  //     { id: 1, layoutName: 'Layout 1', layoutImg: '../../../assets/images/Blue_bg.jpg', layoutSelected: 1 },
  //     { id: 2, layoutName: 'Layout 2', layoutImg: '../../../assets/images/pubg.jpeg', layoutSelected: 0 },
  //     { id: 3, layoutName: 'Layout 3', layoutImg: '../../../assets/images/team.png', layoutSelected: 0 },
  //   ],
  //   secondaryList: [
  //     { id: 1, layoutName: 'Layout 1', layoutImg: '../../../assets/images/cover2.jpg', layoutSelected: 1 },
  //     { id: 2, layoutName: 'Layout 2', layoutImg: '../../../assets/images/alan.png', layoutSelected: 0 },
  //     { id: 3, layoutName: 'Layout 3', layoutImg: '../../../assets/images/camera4.jpg', layoutSelected: 0 },
  //   ]
  // }
  @ViewChild('fileInput') myInputVariable: ElementRef;
  selectedImgDetails: any
  previewImg: any
  edited: any = 0
  sectionData: any
  imgUrl = webApi.baseUrl + webApi.apiUrl.getImg
  secquantity: any = false

  constructor(private fb: FormBuilder, private router: Router, private dialogService: NbDialogService,
    private toastr: ToastrService, private http: HttpClient, private commonService: CommonService, private sectionsService: SectionsService) { }

  ngOnInit() {
    this.setForm()
    this.fetchddlList()
    this.sectionForm.controls.isMandatory.setValue(2)
    this.sectionData = this.sectionsService.sectionData
    if (this.sectionData) {
      this.prepareEditData(this.sectionData)
    }
  }


  setForm() {
    this.sectionForm = this.fb.group({
      sectionName: ['', [Validators.required]],
      primaryLayout: ['', [Validators.required]],
      primaryLayoutQty: ['1', [Validators.required]],
      secondaryLayout: [''],
      secondaryLayoutQty: [null],
      isMandatory: ['', [Validators.required]],
      sectionStatus: ['', [Validators.required]],
      sectionDescription: [''],
      sectionImg: [null],
      webprimaryLayout: ['', [Validators.required]],
      websecondaryLayout: [''],
    });
  }
  get f() {
    return this.sectionForm.controls;
  }

  onChange(val) {
    console.log(val.target.value)
  }

  prepareEditData(data: any) {
    this.sectionForm.controls.sectionName.setValue(data.name)
    this.sectionForm.controls.primaryLayout.setValue(data.prilayout)
    this.sectionForm.controls.primaryLayoutQty.setValue(data.prilayoutquantity)
    this.sectionForm.controls.secondaryLayout.setValue(data.seclayout ? data.seclayout : '')
    this.sectionForm.controls.secondaryLayoutQty.setValue(data.seclayoutquantity ? data.seclayoutquantity : null)
    this.sectionForm.controls.isMandatory.setValue(data.ismandatory)
    this.sectionForm.controls.sectionStatus.setValue(data.status)
    this.sectionForm.controls.sectionDescription.setValue(data.description)
    this.sectionForm.controls.sectionImg.setValue(data.sectionimg)
    this.sectionForm.controls.webprimaryLayout.setValue(data.webprimaryLayoutId)
    this.sectionForm.controls.websecondaryLayout.setValue(data.websecondaryLayoutId ? data.websecondaryLayoutId : '')
    this.previewImg = data.sectionimg
    // data.sectionImg ? this.previewImg = data.sectionImg : null
    // this.sectionForm.controls.primaryLayout.disable();
    // this.sectionForm.controls.webprimaryLayout.disable();
  }


  back() {
    this.router.navigate(['pages/sectionmanagement'])
    this.sectionForm.reset()
    this.sectionsService.sectionData = ''
  }

  open(val, idx) {
    if (val) {
      this.dialogService.open(ImagepopupComponent, {
        context: {
          data: {
            img: idx == 1 ? this.primaryList.find(a => a.layoutId == val).layoutImage : this.secondaryList.find(a => a.layoutId == val).layoutImage
          },
        },
      });
    }
  }
  openWeb(val, idx) {
    if (val) {
      this.dialogService.open(ImagepopupComponent, {
        context: {
          data: {
            img: idx == 1 ? this.webprimaryList.find(a => a.layoutId == val).layoutImage : this.websecondaryList.find(a => a.layoutId == val).layoutImage
          },
        },
      });
    }
  }
  openImageLibrary(): void {
    console.log('open popup')
    let dialogRef = this.dialogService.open(ImageLibraryPopupComponent, {
      context: {
        data: ''
      },
      hasBackdrop: true,
      hasScroll: true
    })

    dialogRef.onClose.subscribe(result => {
      if (result) {
        this.sectionForm.controls.sectionImg.setValue(this.imgUrl + result[0][0].fileurl)
        this.previewImg = this.imgUrl + result[0][0].fileurl
      }
      console.log(result, 'The dialog was closed');
      // this.animal = result;
    });
  }

  uploadImage(event) {
    console.log('uploadImage', event)
    const files = event.target.files;
    if (files.length === 0)
      return;
    const reader = new FileReader();
    this.selectedImgDetails = files[0]
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.previewImg = reader.result;
    }
  }

  resetImg() {
    this.sectionForm.controls.sectionImg.setValue(null)
    this.previewImg = null
    // this.myInputVariable.nativeElement.value = "";
  }


  // onSubmit() {
  //   if (!window.navigator.onLine) {
  //     this.toastr.error('Please check your internet connection');
  //   } else {
  //     if (this.sectionForm.invalid) {
  //       this.submitted = true
  //       return
  //     } else {
  //       this.submitted = false
  //       this.addEditSections()
  //     }
  //   }
  // }
  onSubmit() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      console.log('section', this.sectionForm)
      if (this.sectionForm.invalid) {
        this.submitted = true;
        // this.toastr.error('Please fill in all required fields.');
        return;
      } else {
        if (this.sectionForm.controls.secondaryLayoutQty.value === null && this.sectionForm.controls.secondaryLayout.value !== "") {
          // this.secquantity = true;
          this.submitted = true;
          this.toastr.error('Secondary Layout Count is required.');
        } else {
          // this.secquantity = false;
          this.submitted = false;
          this.addEditSections();
        }
      }
    }
  }


  addEditSections() {
    const param = {
      bname: this.commonService.bowserInfo.name,
      bversion: this.commonService.bowserInfo.version,
      uid: this.commonService.userData.userid,
      uname: this.commonService.userData.subname,
      sectionid: this.sectionData ? this.sectionData.id : 0,
      name: this.sectionForm.controls.sectionName.value,
      prilayout: this.sectionForm.controls.primaryLayout.value,
      seclayout: this.sectionForm.controls.secondaryLayout.value ? this.sectionForm.controls.secondaryLayout.value : null,
      seclayquantity: this.sectionForm.controls.secondaryLayoutQty.value ? this.sectionForm.controls.secondaryLayoutQty.value : null,
      mandatorysec: this.sectionForm.controls.isMandatory.value,
      secdesc: this.sectionForm.controls.sectionDescription.value,
      secimg: this.sectionForm.controls.sectionImg.value ? this.sectionForm.controls.sectionImg.value : null,
      isactive: this.sectionForm.controls.sectionStatus.value == 1 ? 1 : 2,
      prilayquantity: this.sectionForm.controls.primaryLayoutQty.value ? this.sectionForm.controls.primaryLayoutQty.value : null,
      webprilayout: this.sectionForm.controls.webprimaryLayout.value,
      webseclayout: this.sectionForm.controls.websecondaryLayout.value ? this.sectionForm.controls.websecondaryLayout.value : null,
    }
    console.log(param)
    const url = webApi.baseUrl + webApi.apiUrl.addEditSection
    this.commonService.httpPostRequest(url, param).then((res: any) => {
      console.log(res)
      try {
        if (res.type) {
          if (res.data.saved) {
            this.toastr.success(res.data.saved);
          } else if (res.data.updated) {
            this.toastr.success(res.data.updated);
          }
          this.back()
        } else {

        }
      } catch (error) {

      }
    })
  }


  fetchddlList() {
    const param = {
      bname: this.commonService.bowserInfo.name,
      bversion: this.commonService.bowserInfo.version,
      uid: this.commonService.userData.userid,
      uname: this.commonService.userData.subname,
    }
    console.log(param)
    const url = webApi.baseUrl + webApi.apiUrl.sectionDropdown
    this.commonService.httpPostRequest(url, param).then((res: any) => {
      console.log(res)
      try {
        if (res.type) {
          this.primaryList = res.prilayout
          this.secondaryList = res.seclayout
          this.webprimaryList = res.webprilayout
          this.websecondaryList = res.webseclayout
        }
      } catch (error) {

      }
    })
  }
}

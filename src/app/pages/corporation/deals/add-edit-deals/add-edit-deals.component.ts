import { Component, OnInit } from "@angular/core";
import { PassServService } from "../../../../service/pass-serv.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { AddEditDealService } from "../../../../service/add-edit-deal/add-edit-deal.service";
import { FetchDealSubscribersService } from "../../../../service/fetch-deal-subscribers/fetch-deal-subscribers.service";
import { BrowserInfoService } from "../../../../service/browser-info.service";
// import { NullTree } from '../../../../../../node_modules1/@angular-devkit/schematics/src/tree/null';

@Component({
  selector: "ngx-add-edit-deals",
  templateUrl: "./add-edit-deals.component.html",
  styleUrls: ["./add-edit-deals.component.scss"],
})
export class AddEditDealsComponent implements OnInit {
  dropdownList: any = [];
  durationList: any = [];
  packageList: any = [];
  dealsData: any = [];
  gstList: any = [];
  registerForm: FormGroup;
  submitted = false;
  trkId: any = Math.random().toString(36).substr(2, 8).toUpperCase();
  checkAdd: any = false;
  selectedItems: any = [];
  dropdownSettings: any = {
    singleSelection: false,
    idField: "featureId",
    textField: "featurename",
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };
  userData: any;
  bowserInfo: any;
  dropdownList1: any = [
    // {subId: 1, subname: "Dwayne Johnson"},
    // {subId: 2, subname: "Renolds"},
    // {subId: 3, subname: "Tom"},
    // {subId: 4, subname: "testUser1"},
    // {subId: 5, subname: "testUser2"}
  ];
  selectedItems1: any = [
    // {subId: 1, subname: "Dwayne Johnson"},
    // {subId: 4, subname: "testUser1"},
  ];
  dropdownSettings1: any = {
    singleSelection: false,
    idField: "subId",
    textField: "subname",
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };
  isCheck: any;
  isCheck1: any;
  todayDate: any = new Date();
  fromDate: any = new Date();
  frmdt: any = new Date();
  prdt: any;
  todt: any;
  grcPir: any;
  gracePir: any;
  duration: any = 0;

  maxDate: any = new Date();

  feaCostArray: any = [];
  featuresCost: any = 0;
  discount: any = 0;
  perCount: any = 0;

  corpData: any = [];
  baseAmt: any = [];
  fidList: any = [];
  tempCost: any = 0;
  disablebtn: any = false;
  hidePopup: boolean = true;
  isdis: boolean = true;
  singlePkg: any;
  tempfeature: any = [];
  pkgPrice: any;
  feaAdd: any = false;
  uAdd: any = false;
  graceAdd: any = true;
  disDcnt: any = false;
  renewList: any = [];
  confirmList: any = {
    countUser: "",
    amountBase: "",
    discountPer: "",
    gstNo: "",
    totalAmount: "",
    baseAmt: "",
    accprice: "",
    pkg: "",
  };

  cgst: any = 0;
  sgst: any = 0;
  igst: any = 0;
  gst: any = 0;

  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private dealSer: AddEditDealService,
    private dealSubSer: FetchDealSubscribersService,
    private browserInfoService: BrowserInfoService
  ) {
    this.maxDate.setDate(this.maxDate.getDate() - 1);
    this.corpData = this.passSer.editCorp;
    this.dealsData = this.passSer.editDeals;
    this.dropdownList = this.passSer.featureList;
    this.durationList = this.passSer.durationList;
    this.packageList = this.passSer.packageList;
    this.gstList = this.passSer.gstList;
    this.renewList = this.passSer.renewDeals;
    this.userData = JSON.parse(localStorage.getItem("UserData"));
    this.bowserInfo = this.browserInfoService.get_browser();
    console.log("this.corpData", this.corpData);


  }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    if (this.dealsData[0]) {
      console.log('this.dealsData[0]', this.dealsData[0]);
      this.selectedItems = this.dealsData[0].featureList;
      for (let i = 0; i < this.selectedItems.length; i++) {
        this.tempfeature.push(this.selectedItems[i].featureId);
      }
      // const tempObj = {
      //   featureId: 2, featurename: "Polls",
      //   // featureId: 3, featurename: "Forums"
      // };
      // this.selectedItems.push(tempObj)
      this.feaCostArray = [];
      for (let i = 0; i < this.dropdownList.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (
            this.selectedItems[j].featureId === this.dropdownList[i].featureId
          ) {
            this.feaCostArray.push(this.dropdownList[i]);
          }
        }
      }
      this.pkgPrice = this.dealsData[0].selPckage.price;
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }
      this.registerForm = this.formBuilder.group({
        trackId: [
          { value: this.dealsData[0].trackId, disabled: true },
          [Validators.required],
        ],
        duration: [this.dealsData[0].durationId, [Validators.required]],
        purDate: [
          {
            value: this.formatDateNew(this.dealsData[0].purDate),
            disabled: true,
          },
          [Validators.required],
        ],
        fromDate: [
          this.formatDateNew(this.dealsData[0].fromDate),
          [Validators.required],
        ],
        toDate: [
          {
            value: this.formatDateNew(this.dealsData[0].toDate)
          },
          [Validators.required],
        ],
        discountPercent: [
          this.dealsData[0].discountPercent,
          [
            Validators.required,
            Validators.pattern("^([0-9]|([1-9][0-9])|100)$"),
          ],
        ],
        userCount: [
          this.dealsData[0].userCount,
          [Validators.required, Validators.pattern("[0-9]*")],
        ],
        totalAmt: [
          { value: this.dealsData[0].totalAmt.toFixed(4), disabled: true },
          [Validators.required],
        ],
        dealstaname: [this.dealsData[0].dealstaId, [Validators.required]],
        status: [this.dealsData[0].isActive, [Validators.required]],
        paytype: [this.dealsData[0].payType],
        recptno: [this.dealsData[0].receiptNo],
        netRecAmt: [this.dealsData[0].totalAmt.toFixed(4) - this.dealsData[0].tdsamount.toFixed(4)],
        instdetails: [this.dealsData[0].instdetails],
        bank: [this.dealsData[0].bank],
        paymtSectn: [this.dealsData[0].paymtSectn],
        tdsamt: [this.dealsData[0].tdsamount],
        tdsrate: [this.dealsData[0].tdsrate],
        paydate: [this.dealsData[0].payDate],
        pkg: [this.dealsData[0].selPckage.pkgId, [Validators.required]],
        grcPir: [this.dealsData[0].gracePeriod],
        gsttype: [this.dealsData[0].gtypeid, [Validators.required]],
        currencyRatio: [this.dealsData[0].currencyRatio]
      });
      // this.todt = this.formatDate(this.dealsData[0].toDate);
      this.maxDate = new Date(this.dealsData[0].purDate);
      this.frmdt = new Date(this.dealsData[0].fromDate);
      this.duration = parseInt(this.dealsData[0].durationId);
      this.discount = this.dealsData[0].discountPercent;
      this.perCount = this.dealsData[0].userCount;
      this.baseAmt = this.dealsData[0].baseAmt;
      this.prdt = new Date(this.dealsData[0].purDate);
      this.todt = new Date(this.dealsData[0].toDate);
      this.grcPir = new Date(this.dealsData[0].gracePeriod);
      this.tempCost = this.pkgPrice * this.perCount;

      this.cgst = this.dealsData[0].cgst;
      this.sgst = this.dealsData[0].sgst;
      this.igst = this.dealsData[0].igst;
      this.gst = this.dealsData[0].gst;

      this.singlePkg = this.dealsData[0].selPckage;
      // this.todt = new Date(preDate.toLocaleDateString());

      if (this.dealsData[0].dealstaname === "Running") {
        this.disablebtn = false;
        this.graceAdd = false;
        // this.checkAdd = true;
        // this.registerForm.controls["fromDate"].disable();
        this.registerForm.controls["duration"].disable();
        this.registerForm.controls["discountPercent"].disable();
        this.registerForm.controls["dealstaname"].disable();
        this.registerForm.controls["userCount"].disable();
        // this.registerForm.controls["status"].disable();
        // this.registerForm.controls["paytype"].disable();
        // this.registerForm.controls["recptno"].disable();
        this.registerForm.controls["netRecAmt"].disable();
        // this.registerForm.controls["instdetails"].disable();
        // this.registerForm.controls["bank"].disable();


        // this.registerForm.controls["tdsamt"].disable();
        // this.registerForm.controls["paydate"].disable();
        this.registerForm.controls["pkg"].disable();
      } else if (this.dealsData[0].dealstaname === "Expired") {
        this.disablebtn = true;
        this.uAdd = true;
        // this.checkAdd = true;
        this.registerForm.controls["fromDate"].disable();
        this.registerForm.controls["duration"].disable();
        this.registerForm.controls["discountPercent"].disable();
        this.registerForm.controls["dealstaname"].disable();
        this.registerForm.controls["userCount"].disable();
        this.registerForm.controls["status"].disable();
        this.registerForm.controls["paytype"].disable();
        this.registerForm.controls["recptno"].disable();
        this.registerForm.controls["netRecAmt"].disable();
        this.registerForm.controls["instdetails"].disable();
        this.registerForm.controls["bank"].disable();
        this.registerForm.controls["paymtSectn"].disable();

        this.registerForm.controls["tdsamt"].disable();
        this.registerForm.controls["tdsrate"].disable();


        this.registerForm.controls["paydate"].disable();
        this.registerForm.controls["pkg"].disable();
        this.registerForm.controls["grcPir"].disable();
        this.registerForm.controls["toDate"].disable();
        this.registerForm.controls["currencyRatio"].disable();
      } else {
        this.registerForm.controls["grcPir"].disable();
      }
    } else if (this.renewList[0]) {
      this.disDcnt = true;
      this.selectedItems = this.renewList[0].featureList;
      for (let i = 0; i < this.selectedItems.length; i++) {
        this.tempfeature.push(this.selectedItems[i].featureId);
      }
      // const tempObj = {
      //   featureId: 2, featurename: "Polls",
      //   // featureId: 3, featurename: "Forums"
      // };
      // this.selectedItems.push(tempObj)
      this.feaCostArray = [];
      for (let i = 0; i < this.dropdownList.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (
            this.selectedItems[j].featureId === this.dropdownList[i].featureId
          ) {
            this.feaCostArray.push(this.dropdownList[i]);
          }
        }
      }
      console.log("this.featuresCost", this.feaCostArray);
      this.pkgPrice = this.renewList[0].selPckage.price;
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }
      let preDate = new Date(this.renewList[0].toDate);
      preDate.setDate(preDate.getDate() - 1);
      preDate.setMonth(preDate.getMonth() + this.renewList[0].durationId);
      const lastdate = preDate.toLocaleDateString();
      this.registerForm = this.formBuilder.group({
        trackId: [{ value: this.trkId, disabled: true }, [Validators.required]],
        duration: [this.renewList[0].durationId, [Validators.required]],
        purDate: [
          { value: this.todayDate, disabled: true },
          [Validators.required],
        ],
        fromDate: [
          this.formatDateNew(this.renewList[0].toDate),
          [Validators.required],
        ],
        toDate: [
          { value: this.formatDateNew(lastdate) },
          [Validators.required],
        ],
        discountPercent: [
          this.renewList[0].discountPercent,
          [
            Validators.required,
            Validators.pattern("^([0-9]|([1-9][0-9])|100)$"),
          ],
        ],
        userCount: [
          this.renewList[0].userCount,
          [Validators.required, Validators.pattern("[0-9]*")],
        ],
        totalAmt: [
          { value: this.renewList[0].totalAmt.toFixed(4), disabled: true },
          [Validators.required],
        ],
        dealstaname: ["1", [Validators.required]],
        status: ["1", [Validators.required]],
        paytype: [""],
        recptno: [""],
        netRecAmt: [""],

        instdetails: [""],
        bank: [""],
        paymtSectn: [""],
        tdsamt: [""],
        tdsrate: [""],
        paydate: [""],
        pkg: [this.renewList[0].selPckage.pkgId, [Validators.required]],
        gsttype: [this.renewList[0].gtypeid, [Validators.required]],
        grcPir: [""],
        currencyRatio: [""],
      });
      // this.todt = this.formatDate(this.dealsData[0].toDate);
      this.duration = parseInt(this.renewList[0].durationId);
      this.discount = this.renewList[0].discountPercent;
      this.perCount = this.renewList[0].userCount;
      this.baseAmt = this.renewList[0].baseAmt;
      this.prdt = new Date();
      this.frmdt = new Date(this.renewList[0].toDate);
      this.frmdt.setDate(this.frmdt.getDate() + 1);
      this.todt = new Date(lastdate);
      this.tempCost = this.pkgPrice * this.perCount;

      // this.todt = new Date(preDate.toLocaleDateString());
      // this.disablebtn = true;
      this.graceAdd = true;
      this.uAdd = true;

      this.cgst = this.renewList[0].cgst;
      this.sgst = this.renewList[0].sgst;
      this.igst = this.renewList[0].igst;
      this.gst = this.renewList[0].gst;

      this.singlePkg = this.renewList[0].selPckage;
      // this.checkAdd = true;
      // this.registerForm.controls['fromDate'].disable();
      this.registerForm.controls["duration"].disable();
      // this.registerForm.controls['discountPercent'].disable();
      // this.registerForm.controls['dealstaname'].disable();
      this.registerForm.controls["userCount"].disable();
      // this.registerForm.controls['status'].disable();
      // this.registerForm.controls['paytype'].disable();
      // this.registerForm.controls['recptno'].disable();
      // this.registerForm.controls['paydate'].disable();
      this.registerForm.controls["pkg"].disable();

      // this.checkAdd = true;
      // this.registerForm.controls['fromDate'].disable();
      // this.registerForm.controls['duration'].disable();
      // this.registerForm.controls['discountPercent'].disable();
      // this.registerForm.controls['dealstaname'].disable();
      // this.registerForm.controls['userCount'].disable();
      // this.registerForm.controls['status'].disable();
      // this.registerForm.controls['paytype'].disable();
      // this.registerForm.controls['recptno'].disable();
      // this.registerForm.controls['paydate'].disable();
      // this.registerForm.controls['pkg'].disable();
      this.registerForm.controls["grcPir"].disable();
    } else {
      this.checkAdd = true;
      this.feaAdd = true;
      this.todayDate = this.formatDate(this.todayDate);
      this.fromDate = this.formatDate(this.fromDate);
      this.prdt = new Date();
      console.log("this.todayDate", this.todayDate);
      this.registerForm = this.formBuilder.group({
        trackId: [{ value: this.trkId, disabled: true }, [Validators.required]],
        duration: ["", [Validators.required]],
        purDate: [
          { value: this.todayDate, disabled: true },
          [Validators.required],
        ],
        fromDate: [
          { value: this.formatDate(this.fromDate) },
          [Validators.required],
        ],
        toDate: ["", [Validators.required]],
        discountPercent: [
          "0",
          [
            Validators.required,
            Validators.pattern("^([0-9]|([1-9][0-9])|100)$"),
          ],
        ],
        userCount: ["", [Validators.required, Validators.pattern("[1-9]*")]],
        totalAmt: ["", [Validators.required]],
        dealstaname: ["1", [Validators.required]],
        status: ["1", [Validators.required]],
        paytype: [""],
        recptno: [""],
        netRecAmt: [""],
        instdetails: [""],
        bank: [""],
        paymtSectn: [""],
        tdsamt: [""],
        tdsrate: [""],

        paydate: [""],
        pkg: ["", [Validators.required]],
        grcPir: [""],
        gsttype: [this.gstList[0] ? this.gstList[0].gtypeid : '', [Validators.required]],
        currencyRatio: [""],
      });

      var splitGST = this.gstList[0].percent.split('+');

      console.log('splitGST', splitGST);
      this.cgst = parseFloat(splitGST[0]);
      this.sgst = parseFloat(splitGST[1]);
      this.igst = 0;
      this.gst = this.cgst + this.sgst + this.igst;

      // this.registerForm.controls["toDate"].disable();
      this.registerForm.controls["totalAmt"].disable();
      this.registerForm.controls["grcPir"].disable();
    }
    this.fetchSubscribers();

    this.registerForm.get('totalAmt').valueChanges.subscribe(val => {
      let tdsCalcluation = this.registerForm.controls.totalAmt.value.toFixed(4)

      this.registerForm.controls['netRecAmt'].setValue(tdsCalcluation);

    })
    this.registerForm.get('tdsamt').valueChanges.subscribe(val => {
      let tdsCalcluation = this.registerForm.controls.totalAmt.value - val

      this.registerForm.controls['netRecAmt'].setValue(tdsCalcluation.toFixed(4));

    })
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.selectedItems1.length === 0) {
      this.isCheck1 = "Please select users";
    } else if (
      this.selectedItems1.length !==
      parseInt(this.registerForm.controls.userCount.value, 10)
    ) {
      this.toastr.warning(
        "No. of users entered doesnt match with no. of selected users"
      );
      return;
    } else if (this.registerForm.invalid) {
      return;
    } else {
      this.hidePopup = false;
      this.confirmList.countUser = this.registerForm.controls.userCount.value;
      this.confirmList.amountBase = this.baseAmt;
      this.confirmList.discountPer = this.registerForm.controls.discountPercent.value;
      this.confirmList.gstNo = this.gst;
      this.confirmList.totalAmount = this.registerForm.controls.totalAmt.value;
      this.confirmList.baseAmt = this.tempCost;
      this.confirmList.pkg = this.singlePkg;
    }
  }

  get f() {
    return this.registerForm.controls;
  }

  back() {
    this.dealsData = [];
    this.dealsData = [];
    this.router.navigate(["pages/corporations/deals"]);
  }

  onItemSelect(event) {
    if (event.featureId) {
      this.feaCostArray = [];
      console.log("select", this.selectedItems);
      this.isCheck = "";
      for (let i = 0; i < this.dropdownList.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (
            this.selectedItems[j].featureId === this.dropdownList[i].featureId
          ) {
            this.feaCostArray.push(this.dropdownList[i].subname);
          }
        }
      }
      console.log("this.featuresCost", this.feaCostArray);
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }
      console.log("this.featuresCost", this.featuresCost);
      this.calculate();
    }
    if (event.subId) {
      console.log("select1", this.selectedItems1);
      // for(let i=0; i< this.selectedItems1.length; i++) {
      //   var res = this.selectedItems1[i].data.split(" ");
      //   this.selectedItems1[i].data = res[0];

      // }

      this.isCheck1 = "";
    }
  }
  onItemDeSelect(des) {
    if (des.featureId) {
      this.feaCostArray = [];
      for (let i = 0; i < this.selectedItems.length; i++) {
        if (des.featureId === this.selectedItems[i].featureId) {
          this.selectedItems.splice(i, 1);
        }
      }
      console.log("deselect", this.selectedItems);
      if (this.selectedItems.length === 0) {
        this.isCheck = "Please select features";
      }
      for (let i = 0; i < this.dropdownList.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (
            this.selectedItems[j].featureId === this.dropdownList[i].featureId
          ) {
            this.feaCostArray.push(this.dropdownList[i]);
          }
        }
      }
      console.log("this.featuresCost", this.feaCostArray);
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }
      this.calculate();
    }
    if (des.subId) {
      for (let i = 0; i < this.selectedItems1.length; i++) {
        if (des.subId === this.selectedItems1[i].featureId) {
          this.selectedItems1.splice(i, 1);
        }
      }
      console.log("deselect1", this.selectedItems1);
      if (this.selectedItems1.length === 0) {
        this.isCheck1 = "Please select users";
      }
    }
  }
  onSelectAll(allSelect) {
    if (allSelect.featureId) {
      this.feaCostArray = [];
      // if (this.selectedItems.length === 0) {
      //   this.selectedItems = allSelect;
      // } else {
      //   this.selectedItems = [];
      // }
      this.isCheck = "";
      this.selectedItems = allSelect;
      console.log("selectAll", this.selectedItems);
      for (let i = 0; i < this.dropdownList.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (
            this.selectedItems[j].featureId === this.dropdownList[i].featureId
          ) {
            this.feaCostArray.push(this.dropdownList[i]);
          }
        }
      }
      console.log("this.featuresCost", this.feaCostArray);
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }
      this.calculate();
    }
    if (allSelect.subId) {
      this.isCheck1 = "";
      this.selectedItems1 = allSelect;
      // for(let i=0; i< this.selectedItems1.length; i++) {
      //   var res = this.selectedItems1[i].data.split(" ");
      //   this.selectedItems1[i].data = res[0];

      // }

      console.log("selectAll", this.selectedItems1);
    }
  }

  checkPackage(pkg) {
    if (pkg.target.value) {
      for (let i = 0; i < this.packageList.length; i++) {
        if (parseInt(pkg.target.value, 10) === this.packageList[i].pkgId) {
          this.singlePkg = this.packageList[i];
        }
      }
      if (this.singlePkg) {
        this.duration = this.singlePkg.duration;
        this.getToDate(this.registerForm.controls.fromDate.value);
        // this.tempfeature = this.singlePkg.featureId.split("|").map(Number);
        this.selectedItems = [];
        // if (this.tempfeature.length > 0) {
        //   for (let i = 0; i < this.dropdownList.length; i++) {
        //     for (let j = 0; j < this.tempfeature.length; j++) {
        //       if (this.tempfeature[j] === this.dropdownList[i].featureId) {
        //         this.selectedItems.push(this.dropdownList[i]);
        //         this.feaAdd = false;
        //       }
        //     }
        //   }
        // }

        const gstNO = this.corpData[0].gst.substring(0, 2)
        if (this.singlePkg.isOverseas == 1) {
          if (gstNO == 27) {
            this.registerForm.controls.gsttype.setValue(this.gstList[0].gtypeid)
          } else {
            this.registerForm.controls.gsttype.setValue(this.gstList[1].gtypeid)
          }
        } else {
          this.registerForm.controls.gsttype.setValue(null)
          // this.registerForm.controls.gsttype.clearValidators()
          // this.registerForm.controls.gsttype.updateValueAndValidity()
        }

      }
    }

    this.calculate();
    // this.calculateAmount(this.singlePkg)
  }

  setDisc(disc) {
    if (!disc.target.value) {
      this.toastr.error("Discount is required");
    } else {
      this.discount = parseInt(disc.target.value);
      this.calculate();
    }
  }

  setPerson(Per) {
    if (!Per.target.value) {
      this.toastr.error("No. of Persons is required");
    } else {
      this.perCount = parseInt(Per.target.value);
      this.calculate();
    }
  }

  formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [month, day, year].join("/");
  }

  formatDateNew(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;
    return [day, month, year].join("/");
  }

  formatDateNewDash(date) {
    if (date) {
      var d = new Date(date),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = "0" + month;
      if (day.length < 2) day = "0" + day;
      return [year, month, day].join("-");
    } else {
      return '';
    }


  }

  checkDuration(duration) {
    this.duration = parseInt(duration.target.value);
    this.getToDate(this.registerForm.controls.fromDate.value);
    this.calculate();
  }

  chnageGST(gst) {
    var gst1 = parseInt(gst.target.value);
    console.log('gst1', gst1);
    if (gst1 === 1) {
      var splitGST = this.gstList[0].percent.split('+');

      console.log('splitGST', splitGST);
      this.cgst = parseFloat(splitGST[0]);
      this.sgst = parseFloat(splitGST[1]);
      this.igst = 0;
      this.gst = this.cgst + this.sgst + this.igst;
    } else if (gst1 === 2) {

      this.cgst = 0;
      this.sgst = 0;
      this.igst = parseFloat(this.gstList[1].percent);
      this.gst = this.cgst + this.sgst + this.igst;
    }
    this.calculate();
  }
  giveDateFormat(date) {
    if (date == null) {
      var dateObj = new Date();
      var month = dateObj.getUTCMonth() + 1; //months from 1-12
      var day = dateObj.getUTCDate();
      var year = dateObj.getUTCFullYear();

      return year + "/" + month + "/" + day;
    } else {
      var dateObj = new Date(date);
      var month = dateObj.getUTCMonth() + 1; //months from 1-12
      var day = dateObj.getUTCDate();
      var year = dateObj.getUTCFullYear();

      return year + "/" + month + "/" + day;
    }

  }

  changeFromDate(date) {
    console.log(date.target.value);
    let pastDate = new Date(date.target.value);
    this.frmdt = new Date(pastDate.toLocaleDateString());
    pastDate.setMonth(pastDate.getMonth() + this.duration);
    let todayDate = this.giveDateFormat(null);

    let changeDAte = new Date(this.frmdt);
    changeDAte.setDate(changeDAte.getDate() + 1)


    let fromDate = this.giveDateFormat(changeDAte);
    let Todate = this.giveDateFormat(this.todt);

    if (fromDate == todayDate) {
      this.registerForm.controls.dealstaname.setValue('2');
    } else if (Todate < todayDate) {
      this.registerForm.controls.dealstaname.setValue('3');

    }
    // else {
    //   this.registerForm.controls.dealstaname.setValue('1');

    // }

    this.registerForm.controls.toDate.setValue(pastDate.toLocaleDateString());
    this.registerForm.controls.grcPir.setValue(pastDate.toLocaleDateString());
    this.todt = new Date(pastDate.toLocaleDateString());
    this.grcPir = new Date(pastDate.toLocaleDateString());
  }
  changeToDate(date) {
    console.log(date.target.value);
    let pastDate = new Date(date.target.value);
    this.todt = new Date(pastDate.toLocaleDateString());
    this.registerForm.controls.grcPir.setValue(pastDate.toLocaleDateString());
    this.grcPir = new Date(pastDate.toLocaleDateString());
    let today = new Date()
    if (this.dealsData.length > 0) {
      if (this.formatDateNewDash(today) > this.formatDateNewDash(pastDate)) {
        this.registerForm.controls.dealstaname.setValue('3');
      } else {
        this.registerForm.controls.dealstaname.setValue(this.dealsData[0].dealstaId)
      }
    } else {
      if (this.formatDateNewDash(today) > this.formatDateNewDash(pastDate)) {
        this.registerForm.controls.dealstaname.setValue('3');
      } else {
        this.registerForm.controls.dealstaname.setValue('2')
      }
    }




  }
  changerange(date) {
    console.log(date.target.value);
    let pastDate = new Date(date.target.value);

    let today = new Date()
    if (today > pastDate) {
      this.registerForm.controls.dealstaname.setValue('3');
    } else {
      this.registerForm.controls.dealstaname.setValue(this.dealsData[0].dealstaId)
    }

  }
  getToDate(date) {
    let pastDate;
    if (this.duration) {
      if (date.target) {
        if (!date.target.value) {
          this.toastr.error("From Date is required");
        } else {
          pastDate = new Date(date.target.value);
        }
      } else {
        pastDate = new Date(date);
      }
      this.frmdt = new Date(pastDate.toLocaleDateString());
      let todayDate = this.giveDateFormat(null);

      let changeDAte = new Date(this.frmdt);
      changeDAte.setDate(changeDAte.getDate() + 1)


      let fromDate = this.giveDateFormat(changeDAte);
      let Todate = this.giveDateFormat(this.todt);
      if (fromDate == todayDate) {
        this.registerForm.controls.dealstaname.setValue('2');
      } else if (Todate < todayDate) {
        this.registerForm.controls.dealstaname.setValue('3');

      }
      // else {
      //   this.registerForm.controls.dealstaname.setValue('1');

      // }




      console.log('this.frmdt', this.frmdt);
      pastDate.setMonth(pastDate.getMonth() + this.duration);
      pastDate.setDate(pastDate.getDate() - 1);
      console.log(pastDate.toLocaleDateString());
      this.registerForm.controls.toDate.setValue(pastDate.toLocaleDateString());
      this.registerForm.controls.grcPir.setValue(pastDate.toLocaleDateString());
      this.todt = new Date(pastDate.toLocaleDateString());
      this.grcPir = new Date(pastDate.toLocaleDateString());
    }
  }

  calculateAmount(singlePkg) {
    let grossCost
    let totalCost
    console.log(singlePkg)
    if (singlePkg.duration) {
      let tempVal = singlePkg.price ? singlePkg.price : 0
      tempVal = tempVal * this.registerForm.controls.userCount.value ? this.registerForm.controls.userCount.value : 0;
      grossCost = tempVal - tempVal * (singlePkg.discount / 100);
      totalCost = grossCost + grossCost * (this.gst / 100);
      totalCost = totalCost.toFixed(4);
      this.registerForm.controls.totalAmt.setValue(totalCost);
      console.log('totalCost', totalCost)

    }
  }

  calculate() {
    if (!this.duration) {
      this.toastr.warning('Please select duration');
    } else {
      let grossCost;
      let totalCost;
      if (this.singlePkg) {
        this.tempCost = this.singlePkg.price ? this.singlePkg.price : 0;
        this.tempCost = this.tempCost * this.perCount;
      } else {
        this.tempCost = this.pkgPrice * this.perCount;
      }
      grossCost = this.tempCost - this.tempCost * (this.discount / 100);
      this.baseAmt = this.tempCost;
      if (this.singlePkg.isOverseas == 1) {
        this.gst = 0
      }
      totalCost = grossCost + grossCost * (this.gst / 100);
      totalCost = totalCost.toFixed(4);
      console.log('totalCost', totalCost)
      this.registerForm.controls.totalAmt.setValue(totalCost);
    }
  }

  hideinvoice() {
    this.hidePopup = true;
  }

  featuretotal(price) {
    return (price * this.confirmList.countUser).toFixed(0);
  }
  percent(baseAmount, percent) {
    return ((baseAmount / 100) * percent).toFixed(0);
  }

  gstpercent() {
    let discount =
      (this.confirmList.baseAmt / 100) * this.confirmList.discountPer;
    let accprice = this.confirmList.baseAmt - discount;
    this.confirmList.accprice = accprice;
    this.confirmList.gstvalue = (accprice / 100) * this.confirmList.gstNo;
    return ((accprice / 100) * this.confirmList.gstNo).toFixed(0);
  }

  GrandTotal() {
    return (this.confirmList.accprice + this.confirmList.gstvalue).toFixed(0);
  }

  fetchSubscribers() {
    this.spinner.show();
    const params = {
      corid: 0,
      TrackId: this.registerForm.controls.trackId.value
        ? this.registerForm.controls.trackId.value
        : 0,
      uid: this.userData.userid,
      uname: this.userData.subname,
      bname: this.bowserInfo.name,
      bversion: this.bowserInfo.version,
    };
    if (this.corpData[0]) {
      if (this.corpData[0].corpId) {
        params.corid = this.corpData[0].corpId;
      }
    }

    if (this.renewList[0]) {
      if (this.renewList[0].trackId) {
        params.TrackId = this.renewList[0].trackId;
      }
    }

    // params.fomdate = this.formatDate(this.registerForm.controls.fromDate.value);
    console.log("params", params);
    this.dealSubSer.fetchDealSubscriber(params).then(
      (res) => {
        this.spinner.hide();
        try {
          console.log("res", res);
          if (res.type === true && res.status_code === 200) {
            console.log("res", res);
            try {
              if (res.allsubs.length > 0) {
                this.dropdownList1 = res.allsubs;
                for (let i = 0; i < this.dropdownList1.length; i++) {
                  let status = this.dropdownList1[i].status == 1 ? 'Active' : 'Not Active'
                  this.dropdownList1[i].data = this.dropdownList1[i].subname + ' ' + this.dropdownList1[i].phoneno + ' ' + this.dropdownList1[i].statusSub;
                }
                console.log('this.dropdownlist', this.dropdownList1);
              }
              if (res.dealsubs.length > 0) {
                this.selectedItems1 = res.dealsubs;
                // for(let i =0; i < this.selectedItems1.length; i++) {
                //   let status = this.selectedItems1[i].status == 1 ? 'Active' : 'Not Active'
                //   this.selectedItems1[i].data = this.selectedItems1[i].subname; 
                // }
              }
              // if (this.dealsData[0]) {
              //   this.dropdownList1 = this.selectedItems1.concat(
              //     this.dropdownList1
              //   );
              // }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log("res", res);
            this.toastr.error("Server Error");
          }
        } catch (e) {
          console.log(e);
        }
      },
      (err) => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error("Server Error");
      }
    );
  }

  confirmSubmit() {
    this.spinner.show();
    var uList = [];
    // for (let i = 0; i < this.feaCostArray.length ; i++) {
    //   this.fidList.push(this.feaCostArray[i].featureId);
    // }
    for (let i = 0; i < this.selectedItems1.length; i++) {
      uList.push(this.selectedItems1[i].subId);
    }
    console.log('registerForm', this.registerForm);
    const params = {
      tid: 0,
      trkid: this.registerForm.controls.trackId.value,
      corpid: this.corpData[0].corpId,
      corcod: this.corpData[0].corpCode,
      sid: null,
      purcdate: this.formatDateNewDash(
        this.registerForm.controls.purDate.value
      ),
      fomdate: this.formatDateNewDash(
        this.registerForm.controls.fromDate.value
      ),
      todate: this.formatDateNewDash(this.registerForm.controls.toDate.value),
      graceDate: this.registerForm.controls.grcPir.value
        ? this.formatDateNewDash(this.registerForm.controls.grcPir.value)
        : this.formatDateNewDash(this.registerForm.controls.toDate.value),
      duraid: this.duration,
      noPerson: this.registerForm.controls.userCount.value,
      baAmt: this.baseAmt,
      discpre: this.registerForm.controls.discountPercent.value,
      gstno: this.gst,
      totamt: this.registerForm.controls.totalAmt.value,
      paytype: this.registerForm.controls.paytype.value,
      receiptno: this.registerForm.controls.recptno.value,
      tdsamt: this.registerForm.controls.tdsamt.value,
      tdsrate: this.registerForm.controls.tdsrate.value,
      paydate: this.formatDateNewDash(this.registerForm.controls.paydate.value),
      isAct: this.registerForm.controls.status.value,
      deastId: this.registerForm.controls.dealstaname.value,
      instdetails: this.registerForm.controls.instdetails.value,
      bank: this.registerForm.controls.bank.value,
      paymtSectn: this.registerForm.controls.paymtSectn.value,
      // feaIds : this.fidList.join('|'),
      // fealen : this.fidList.length,
      feaIds: this.tempfeature.join("|"),
      fealen: this.tempfeature.length,
      pkgid: parseInt(this.registerForm.controls.pkg.value, 10),
      subIds: uList.join("|"),
      sublen: uList.length,
      uid: this.userData.userid,
      uname: this.userData.subname,
      bname: this.bowserInfo.name,
      bversion: this.bowserInfo.version,
      sgst: this.sgst,
      cgst: this.cgst,
      igst: this.igst,
      gtypeid: this.registerForm.controls.gsttype.value,
      tdsamount: this.registerForm.controls.tdsamt.value,
      tdsrat: this.registerForm.controls.tdsrate.value,
      payRecDate: null,
      panNo: null,
      gross: null,
      deducted: 10,
      netAmount: null,
      currencyRatio: this.registerForm.controls.currencyRatio.value
    };
    if (this.dealsData[0]) {
      if (this.dealsData[0].tmId) {
        params.tid = this.dealsData[0].tmId;
      }
    }
    // params.fomdate = this.formatDate(this.registerForm.controls.fromDate.value);
    // for (let i = 0; i < this.durationList.length ; i++) {
    //   if (parseInt(this.registerForm.value.duration) === this.durationList[i].duration) {
    //     params.duraid = this.durationList[i].durationId;
    //   }
    // }
    console.log("params", params);
    this.dealSer.addEditDeals(params).then(
      (res) => {
        this.spinner.hide();
        try {
          console.log("res", res);
          if (res.type === true && res.status_code === 200) {
            console.log("res", res);
            try {
              if (res.data[0]) {
                if (res.data[0].updated) {
                  this.router.navigate(["pages/corporations/deals"]);
                  this.toastr.success(res.data[0].updated);
                } else if (res.data[0].saved) {
                  this.router.navigate(["pages/corporations/deals"]);
                  this.toastr.success(res.data[0].saved);
                }
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log("res", res);
            this.toastr.error("Server Error");
          }
        } catch (e) {
          console.log(e);
        }
      },
      (err) => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error("Server Error");
      }
    );
  }
}

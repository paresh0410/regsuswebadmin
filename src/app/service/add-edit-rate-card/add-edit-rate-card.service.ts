import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class AddEditRateCardService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addeditRateCardUrl = this.baseUrl + webApi.apiUrl.addEditRateCard;
  constructor(public http: HttpClient) { }
  public addeditRateCard(rateCardData: any): Promise<any> {

    return this.http.post(this.addeditRateCardUrl, rateCardData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

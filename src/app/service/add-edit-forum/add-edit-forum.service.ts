import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class AddEditForumService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addEditForumUrl = this.baseUrl + webApi.apiUrl.addEditForum;
  constructor(public http: HttpClient) { }
  public addEditForum(forumData: any): Promise<any> {

    return this.http.post(this.addEditForumUrl, forumData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

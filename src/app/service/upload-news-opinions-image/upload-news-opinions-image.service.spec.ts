import { TestBed } from '@angular/core/testing';

import { UploadNewsOpinionsImageService } from './upload-news-opinions-image.service';

describe('UploadNewsOpinionsImageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadNewsOpinionsImageService = TestBed.get(UploadNewsOpinionsImageService);
    expect(service).toBeTruthy();
  });
});

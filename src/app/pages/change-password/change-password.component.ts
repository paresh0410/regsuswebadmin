import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoginService } from '../../service/login/login.service';

@Component({
  selector: 'ngx-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  public type2 = 'password';
  public showPass = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,

    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private loginSer: LoginService,
  ) { }

  ngOnInit() {

    this.changePasswordForm = this.formBuilder.group({
      oldpass: ['', Validators.required],
      password: ['', Validators.required],
      recPass: ['', Validators.required],
    });

  }

  get f() { return this.changePasswordForm.controls; }


  goBack() {
    window.history.back();
  }

  showPassword() {
    this.showPass = !this.showPass;
    if (this.showPass) {
      this.type2 = 'text';
    } else {
      this.type2 = 'password';
    }
  }

  onSubmit() {
    console.log('new')

    if (this.changePasswordForm.value.password !== this.changePasswordForm.value.recPass) {
      this.toastr.error('New password and confirmed password is not matching');
      return;
    }
    if (this.changePasswordForm.invalid) {
      // this.spinner.hide();
      if (!this.changePasswordForm.value.email) {
        this.toastr.error('email is required');
      }
      return;
    } else if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    }
    else {
      this.spinner.show();
      const name = JSON.parse(localStorage.getItem("UserName"));
      const useremail = JSON.parse(localStorage.getItem("UserData"));
      const params = {
        user: name,
        email: useremail.subemail,
        pass: this.changePasswordForm.value.password,
        oldpass: this.changePasswordForm.value.oldpass,
      }
      console.log('params', params);

      this.loginSer.changePassword(params).then(res => {
        this.spinner.hide();

        try {
          if (res.type === true) {
            console.log('res', res);
            try {

              if (res.usercount == 0) {
                this.toastr.success('Invalid credentails');

              } else {
                this.toastr.success('New password has set');

                this.router.navigate(['/login']);
                localStorage.clear();


              }




              // setTimeout(() => {
              //   /** spinner ends after 5 seconds */
              //   this.toastr.success(res.message);
              // }, 1000);
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });

    }

  }

}

import { TestBed } from '@angular/core/testing';

import { FetchPollsService } from './fetch-polls.service';

describe('FetchPollsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchPollsService = TestBed.get(FetchPollsService);
    expect(service).toBeTruthy();
  });
});

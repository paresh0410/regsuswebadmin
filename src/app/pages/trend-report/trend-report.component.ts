import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../service/pass-serv.service';
import { BrowserInfoService } from '../../service/browser-info.service';
import { ExcelService } from '../../service/excel.service';
import { DashboardService } from '../../service/dashboard/dashboard.service';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';


export const MY_CUSTOM_FORMATS = {
  // parseInput: 'LL LT',
  // fullPickerInput: 'LL LT',
  // datePickerInput: 'LL',
  parseInput: 'DD-MM-YYYY',
  fullPickerInput: 'DD-MM-YYYY',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};

@Component({
  selector: 'ngx-trend-report',
  templateUrl: './trend-report.component.html',
  styleUrls: ['./trend-report.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
      // `MomentDateTimeAdapter` can be automatically provided by importing
      // `OwlMomentDateTimeModule` in your applications root module. We provide it at the component level
      // here, due to limitations of our example generation script.
      {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},

      {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS},
  ],
})
export class TrendReportComponent implements OnInit {
  registerForm: any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,

               private spinner: NgxSpinnerService,
               private toastr: ToastrService,
               private dashboard: DashboardService,
               private passService: PassServService,
               private browserInfoService: BrowserInfoService,
               private excelService: ExcelService,
                public cdf: ChangeDetectorRef
  ) { }

  ngOnInit() {

    const today = new Date();
    const end = this.convert(today);
    const start = this.convert(today.setMonth(today.getMonth() - 6));

    this.registerForm = this.formBuilder.group({
      startDt: [start, [Validators.required]],
      endDt: [end, [Validators.required]],
    });

    this.startDate = this.convert(this.registerForm.value.startDt);
    this.endDate = this.convert(this.registerForm.value.endDt);

   

    this.fetchTrendActivity(this.startDate, this.endDate)
    // '2019-01-23', '2020-11-01'

  }

  maxDate: any = new Date();


  // bar test 
  public barChartOptions: ChartOptions = {
    responsive: true,
   
  };

  public fourbarChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false
   
  };
  public barChartLabels: Label[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public singlebarChartLegend = false;
  public barChartPlugins = [];

  //
  public totRegisChartLabels: Label[] = [];
  public totRegisChartData: ChartDataSets[] = [];


  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56], label: 'Bulk', stack: 'a' },
    { data: [28, 48, 40, 19, 86], label: 'Non Bulk', stack: 'a' }
  ];

  activityLists: any;
  trailList: any;
  public invoicebarChartData: ChartDataSets[] = []
  //   { data: [65, 59, 80, 81, 56], label: 'New subscription', stack: 'a' },
  //   { data: [28, 48, 40, 19, 86], label: 'Renewals', stack: 'a' },
  //   { data: [28, 48, 40, 19, 30], label: 'Upgrades', stack: 'a' }
  // ];


  public gstbarChartData: ChartDataSets[] = []
  //   { data: [65, 59, 80, 81, 20], label: 'IGST', stack: 'a' },
  //   { data: [28, 48, 59, 20, 81], label: 'CGST', stack: 'a' },
  //   { data: [28, 48, 40, 19, 30], label: 'SGST', stack: 'a' }
  // ];

  

  public singlebarChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56], label: 'Deals Count' , stack: 'a' },
   
  ];

  startDate : any;
  endDate : any;

  public bulkDealData: ChartDataSets[] = [];

  public activeSubsChartData: ChartDataSets[] = [];

  
  public activeNewsChartData: ChartDataSets[] = [];
  
  public renewalsChartData: ChartDataSets[] = [];

  public nonRenewalChartData: ChartDataSets[] = [];

  public multiplebarChartData: ChartDataSets[] = []
  //   { data: [65, 59, 80, 81, 56], label: 'News' },
  //   { data: [28, 48, 40, 19, 86], label: 'Opinion' },
  //   { data: [65, 59, 80, 81, 56], label: 'Polls' },
  //   { data: [28, 48, 40, 19, 86], label: 'Forums' },
   
  // ];


    // bar test 




  // public pieChartOptions: ChartOptions = {
  //   responsive: true,
  //   legend: {
  //     position: 'top',
  //   },
  //   plugins: {
  //     datalabels: {
  //       formatter: (value, ctx) => {
  //         const label = ctx.chart.data.labels[ctx.dataIndex];
  //         return label;
  //       },
  //     },
  //   }
  // };
  // public pieChartLabels: Label[] = [['Download', 'Sales'], ['In', 'Store', 'Sales'], 'Mail Sales'];
  // public pieChartData: number[] = [300, 500, 100];
  // public pieChartType: ChartType = 'pie';
  // public pieChartLegend = true;
  // public pieChartPlugins = [pluginDataLabels];
  // public pieChartColors = [
  //   {
  //     backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)'],
  //   },
  // ];


  // // events
  // public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  //   console.log(event, active);
  // }

  // public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  //   console.log(event, active);
  // }

  // changeLabels() {
  //   const words = ['hen', 'variable', 'embryo', 'instal', 'pleasant', 'physical', 'bomber', 'army', 'add', 'film',
  //     'conductor', 'comfortable', 'flourish', 'establish', 'circumstance', 'chimney', 'crack', 'hall', 'energy',
  //     'treat', 'window', 'shareholder', 'division', 'disk', 'temptation', 'chord', 'left', 'hospital', 'beef',
  //     'patrol', 'satisfied', 'academy', 'acceptance', 'ivory', 'aquarium', 'building', 'store', 'replace', 'language',
  //     'redeem', 'honest', 'intention', 'silk', 'opera', 'sleep', 'innocent', 'ignore', 'suite', 'applaud', 'funny'];
  //   const randomWord = () => words[Math.trunc(Math.random() * words.length)];
  //   this.pieChartLabels = Array.apply(null, { length: 3 }).map(_ => randomWord());
  // }

  // addSlice() {
  //   this.pieChartLabels.push(['Line 1', 'Line 2', 'Line 3']);
  //   this.pieChartData.push(400);
  //   this.pieChartColors[0].backgroundColor.push('rgba(196,79,244,0.3)');
  // }

  // removeSlice() {
  //   this.pieChartLabels.pop();
  //   this.pieChartData.pop();
  //   this.pieChartColors[0].backgroundColor.pop();
  // }

  // changeLegendPosition() {
  //   this.pieChartOptions.legend.position = this.pieChartOptions.legend.position === 'left' ? 'top' : 'left';
  // }

  showtotalReg = false;


  fetchTrendActivity(start, end) {
    this.totRegisChartLabels = [];
    this.spinner.show();
    if (!window.navigator.onLine) {
      this.spinner.hide();
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        FrmDate : start,
        ToDate : end,
      };
      this.dashboard.getDashboardTrendReport(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('getDashboardTrendReport', res);
            try {
            this.activityLists = [];
            this.trailList = res.data;

            let totalReg: any = this.trailList[0];
            let bulk: any = [];
            let nonbulk: any = [];
            for(let j = 0; j < totalReg.length; j++) {
              this.totRegisChartLabels.push(totalReg[j].mon1);
              bulk.push(totalReg[j].bulkRegCount);
              nonbulk.push(totalReg[j].nonBulkRegCount);
            }
            this.totRegisChartData = [
              { data: bulk, label: 'Bulk', stack: 'a' },
              { data: nonbulk, label: 'Non Bulk', stack: 'a' }
            ]



            let totalBulkDeal: any = this.trailList[1];
            let nonbulkDeal: any = [];
            for(let j = 0; j < totalBulkDeal.length; j++) {
              // this.totRegisChartLabels.push(totalReg[j].mon1);
              nonbulkDeal.push(totalBulkDeal[j].actBulkDealCont);
            }
            this.bulkDealData = [
              { data: nonbulkDeal, label: 'Deals Count' , stack: 'a' },
            ]


            
            let totalactiveSub: any = this.trailList[2];
            let bulkActiveSubs: any = [];
            let nonbulkActiveSubs: any = [];
            for(let j = 0; j < totalactiveSub.length; j++) {
              bulkActiveSubs.push(totalactiveSub[j].bulkActRegCount);
              nonbulkActiveSubs.push(totalactiveSub[j].nonBulkActRegCount);
            }
            this.activeSubsChartData = [
              { data: bulkActiveSubs, label: 'Bulk', stack: 'a' },
              { data: nonbulkActiveSubs, label: 'Non Bulk', stack: 'a' }
            ]


            let totalnewSub: any = this.trailList[3];
            let bulkNewSubs: any = [];
            let nonbulkNewSubs: any = [];
            for(let j = 0; j < totalnewSub.length; j++) {
              bulkNewSubs.push(totalnewSub[j].bulkNewSubscCount);
              nonbulkNewSubs.push(totalnewSub[j].nonBulkNewSubscCount);
            }
            this.activeNewsChartData = [
              { data: bulkNewSubs, label: 'Bulk', stack: 'a' },
              { data: nonbulkNewSubs, label: 'Non Bulk', stack: 'a' }
            ]

            

            let totalRenewalSub: any = this.trailList[4];
            let bulkRenewalsSubs: any = [];
            let nonbulkRenewalsSubs: any = [];
            for(let j = 0; j < totalRenewalSub.length; j++) {
              bulkRenewalsSubs.push(totalRenewalSub[j].bulkRenewalsSubscCount);
              nonbulkRenewalsSubs.push(totalRenewalSub[j].nonBulkRenewalsSubscCount);
            }
            this.renewalsChartData = [
              { data: bulkRenewalsSubs, label: 'Bulk', stack: 'a' },
              { data: nonbulkRenewalsSubs, label: 'Non Bulk', stack: 'a' }
            ]


            let totalNonRenewalSub: any = this.trailList[5];
            let bulkNonRenewalsSubs: any = [];
            let nonbulkNonRenewalsSubs: any = [];
            for(let j = 0; j < totalNonRenewalSub.length; j++) {
              bulkNonRenewalsSubs.push(totalNonRenewalSub[j].bulkExpiredCount);
              nonbulkNonRenewalsSubs.push(totalNonRenewalSub[j].nonBulkRenewalsSubscCount);
            }
            this.nonRenewalChartData = [
              { data: bulkRenewalsSubs, label: 'Bulk', stack: 'a' },
              { data: nonbulkRenewalsSubs, label: 'Non Bulk', stack: 'a' }
            ]


           
            let totalInvoice: any = this.trailList[6];
            let Renewals: any = [];
            let Upgrades: any = [];
            let subscription: any = [];
            for(let j = 0; j < totalInvoice.length; j++) {
              subscription.push(totalInvoice[j].totNewSubscReven);
              Renewals.push(totalInvoice[j].totRenewalsSubscripReven);
              Upgrades.push(totalInvoice[j].totUpgradesSubscripReven);

            }
            this.invoicebarChartData = [
              { data: subscription, label: 'New subscription', stack: 'a' },
              { data: Renewals, label: 'Renewals', stack: 'a' },
              { data: Upgrades, label: 'Upgrades', stack: 'a' }
            ]


            let totalGst: any = this.trailList[7];
            let igstValue: any = [];
            let cgstValue: any = [];
            let sgstValue: any = [];
            for(let j = 0; j < totalGst.length; j++) {
              igstValue.push(totalGst[j].igstValue);
              cgstValue.push(totalGst[j].cgstValue);
              sgstValue.push(totalGst[j].sgstValue);

            }

            this.gstbarChartData = [
              { data: igstValue, label: 'IGST', stack: 'a' },
              { data: cgstValue , label: 'CGST', stack: 'a' },
              { data: sgstValue, label: 'SGST', stack: 'a' }
            ];




            let totalPost: any = this.trailList[8];
            let newsCount: any = [];
            let opinionCount: any = [];
            let pollCount: any = [];
            let forumCount: any = [];
            for(let j = 0; j < totalPost.length; j++) {
              newsCount.push(totalPost[j].newsCount);
              opinionCount.push(totalPost[j].opinionCount);
              pollCount.push(totalPost[j].pollCount);
              forumCount.push(totalPost[j].forumCount);
            }

            this.multiplebarChartData = [
              { data: newsCount, label: 'News' },
              { data: opinionCount, label: 'Opinion' },
              { data: pollCount, label: 'Polls' },
              { data: forumCount, label: 'Forums' },
            ];


            this.showtotalReg =true;
            this.cdf.detectChanges();


            console.log('this.trailList',this.trailList);
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
    }
  }


  onSubmit() {
    // this.submitted = true;
    console.log(this.registerForm.value);
    if (this.registerForm.invalid) {
      return;
    } else {
      // this.spinner.show();
      this.startDate = this.convert(this.registerForm.value.startDt);
      this.endDate = this.convert(this.registerForm.value.endDt);
      
      this.fetchTrendActivity(this.startDate,this.endDate);
    }
  }

  exportExcelTotalRegitra() {
    let data = [];
    for (let i = 0; i < this.trailList[0].length; i++) {
      let dataObjforExcel = {
        'MONTH': this.trailList[0][i].mon1,
        'BULK': this.trailList[0][i].bulkRegCount,
        'NON-BULK': this.trailList[0][i].nonBulkRegCount
      };
      data.push(dataObjforExcel);
    }
    this.excelService.exportAsExcelFile(data, 'Total Registration');
  }

  exportExcelActivebulkD() {
    let data = [];
    for (let i = 0; i < this.trailList[0].length; i++) {
      let dataObjforExcel = {
        'MONTH': this.trailList[1][i].mon1,
        'BULK': this.trailList[1][i].actBulkDealCont
      };
      data.push(dataObjforExcel);
    }
    this.excelService.exportAsExcelFile(data, 'Active Bulk Deal');
  }

  exportExcelTotalActiveSubs() {
    let data = [];
    for (let i = 0; i < this.trailList[0].length; i++) {
      let dataObjforExcel = {
        'MONTH': this.trailList[2][i].mon1,
        'BULK': this.trailList[2][i].bulkActRegCount,
        'NON-BULK': this.trailList[2][i].nonBulkActRegCount
      };
      data.push(dataObjforExcel);
    }
    this.excelService.exportAsExcelFile(data, 'Total Active Subscribers');
  }

  exportExcelNewSubs() {
    let data = [];
    for (let i = 0; i < this.trailList[0].length; i++) {
      let dataObjforExcel = {
        'MONTH': this.trailList[3][i].mon1,
        'BULK': this.trailList[3][i].bulkNewSubscCount,
        'NON-BULK': this.trailList[3][i].nonBulkNewSubscCount
      };
      data.push(dataObjforExcel);
    }
    this.excelService.exportAsExcelFile(data, 'New Subscribers');
  }

  exportExcelReneWals() {
    let data = [];
    for (let i = 0; i < this.trailList[0].length; i++) {
      let dataObjforExcel = {
        'MONTH': this.trailList[4][i].mon1,
        'BULK': this.trailList[4][i].bulkRenewalsSubscCount,
        'NON-BULK': this.trailList[4][i].nonBulkRenewalsSubscCount
      };
      data.push(dataObjforExcel);
    }
    this.excelService.exportAsExcelFile(data, 'Renewals');
  }

  exportExcelNonRenewals() {
    let data = [];
    for (let i = 0; i < this.trailList[0].length; i++) {
      let dataObjforExcel = {
        'MONTH': this.trailList[5][i].mon1,
        'BULK': this.trailList[5][i].bulkExpiredCount,
        'NON-BULK': this.trailList[5][i].nonBulkRenewalsSubscCount
      };
      data.push(dataObjforExcel);
    }
    this.excelService.exportAsExcelFile(data, 'Non Renewals');
  }

  exportExcelInvoiceVal() {
    let data = [];
    for (let i = 0; i < this.trailList[0].length; i++) {
      let dataObjforExcel = {
        'MONTH': this.trailList[6][i].mon1,
        'NEW-SUBS-REVENU': this.trailList[6][i].totNewSubscReven,
        'RENEWAL-SUBS-REVENU': this.trailList[6][i].totRenewalsSubscripReven,
        'UPGRADE-SUBS-REVENU': this.trailList[6][i].totUpgradesSubscripReven
      };
      data.push(dataObjforExcel);
    }
    this.excelService.exportAsExcelFile(data, 'Invoice Value');
  }

  exportExcelGSTVal() {
    let data = [];
    for (let i = 0; i < this.trailList[0].length; i++) {
      let dataObjforExcel = {
        'MONTH': this.trailList[7][i].mon1,
        'CGST': this.trailList[7][i].cgstValue,
        'SGST': this.trailList[7][i].sgstValue,
        'IGST': this.trailList[7][i].igstValue
      };
      data.push(dataObjforExcel);
    }
    this.excelService.exportAsExcelFile(data, 'Invoice Value');
  }

  exportExcelPost() {
    let data = [];
    for (let i = 0; i < this.trailList[0].length; i++) {
      let dataObjforExcel = {
        'MONTH': this.trailList[8][i].mon1,
        'NEWS': this.trailList[8][i].newsCount,
        'FORUM': this.trailList[8][i].forumCount,
        'OPINION': this.trailList[8][i].opinionCount,
        'POLL': this.trailList[8][i].pollCount
      };
      data.push(dataObjforExcel);
    }
    this.excelService.exportAsExcelFile(data, 'Posts');
  }

  checkVal(event) {
    console.log('event', event);
  
  }


  convert(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
      console.log([date.getFullYear(), mnth, day].join("-"));
    return [date.getFullYear(), mnth, day].join("-");
  }

  onYearChange(year) {
    console.log(year)
  }

  onMonthChange(month) {
    console.log(month)

  }

  onYearEnd(year) {
    console.log(year)

  }

  onMonthEnd(month) {
    console.log(month)

  }




  
}

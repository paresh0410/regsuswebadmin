/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './pages/login/login.component';
import { ExcelService } from '../app/service/excel.service';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { EventsModule } from 'angular4-events';
import { SafePipePipe } from './safe-pipe.pipe';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { MessagingService } from '../app/service/messaging.service';

import { DeviceDetectorModule } from 'ngx-device-detector';
import { DeviceDetectorService } from 'ngx-device-detector';
import { FcmService } from './service/fcm/fcm.service';
import { ImagepopupComponent } from './pages/imagepopup/imagepopup.component';
import { ImageLibraryPopupComponent } from './pages/image-library-popup/image-library-popup.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// import { AddParticipantsComponent } from './components/add-participants/add-participants.component';
// import { ComponentsModule } from './components/components.module';
const firebaseConfig = {
  apiKey: 'AIzaSyAHkj_5kh6CODWDvXv88aTIQ3hFJlNH7dA',
  authDomain: 'regsusapp.firebaseapp.com',
  databaseURL: 'https://regsusapp.firebaseio.com',
  projectId: 'regsusapp',
  storageBucket: 'regsusapp.appspot.com',
  messagingSenderId: '695210995553',
  appId: '1:695210995553:web:92a8a6d254ed5252dfdf20',
  measurementId: 'G-FY0BB6DWP4'
};
@NgModule({
  declarations: [AppComponent, LoginComponent, SafePipePipe, ImagepopupComponent, ImageLibraryPopupComponent],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxSpinnerModule,
    // AngularMultiSelectModule,
    // ComponentsModule,
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    ToastrModule.forRoot(),
    EventsModule.forRoot(),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    DeviceDetectorModule,
    CommonModule, InfiniteScrollModule
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    ExcelService,
    MessagingService,
    DeviceDetectorService,
    FcmService
  ],
  entryComponents: [ImagepopupComponent, ImageLibraryPopupComponent]
})
export class AppModule {
}

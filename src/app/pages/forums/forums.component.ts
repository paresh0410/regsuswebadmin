import { Component, OnInit, ViewContainerRef, ComponentRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../service/pass-serv.service';
import { FetchForumsService } from '../../service/fetch-forum/fetch-forums.service';
import { SearchForumsService } from '../../service/search-forums/search-forums.service';
import { BrowserInfoService } from '../../service/browser-info.service';
import { ExcelService } from '../../service/excel.service';


@Component({
  selector: 'ngx-forums',
  templateUrl: './forums.component.html',
  styleUrls: ['./forums.component.scss'],
})
export class ForumsComponent implements OnInit {
  forumList = [
    // {
    //   forumname : 'Where can I buy some nice and fashionable Polo T shirts?',
    //   forumDesc : 'Polo T-shirts are a very good choice of fashion wear for all occasions. so many people do not know How to Wear a Classic polo t-shirt. Dont pop your collar. Ever. Avoid unbuttoning all the buttons. The sleeves should fall mid-bicep and be fitted, but not too tight around the arms. Your shirt should fall to your hip. Any longer and itll end up looking loose, and any shorter and it ll ride up when you lift your arms.',
    //   startDate : 'Thu Aug 01 2019 05:30:00 GMT+0530',
    //   endDate : 'Sun Aug 04 2019 05:30:00 GMT+0530',
    //   pollsta : 3,
    //   sta : 'Ended',
    //   replies : 34,
    //   intersetLit : [
    //     { interestId : 2, intersetName : 'Copper'},
    //     { interestId : 3, intersetName : 'Lead'},
    //     { interestId : 4, intersetName : 'Bronze'},
    //   ],
    //   likeCount : 17,
    //   favCount : 8,
    //   isActive : 1,
    // },
    // {
    //   forumname : 'Where Do I Start When I Want to Make Money Online?',
    //   forumDesc : 'A lot of people probably find themselves asking this exact question.. and it can be a tough one to answer.There are many ways to make money online - but which one will work best for you as an entrepreneur?',
    //   startDate : 'Thu Aug 29 2019 05:30:00 GMT+0530',
    //   endDate : 'Fri Aug 30 2019 05:30:00 GMT+0530',
    //   pollsta : 2,
    //   sta : 'Ongoing',
    //   replies : 0,
    //   intersetLit : [
    //     { interestId : 3, intersetName : 'Lead'},
    //     { interestId : 6, intersetName : 'Zinc'},
    //   ],
    //   likeCount : 35,
    //   favCount : 0,
    //   isActive : 1,
    // },
    // {
    //   forumname : 'What is the best virtual phone number provider?',
    //   forumDesc : 'Please tell me, if you know, which company offer the best value for money virtual phone number to publish on my website for clients all over the world to call me?',
    //   startDate : 'Fri Jul 12 2019 05:30:00 GMT+0530',
    //   endDate : 'Sat Jul 13 2019 05:30:00 GMT+0530',
    //   pollsta : 3,
    //   sta : 'Ended',
    //   replies : 57,
    //   intersetLit : [
    //     { interestId : 1, intersetName : 'Aluminium'},
    //     { interestId : 7, intersetName : 'Nickel'},
    //   ],
    //   likeCount : 203,
    //   favCount : 67,
    //   isActive : 1,
    // },
    // {
    //   forumname : 'Are there successful people who are known for being really lazy?',
    //   forumDesc : 'Are there successful people who are known for being really lazy?',
    //   startDate : 'Mon Sep 09 2019 05:30:00 GMT+0530',
    //   endDate : 'Wed Sep 11 2019 05:30:00 GMT+0530',
    //   pollsta : 1,
    //   sta : 'Not Started',
    //   replies : 0,
    //   intersetLit : [
    //     { interestId : 1, intersetName : 'Aluminium'},
    //     { interestId : 4, intersetName : 'Bronze'},
    //     { interestId : 7, intersetName : 'Nickel'},
    //   ],
    //   likeCount : 0,
    //   favCount : 0,
    //   isActive : 1,
    // },
    // {
    //   forumname : 'Searching the best VPN service for small business. Any recommendations?',
    //   forumDesc : 'I started my business half a year ago, and now I want to get a VPN for my business, so I would like to ask for some recommendations, guys.',
    //   startDate : 'Fri Aug 30 2019 05:30:00 GMT+0530',
    //   endDate : 'Sat Aug 31 2019 05:30:00 GMT+0530',
    //   pollsta : 2,
    //   sta : 'Ongoing',
    //   replies : 9,
    //   intersetLit : [
    //     { interestId : 1, intersetName : 'Aluminium'},
    //     { interestId : 3, intersetName : 'Lead'},
    //     { interestId : 5, intersetName : 'Iron'},
    //   ],
    //   likeCount : 50,
    //   favCount : 36,
    //   isActive : 1,
    // },
  ];
  p: number = 1;
  showhide = true;
  today: any = new Date();
  column: any = [];
  search = {
    forumTitle: '',
  };

  search1 = {
    fromdate: '',
    todate: '',
  };

  viewsName: any;

  display = 'none';
  isDesc: any = [];
  showDiv: boolean = true;
  flag: any;
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  arrow = false;
  checkFlag: any = false;
  featureLength: any = true;
  setveractive: any = false;
  userData :any;
  bowserInfo:any;
  search2 = {
    forumTitle: '',
    interest: '',
    forumStartDate: '',
    forumEndDate: '',
    forumsta: '',
    favCount: '',
    commentCount: '',
    isActive: '',
    viewCount: '',
  };
  hidePopup: boolean = true;
  detailView: any = [];
  interestList: any = [
    {interestId: 1, intersetName: "Aluminium"},
    {interestId: 2, intersetName: "Copper"},
    {interestId: 3, intersetName: "Lead"},
    {interestId: 4, intersetName: "Bronze"},
    {interestId: 5, intersetName: "Iron"},
    {interestId: 6, intersetName: "Zinc"},
    {interestId: 7, intersetName: "Nickel"}
  ];
  searchCount: any = 0;
  dropdownList: any = [];
  selectedItems: any = [];
  featureList: any = [];
  unamePattern = "[a-zA-Z ]*$" ; 
  dropdownSettings: any = {
    singleSelection: false,
    idField: 'interestId',
    textField: 'intersetName',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };
  isCheck1: any;
  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private toastr: ToastrService,
    private passService: PassServService,
    private forumSer: FetchForumsService,
    private srchForSer: SearchForumsService,
    private browserInfoService: BrowserInfoService,
    private excelService: ExcelService,
  ) {
    this.dropdownList = this.interestList;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.spinner.show();
    this.fetchForums();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
  }

  openModalDialog() {
    this.display = 'block';
    this.showDiv = false;
    // console.log(this.showDiv);
  }

  closeModalDialog() {
    this.display = 'none';
    this.showDiv = true;
  }

  clearBtn() {
    this.search.forumTitle = '';
  }

  clearBtn1() {
    this.selectedItems = [];
    this.isCheck1 = '';
    if (this.searchCount > 0) {
      this.closeModalDialog();
      this.fetchForums();
      this.searchCount= 0;
    }
  }

  invert() {
    this.arrow = !this.arrow;
  }

  myFunction(event) {
    this.search2.isActive = event.target.value;
  }

  showViewDetails(item) {
    console.log('item',item);
    if(item.viewCount == 0) {
      return;
    }

    this.viewsName = "View Details";

    this.hidePopup = false;
    this.spinner.show();

    let data = {
      favId : item.forumId,
    }
    this.forumSer.fetchForumViewDetail(data).then(res => {
      this.spinner.hide();
        try {
          console.log('detail Res', res.data);
          this.detailView = res.data;
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        this.spinner.hide();
        console.log('err', err);
        this.toastr.error('Server Error');
    });
    // this.cardDetails = item;
    // console.log("carddetails ==", this.cardDetails);
  
}


  myFunction1(event1) {
    this.search2.forumsta = event1.target.value;
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.forumList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  clearBtn2() {
    this.search2.forumTitle = '';
    this.search2.interest = '';
    this.search2.forumStartDate = '';
    this.search2.forumEndDate = '';
    this.search2.forumsta = '';
    this.search2.favCount ='';
    this.search2.commentCount = '';
    this.search2.isActive = '';
    this.search2.viewCount = ''
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  addForums() {
    this.passService.editForum = [];
    this.passService.interestList = this.interestList;
    this.router.navigate(['pages/forums/add-edit-forums']);
  }
  editForums(data) {
    this.passService.editForum[0] = (data);
    this.passService.interestList = this.interestList;
    this.router.navigate(['pages/forums/add-edit-forums']);
  }
  viewForum(view) {
    this.passService.editForum[0] = (view);
    this.router.navigate(['pages/forums/view-forums']);
  }

  onItemSelect(item) {
    console.log('select', this.selectedItems);
    this.isCheck1 = '';
}



exportExcel() {
  this.dataArrayforExcel = [];
  for (let i = 0; i < this.forumList.length; i++) {
    if (this.forumList[i].isActive === 1) {
      this.flag = 'Active';
    }
    if (this.forumList[i].isActive === 2) {
      this.flag = 'Inactive';
    }
    this.dataObjforExcel = {
      'FORUM NAME': this.forumList[i].forumTitle,
      'START DATE': this.forumList[i].forumStartDate,
      'END DATE': this.forumList[i].forumEndDate,
      'STATE': this.forumList[i].sta,
      'LIKES' : this.forumList[i].favCount,
      'COMMENTS' : this.forumList[i].commentCount,
      'STATUS' : this.flag,
    }
    // console.log('this.dataArray',this.dataObjforExcel)

    this.dataArrayforExcel.push(this.dataObjforExcel);
    //  for export to excel END //
  }
  this.exportAsXLSX();
}


showFavouriteDetails(item) {
    console.log('item',item);
    if(item.favCount == 0) {
      return;
    }

    this.viewsName = "Bookmark Details";

    this.hidePopup = false;
    this.spinner.show();

    let data = {
      favId : item.forumId
    }
    this.forumSer.fetchForumFavDetail(data).then(res => {
      this.spinner.hide();
        try {
          console.log('detail Res', res.data);
          this.detailView = res.data;
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        this.spinner.hide();
        console.log('err', err);
        this.toastr.error('Server Error');
    });
    // this.cardDetails = item;
    // console.log("carddetails ==", this.cardDetails);
  
}

hideViewDetails() {
  this.hidePopup = true;
}


exportAsXLSX(): void {
  this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Forum Details');
}

sortView(property) {
  this.isDesc = !this.isDesc; // change the direction
  this.column = property;
  const direction = this.isDesc ? 1 : -1;
  this.detailView.sort(function (a, b) {
    if (a[property] < b[property]) {
      return -1 * direction;
    } else if (a[property] > b[property]) {
      return 1 * direction;
    } else {
      return 0;
    }
  });
}


searchView = {
  subname: '',
  subemail: '',
  phoneno: '',
  corpname: '',
}

clearViewDetail() {
  this.searchView.subname = '';
  this.searchView.subemail = '';
  this.searchView.phoneno = '';
  this.searchView.corpname= '';
}



  onSelectAll(items) {
    // console.log(items);
    if (this.selectedItems.length === this.dropdownList.length) {
      this.selectedItems = [];
      this.isCheck1 = 'Please select interests';
    } else {
      this.selectedItems = items;
      this.isCheck1 = '';
    }
    console.log('selectAll', this.selectedItems);
  }
  onItemDeSelect(des) {
    for (let i = 0; i < this.selectedItems.length; i++) {
      if (des.interestId === this.selectedItems[i].interestId) {
        this.selectedItems.splice(i, 1);
      }
    }
    if (this.selectedItems.length === 0) {
      this.isCheck1 = 'Please select interests';
    }
     console.log('deselect', this.selectedItems);
  }

  searchPost(){
    if (new Date(this.search1.fromdate) > new Date(this.search1.todate)) {
      this.toastr.warning('To date should not be greater than From date');
      if (this.selectedItems.length === 0) {
        this.isCheck1 = 'Please select interests';
      }
      return;
    }
    if (this.selectedItems.length === 0) {
        this.isCheck1 = 'Please select interests';
        return;
    } else {
      const intrList = [];
    for(let i = 0;i < this.selectedItems.length;i++){
      intrList.push(this.selectedItems[i].interestId);
    }
    const content = {
      IDS : intrList.join(','),
      FrmDate: this.formatDate(this.search1.fromdate),
      ToDate: this.formatDate(this.search1.todate),
      uid: this.userData.userid,
      uname: this.userData.subname,
      bname: this.bowserInfo.name,
      bversion: this.bowserInfo.version,
    };
    console.log('content',content);
    this.spinner.show();
      this.srchForSer.searchForums(content).then(res => {
        if (res.type === true) {
          console.log('res', res);
          // res.data = this.posts;
          this.forumList = res.data;
          console.log('this.forumList',this.forumList);
          if(this.forumList.length === 0) {
            this.featureLength = false;
            this.closeModalDialog();
          } else if(this.forumList.length > 0) {
            this.featureLength = true;
            for (let i = 0; i < this.forumList.length; i++) {
              this.forumList[i].forumStartDate = new Date(this.forumList[i].forumStartDate);
              this.forumList[i].forumEndDate = new Date(this.forumList[i].forumEndDate);
              }
            this.closeModalDialog();
          }
          this.spinner.hide();
          this.searchCount++;
        }
        if (res.type === false) {
          this.spinner.hide();
          console.log('res', res);
        }
      }, err => {
        this.spinner.hide();
        // console.log(err);
        this.forumList = [];
        this.setveractive = true;
        this.toastr.error('Server Error');
      });
    }
    }
    numberOnly(event): boolean {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
  
    }

    fetchForums() {
      if (!window.navigator.onLine) {
        this.toastr.error('Please check your internet connection');
      } else {
        const data = {
          uid: this.userData.userid,
          uname: this.userData.subname,
          bname: this.bowserInfo.name,
          bversion: this.bowserInfo.version,
        };
        this.forumSer.fetchForums(data).then(res => {
          this.spinner.hide();
          try {
            if (res.type === true && res.status_code === 200) {
              console.log('res', res);
              try {
              // let userD = [];
              this.forumList = res.data;
              this.interestList = res.interest;
              if(this.forumList.length === 0) {
                this.featureLength = false;
              } else if(this.forumList.length > 0) {
                this.featureLength = true;
                for (let i = 0; i < this.forumList.length; i++) {
                // this.forumList[i].forumStartDate = new Date(this.forumList[i].forumStartDate);
                // this.forumList[i].forumEndDate = new Date(this.forumList[i].forumEndDate);
                }
              }
  
              // this.featureLength = this.polls.length === 0 ? false :  true;
              console.log('this.forumList',this.forumList);
              } catch (e) {
                console.log(e);
              }
            }
            if (res.type === false) {
              this.spinner.hide();
              console.log('res', res);
            }
          } catch (e) {
            console.log(e);
          }
        },
        err => {
          console.log(err);
          this.spinner.hide();
          this.forumList = [];
          this.setveractive = true;
          this.toastr.error('Server Error');
        });
      }
    }
}

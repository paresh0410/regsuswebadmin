import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class FetchForumsCommentsService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchForumCommentsUrl = this.baseUrl + webApi.apiUrl.fetchForumComments;
  constructor(public http: HttpClient) { }
  public fetchForumComments(viewData: any): Promise<any> {

    return this.http.post(this.fetchForumCommentsUrl, viewData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

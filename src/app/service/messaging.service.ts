import { Injectable } from "@angular/core";
import { AngularFireMessaging } from "@angular/fire/messaging";
import { BehaviorSubject } from "rxjs";
import { FcmService } from './fcm/fcm.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class MessagingService {
  currentMessage = new BehaviorSubject(null);
  constructor(private angularFireMessaging: AngularFireMessaging, private fcmservice: FcmService,
    private toastr: ToastrService) {
    this.angularFireMessaging.messaging.subscribe((_messaging) => {
      _messaging.onMessage = _messaging.onMessage.bind(_messaging);
      _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
    });
  }
  requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log('The token is', token);
        this.fcmservice.setPushIds(token);
        const userID = 0;
        if (!localStorage.getItem('userid')) {
          this.fcmservice.saveUserTokenInfo(userID, "login", (response) => {
            console.log("registerUserTokenInfo", response);
          });
        }
      },
      (err) => {
        console.error("Unable to get permission to notify.", err);
      }
    );
  }
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe((payload) => {
    //   console.log("new message received. ", payload);
      var payload1: any = payload;
      let data = {
        event : payload1.data.event,
        id : payload1.data.id,
        body : payload1.notification.body,
        click_action : payload1.notification.click_action,
        title : payload1.notification.title,
      };
      this.toastr.success(data.title, data.body, {
        timeOut: 3000,
        closeButton : true,
        positionClass : 'toast-top-left',
      }).onShown;
      this.fcmservice.setnotifbadge(data);
    });
  }
}

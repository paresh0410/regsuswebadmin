import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { FetchPromotionService } from '../../service/fetch-promotion/fetch-promotion.service';
import { PassServService } from '../../service/pass-serv.service';
import { BrowserInfoService } from '../../service/browser-info.service';
import { ExcelService } from '../../service/excel.service';

@Component({
  selector: 'ngx-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.scss']
})
export class PromotionComponent implements OnInit {
  promoList: any = [ ];
  search:any={};
  isDesc: any = [];
  p: number = 1;
  column: any = [];  
  arrow = false;
  userData :any;
  bowserInfo:any;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  flag: any;

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private fetchPromo: FetchPromotionService,
    private passSer: PassServService,
    private browserInfoService: BrowserInfoService,
    private excelService: ExcelService,
  ) { 
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
    this.search = {
      promoHeader: '',
      promodesc: '',
      startDate: '',
      endDate: '',
      isActive: '',
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.fetchPromotion();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
   
  }

  editSub(editData) { 
    this.passSer.editPromo[0] = (editData);
    this.router.navigate(['pages/promotion/add-edit-promotion']);
  }

  AddSub() {
    this.passSer.editPromo = [] ;
    this.router.navigate(['pages/promotion/add-edit-promotion']);
  }

  fetchPromotion() {
    if (!window.navigator.onLine){
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
    this.fetchPromo.fetchPromotion(data).then(res => {
      this.promoList = res.data;
      console.log('res', res);
    });

   }
  }

  // clear button
  clearBtn() {
    this.search.promoHeader = '';
    this.search.promodesc = '';
    this.search.startDate = '';
    this.search.endDate = '';
    this.search.isActive = '';
  }

  // status
  myFunction(event) {
    this.search.isActive = event.target.value;
    console.log('value', event);
  }

  // sort
  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;

    this.promoList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // Export To Excel
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.promoList.length; i++) {
      if (this.promoList[i].isActive === 1) {
        this.flag = 'Active';
      }
      if (this.promoList[i].isActive === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'TITLE': this.promoList[i].promoHeader,
        'DESCRIPTION': this.promoList[i].promodesc,
        'START DATE': this.promoList[i].startDate,
        'END DATE': this.promoList[i].endDate,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Promotion Details');
  }
}

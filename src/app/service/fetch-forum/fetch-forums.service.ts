import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class FetchForumsService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchForumsUrl = this.baseUrl + webApi.apiUrl.fetchForum;
  fetchForumsFavUrl = this.baseUrl + webApi.apiUrl.fetchForumFavDet;
  fetchForumsViewUrl = this.baseUrl + webApi.apiUrl.fetchForumFavView;

  


  
  constructor(public http: HttpClient) { }
  public fetchForums(data: any): Promise<any> {

    return this.http.post(this.fetchForumsUrl, data, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public fetchForumFavDetail(data: any): Promise<any> {

    return this.http.post(this.fetchForumsFavUrl, data, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }


  public fetchForumViewDetail(data: any): Promise<any> {

    return this.http.post(this.fetchForumsViewUrl, data, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }



  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

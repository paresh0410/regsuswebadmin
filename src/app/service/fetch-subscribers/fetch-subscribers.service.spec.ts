import { TestBed } from '@angular/core/testing';

import { FetchSubscribersService } from './fetch-subscribers.service';

describe('FetchSubscribersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchSubscribersService = TestBed.get(FetchSubscribersService);
    expect(service).toBeTruthy();
  });
});

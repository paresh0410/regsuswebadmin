import { TestBed } from '@angular/core/testing';

import { FetchDealSubscribersService } from './fetch-deal-subscribers.service';

describe('FetchDealSubscribersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchDealSubscribersService = TestBed.get(FetchDealSubscribersService);
    expect(service).toBeTruthy();
  });
});

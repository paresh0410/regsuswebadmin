import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditBroadcastComponent } from './add-edit-broadcast.component';

describe('AddEditBroadcastComponent', () => {
  let component: AddEditBroadcastComponent;
  let fixture: ComponentFixture<AddEditBroadcastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditBroadcastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditBroadcastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

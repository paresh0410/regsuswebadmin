import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditForumsComponent } from './add-edit-forums.component';

describe('AddEditForumsComponent', () => {
  let component: AddEditForumsComponent;
  let fixture: ComponentFixture<AddEditForumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditForumsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditForumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { SeparatorsService } from './separators.service';

describe('SeparatorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SeparatorsService = TestBed.get(SeparatorsService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class UploadMsReportFileService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  uploadMsReportFileUrl = this.baseUrl + webApi.apiUrl.uploadMsReportFile;
  constructor(public http: HttpClient) { }
  public uploadMsReportFile(msrData: any): Promise<any> {

    return this.http.post(this.uploadMsReportFileUrl, msrData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

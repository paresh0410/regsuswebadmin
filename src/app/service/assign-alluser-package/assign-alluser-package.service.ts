import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class AssignAlluserPackageService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  assignAllPackageUrl = this.baseUrl + webApi.apiUrl.assignAllPackage;
  assignSpecialPackageUrl = this.baseUrl + webApi.apiUrl.assignSpecialPackage;
  constructor(public http: HttpClient) { }
  public AssignAllPackages(pkgData: any): Promise<any> {

    return this.http.post(this.assignAllPackageUrl, pkgData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public AssignSpecialPackages(pkgData: any): Promise<any> {

    return this.http.post(this.assignSpecialPackageUrl, pkgData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

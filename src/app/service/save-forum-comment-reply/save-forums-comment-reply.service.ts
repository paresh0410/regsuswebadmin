import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class SaveForumsCommentReplyService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  saveForumCommentReplyUrl = this.baseUrl + webApi.apiUrl.saveForumCommentReply;
  constructor(public http: HttpClient) { }
  public saveForumCommentReps(commRepData: any): Promise<any> {

    return this.http.post(this.saveForumCommentReplyUrl, commRepData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

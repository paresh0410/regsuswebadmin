import { TestBed } from '@angular/core/testing';

import { FetchCorporationDealsService } from './fetch-corporation-deals.service';

describe('FetchCorporationDealsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchCorporationDealsService = TestBed.get(FetchCorporationDealsService);
    expect(service).toBeTruthy();
  });
});

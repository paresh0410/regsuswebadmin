import { Component, OnInit, ElementRef } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { FetchDuartionService } from '../../service/fetch-duration/fetch-duartion.service';

@Component({
  selector: 'ngx-duration',
  templateUrl: './duration.component.html',
  styleUrls: ['./duration.component.scss']
})
export class DurationComponent implements OnInit {
  durationList: any = [
    // {
    //   perid : 1,
    //   months : 1,
    //   perPrice : 0,
    //   discount: 0,
    //   status: 1,
    // },
    // {
    //   perid : 2,
    //   months : 3,
    //   perPrice : 1,
    //   discount: 10,
    //   status: 1,
    // },
    // {
    //   perid : 3,
    //   months : 6,
    //   perPrice : 1,
    //   discount: 20,
    //   status: 1,
    // },
    // {
    //   perid : 4,
    //   months : 12,
    //   perPrice : 1,
    //   discount: 30,
    //   status: 1,
    // },
    // {
    //   perid : 5,
    //   months : 24,
    //   perPrice : 1,
    //   discount: 35,
    //   status: 1,
    // },
  ];
  p: number = 1;
  arrow = false;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  search = {
    months: '',
    perPrice: '',
    discount: '',
    status: '',
  };
  featureLength: any = true;
  setveractive:any = false;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private fetchDurationSer: FetchDuartionService,
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.fetchDuration();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.durationList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.months = '';
    this.search.perPrice = '';
    this.search.discount = '';
    this.search.status = '';
  }

  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }

  // Export To Excel
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.durationList.length; i++) {
      if (this.durationList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.durationList[i].status === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'DURATION': this.durationList[i].months,
        // 'PER PRICE': this.durationList[i].perPrice,
        'DISCOUNT': this.durationList[i].discount,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Duration Details');
  }

  addDuration() {
    this.passSer.editDuration = [];
    this.router.navigate(['pages/duration/add-edit-duration']);
  }

  editDuration(editdata) {
    this.passSer.editDuration[0] = (editdata);
    this.router.navigate(['pages/duration/add-edit-duration']);
  }

  fetchDuration() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      this.fetchDurationSer.fetchDurations().then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
            // let userD = [];
            this.durationList = res.data;
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            if(this.durationList.length === 0) {
              this.featureLength = false;
            } else if(this.durationList.length > 0){
              this.featureLength = true;
            }
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.durationList = [];
        this.setveractive = true;
        this.toastr.error('Server Error');
      });
    }
  }
}

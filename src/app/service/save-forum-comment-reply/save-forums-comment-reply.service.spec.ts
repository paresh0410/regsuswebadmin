import { TestBed } from '@angular/core/testing';

import { SaveForumsCommentReplyService } from './save-forums-comment-reply.service';

describe('SaveForumsCommentReplyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveForumsCommentReplyService = TestBed.get(SaveForumsCommentReplyService);
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { FetchForumsCommentsService } from '../../../service/fetch-forum-comments/fetch-forums-comments.service';
import { SaveForumsCommentService } from '../../../service/save-forum-comment/save-forums-comment.service';
import { SaveForumsCommentReplyService } from '../../../service/save-forum-comment-reply/save-forums-comment-reply.service';
import { BrowserInfoService } from '../../../service/browser-info.service';

@Component({
  selector: 'ngx-view-forums',
  templateUrl: './view-forums.component.html',
  styleUrls: ['./view-forums.component.scss']
})
export class ViewForumsComponent implements OnInit {
  viewList: any = [];
  isChecked = false;
  viewForum: any = [
    // {
    //   id : 1,
    //   commentUser : 'John Cena',
    //   commentText : 'Would you be interested in looking at my code in the future if I need help? I really need someone to review my code so I know I’m on the right path. I recently had an interview and the developer who was there said my code was mostly good but one part of my code test wasn’t written the “Angular way”.',
    //   commentDate : '2019-04-15 9:00',
    //   likes : 14,
    //   replies : 2,
    //   replylist : [
    //     {
    //       commentUser : 'Kane',
    //   commentText : 'I’ve been look for any updates but it seems that the core library has only been updated 4 months ago. I’m not sure if this project is still active I haven’t found any announcement yet.',
    //   commentDate : '2019-04-16 11:00',
    //   likes: 1,
    //     },
    //     {
    //       commentUser : 'Rock',
    //   commentText : 'I, too am using angular-meteor with the latest release of Angular 4 without issue, but my use of Meteor calls on the client-side is quite minimal, as we’re primarily using Apollo GraphQL.',
    //   commentDate : '2019-04-15 15:00',
    //   likes: 0,
    //     },
    //   ]
    // },
    // {
    //   id : 2,
    //   commentUser : 'astv99',
    //   commentText : 'Most modern frameworks, including Angular and React, use OOP principles in some way so it’s definitely worth learning.',
    //   commentDate : '2019-03-23 22:00',
    //   likes : 3,
    //   replies : 0,
    //   replylist : [],
    // },
    // {
    //   id : 3,
    //   commentUser : 'arw2015',
    //   commentText : 'Your friend is wrong here. OOP has been the most popular programming paradigm for decades, and you should understand the basic principles. JavaScript does not require you to use OOP structures in the same way as Java, but it is still an important part of the language.',
    //   commentDate : '2019-03-12 18:00',
    //   likes : 56,
    //   replies : 1,
    //   replylist : [
    //     {
    //       commentUser : 'Ella_Brown',
    //       commentText : 'Hi, I think before jumping to angularjs you should learn why angularjs?? here you can find better understanding about that why should we chosse angularjs',
    //       commentDate : '2019-03-16 12:00',
    //       likes: 3,
    //     },
    //   ],
    // }
  ];
  title: any;
  discription: any;

  admCom: any = '';
  admNewCom: any = '';
  showComm: any = false;
  adminCommList: any = [];
  userName: any;
  userD: any = [];
  disableAll: any = false;
  stDate: any;
  eDate: any;
  forumID: any;
  featureLength: any = false;
  setveractive: any = false;
  userData :any;
  bowserInfo:any;
  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private toastr: ToastrService,
    private passService: PassServService,
    private fetchFComSer: FetchForumsCommentsService,
    private savecmtSer: SaveForumsCommentService,
    private savecmtRepSer: SaveForumsCommentReplyService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.viewList = this.passService.editForum;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
    // this.title = this.viewList[0].forumname;
    // this.stDate = this.viewList[0].startDate;
    // this.eDate = this.viewList[0].endDate;
    this.userD = JSON.parse(localStorage.getItem('UserData'));
    this.userName = this.userD.subId;
    if(this.viewList[0].forumsta === 3 || this.viewList[0].forumsta === 1) {
      this.disableAll = true;
    }
   }

  ngOnInit() {
    this.spinner.show();
    this.fetchForumComments();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
  }

  questionList: any = [];
  chechedComents: any = [];
  commentSection = false;
  showAnsBtn = false;
  checkValue(event) {
    console.log('event',event);
    
    this.questionList =[];
    this.chechedComents =[];
   
    for(let i =0; i < this.viewForum.length; i++) {
      if(this.viewForum[i].isChecked === true) {
        this.showAnsBtn = true;
        this.questionList.push(this.viewForum[i].commentText);
        this.chechedComents.push(this.viewForum[i].forumCommentId);
        console.log('this.chechedComents',this.chechedComents);
        console.log('this.questionList',this.questionList);
      }
    }

    if(this.chechedComents.length == 0) {
      this.showAnsBtn = false;
    }


   
    



  }

  giveAnswerMultiple() {
    this.commentSection = true;
  }

  onMainScreen(){
    for   (let i = 0 ; i < this.viewForum.length; i++) {
      this.viewForum[i].isChecked = false;
    }
    console.log('this.viewForum',this.viewForum);
    this.adminComment = false;
    this.commentSection = false;
    this.showAnsBtn = false;
    this.questionList = [];
    this.chechedComents = [];
  }

  

  goToForums() {
    this.router.navigate(['pages/forums']);
  }

  postComment(item) {
    if (!this.admCom) {
      this.toastr.warning('Please enter your reply');
    } else {
            let content = {
              foId : this.forumID,
              focoId : item.forumCommentId,
              soId : this.userName,
              commText : this.admCom,
              uid: this.userData.userid,
              uname: this.userData.subname,
              bname: this.bowserInfo.name,
              bversion: this.bowserInfo.version,
             };
  
            console.log('content' , content);
            this.savecmtRepSer.saveForumCommentReps(content).then(res => {
              this.spinner.hide();
              try {
                if (res.type === true && res.status_code === 200) {
                  console.log('res2', res);
                  try {
                  console.log('res2', res);
                  this.admCom = '';
                  this.fetchForumComments();
                  } catch (e) {
                    console.log(e);
                  }
                }
                if (res.type === false) {
                  this.spinner.hide();
                  console.log('res', res);
                }
              } catch (e) {
                console.log(e);
              }
            },
            err => {
              console.log(err);
              this.spinner.hide();
              this.toastr.error('Server Error');
            });
          // }
      // let commObj = {
      //   commentUser : this.userName,
      //   commentDate : new Date(),
      //   commentText : this.admCom,
      //   likes: 0,
      // }
      // for (let i = 0; i < this.viewForum.length; i++) {
      //   if  (item.id === this.viewForum[i].id) {
      //     this.viewForum[i].replylist.push(commObj);
      //   }
      // }
      // // this.adminCommList.push(commObj);
      // // this.adminCommList.reverse();
      // this.showComm = true;
      // this.admCom = '';
    }
    console.log('this.adminCommList',this.adminCommList);
  }

  postNewComment() {
    if (!this.admNewCom) {
      this.toastr.warning('Please enter your comment');
    } else {
      // let commObj = {
      //   commentUser : this.userName,
      //   commentDate : new Date(),
      //   commentText : this.admNewCom,
      //   likes : 0,
      //   replylist : [],
      // }
      //     this.viewForum.push(commObj);
          // this.admNewCom = '';
          let content = {
            foId : this.forumID,
            soId : this.userName,
            commText : this.admNewCom,
            uid: this.userData.userid,
            uname: this.userData.subname,
            bname: this.bowserInfo.name,
            bversion: this.bowserInfo.version,
           };

          console.log('content' , content);
          this.savecmtSer.saveForumComments(content).then(res => {
            this.spinner.hide();
            try {
              if (res.type === true && res.status_code === 200) {
                console.log('res1', res);
                try {
                console.log('res1', res);
                this.admNewCom = '';
                this.fetchForumComments();
                } catch (e) {
                  console.log(e);
                }
              }
              if (res.type === false) {
                this.spinner.hide();
                console.log('res', res);
              }
            } catch (e) {
              console.log(e);
            }
          },
          err => {
            console.log(err);
            this.spinner.hide();
            this.toastr.error('Server Error');
          });
        }
  }

  adminComment = false;
  onadminComment(){
    this.adminComment = !this.adminComment;  
  }


  postMultipleAnswer() {

    this.postcommentForMultipleAnswer(this.adminComment,  (function () {
     const num =  this.viewForum[0].forumCommentId;
     console.log('num',num);
      if (!this.admCom) {
        this.toastr.warning('Please enter your reply');
      } else { 
        if(this.adminComment == true) {
          this.chechedComents.push(num + 1);
        }
        console.log('this.viewForum[0].forumCommentId', this.chechedComents);
        for(let i = 0; i < this.chechedComents.length; i++) {
          let content = {
            foId : this.forumID,
            focoId : this.chechedComents[i],
            soId : this.userName,
            commText : this.admCom,
            uid: this.userData.userid,
            uname: this.userData.subname,
            bname: this.bowserInfo.name,
            bversion: this.bowserInfo.version,
           };
      
          console.log('content' , content);
          this.savecmtRepSer.saveForumCommentReps(content).then(res => {
            this.spinner.hide();
            try {
              if (res.type === true && res.status_code === 200) {
                console.log('res2', res);
                try {
                console.log('res2', res);
                this.admCom = '';
                this.fetchForumComments();
                } catch (e) {
                  console.log(e);
                }
              }
              if (res.type === false) {
                this.spinner.hide();
                console.log('res', res);
              }
            } catch (e) {
              console.log(e);
            }
          },
          err => {
            console.log(err);
            this.spinner.hide();
            this.toastr.error('Server Error');
          });
      
        }
        this.adminComment = false;
        this.commentSection = false;
        this.showAnsBtn = false;
        this.questionList = [];
        this.chechedComents = [];
      }
    }).bind(this));

}


  postcommentForMultipleAnswer(data, cb){

    if(data === true) {
      if (!this.admNewCom) {
        // this.toastr.warning('Please enter your comment');
      } else {
            const content = {
              foId : this.forumID,
              soId : this.userName,
              commText : this.admNewCom,
              uid: this.userData.userid,
              uname: this.userData.subname,
              bname: this.bowserInfo.name,
              bversion: this.bowserInfo.version,
             };
            console.log('content' , content);
            this.savecmtSer.saveForumComments(content).then(res => {
              this.spinner.hide();
              try {
                if (res.type === true && res.status_code === 200) {
                  console.log('res1', res);
                  try {
                  console.log('res1', res);
                  this.admNewCom = '';
                  this.fetchForumComments();
                  } catch (e) {
                    console.log(e);
                  }
                }
                if (res.type === false) {
                  this.spinner.hide();
                  console.log('res', res);
                }
              } catch (e) {
                console.log(e);
              }
            },
            err => {
              console.log(err);
              this.spinner.hide();
              this.toastr.error('Server Error');
            });
      }

     
      cb();
    } else {
      cb();
    }

   
  }



  

  fetchForumComments(){
    let content = {
      forId : null,
      uid: this.userData.userid,
      uname: this.userData.subname,
      bname: this.bowserInfo.name,
      bversion: this.bowserInfo.version,
     };
    if (this.viewList[0]) {
      if (this.viewList[0].forumId) {
      content.forId = this.forumID = this.viewList[0].forumId;
      }
    }
    console.log('content' , content);
    this.fetchFComSer.fetchForumComments(content).then(res => {
      this.spinner.hide();
      try {
        if (res.type === true && res.status_code === 200) {
          console.log('res', res);
          try {
            if (res.forumdata) {
              let headerData = [];
              headerData = res.forumdata;
              if (headerData.length > 0) {
                this.title = headerData[0].forumTitle;
                this.discription = headerData[0].forumDescription;
                this.stDate = headerData[0].forumStartDate;
                this.eDate = headerData[0].forumEndDate;
                if(headerData[0].forumsta === 3 || headerData[0].forumsta === 1) {
                  this.disableAll = true;
                }
              }
            }
            if (res.forumComment) {
              this.viewForum = res.forumComment;
              console.log('this.viewForum', this.viewForum );
              for(let i =0; i< this.viewForum.length ; i++ ){
                this.viewForum[i].isChecked = false;
                // this.viewForum[i].trial= false;
              }
             
              // if (this.viewForum.length > 0) {
              //   this.title = headerData[0].forumTitle;
              //   this.stDate = headerData[0].forumStartDate;
              //   this.eDate = headerData[0].forumEndDate;
              // }
            this.featureLength = this.viewForum.length === 0 ? true :  false;
            }
          } catch (e) {
            console.log(e);
          }
        }
        if (res.type === false) {
          this.spinner.hide();
          console.log('res', res);
        }
      } catch (e) {
        console.log(e);
      }
    },
    err => {
      console.log(err);
      this.spinner.hide();
      this.toastr.error('Server Error');
      this.viewForum = [];
      this.setveractive = true;
    });
  }
}

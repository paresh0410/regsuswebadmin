import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionmanagementComponent } from './sectionmanagement.component';

describe('SectionmanagementComponent', () => {
  let component: SectionmanagementComponent;
  let fixture: ComponentFixture<SectionmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

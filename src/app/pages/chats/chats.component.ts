import { Component, OnInit, AfterViewChecked, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'ngx-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class ChatsComponent implements OnInit{
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  groups: any = [
    {
      id : 1,
      name : 'Group 1 Group 1 Group 1 Group 1 Group 1',
      lastUser : 'User 1',
      lastMsg : 'Message 1',
      date : '26-Sep-2017',
      msgs : [
        {
          text : 'This is first msg',
          username : 'Joe', 
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-15 10:00'),
        },
        {
          text : 'This is second msg',
          username : 'Ben',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-15 11:00'),
        },
        {
          text : 'This is third msg',
          username : 'Admin',
          avatar : '',
          date : new Date('2019-09-15 12:00'),
        },
        {
          text : 'This is fourth msg',
          username : 'josh',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-15 13:00'),
        },
        {
          text : 'This is first msg',
          username : 'Joe', 
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-16 10:00'),
        },
        {
          text : 'This is second msg',
          username : 'Ben',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-16 11:00'),
        },
        {
          text : 'This is third msg',
          username : 'Admin',
          avatar : '',
          date : new Date('2019-09-16 12:00'),
        },
        {
          text : 'This is fourth msg',
          username : 'josh',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-16 13:00'),
        },
        {
          text : 'This is first msg',
          username : 'Joe', 
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-17 10:00'),
        },
        {
          text : 'This is second msg',
          username : 'Ben',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-17 11:00'),
        },
        {
          text : 'This is third msg',
          username : 'Admin',
          avatar : '',
          date : new Date('2019-09-17 12:00'),
        },
        {
          text : 'This is fourth msg',
          username : 'josh',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-17 13:00'),
        },
        {
          text : 'This is first msg',
          username : 'Joe', 
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-18 10:00'),
        },
        {
          text : 'This is second msg',
          username : 'Ben',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-18 11:00'),
        },
        {
          text : 'This is third msg',
          username : 'Admin',
          avatar : '',
          date : new Date('2019-09-18 12:00'),
        },
        {
          text : 'This is fourth msg',
          username : 'josh',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-18 13:00'),
        },
      ],
      participants : [
        {
          id: 1,
          isAdmin: 1,
          pname : 'Joe'},
      {
        id: 2,
        isAdmin: 2,
        pname : 'Ben'},
      {
        id: 3,
        isAdmin: 1,
        pname : 'admin'},
      {
        id: 4,
        isAdmin: 2,
        pname : 'Josh'},
      {
        id: 5,
        isAdmin: 2,
        pname : 'Tim'}
      ],
    },
    {

      id : 2,
      name : 'Group 2',
      lastUser : 'User 2',
      lastMsg : 'Message 2',
      date : '25-Sep-2017',
      msgs : [
        {
          text : 'This is first msg',
          username : 'Joe', 
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 10:00'),
        },
        {
          text : 'This is second msg',
          username : 'Ben',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 11:00'),
        },
        {
          text : 'This is third msg',
          username : 'Admin',
          avatar : '',
          date : new Date('2019-09-26 12:00'),
        },
        {
          text : 'This is fourth msg',
          username : 'josh',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 13:00'),
        }
      ],
      participants : [
        {
          id: 1,
          isAdmin: 1,
          pname : 'Joe'},
      {
        id: 2,
        isAdmin: 2,
        pname : 'Ben'},
      {
        id: 3,
        isAdmin: 1,
        pname : 'admin'},
      {
        id: 4,
        isAdmin: 2,
        pname : 'Josh'},
      {
        id: 5,
        isAdmin: 2,
        pname : 'Tim'}
      ]
    },
    {
      id : 3,
      name : 'Group 3',
      lastUser : 'User 3',
      lastMsg : 'Message 3',
      date : '24-Sep-2017',
      msgs : [
        {
          text : 'This is first msg',
          username : 'Joe', 
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 10:00'),
        },
        {
          text : 'This is second msg',
          username : 'Ben',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 11:00'),
        },
        {
          text : 'This is third msg',
          username : 'Admin',
          avatar : '',
          date : new Date('2019-09-26 12:00'),
        },
        {
          text : 'This is fourth msg',
          username : 'josh',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 13:00'),
        }
      ],
      participants : [      {
        id: 1,
        isAdmin: 1,
        pname : 'Joe'},
    {
      id: 2,
      isAdmin: 2,
      pname : 'Ben'},
    {
      id: 3,
      isAdmin: 1,
      pname : 'admin'},
    {
      id: 4,
      isAdmin: 2,
      pname : 'Josh'},
    {
      id: 5,
      isAdmin: 2,
      pname : 'Tim'}]
    },
    {
      id : 4,
      name : 'Group 4',
      lastUser : 'User 4',
      lastMsg : 'Message 4',
      date : '23-Sep-2017',
      msgs : [
        {
          text : 'This is first msg',
          username : 'Joe', 
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 10:00'),
        },
        {
          text : 'This is second msg',
          username : 'Ben',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 11:00'),
        },
        {
          text : 'This is third msg',
          username : 'Admin',
          avatar : '',
          date : new Date('2019-09-26 12:00'),
        },
        {
          text : 'This is fourth msg',
          username : 'josh',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 13:00'),
        }
      ],
      participants : [
        {
          id: 1,
          isAdmin: 1,
          pname : 'Joe'},
      {
        id: 2,
        isAdmin: 2,
        pname : 'Ben'},
      {
        id: 3,
        isAdmin: 1,
        pname : 'admin'},
      {
        id: 4,
        isAdmin: 2,
        pname : 'Josh'},
      {
        id: 5,
        isAdmin: 2,
        pname : 'Tim'}
      ]
    },
    {
      id : 5,
      name : 'Group 5',
      lastUser : 'User 5',
      lastMsg : 'Message 5',
      date : '22-Sep-2017',
      msgs : [
        {
          text : 'This is first msg',
          username : 'Joe', 
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 10:00'),
        },
        {
          text : 'This is second msg',
          username : 'Ben',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 11:00'),
        },
        {
          text : 'This is third msg',
          username : 'Admin',
          avatar : '',
          date : new Date('2019-09-26 12:00'),
        },
        {
          text : 'This is fourth msg',
          username : 'josh',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 13:00'),
        }
      ],
      participants : [
        {
          id: 1,
          isAdmin: 1,
          pname : 'Joe'},
      {
        id: 2,
        isAdmin: 2,
        pname : 'Ben'},
      {
        id: 3,
        isAdmin: 1,
        pname : 'admin'},
      {
        id: 4,
        isAdmin: 2,
        pname : 'Josh'},
      {
        id: 5,
        isAdmin: 2,
        pname : 'Tim'}
      ]
    },
    {
      id : 6,
      name : 'Group 5',
      lastUser : 'User 5',
      lastMsg : 'Message 5',
      date : '22-Sep-2017',
      msgs : [
        {
          text : 'This is first msg',
          username : 'Joe', 
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 10:00'),
        },
        {
          text : 'This is second msg',
          username : 'Ben',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 11:00'),
        },
        {
          text : 'This is third msg',
          username : 'Admin',
          avatar : '',
          date : new Date('2019-09-26 12:00'),
        },
        {
          text : 'This is fourth msg',
          username : 'josh',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 13:00'),
        }
      ],
      participants : [
        {
          id: 1,
          isAdmin: 1,
          pname : 'Joe'},
      {
        id: 2,
        isAdmin: 2,
        pname : 'Ben'},
      {
        id: 3,
        isAdmin: 1,
        pname : 'admin'},
      {
        id: 4,
        isAdmin: 2,
        pname : 'Josh'},
      {
        id: 5,
        isAdmin: 2,
        pname : 'Tim'}
      ]
    },
    {
      id : 7,
      name : 'Group 5',
      lastUser : 'User 5',
      lastMsg : 'Message 5',
      date : '22-Sep-2017',
      msgs : [
        {
          text : 'This is first msg',
          username : 'Joe', 
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 10:00'),
        },
        {
          text : 'This is second msg',
          username : 'Ben',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 11:00'),
        },
        {
          text : 'This is third msg',
          username : 'Admin',
          avatar : '',
          date : new Date('2019-09-26 12:00'),
        },
        {
          text : 'This is fourth msg',
          username : 'josh',
          avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
          date : new Date('2019-09-26 13:00'),
        }
      ],
      participants : [
        {
          id: 1,
          isAdmin: 1,
          pname : 'Joe'},
      {
        id: 2,
        isAdmin: 2,
        pname : 'Ben'},
      {
        id: 3,
        isAdmin: 1,
        pname : 'admin'},
      {
        id: 4,
        isAdmin: 2,
        pname : 'Josh'},
      {
        id: 5,
        isAdmin: 2,
        pname : 'Tim'}
      ]
    },
  ];
  interestList: any = [
    {interestId: 1, iname: "Aluminium"},
    {interestId: 2, iname: "Copper"},
    {interestId: 3, iname: "Lead"},
    {interestId: 4, iname: "Bronze"},
    {interestId: 5, iname: "Iron"},
    {interestId: 6, iname: "Zinc"},
    {interestId: 7, iname: "Nickel"},
  ];
  corpList: any = [
    {corpId: 1, corname: "Tata International"},
    {corpId: 2, corname: "Jindal Steel & Power"},
    {corpId: 3, corname: "XYZ Company"},
    {corpId: 4, corname: "NEW test company"},
    {corpId: 5, corname: "birla corporate"},
  ];
  allList: any = [
    {id: 1, pname : 'Harshavardhan malve'}, {id: 2, pname : 'Ben'}, {id: 3, pname : 'admin'},
                      {id: 4, pname : 'Josh'}, {id: 5, pname : 'Tim'}, {id: 6, pname : 'TestUser1'}, {id: 7, pname : 'TestUser2'}, {id: 8, pname : 'TestUser3'},
                      {id: 9, pname : 'TestUser4'}, {id: 10, pname : 'TestUser5'}
  ];
  enableChat: any = false;
  userName: any;
  userD: any = [];
  messages: any = [];
  groupName: any = [];
  members: any;
  sendMsg: any = '';
  enableInfo: any = false;
  falseInfo: any = false;
  enableGrpNameEdit: any = false;
  enableGrpDescEdit: any = false;
  partcList: any = [];
  toggleNgModel: any = true;
  public name1: String = 'Choose image';
  public str1: String = '';
  message: string;
  public imagePath;
  public filePath;
  imgURL: any = 'assets/images/userfor.png';
  imgUser: any = 'assets/images/AddUser.jpg';
  isImg: any;
  isFile: any;
  hidePopup: any = false;
  openCreateGrp: any = false;
  getChats: any = [
    {
      text : 'This is first msg',
      username : 'Joe', 
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 10:00'),
    },
    {
      text : 'This is second msg',
      username : 'Ben',
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 11:00'),
    },
    {
      text : 'This is third msg',
      username : 'Admin',
      avatar : '',
      date : new Date('2019-09-26 12:00'),
    },
    {
      text : 'This is fourth msg',
      username : 'josh',
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 13:00'),
    },
    {
      text : 'This is first msg',
      username : 'Joe', 
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 10:00'),
    },
    {
      text : 'This is second msg',
      username : 'Ben',
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 11:00'),
    },
    {
      text : 'This is third msg',
      username : 'Admin',
      avatar : '',
      date : new Date('2019-09-26 12:00'),
    },
    {
      text : 'This is fourth msg',
      username : 'josh',
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 13:00'),
    },
    {
      text : 'This is first msg',
      username : 'Joe', 
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 10:00'),
    },
    {
      text : 'This is second msg',
      username : 'Ben',
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 11:00'),
    },
    {
      text : 'This is third msg',
      username : 'Admin',
      avatar : '',
      date : new Date('2019-09-26 12:00'),
    },
    {
      text : 'This is fourth msg',
      username : 'josh',
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 13:00'),
    },
    {
      text : 'This is first msg',
      username : 'Joe', 
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 10:00'),
    },
    {
      text : 'This is second msg',
      username : 'Ben',
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 11:00'),
    },
    {
      text : 'This is third msg',
      username : 'Admin',
      avatar : '',
      date : new Date('2019-09-26 12:00'),
    },
    {
      text : 'This is fourth msg',
      username : 'josh',
      avatar : 'https://www.pnglot.com/pngfile/detail/192-1925683_user-icon-png-small.png',
      date : new Date('2019-09-26 13:00'),
    }
  ];
  constructor(private cdf: ChangeDetectorRef) { }

  ngOnInit() {
    this.userD = JSON.parse(localStorage.getItem('UserData'));
    this.userName = this.userD.subname;
    for(let i = 0; i < this.groups.length; i++) {
      this.groups[i].msg = this.groups[i].lastUser + ' ' + ':' + ' ' + this.groups[i].lastMsg;
    }
    this.formatDateForChat();
  }

  getMessages(item) {
    this.enableChat = true;
    this.messages = item.msgs;
    this.groupName = item.name;
    this.partcList = item.participants;
    console.log('partcList', this.partcList)
    let tempList = [];
    for (let i = 0; i < this.partcList.length; i++) {
      tempList.push(this.partcList[i].pname);
    }
    let userNM = [];
    this.members = tempList.join(',');
    for (let i = 0; i < this.messages.length; i++) {
      userNM = this.messages[i].user;
      // this.messages[i].reply = userNM[0].name === this.userName ? true : false;
    }
    console.log('this.members',this.members);
  }

  sendMessage() {
    this.messages.push({
      text: this.sendMsg,
      date: new Date(),
      username: this.userName,
      avatar: '',
    });
    this.sendMsg = '';
    this.scrollToBottom();
    // window.setInterval(function() {
    //   var elem = document.getElementById('scrollMe');
    //   elem.scrollTop = elem.scrollHeight;
    // }, 1000);
    // this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      setTimeout(() => {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        this.cdf.detectChanges();
      }, 100);
    } catch (err) {

    }
    }
    openDeatails() {
      this.enableInfo = true;
    }
    closeDeatails() {
      this.falseInfo = true;
      this.enableInfo = false;
    }
    enableEdit() {
      this.enableGrpNameEdit = !this.enableGrpNameEdit;
    }
    enableEditDesc() {
      this.enableGrpDescEdit = !this.enableGrpDescEdit;
    }
    onChange(event) {
      console.log(event);
    }
    onScroll() {
      console.log('scrolled!!');
    }
    onScrollUp() {
      // this.messages = this.messages.concat(this.getChats);
    }
      // image preview
  preview(files) {
    this.name1 = this.str1;
    const str11: any[] = this.name1.split('\\');
    this.name1 = str11[str11.length - 1];

    if (files.length === 0)
      return;

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.name1 = 'Choose image';
      this.message = 'Only images are supported.';
      return;
    } else {
      this.message = '';
      const reader = new FileReader();
      this.imagePath = files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.imgURL = reader.result;
      };
    }
  }
  
   onDeSelectAll(items: any) {
    console.log(items);
  }
  hideinvoice() {
    this.hidePopup = !this.hidePopup;
    console.log('hideinvoice',this.partcList);
  }
  createGroup() {
    this.openCreateGrp = !this.openCreateGrp;
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear(),
      time = d.getTime();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  formatDateForChat(){
    this.groups.forEach(element => {
        element.msgs.forEach(item => {
          item.formatedDate = this.formatDate(item.date);
        })
    });
  }
}
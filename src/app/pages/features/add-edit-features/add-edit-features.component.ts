import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { DatePipe } from '@angular/common';
import { EditFeaturesService } from '../../../service/edit-features/edit-features.service';
import { BrowserInfoService } from '../../../service/browser-info.service';

@Component({
  selector: 'ngx-add-edit-features',
  templateUrl: './add-edit-features.component.html',
  styleUrls: ['./add-edit-features.component.scss']
})
export class AddEditFeaturesComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  isNotification: any = 0;
  isDeleted: any = 0;
  isSuspended: any = 0;
  isPolicyAgreed: any = 0;
  isotpVerified: any;
  roleArray: any = [];
  selectRole: any;
  editFeatureData: any = [];
  userStatus: any;
  userFlag: any;
  nameVal: any = new RegExp('^[A-Za-z& -]+$');
  userData :any;
  bowserInfo:any;
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private passSer: PassServService,
    private datePipe: DatePipe,
    private editFeatureSer: EditFeaturesService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.editFeatureData = this.passSer.editFeature;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    this.registerForm = this.formBuilder.group({
      fcode : [this.editFeatureData.fcode, [Validators.required]],
      featureName: [this.editFeatureData.featureName, [Validators.required, Validators.pattern('^[A-Za-z& -]+$')]],
      paidType : [this.editFeatureData.paidType, [Validators.required]],
      status: [this.editFeatureData.status, [Validators.required]],
    });
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.spinner.show();
      const params = {
        f_code : this.registerForm.value.fcode,
        f_name : this.registerForm.value.featureName,
        f_paytype : this.registerForm.value.paidType,
        f_status : this.registerForm.value.status,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      console.log('params', params);
      this.editFeatureSer.editFeature(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.data.updated) {
                this.router.navigate(['pages/features']);
                this.toastr.success(res.data.updated);
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }
  back() {
    this.router.navigate(['pages/features']);
  }
  Validation(event) {
    console.log('event', event);
    if (event.target.placeholder === 'Feature Code') {
      if (!event.target.value) {
        this.toastr.error('Feature Code is required');
      }
    }
    if (event.target.placeholder === 'Feature') {
      if (!event.target.value) {
        this.toastr.error('Feature is required');
      } else if (!this.nameVal.test(event.target.value)) {
        this.toastr.error('Numbers and Special characters are not allowed');
      }
    }
  }
}

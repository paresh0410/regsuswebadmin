import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../../config';
import { FetchCorporationDealsService } from '../../../service/fetch-corporation-deals/fetch-corporation-deals.service';
import { XlsxToJsonService } from '../../../service/xlsx-to-json-service';
import { FetchSubscribersService } from '../../../service/fetch-subscribers/fetch-subscribers.service';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { BrowserInfoService } from '../../../service/browser-info.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ngx-deals',
  templateUrl: './deals.component.html',
  styleUrls: ['./deals.component.scss']
})
export class DealsComponent implements OnInit {
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  dealsList: any = [];

  invoice: any = [];
  tooggleshowHidw = false;
  p: number = 1;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  search = {
    trackId: '',
    pkgName: '',
    purDate: '',
    fromDate: '',
    toDate: '',
    modifiedDate: '',
    felenth: '',
    discountPercent: '',
    // duration: '',
    durationId: '',
    userCount: '',
    totalAmt: '',
    payType: '',
    receiptNo: '',
    payDate: '',
    dealstaId: '',
    isActive: '',
  };
  display: any = 'none';
  showDiv: boolean = true;
  fileName: any = 'Select user template';
  fileReaded: any;
  enableUpload: boolean;
  bulkUploadAssetData: any;
  resultSheets: any = [];
  result: any = [];
  uploadedData: any = [];
  fileUrl: any;
  display1: any = 'none';
  showDiv1: boolean = true;
  address: any
  arrow: any = false;
  featureLength: any = true;
  setveractive: any = false;
  gstNumber: any;
  tempData: any = [];
  corpID: any;
  corpName: any;
  featuresData: any;
  durationData: any;
  packageData: any = [];
  gstData: any = [];
  hidePopup: boolean = true;
  cardDetails: any;
  makenot: any = true;
  showOptions: any = false;
  validdata: any = [];
  invaliddata: any = [];
  todayDate: any = new Date();
  emailVal: any = new RegExp('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$');
  mobVal: any = new RegExp('^[0-9]{10}$');
  baseUrl: any = webApi.baseUrl;
  getDownload = this.baseUrl + webApi.apiUrl.getTemplate;
  userData: any;
  bowserInfo: any;
  showGSt: boolean;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private fetchDealSer: FetchCorporationDealsService,
    private fetchSubSer: FetchSubscribersService,
    private browserInfoService: BrowserInfoService,
    private http: HttpClient
  ) {
    this.tempData = this.passSer.editCorp;
    this.gstNumber = this.tempData[0].gst;
    this.address = this.tempData[0].address;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
    if (this.tempData[0]) {
      this.corpID = this.tempData[0].corpId;
      this.corpName = this.tempData[0].corpName;
      console.log('this.corpID', this.corpID);
    } else {
      this.makenot = false;
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.fetchDeals();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.dealsList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  clearBtn() {
    this.search.trackId = '';
    this.search.pkgName = '';
    this.search.purDate = '';
    this.search.fromDate = '';
    this.search.toDate = '';
    this.search.modifiedDate = '';
    this.search.felenth = '';
    this.search.discountPercent = '';
    this.search.durationId = '';
    this.search.userCount = '';
    this.search.totalAmt = '';
    this.search.payType = '';
    this.search.receiptNo = '';
    this.search.payDate = '';
    this.search.dealstaId = '';
    this.search.isActive = '';
  }

  myFunction(event) {
    this.search.isActive = event.target.value;
  }

  changeState(state) {
    this.search.dealstaId = state.target.value;
  }

  back() {
    this.router.navigate(['pages/corporations']);
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join('-');
  }

  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.dealsList.length; i++) {
      if (this.dealsList[i].isActive === 1) {
        this.flag = 'Active';
      }
      if (this.dealsList[i].isActive === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'TRACKER ID': this.dealsList[i].trackId,
        'PACKAGE': this.dealsList[i].selPckage.pkgName,
        'PURCHASE DATE': this.formatDate(this.dealsList[i].purDate),
        'FROM DATE': this.formatDate(this.dealsList[i].fromDate),
        'TO DATE': this.formatDate(this.dealsList[i].toDate),
        'MODIFIED DATE': this.formatDate(this.dealsList[i].modifiedDate),
        'NO. OF FEATURES': this.dealsList[i].felenth,
        'DISCOUNT(%)': this.dealsList[i].discountPercent,
        'DURATION(MONTHS) ': this.dealsList[i].durationId,
        'NO. OF PERSONS': this.dealsList[i].userCount,
        'TOTAL PRICE': this.dealsList[i].totalAmt,
        'PAYMENT TYPE': this.dealsList[i].payType,
        'RECEIPT NO.': this.dealsList[i].receiptNo,
        'PAYMENT DATE': this.formatDate(this.dealsList[i].payDate),
        'STATE': this.dealsList[i].dealstaname,
        'STATUS': this.flag,
      },
        // console.log('this.dataArray',this.dataObjforExcel)

        this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Deals Details');
  }

  AddDeals() {
    this.passSer.editDeals = [];
    this.passSer.renewDeals = [];
    this.passSer.featureList = this.featuresData;
    this.passSer.durationList = this.durationData;
    this.passSer.packageList = this.packageData;
    this.passSer.gstList = this.gstData;
    console.log(this.passSer.featureList);
    this.router.navigate(['pages/corporations/deals/add-edit-deals']);
  }

  editDeals(editData) {
    this.passSer.editDeals[0] = editData;
    this.passSer.renewDeals = [];
    this.passSer.featureList = this.featuresData;
    this.passSer.durationList = this.durationData;
    this.passSer.packageList = this.packageData;
    this.passSer.gstList = this.gstData;
    this.router.navigate(['pages/corporations/deals/add-edit-deals']);
  }

  renewDeals(newData) {
    this.passSer.renewDeals[0] = newData;
    this.passSer.editDeals = [];
    this.passSer.featureList = this.featuresData;
    this.passSer.durationList = this.durationData;
    this.passSer.packageList = this.packageData;
    this.router.navigate(['pages/corporations/deals/add-edit-deals']);
  }

  showinvoice(item) {
    this.hidePopup = false;
    this.cardDetails = item;
    if (item.gtypeid == 2) {
      this.showGSt = false;
    } else {
      this.showGSt = true;
    }
    console.log("carddetails ==", this.cardDetails);
  }

  hideinvoice() {
    this.hidePopup = true;
  }

  featuretotal(price, unit) {
    return (price * unit).toFixed(0);
  }

  userTotal(tprice, user) {
    return (tprice * user).toFixed(0);
  }

  percent(baseAmount, percent) {
    return ((baseAmount / 100) * percent).toFixed(0);
  }

  gstpercent(baseAmount, percent, gst) {
    let discount = (baseAmount * percent) / 100;
    let accprice = baseAmount - discount;
    this.cardDetails.accprice = accprice;
    this.cardDetails.gstvalue = (accprice * gst) / 100;
    return ((accprice / 100) * gst).toFixed(0);
  }
  acGrandTotal() {
    if (!this.cardDetails.gst) {
      let discount = (this.cardDetails.baseAmt * this.cardDetails.discountPercent) / 100;
      this.cardDetails.accprice = this.cardDetails.baseAmt - discount;
      return this.cardDetails.accprice
    } else {
      var gval = (this.cardDetails.accprice / 100) * this.cardDetails.gst;
      return (this.cardDetails.accprice + gval).toFixed(0);
    }
  }

  acGrandTotalW() {
    // const converter = require('number-to-words');
    // const wvalue = (this.cardDetails.accprice + this.cardDetails.gstvalue).toFixed(0);
    // const main = converter.toWords(wvalue);
    // const {toWords} = require('number-in-words');
    // const main = parseInt((this.cardDetails.accprice + this.cardDetails.gstvalue).toFixed(0), 10);
    // return toWords(main);
    var gval = (this.cardDetails.accprice / 100) * this.cardDetails.gst;
    const writtenNumber = require('written-number');
    const main = parseInt((this.cardDetails.accprice + gval).toFixed(0), 10);
    let word = writtenNumber(main);
    word = word.replace(/-/g, ' ');
    return word;
  }

  showFile() {
    this.display = 'block';
    this.showDiv = false;
    this.fileName = 'Select user template';
  }

  showMore() {
    this.showOptions = !this.showOptions;
  }

  public captureScreen() {
    var data = document.getElementById('contentToConvert');
    var currentPosition = document.getElementById("contentToConvert").scrollTop;
    var w = document.getElementById("contentToConvert").offsetWidth;
    var h = document.getElementById("contentToConvert").offsetHeight;
    document.getElementById("contentToConvert").style.height = "auto";
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save(this.corpName + '.pdf'); // Generated PDF   
    });
    document.getElementById("contentToConvert").style.height = "100px";
    document.getElementById("contentToConvert").scrollTop = currentPosition;
    this.hideinvoice();
  }


  readFileUrl(event: any) {
    // this.fileName = 'Select user template';
    const validExts = new Array('.xlsx', '.xls');
    let fileExt = event.target.files[0].name;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      this.toastr.error('Please select the user template excel file');
    } else {
      // return true;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        // read file from input
        this.fileReaded = event.target.files[0];

        if (this.fileReaded) {
          this.enableUpload = true;
        }

        let file = event.target.files[0];
        this.bulkUploadAssetData = event.target.files[0];
        console.log('this.bulkUploadAssetDataFileRead', this.bulkUploadAssetData);

        this.xlsxToJsonService.processFileToJson({}, file).subscribe(data => {
          this.resultSheets = Object.getOwnPropertyNames(data['sheets']);
          console.log('File Property Names ', this.resultSheets);
          let sheetName = this.resultSheets[0];
          if (sheetName) {
            this.result = data['sheets'][sheetName];
          } else {
            this.result = [];
          }
          console.log('dataSheet', data);

          if (this.result.length > 0) {
            this.uploadedData = this.result;
            console.log(' this.uploadedData', this.uploadedData);
          } else {
            this.uploadedData = [];
          }
        });
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }
  }

  uploadSheet() {
    this.spinner.show();
    if (this.uploadedData.length === 0) {
      this.spinner.hide();
      this.toastr.error('Please select valid file');
    } else {
      console.log(this.uploadedData);
      // this.rows = this.uploadedData;
      this.passSer.editCorp = this.tempData[0];
      for (let i = 0; i < this.uploadedData.length; i++) {
        this.uploadedData[i].valid = 'Valid';
        this.uploadedData[i].reason = '';
        if (!this.uploadedData[i].Name) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Name is required';
        }
        // else if (!this.fnameVal.test(this.uploadedData[i].FirstName)){
        //   this.uploadedData[i].valid = 'Invalid';
        //   this.uploadedData[i].reason  += ' First Name is invalid';
        // }
        if (!this.uploadedData[i].Email) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Email is required';
        } else if (!this.emailVal.test(this.uploadedData[i].Email)) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Email is invalid';
        }
        if (!this.uploadedData[i].Mobile) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Mobile no is required';
        } else if (!this.mobVal.test(this.uploadedData[i].Mobile)) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Mobile no is invalid';
        }
        // if (!this.uploadedData[i].CorporateCode) {
        //   this.uploadedData[i].valid = 'Invalid';
        //   this.uploadedData[i].reason += ' Corporate Code is required';
        // } else if (this.uploadedData[i].CorporateCode !== this.company) {
        //   this.uploadedData[i].valid = 'Invalid';
        //   this.uploadedData[i].reason += ' Corporate code is invalid';
        // }
        if (!this.uploadedData[i].Designation) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Designation is required';
        }
        if (!this.uploadedData[i].Address) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Address is required';
        } else if (this.uploadedData[i].Address !== this.tempData[0].address) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Address is invalid';
        }

        if (!this.uploadedData[i].Country_Code) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += 'Country code is required';
        } else if (!this.mobVal.test(this.uploadedData[i].Mobile)) {
          this.uploadedData[i].valid = 'Invalid';
          this.uploadedData[i].reason += ' Country code is invalid';
        }

        // if (!this.uploadedData[i].GST) {
        //   this.uploadedData[i].valid = 'Invalid';
        //   this.uploadedData[i].reason += ' GST is required';
        // } else if (this.uploadedData[i].GST !== this.tempData[0].gst) {
        //   this.uploadedData[i].valid = 'Invalid';
        //   this.uploadedData[i].reason += ' GST is invalid';
        // }


        // else if (!this.gstVal.test(this.uploadedData[i].GST)) {
        //   this.uploadedData[i].valid = 'Invalid';
        //   this.uploadedData[i].reason += ' GST is invalid';
        // }
      }
      for (let i = 0; i < this.uploadedData.length; i++) {
        if (this.uploadedData[i].valid === 'Valid') {
          this.validdata.push(this.uploadedData[i]);
        } else {
          this.invaliddata.push(this.uploadedData[i]);
        }
      }
      this.spinner.hide();
      this.fetchSubSer.resetUser();
      this.fetchSubSer.setvalidUser(this.validdata);
      this.fetchSubSer.setinvalidUser(this.invaliddata);
      console.log('valid data:', this.validdata);
      console.log('invalid data:', this.invaliddata);
      this.router.navigate(['pages/user/subscriber-validation']);
    }
  }

  download() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      window.open(this.getDownload, '_self');
    }
  }

  remFile() {
    this.display = 'none';
    this.showDiv = true;
  }

  fetchDeals() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    }
    else {
      const params = {
        corid: this.corpID,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      }
      this.fetchDealSer.fetchCorpDeals(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              // let userD = [];
              this.dealsList = res.data;
              for (let i = 0; i < this.dealsList.length; i++) {
                this.dealsList[i].purDate = new Date(this.dealsList[i].purDate);
                this.dealsList[i].fromDate = new Date(this.dealsList[i].fromDate);
                this.dealsList[i].toDate = new Date(this.dealsList[i].toDate);
                this.dealsList[i].modifiedDate = new Date(this.dealsList[i].modifiedDate);
                this.dealsList[i].payDate = new Date(this.dealsList[i].payDate);
                if (isNaN(this.dealsList[i].payDate)) {
                  this.dealsList[i].payDate = '';
                }
                this.dealsList[i].pkgName = this.dealsList[i].selPckage.pkgName;
                let preDate = new Date(this.dealsList[i].toDate);
                preDate.setMonth(preDate.getMonth() - 2);
                const lastdate = preDate.toLocaleDateString();
                // console.log(preDate.toLocaleDateString());
                if (new Date(lastdate) < this.todayDate) {
                  this.dealsList[i].activeInvoice = true;
                } else {
                  this.dealsList[i].activeInvoice = false;
                }
              }
              this.featuresData = res.featureList;
              this.durationData = res.durationList;
              this.packageData = res.packageList;
              this.gstData = res.gstList;
              if (this.dealsList.length === 0) {
                this.featureLength = false;
              } else if (this.dealsList.length > 0) {
                this.featureLength = true;
              }
              console.log('this.dealsList', this.dealsList, this.featuresData, this.durationData);
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            if (this.dealsList.length === 0) {
              this.featureLength = false;
            } else if (this.dealsList.length > 0) {
              this.featureLength = true;
            }
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.dealsList = [];
          this.setveractive = true;
          this.toastr.error('Server Error');
        });
    }
  }

  emailInvoice(tmId) {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    }
    else {
      this.spinner.show();
      const params = {
        tmId: tmId
      }
      const url = webApi.baseUrl + webApi.apiUrl.sendCorporateEmail
      this.http.post(url, params).subscribe((res: any) => {
        console.log(res)
        try {
          if (res.type) {
            this.toastr.success(res.data);
          } else {
            this.toastr.error(res.data);
          }
          this.spinner.hide();

        } catch (error) {
          console.log(error);
          this.spinner.hide();

        }
      }, err => {
        console.log(err);
        this.toastr.error('Server Error');
        this.spinner.hide();

      })
    }
  }
}

import { TestBed } from '@angular/core/testing';

import { FetchDealsOfCorporationService } from './fetch-deals-of-corporation.service';

describe('FetchDealsOfCorporationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchDealsOfCorporationService = TestBed.get(FetchDealsOfCorporationService);
    expect(service).toBeTruthy();
  });
});

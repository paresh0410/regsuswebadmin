import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../service/pass-serv.service';
import { webApi } from '../../../config';
import { FetchNewsOpinionsService } from '../../service/fetch-news-opinions/fetch-news-opinions.service';
import { SearchNewsOpinionsService } from '../../service/search-news-opinions/search-news-opinions.service';
import { BrowserInfoService } from '../../service/browser-info.service';
import { ExcelService } from '../../service/excel.service';

@Component({
  selector: 'ngx-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  p: number = 1;
  q: number = this.p;
  showhide = true;
  title: any;
  viewsName: any;
  userData: any;
  bowserInfo: any;
  hidePopup: boolean = true;
  dropdownList: any = [
    // { catId: 1 , catname: 'News & Opinions'},
    // { catId: 2 , catname: 'Opinions'},
    // { catId: 3 , catname: 'Economical Data'},
    // { catId: 4 , catname: 'Breaking News'},
    // { catId: 5 , catname: 'Forecasts'},
  ];
  selectedItems: any = [];
  dropdownSettings: any = {
    singleSelection: false,
    idField: 'categoryId',
    textField: 'cateName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 2,
    allowSearchFilter: true,
  };
  selectedItems1: any = [];
  dropdownSettings1: any = {
    singleSelection: false,
    idField: 'interestId',
    textField: 'intersetName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 2,
    allowSearchFilter: true,
  };
  posts: any = [
    // {
    // id: 1,
    // postname : 'Angular session 1',
    // description : 'Angular 2 follows component/Service model of architecture.An angular 2 Application is made of components. A component is the combination of an HTML template and a component class (A typescript class ) that controls a portion of the screen.For the good practice, component class is used for data binding to the respective view. Two way data binding is a great feature provided by angular framework.Components are reusable across your application using selector name provided.Component is also a kind of directive with a template.',
    // createddate : '08-04-2019',
    // author : 'Shubham',
    // imgpath : '../../../assets/images/camera1.jpg',
    // likecount : 2,
    // favcount : 1,
    // categories : [
    //   { catID : 1, cat: 'News & Opinions', catIcon: '../../../assets/images/news.png'},
    // ],
    // interests : [
    //   { interestId : 1, intersetName : 'Aluminium' },
    // ],
    // catIcon: '../../../assets/images/news.png',
    // postUrl : 'https://www.tutorialspoint.com/angular2/',
    // comment : 'this is test comment',
    // status : 1,
    // },
    // {
    //   id: 2,
    //   postname : 'Angular session 2',
    //   description : 'Data binding help us coordinate communication between a component and its view template. Data binding consist of One-Way Data-Binding and Two-Way Data-Binding.Interpolation is all about data binding and so as property binding as the name justify, but they both flow a value in one direction. it’s all about moving data in one direction from our components to HTML elements.',
    //   createddate : '03-03-2018',
    //   author : 'Shubham',
    //   imgpath : '../../../assets/images/camera2.jpg',
    //   likecount : 6,
    //   favcount : 3,
    //   categories : [
    //     { catID : 2, cat: 'Opinions', catIcon: '../../../assets/images/opinions.png'},
    //   ],
    //   interests : [
    //     { interestId : 1, intersetName : 'Aluminium' },
    //     { interestId : 2, intersetName : 'Copper' },
    //   ],
    //   catIcon: '../../../assets/images/opinions.png',
    //   postUrl : 'https://www.c-sharpcorner.com/article/data-binding-in-angular-2/',
    //   comment : 'this is another test comment',
    //   status : 1,
    //   },
    //   {
    //     id: 3,
    //     postname : 'Angular session 3',
    //     description : 'Angular is a platform that makes it easy to build applications with the web. Angular combines declarative templates, dependency injection, end to end tooling, and integrated best practices to solve development challenges. Angular empowers developers to build applications that live on the web, mobile, or the desktop',
    //     createddate : '07-04-2019',
    //     author : 'Sachin',
    //     imgpath : '../../../assets/images/camera3.jpg',
    //     likecount : 12,
    //     favcount : 2,
    //     categories : [
    //       { catID : 3, cat: 'Economical Data', catIcon: '../../../assets/images/economicaldata.png'},
    //     ],
    //     interests : [
    //       { interestId : 5, intersetName : 'Iron' },
    //       { interestId : 6, intersetName : 'Zinc' },
    //       { interestId : 7, intersetName : 'Nickel' },
    //       { interestId : 3, intersetName : 'Lead' },
    //     ],
    //     catIcon: '../../../assets/images/economicaldata.png',
    //     postUrl : 'https://angular.io/guide/cheatsheet',
    //     comment : 'this is another economical test comment',
    //     status : 2,
    //   },
    //   {
    //     id: 4,
    //     postname : 'Ionic session 1',
    //     description : 'Ionic apps are made of high-level building blocks called Components, which allow you to quickly construct the UI for your app. Ionic comes stock with a number of components, including cards, lists, and tabs. Once you’re familiar with the basics, refer to the API Index for a complete list of each component and sub-component.',
    //     createddate : '04-04-2019',
    //     author : 'Ronit',
    //     imgpath : '../../../assets/images/camera4.jpg',
    //     likecount : 45,
    //     favcount : 8,
    //     categories : [
    //       { catID : 4, cat: 'Breaking News', catIcon: '../../../assets/images/breakingnews.png'},
    //     ],
    //     interests : [
    //       { interestId : 1, intersetName : 'Aluminium' },
    //       { interestId : 3, intersetName : 'Lead' },
    //       { interestId : 5, intersetName : 'Iron' },
    //     ],
    //     catIcon: '../../../assets/images/breakingnews.png',
    //     postUrl : 'https://ionicframework.com/',
    //     comment : 'this is another breaking news test comment',
    //     status : 1,
    //   },
  ];
  search = {
    neOpinName: '',
    cateName: '',
    categoryId: '',
    neOpindesc: '',
    neOpincomment: '',
    // intersetName: '',
    createdDate: '',
    publishDate: '',
    viewCount: '',
    favCount: '',
    isActive: '',
  };

  searchView = {
    subname: '',
    subemail: '',
    phoneno: '',
    corpname: '',
  }


  searchTag = {
    intersetName: ''
  }

  search1 = {
    fromdate: '',
    todate: '',
    selectTag: [],
  };
  arrow = false;
  detailView: any = [];
  isDesc: any = [];
  column: any = [];
  searchCount: any = 0;
  featureLength: any = true;
  setveractive: any = false;
  baseUrl: any = webApi.baseUrl;
  getDownload = this.baseUrl + webApi.apiUrl.getNewsOpinionImage;
  getDownloadI = this.baseUrl + webApi.apiUrl.getCatImage;
  splitArray: any = [];
  subsplitArray: any = [];
  categoryList: any = [];
  authorlist: any = [];
  interestList: any = [];
  isCheck: string;
  isCheck1: string;
  catID: any = '';
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  flag: any;

  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private toastr: ToastrService,
    private passService: PassServService,
    private fetchNOSer: FetchNewsOpinionsService,
    private searchSer: SearchNewsOpinionsService,
    private browserInfoService: BrowserInfoService,
    private excelService: ExcelService,
    private newopinServ: FetchNewsOpinionsService,
  ) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.spinner.show();
    this.getNewsopinions();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
  }

  onItemSelect(event) {
    if (event.categoryId) {
      console.log('select', this.selectedItems);
      this.isCheck = '';
    }
    if (event.interestId) {
      console.log('select', this.selectedItems1);
      this.isCheck1 = '';
    }
  }
  onItemDeSelect(des) {
    if (des.categoryId) {
      for (let i = 0; i < this.selectedItems.length; i++) {
        if (des.categoryId === this.selectedItems[i].categoryId) {
          this.selectedItems.splice(i, 1);
        }
      }
      console.log('deselect', this.selectedItems);
      if (this.selectedItems.length === 0) {
        this.isCheck = 'Please select categories';
      }
    }
    if (des.interestId) {
      for (let i = 0; i < this.selectedItems1.length; i++) {
        if (des.interestId === this.selectedItems1[i].interestId) {
          this.selectedItems1.splice(i, 1);
        }
      }
      console.log('deselect', this.selectedItems1);
      if (this.selectedItems1.length === 0) {
        this.isCheck1 = 'Please select interests';
      }
    }
  }
  onSelectAll(allSelect) {
    if (allSelect[0].categoryId) {
      this.isCheck = '';
      this.selectedItems = allSelect;
      console.log('selectAll', this.selectedItems);
    }
    if (allSelect[0].interestId) {
      this.isCheck1 = '';
      this.selectedItems1 = allSelect;
      console.log('selectAll', this.selectedItems1);
    }
  }

  addArticle() {
    this.passService.editPost = [];
    this.passService.CategoryList = this.categoryList;
    this.passService.AuthorList = this.authorlist;
    this.passService.interestList = this.interestList;
    this.router.navigate(['pages/posts/add-edit-posts']);
  }
  viewArticle(postData) {
    // this.passService.viewPost = postData;
    // this.router.navigate(['pages/article/view-article']);
  }
  editArticle(editData, image, action) {
    this.passService.editPost[0] = (editData);
    this.passService.imageflag = image;
    this.passService.CategoryList = this.categoryList;
    this.passService.AuthorList = this.authorlist;
    this.passService.interestList = this.interestList;
    this.passService.editAction = action;
    this.router.navigate(['pages/posts/add-edit-posts']);
  }

  display = 'none';
  showDiv: boolean = true;
  openModalDialog() {
    this.display = 'block';
    this.showDiv = false;
    // console.log(this.showDiv);
  }

  closeModalDialog() {
    this.display = 'none';
    this.showDiv = true;
  }

  clearBtn() {
    console.log('clearBtn')
    this.search.neOpinName = '';
    this.search.cateName = '';
    this.search.categoryId = ''
    this.search.neOpindesc = '';
    this.search.neOpincomment = '';
    // intersetName: '',
    this.search.createdDate = '';
    this.search.publishDate = '';
    this.search.viewCount = '';
    this.search.favCount = '';
    this.search.isActive = ''
    // this.myFunction('')
  }

  clearViewDetail() {
    console.log('clearViewDetail')
    this.searchView.subname = '';
    this.searchView.subemail = '';
    this.searchView.phoneno = '';
    this.searchView.corpname = '';
  }

  // sort 
  invert() {
    this.arrow = !this.arrow;
  }

  myFunction(event) {
    // if (event) {
    this.search.isActive = event.target.value;
    // } else {
    //   this.search.isActive = '0'
    // }
    console.log('value', event);
  }

  myFunctionCat(event) {
    if (this.categoryList.length > 0) {
      if (event.target.value) {
        this.search.categoryId = this.categoryList.find(a => a.categoryId == event.target.value).categoryId
        // this.categoryList.find(a => a.cateName == this.search.cateName).categoryId
      } else {
        this.search.categoryId = event.target.value
      }
    }
    console.log('value', event.target.value);
  }
  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.posts.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  sortView(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.detailView.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.posts.length; i++) {
      if (this.posts[i].isActive === 1) {
        this.flag = 'Active';
      }
      if (this.posts[i].isActive === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'TITLE': this.posts[i].neOpinName,
        'CATEGORY': this.posts[i].cateName,
        'DESCRIPTION': this.posts[i].neOpindesc,
        'COMMENT': this.posts[i].neOpincomment,
        'CREATED DATE': this.posts[i].createdDate,
        'PUBLISH DATE': this.posts[i].publishDate,
        'VIEWS': this.posts[i].viewCount,
        'FAVOURITES': this.posts[i].favCount,
        'STATUS': this.flag ? this.flag : null,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'News & Opinions Details');
  }


  // clear button
  // clearBtn() {
  //   this.search.neOpinName = '';
  // }
  clearBtn1() {
    // this.search1.fromdate = '';
    // this.search1.todate = '';
    this.search1.selectTag = [];
    this.selectedItems = [];
    this.selectedItems1 = [];
    this.catID = '';
    this.isCheck = '';
    if (this.searchCount > 0) {
      this.getNewsopinions();
      this.closeModalDialog();
      this.searchCount = 0;
    }
  }
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  getNewsopinions() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      this.fetchNOSer.fetchNewsOpinions(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              // let userD = [];
              this.posts = res.data;
              this.categoryList = this.dropdownList = res.category;
              this.authorlist = res.author;
              console.log('this.categoryList', this.categoryList);
              this.interestList = res.interest;
              if (this.posts.length === 0) {
                this.featureLength = false;
              } else if (this.posts.length > 0) {
                this.featureLength = true;
                for (let i = 0; i < this.posts.length; i++) {
                  this.posts[i].cateIcon = this.getDownloadI + this.posts[i].cateIcon;
                  // this.posts[i].neOpinimage = this.getDownload + this.posts[i].neOpinimage;
                  // this.posts[i].neOpImages = this.getDownload + this.posts[i].neOpImages;
                  // this.posts[i].createdDate = new Date(this.posts[i].createdDate);
                  // this.splitArray = this.posts[i].neOpinimage.split(',');
                  // for (let j = 0; j < this.splitArray.length; j++) {
                  //   this.subsplitArray = this.splitArray[i].split('|');
                  //   let intObj = {
                  //     interestId = 
                  //   }
                  // }
                }
              }
              console.log('this.posts', this.posts);
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.posts = [];
          this.setveractive = true;
          this.toastr.error('Server Error');
        });
    }
  }

  showViewDetails(item) {
    console.log('item', item);
    if (item.viewCount == 0) {
      return;
    }


    this.hidePopup = false;
    this.viewsName = 'View Details';
    this.spinner.show();

    let data = {
      newsOpinionId: item.newsOpinionId
    }
    this.newopinServ.fetchNewsOpinionViewDetail(data).then(res => {
      this.spinner.hide();
      try {
        console.log('detail Res', res.data);
        this.detailView = res.data;
      } catch (e) {
        console.log(e);
      }
    },
      err => {
        this.spinner.hide();
        console.log('err', err);
        this.toastr.error('Server Error');
      });
    // this.cardDetails = item;
    // console.log("carddetails ==", this.cardDetails);
  }

  showViewFavourite(item) {
    console.log('item', item);
    if (item.favCount == 0) {
      return;
    }


    this.hidePopup = false;
    this.viewsName = "Favourite Details"
    this.spinner.show();

    let data = {
      newsOpinionId: item.newsOpinionId
    }
    this.newopinServ.fetchFavotiteViewDetail(data).then(res => {
      this.spinner.hide();
      try {
        console.log('detail Res', res.data);
        this.detailView = res.data;
      } catch (e) {
        console.log(e);
      }
    },
      err => {
        this.spinner.hide();
        console.log('err', err);
        this.toastr.error('Server Error');
      });
    // this.cardDetails = item;
    // console.log("carddetails ==", this.cardDetails);
  }

  hideViewDetails() {
    this.hidePopup = true;
  }


  searchPost() {
    if (new Date(this.search1.fromdate) > new Date(this.search1.todate)) {
      this.toastr.warning('To date should not be greater than From date');
      if (this.selectedItems1.length === 0) {
        this.isCheck1 = 'Please select interests';
      }
      if (!this.catID) {
        this.isCheck = 'Please select categories';
      }
      return;
    }
    if (this.selectedItems1 === 0 || !this.catID) {
      // if (this.selectedItems.length === 0) {
      //   this.isCheck = 'Please select categories';
      // }
      if (this.selectedItems1.length === 0) {
        this.isCheck1 = 'Please select interests';
      }
      if (!this.catID) {
        this.isCheck = 'Please select categories';
      }
    } else {
      const intrList = [];
      const catList = [];
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.search1));
      for (let i = 0; i < this.selectedItems.length; i++) {
        catList.push(this.selectedItems[i].categoryId);
      }
      for (let i = 0; i < this.selectedItems1.length; i++) {
        intrList.push(this.selectedItems1[i].interestId);
      }
      const content = {
        CateId: parseInt(this.catID),
        IDS: intrList.join(','),
        FrmDate: this.formatDate(this.search1.fromdate),
        ToDate: this.formatDate(this.search1.todate),
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      console.log('content', content);
      this.spinner.show();
      this.searchSer.searchNewsOpinions(content).then(res => {
        if (res.type === true) {
          console.log('res', res);
          // res.data = this.posts;
          this.posts = res.data;
          if (this.posts.length === 0) {
            this.featureLength = false;
            this.closeModalDialog();
          } else if (this.posts.length > 0) {
            this.featureLength = true;
            for (let i = 0; i < this.posts.length; i++) {
              this.posts[i].cateIcon = this.getDownloadI + this.posts[i].cateIcon;
              // this.posts[i].neOpinimage = this.getDownload + this.posts[i].neOpinimage;
            }
            this.closeModalDialog();
          }
          this.spinner.hide();
          this.searchCount++;
        }
        if (res.type === false) {
          this.spinner.hide();
          console.log('res', res);
        }
      }, err => {
        this.spinner.hide();
        // console.log(err);
        this.posts = [];
        this.setveractive = true;
        this.toastr.error('Server Error');
      });
    }
  }
}

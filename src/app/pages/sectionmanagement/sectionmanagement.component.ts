import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { CommonService } from '../../service/common.service';
import { ExcelService } from '../../service/excel.service';
import { SectionsService } from '../../service/sections/sections.service';

@Component({
  selector: 'ngx-sectionmanagement',
  templateUrl: './sectionmanagement.component.html',
  styleUrls: ['./sectionmanagement.component.scss']
})
export class SectionmanagementComponent implements OnInit {
  sectionList: any = []
  tableList: any = []

  column: any = [];
  isDesc: any = [];
  search = {
    name: '',
    description: '',
    ismandatory: '',
    status: ''
  };
  dataArrayforExcel: any = []
  dataObjforExcel: any = []
  setveractive: boolean = false
  constructor(private router: Router, private commonService: CommonService, private toastr: ToastrService,
    private sectionService: SectionsService, private excelService: ExcelService) { }

  ngOnInit() {
    this.fetchSectionData()
  }

  setOrder() {
    this.router.navigate(['pages/sectionmanagement/set-order']);

  }
  addEditSection(item, val) {
    console.log(item)
    this.sectionService.sectionData = item
    this.router.navigate(['pages/sectionmanagement/add-edit-sections']);
  }
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.tableList.length; i++) {
      this.dataObjforExcel = {
        'NAME': this.tableList[i].name,
        'DISCRIPTION': this.tableList[i].description,
        'MANDATARY SECTION': this.tableList[i].ismandatory == 1 ? 'Yes' : 'No',
        'STATUS': this.tableList[i].status == 1 ? 'Active' : 'Inactive',
      }
      // console.log(this.dataObjforExcel)
      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }
  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Section Details');
  }
  sort(val) {

  }
  invert() { }
  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }
  myFunction1(event) {
    this.search.ismandatory = event.target.value;
    console.log('value', event);
  }
  clearBtn() {
    this.search.name = '',
      this.search.description = '',
      this.search.ismandatory = '',
      this.search.status = ''
  }

  fetchSectionData() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      this.sectionService.fetchSectionList().then((res: any) => {
        try {
          if (res.type) {
            this.tableList = res.data
            console.log('this.tablelist', this.tableList)
          } else {

          }
        } catch (error) {
          console.log(error);
        }
      })
    }
  }
}

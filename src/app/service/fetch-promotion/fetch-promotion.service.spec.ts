import { TestBed } from '@angular/core/testing';

import { FetchPromotionService } from './fetch-promotion.service';

describe('FetchPromotionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchPromotionService = TestBed.get(FetchPromotionService);
    expect(service).toBeTruthy();
  });
});

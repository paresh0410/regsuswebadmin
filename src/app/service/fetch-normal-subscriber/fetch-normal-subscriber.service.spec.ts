import { TestBed } from '@angular/core/testing';

import { FetchNormalSubscriberService } from './fetch-normal-subscriber.service';

describe('FetchNormalSubscriberService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchNormalSubscriberService = TestBed.get(FetchNormalSubscriberService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { FetchCategoriesService } from './fetch-categories.service';

describe('FetchCategoriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchCategoriesService = TestBed.get(FetchCategoriesService);
    expect(service).toBeTruthy();
  });
});

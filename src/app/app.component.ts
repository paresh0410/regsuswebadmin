/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { MessagingService } from '../app/service/messaging.service';
import { FcmService } from './service/fcm/fcm.service';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet><ngx-spinner bdOpacity = 0.9 bdColor = "#e6f3fd" size = "medium" color = "#01579b" type = "ball-clip-rotate-multiple" [fullScreen] = "true"></ngx-spinner></router-outlet>',
})
export class AppComponent implements OnInit {
  title = 'push-notification';
  message;
  deviceInfo = null;
  constructor(private analytics: AnalyticsService, private messagingService: MessagingService,
     private fcmservice: FcmService) {
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
  // Added by Bhavesh Tandel
    // this.messagingService.requestPermission();
    // this.messagingService.receiveMessage();
    // this.message = this.messagingService.currentMessage;
  }
}
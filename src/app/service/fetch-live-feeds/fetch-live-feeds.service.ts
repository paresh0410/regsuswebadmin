import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { webApi } from '../../../config';

@Injectable({
  providedIn: 'root'
})
export class FetchLiveFeedService {

  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  fetchLiveFeedUrl = this.baseUrl + webApi.apiUrl.fetchLiveFeed;

  constructor(public http: HttpClient) { }
  
  public fetchLiveFeed(data: any): Promise<any> {

    return this.http.post(this.fetchLiveFeedUrl, data, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

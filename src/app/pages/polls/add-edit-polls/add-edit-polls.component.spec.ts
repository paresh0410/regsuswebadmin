import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPollsComponent } from './add-edit-polls.component';

describe('AddEditPollsComponent', () => {
  let component: AddEditPollsComponent;
  let fixture: ComponentFixture<AddEditPollsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditPollsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPollsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

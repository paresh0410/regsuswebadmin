import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { DatePipe } from '@angular/common';
import { parse } from 'querystring';
import { AddEditPackageService } from '../../../service/add-edit-package/add-edit-package.service';
import { BrowserInfoService } from '../../../service/browser-info.service';
import { AssignAlluserPackageService } from '../../../service/assign-alluser-package/assign-alluser-package.service';
import { webApi } from '../../../../config';
import { CommonService } from '../../../service/common.service';
import { HttpClient } from '@angular/common/http';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'ngx-add-edit-packages',
  templateUrl: './add-edit-packages.component.html',
  styleUrls: ['./add-edit-packages.component.scss']
})
export class AddEditPackagesComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  isNotification: any = 0;
  isDeleted: any = 0;
  isSuspended: any = 0;
  isPolicyAgreed: any = 0;
  isotpVerified: any;
  roleArray: any = [];
  selectRole: any;
  editPkgata: any = [];
  userStatus: any;
  userFlag: any;
  userData: any;
  bowserInfo: any;
  showAssign: any = false;
  edit: any = false;
  frmdt: any = new Date();
  todt: any = new Date();
  featuresData: any = [
    {
      fId: 1,
      featureName: 'News & Opinions',
      isSelected: 0,
    },
    {
      fId: 2,
      featureName: 'Polls',
      isSelected: 0,
    },
    {
      fId: 4,
      featureName: 'Chat',
      isSelected: 0,
    }
  ];
  SpecialDis: any = false;
  nameVal: any = new RegExp('^[A-Za-z -]+$');
  isCheck: string;
  fetId: any;
  selectedItems: any = [];
  dropdownFeature: any = [];
  tempfeature: any = [];
  featureList: any = [];
  feaCostArray: any = [];
  featuresCost: any;
  durationfun: any = '';
  discountfun: any = 0;
  baseAmt: any = 0;

  test: any;
  isSpic: any;
  mobileNo: any;

  featureSetting: any = {
    singleSelection: false,
    idField: 'featureId',
    textField: 'featurename',
    itemsShowLimit: 3,
    allowSearchFilter: true,
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
  };

  countryList: any = []
  currencyList: any = []
  public Editor = ClassicEditor;
  descriptionEditor: any = 'test'
  public config = {
    toolbar: ['heading', '|',
      'fontfamily', 'fontsize',
      'alignment',
      'fontColor', 'fontBackgroundColor', '|',
      'bold', 'italic', 'custombutton', 'strikethrough', 'underline', 'subscript', 'superscript', '|',
      'link', '|',
      'outdent', 'indent', '|',
      'bulletedList', 'numberedList', '|',
      'code', 'codeBlock', '|',
      'insertTable', '|',
      'imageUpload', 'blockQuote', '|',
      'undo', 'redo', '|',
      'youtube',
      'mediaEmbed'
    ]
  }

  currencyId: any
  ckeConfig: CKEDITOR.config;

  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private passSer: PassServService,
    private addEditPackage: AddEditPackageService,
    private datePipe: DatePipe,
    private browserInfoService: BrowserInfoService,
    private allPkgSer: AssignAlluserPackageService,
    private commonService: CommonService,
    private http: HttpClient
  ) {
    this.ckeConfig = {
      allowedContent: false,
      extraPlugins: 'divarea',
      forcePasteAsPlainText: true,
      removePlugins: 'exportpdf',
      height: 100
    };
    this.ckeConfig.easyimage_styles = {
      full: {
        // Changes just the class name, icon label remains unchanged.
        attributes: {
          'class': 'my-custom-full-class'
        }
      },
    }
    this.editPkgata = this.passSer.editPkg;
    // this.dropdownFeature = this.passSer.featureList;
    console.log('this.editPkgata', this.editPkgata);
    // console.log('dropdownFeature', this.dropdownFeature);
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
    // this.fetchDdlList()
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      pkgname: ['', [Validators.required]],
      duration: ['', [Validators.required]],
      price: ['', [Validators.required]],
      discount: [''],
      status: ['1', [Validators.required]],
      package: ['N', [Validators.required]],
      highL: [0, [Validators.required]],
      description: [''],
      country: ['', [Validators.required]],
      currency: ['', [Validators.required]],
    });
    this.getDdlList()
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);

    // match feture id & features

  }

  editedData() {
    if (this.editPkgata[0]) {
      // this.test = this.editPkgata[0].isSpecial;
      if (this.editPkgata[0].isSpecial === 1) {
        this.test = 1;
      } else {
        this.test = 0;
        this.mobileNo = '';
      }
    } else {
      this.test = 0;
    }
    if (this.editPkgata[0]) {
      this.edit = true;
      this.SpecialDis = true;
      this.discountfun = this.editPkgata[0].discount;
      this.baseAmt = this.editPkgata[0].price;
      this.durationfun = this.editPkgata[0].duration;
      const today = new Date();
      today.setMonth(today.getMonth() + this.durationfun);
      this.todt = new Date(today.toLocaleDateString());
      // this.tempfeature = this.editPkgata[0].featureIds.split('|').map(Number);
      // if (this.tempfeature.length > 0) {
      //   for (let i = 0; i < this.dropdownFeature.length; i++) {
      //     for (let j = 0; j < this.tempfeature.length; j++) {
      //       if (this.tempfeature[j] === this.dropdownFeature[i].featureId) {
      //         this.selectedItems.push(this.dropdownFeature[i]);
      //       }
      //     }
      //   }
      //   this.feaCostArray = this.selectedItems;
      //   this.featuresCost = 0;
      //   for (let i = 0; i < this.feaCostArray.length; i++) {
      //     this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      //   }
      // }
      // console.log('tempfeature', this.tempfeature);

      let fArray = [];
      fArray = this.passSer.featureList;
      if (this.editPkgata[0].packType === 'D') {
        this.dropdownFeature = [];
        this.showAssign = true;
        // for(let i = 0; i < fArray.length; i++) {
        //   if (fArray[i].baserate === 0) {
        //     this.dropdownFeature.push(fArray[i]);
        //   }
        // }
        this.dropdownFeature = fArray;
      } else {
        this.showAssign = false;
        this.dropdownFeature = fArray;
      }

      // this.registerForm.controls.pkgname.setValue(this.editPkgata[0].pkgName)
      // this.registerForm.controls.duration.setValue(this.editPkgata[0].duration)
      // this.registerForm.controls.price.setValue(this.editPkgata[0].price)
      // this.registerForm.controls.country.setValue(this.countryList.find(a => a.country == this.editPkgata[0].countryname).countryId)
      // this.registerForm.controls.currency.setValue(this.currencyList.find(a => a.currencyId == this.editPkgata[0].currency).currencyId)

      this.registerForm = this.formBuilder.group({
        pkgname: [this.editPkgata[0].pkgName, [Validators.required]],
        duration: [this.editPkgata[0].duration, [Validators.required]],
        price: [this.editPkgata[0].price.toFixed(4), [Validators.required]],
        discount: [this.editPkgata[0].discount], // [Validators.required, Validators.pattern('^([0-9]|([1-9][0-9])|100)$')]
        status: [this.editPkgata[0].isActive, [Validators.required]],
        package: [this.editPkgata[0].packType, [Validators.required]],
        highL: [this.editPkgata[0].isSpecial, [Validators.required]],
        description: [this.editPkgata[0].description ? this.editPkgata[0].description : null, [Validators.required]],
        country: [this.editPkgata[0].countryname ? this.countryList.find(a => a.country == this.editPkgata[0].countryname).countryId : '', [Validators.required]],
        currency: [this.editPkgata[0].currency ? this.currencyList.find(a => a.currencyId == this.editPkgata[0].currency).currencyId : '', [Validators.required]],
      });
      this.registerForm.controls['highL'].disable();
    } else {
      this.registerForm = this.formBuilder.group({
        pkgname: ['', [Validators.required]],
        duration: ['', [Validators.required]],
        price: ['', [Validators.required]],
        discount: [''],
        status: ['1', [Validators.required]],
        package: ['N', [Validators.required]],
        highL: [0, [Validators.required]],
        description: [''],
        country: ['', [Validators.required]],
        currency: ['', [Validators.required]],
      });
      this.SpecialDis = false;
    }
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    console.log('submit')
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      // if (this.selectedItems.length === 0) {
      //   this.isCheck = 'Please select features';
      // }
      // return;
      // } else if (this.selectedItems.length === 0) {
      //   this.isCheck = 'Please select features';
      //   return;
    } else {
      // join fetures
      // var uList = [];
      // for (let i = 0; i < this.selectedItems.length; i++) {
      //   uList.push(this.selectedItems[i].featureId);
      // }
      if (this.test === 1) {
        this.isSpic = 1;
      } else {
        this.isSpic = 2;
      }
      const params = {
        pcid: 0,
        pcName: this.registerForm.controls.pkgname.value,
        dura: this.registerForm.controls.duration.value,
        price: this.registerForm.controls.price.value,
        disc: this.registerForm.controls.discount.value,
        isAct: this.registerForm.controls.status.value,
        pctyp: this.registerForm.controls.package.value,
        // feat: uList.join('|'),
        // featlen: uList.length,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
        isSpic: this.isSpic,
        country: this.registerForm.controls.country.value,
        currency: this.registerForm.controls.currency.value,
        desc: this.registerForm.controls.description.value
      };
      console.log('params', params);
      // match id
      if (this.editPkgata[0]) {
        if (this.editPkgata[0].pkgId) {
          params.pcid = this.editPkgata[0].pkgId;
        }
        console.log('params.pcid', params.pcid);
      }
      // add edit service
      this.addEditPackage.addeditPackage(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              // if (res) {
              if (res.data.error) {
                this.toastr.error(res.data.error);
              } else if (res.data.saved) {
                this.router.navigate(['pages/packages']);
                this.toastr.success(res.data.saved);
              } else if (res.data.updated) {
                this.router.navigate(['pages/packages']);
                this.toastr.success(res.data.updated);
              } else if (res.data.Exists) {
                this.toastr.warning(res.data.Exists);
              }
              // }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
      // this.toastr.success('Data Saved Sucessfully');
    }
  }

  back() {
    this.router.navigate(['pages/packages']);
    this.registerForm.reset()
    // this.passSer.editPkg = ''
  }

  formatDateNewDash(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  onChange(check) {
    console.log(check);
  }

  checkPackage(event) {
    this.dropdownFeature = [];
    this.selectedItems = [];
    let fArray = [];
    fArray = this.passSer.featureList;
    if (event.target.value === 'D') {
      this.showAssign = true;
      // for(let i = 0; i < fArray.length; i++) {
      //   if (fArray[i].baserate === 0) {
      //     this.dropdownFeature.push(fArray[i]);
      //   }
      // }
      this.dropdownFeature = fArray;

    } else {
      this.showAssign = false;
      this.dropdownFeature = fArray;
    }
  }

  // checkVal(event) {
  //   console.log('event', event);
  //   if (event.target.placeholder === 'Package Name') {
  //     if (!event.target.value) {
  //       this.toastr.error('Package is required');
  //     }
  //     // else if (!this.nameVal.test(event.target.value)) {
  //     //   this.toastr.error('Numbers and Special characters are not allowed');
  //     // }
  //   }
  //   if (event.target.placeholder === 'Price') {
  //     if (!event.target.value) {
  //       this.toastr.error('Price is required');
  //     }
  //   }
  //   if (event.target.placeholder === 'Duration') {
  //     if (!event.target.value) {
  //       this.toastr.error('Period is required');
  //     }
  //   }
  // }

  // single item select (features)
  onItemSelect(event) {
    console.log('select event', event);
    if (event.featureId) {
      this.feaCostArray = [];
      console.log('select', this.selectedItems);
      this.isCheck = '';
      for (let i = 0; i < this.dropdownFeature.length; i++) {
        for (let j = 0; j < this.selectedItems.length; j++) {
          if (this.selectedItems[j].featureId === this.dropdownFeature[i].featureId) {
            this.feaCostArray.push(this.dropdownFeature[i]);
          }
        }
      }
      console.log('this.feaCostArray', this.feaCostArray);
      this.featuresCost = 0;
      for (let i = 0; i < this.feaCostArray.length; i++) {
        this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
      }
      console.log('this.featuresCost', this.featuresCost);
      this.calculate();
    }
  }

  // single item deselect (features)
  onItemDeSelect(des) {
    this.feaCostArray = [];
    for (let i = 0; i < this.selectedItems.length; i++) {
      if (des.featureId === this.selectedItems[i].featureId) {
        this.selectedItems.splice(i, 1);
      }
    }
    console.log('deselect', this.selectedItems);
    if (this.selectedItems.length === 0) {
      this.isCheck = 'Please select features';
    }
    for (let i = 0; i < this.dropdownFeature.length; i++) {
      for (let j = 0; j < this.selectedItems.length; j++) {
        if (this.selectedItems[j].featureId === this.dropdownFeature[i].featureId) {
          this.feaCostArray.push(this.dropdownFeature[i]);
        }
      }
    }
    console.log('this.feaCostArray', this.feaCostArray);
    this.featuresCost = 0;
    for (let i = 0; i < this.feaCostArray.length; i++) {
      this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
    }
    console.log('this.featuresCost', this.featuresCost);
    this.calculate();
  }


  // select all features
  onSelectAll(allSelect) {
    this.feaCostArray = [];
    this.isCheck = '';
    this.selectedItems = allSelect;
    console.log('selectAll', this.selectedItems);
    for (let i = 0; i < this.dropdownFeature.length; i++) {
      for (let j = 0; j < this.selectedItems.length; j++) {
        if (this.selectedItems[j].featureId === this.dropdownFeature[i].featureId) {
          this.feaCostArray.push(this.dropdownFeature[i]);
        }
      }
    }
    console.log('this.featuresCost', this.feaCostArray);
    this.featuresCost = 0;
    for (let i = 0; i < this.feaCostArray.length; i++) {
      this.featuresCost = this.featuresCost + this.feaCostArray[i].baserate;
    }
    this.calculate();
  }

  //  duaration
  checkDuration(duration) {
    if (!duration.target.value) {
      // this.toastr.error('Discount is required');
    } else {
      this.durationfun = parseInt(duration.target.value);
      const today = new Date();
      today.setMonth(today.getMonth() + this.durationfun);
      this.todt = new Date(today.toLocaleDateString());
      this.calculate();
    }
  }

  //  discount
  setDisc(disc) {
    if (!disc.target.value) {
      // this.toastr.error('Discount is required');
    } else {
      this.discountfun = parseInt(disc.target.value);
      this.calculate();
    }
  }

  // calculations
  calculate() {
    if (!this.durationfun) {
      // this.toastr.warning('Please select duration');
    } else if (this.durationfun === '' || this.durationfun === null || this.durationfun === undefined) {
      // this.toastr.error('Discount is required');
    } else if (this.selectedItems.length === 0) {

    } else {
      let tempCost;
      let grossCost;
      tempCost = (this.featuresCost * this.durationfun);
      grossCost = ((tempCost) - (tempCost * (this.discountfun / 100)));
      this.baseAmt = grossCost.toFixed(4);
      this.registerForm.controls.price.setValue(this.baseAmt);
    }
  }

  assign() {
    this.submitted = true;
    console.log(this.registerForm.value);
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      // if (this.selectedItems.length === 0) {
      //   this.isCheck = 'Please select features';
      // }
      // return;
    }
    // else if (this.selectedItems.length === 0) {
    //   this.isCheck = 'Please select features';
    //   return;
    // } 
    else {
      // join fetures
      // var uList = [];
      // for (let i = 0; i < this.selectedItems.length; i++) {
      //   uList.push(this.selectedItems[i].featureId);
      // }

      // parameters
      const params = {
        purcdate: this.formatDateNewDash(this.frmdt),
        fomdate: this.formatDateNewDash(this.frmdt),
        todate: this.formatDateNewDash(this.todt),
        graceDate: this.formatDateNewDash(this.todt),
        duraid: this.registerForm.controls.duration.value,
        discpre: this.registerForm.controls.discount.value,
        gstno: 18,
        // feaIds: uList.join('|'),
        // fealen: uList.length,
        pkgid: this.editPkgata[0].pkgId,
        baAmt: this.registerForm.controls.price.value,
        // pcName: this.registerForm.controls.pkgname.value,
        // dura: this.registerForm.controls.duration.value,
        // price: this.registerForm.controls.price.value,
        // disc: this.registerForm.controls.discount.value,
        // feat: uList.join('|'),
        // featlen: uList.length,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      console.log('params', params);
      // add edit service
      this.allPkgSer.AssignAllPackages(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              // if (res) {
              console.log('res', res);
              this.router.navigate(['pages/packages']);

              this.toastr.success('Package Assigned');

              // if (res.data.error) {
              //   this.toastr.error(res.data.error);
              // } else if (res.data.saved) {
              //   this.router.navigate(['pages/packages']);
              //   this.toastr.success(res.data.saved);
              // } else if (res.data.updated) {
              //   this.router.navigate(['pages/packages']);
              //   this.toastr.success(res.data.updated);
              // } else if (res.data.Exists) {
              //   this.toastr.warning(res.data.Exists);
              // }
              // }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
    }
  }

  assignSpecial() {
    if (!this.mobileNo) {
      this.toastr.error('Enter Mobile No');
      return;
    } else {

      const params = {
        mobno: this.mobileNo,
        pkid: this.editPkgata[0].pkgId,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      console.log('params', params);
      // add edit service
      this.allPkgSer.AssignSpecialPackages(params).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              this.toastr.success('Package Assigned');

            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
            this.toastr.error(res.message);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
    }
  }
  onToggle(value) {
    console.log(value);

    if (value === 1) {
      //   this.test = 1;
      // } else {
      this.test = 0;
      this.mobileNo = '';
    } else {
      this.test = 1;
    }
    console.log('notification value', this.test);
  }

  onCountryChange(event: any) {
    console.log(event.target.value)
    const currId = this.countryList.find(a => a.countryId == event.target.value).currencyId
    this.currencyId = currId
    this.registerForm.controls.currency.setValue(currId)
  }

  getDdlList() {
    const url = webApi.baseUrl + webApi.apiUrl.packageDropdown
    const param = {
      bname: this.commonService.bowserInfo.name,
      bversion: this.commonService.bowserInfo.version,
      uid: this.commonService.userData.userid,
      uname: this.commonService.userData.subname
    }
    this.commonService.httpPostRequest(url, param).then((res: any) => {
      console.log(res)
      try {
        if (res.type) {
          this.countryList = res.country
          this.currencyList = res.currency
          this.editedData()
        }
      } catch (error) {

      }
    })
  }


}

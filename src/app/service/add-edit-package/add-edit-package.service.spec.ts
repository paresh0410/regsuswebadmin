import { TestBed } from '@angular/core/testing';

import { AddEditPackageService } from './add-edit-package.service';

describe('AddEditPackageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditPackageService = TestBed.get(AddEditPackageService);
    expect(service).toBeTruthy();
  });
});

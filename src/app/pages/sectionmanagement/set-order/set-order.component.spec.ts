import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetOrderComponent } from './set-order.component';

describe('SetOrderComponent', () => {
  let component: SetOrderComponent;
  let fixture: ComponentFixture<SetOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

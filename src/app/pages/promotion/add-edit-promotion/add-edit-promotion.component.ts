import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { AddEditPromotionService } from '../../../service/add-edit-promotion/add-edit-promotion.service';
import { UploadPromotionImageService } from '../../../service/upload-promotion-image/upload-promotion-image.service';
import { BrowserInfoService } from '../../../service/browser-info.service';

@Component({
  selector: 'ngx-add-edit-promotion',
  templateUrl: './add-edit-promotion.component.html',
  styleUrls: ['./add-edit-promotion.component.scss']
})
export class AddEditPromotionComponent implements OnInit {
  editProdata: any = [];
  fileToUpload: File = null;
  progress: number = 0;
  isImg: any;
  nsopImage: any;
  isSpel: any;
  submitted = false;
  public imagePath;
  imgURL: any;
  registerForm: FormGroup;
  minDate: any = new Date();
  maxDate: any = new Date();
  videoType: any;
  message: string;
  public name1: String = 'Choose image or video';
  public str1: String = '';
  displayVideo: boolean;
  test: any;
  displayImage: boolean;
  displayPdf : boolean;
  splitArray: any = [];
  fileName: any;
  userData :any;
  bowserInfo:any;
  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private passSer: PassServService,
    private formBuilder: FormBuilder,
    private uploadImgSer: UploadPromotionImageService,
    private addEditPromotion: AddEditPromotionService,
    private browserInfoService: BrowserInfoService,
    private cd: ChangeDetectorRef,
  ) {
    this.editProdata = this.passSer.editPromo;
    console.log('editProdata', this.editProdata);
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
   }

   ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);

    if(this.editProdata[0]) {
      this.test = this.editProdata[0].isActive;
    } else{
      this.test = 1;
    }
    // console.log('this.test',this.test);
    if (this.editProdata[0]) {
      this.imgURL = this.editProdata[0].promoimageUrl;

      this.splitArray = this.editProdata[0].promoimageUrl.split('/');
      this.nsopImage = this.splitArray[this.splitArray.length - 1];
      this.fileToUpload =  this.nsopImage;

      const str11: any[] =  this.editProdata[0].promoimageUrl.split('/');
      this.fileName = str11[str11.length - 1];
     
      this.registerForm = this.formBuilder.group({
        pname: [this.editProdata[0].promoHeader , [Validators.required]],
        desc: [this.editProdata[0].promodesc , [Validators.required]],
        startDt: [new Date(this.editProdata[0].startDate), [Validators.required]],
        endDt: [new Date(this.editProdata[0].endDate), [Validators.required]],
        file: [this.fileName , Validators.required],
        //fileUpload: [this.editProdata[0].promoimageUrl,[Validators.required] ],
        status: [this.editProdata[0].isActive, [Validators.required]],
       
      });
      this.videoType = this.editProdata[0].mimeType;

      // console.log( ' this.videoType',this.videoType);
    
      // const typeMime = this.videoType.split('/');
      // if(typeMime[0] === 'video' ){
      //   this.displayVideo = true;
      // } else {
      //   this.displayImage = true;
      // }

      
      this.minDate = new Date(this.editProdata[0].startDate);
      this.maxDate = new Date(this.editProdata[0].endDate);
    } else {
      this.fileName = 'No file choosen';
      this.registerForm = this.formBuilder.group({
        pname: ['' , [Validators.required]],
        desc: ['' , [Validators.required]],
        startDt: ['', [Validators.required]],
        endDt: ['', [Validators.required]],
        file: [null, Validators.required],
        // fileUpload: ['' ,[Validators.required]],
        status:  ['1', [Validators.required]],
      });
    }
  }

  get f() {
    return this.registerForm.controls;
  }


  back() {
    this.router.navigate(['pages/promotion']);
  }

 

  removeImage() {
    this.str1 = '';
    this.name1 = 'Choose Image or video';
    this.imgURL = '';
    this.displayVideo = false;
    this.displayImage = false;
    this.displayPdf = false;
  }

  checkVal(event) {
    console.log('event', event);
    if (event.target.placeholder === 'Promotion Name') {
      if (!event.target.value) {
        this.toastr.error('Promotion Name is required');
      }
    }
    if (event.target.placeholder === 'Discrepition') {
      if (!event.target.value) {
        this.toastr.error('Discrepition is required');
      }
    }
    
    if (event.target.placeholder === 'Start Date') {
      if (!event.target.value) {
        this.toastr.error('Start Date is required');
      }
    }
    if (event.target.placeholder === 'End Date') {
      if (!event.target.value) {
        this.toastr.error('End Date is required');
      }
    }
  }

  invalid : boolean = false;



  handleFileInput(files: FileList) {
    console.log('files', files);
    


    this.videoType = files[0].type;
    if (files.length === 0)
      return;
      console.log('this.imagePath',files);
      console.log('this.imagePath',files[0].type);
     const mimeType = files[0].type;
      //this.mimeTypeData = mimeType;
    
     var res = mimeType.split('/');

     console.log('mimeName',res[0]);

     console.log('mimeType',mimeType);
   

   
      if(res[0] === 'video' || res[0] === 'image' || mimeType === 'application/pdf') {
        this.message = '';
        const reader = new FileReader();
        this.imagePath = files;
       
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
        this.imgURL = reader.result;
        console.log('this.imgURL',this.imgURL);
      
        
      };
      this.fileToUpload = files.item(0);
      this.fileName = this.fileToUpload.name;
      console.log('this.fileToUpload', this.fileToUpload);
      this.invalid = true;
        // this.isImg = '';
        // this.name1  = files[0].name;
       // this.str1 = this.name1;
      } else {
        this.invalid = false;
        this.isImg = '';
        this.name1 = 'Choose image,video or pdf';
        this.message = 'Only images,video or pdf supported.';
        console.log('invalid');
        return;
      }
  }

  onFileChange(event) {
    const reader = new FileReader();
 
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
  
      reader.onload = () => {
        this.registerForm.patchValue({
          file: reader.result,
       });
      
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }


  onSubmit() {
    this.submitted = true;
    let formData = new FormData();
    console.log('this.registerForm.value',this.registerForm);
    // stop here if form is invalid
   
    if (!this.fileToUpload) {
      this.isImg = 'Please select image';
      console.log('please upload image');
      return;
    }

    // if(this.registerForm.value.highL === 2 ) {
    //   this.isSpel = 1;
    // } else {
    //   this.isSpel = 2;
    // }

    if (this.registerForm.value.startDt && this.registerForm.value.endDt) {
      if (new Date(this.registerForm.value.startDt).setSeconds(0,0) ===
      new Date(this.registerForm.value.endDt).setSeconds(0,0)) {
        this.toastr.warning('End date and time should not be same as start date and time','', {
          timeOut: 6000
        });
        return;
      }
      if (new Date(this.registerForm.value.startDt) > new Date(this.registerForm.value.endDt)) {
        this.toastr.warning('End date and time should not be greater than start date and time','', {
          timeOut: 6000
        });
        return;
      }
    }

    if (this.registerForm.invalid) {
      return;
    } else {
      const content = {
        p_promId : 0,
        p_promHed : this.registerForm.value.pname,
        p_promodes : this.registerForm.value.desc,
        p_staDate  : this.formattime(this.registerForm.value.startDt),
        p_endDate  : this.formattime(this.registerForm.value.endDt),
        // p_promoURL : this.imagePath ? null : this.nsopImage,
        // p_act : this.registerForm.value.highL,
        p_promoMtype: this.fileToUpload.type,
        p_promoURL : this.fileToUpload.name,
        p_act :  this.registerForm.value.status,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,

      }

     
      if (this.editProdata[0]) {
          content.p_promId = this.editProdata[0].promoId;
      }

      if(this.invalid === false) {
        content.p_promoMtype =  this.videoType;
        content.p_promoURL = this.imgURL;
      }


      // if (this.imagePath) {
      //   console.log('imagepath',this.imagePath);
      //   formData.append('file', this.imagePath[0], this.imagePath[0].name);
      // }
      // if(this.invalid === true) {
      //   this.fileName = this.fileToUpload.name;
      // }
      

      if (this.invalid === true) {
        console.log('imagepath',this.fileToUpload);
        formData.append('file', this.fileToUpload, this.fileToUpload.name);
      }

      console.log('formData',formData);
 
      this.spinner.show();
      // service for uploading category images
      if(this.invalid === true) {
        this.uploadImgSer.uploadpromotionImg(formData).then(res => {
          // this.spinner.hide();
          try {
              this.progress = Math.round(res.loaded / res.total * 100);
              console.log('Uploaded!',this.progress);
            if (res.error_code === 0) {
              console.log('res', res);
              content. p_promoURL = res.path;
              console.log('content', content);

       // service for add edit category
        this.addEditPromotion.addeditPromotion(content).then(res => {
          this.spinner.hide();
          try {
            if (res.type === true && res.status_code === 200) {
              console.log('res', res);
              try {
                if (res.data) {
                  if (res.data.error) {
                    this.toastr.error(res.data.error);
                  } else if (res.data.updated) {
                    this.router.navigate(['pages/promotion']);
                    this.toastr.success(res.data.updated);
                } else if (res.data.saved) {
                  this.router.navigate(['pages/promotion']);
                  this.toastr.success(res.data.saved);
              }
                }
              } catch (e) {
                console.log(e);
              }
            }
            if (res.type === false) {
              this.spinner.hide();
              console.log('res', res);
            }
          } catch (e) {
            console.log(e);
          }
        },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
            }
            if (res.error_code === 1) {
              this.spinner.hide();
              console.log('res', res);
              this.toastr.error(res.err_desc);
            }
          } catch (e) {
            console.log(e);
          }
        },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
      }else {
        this.addEditPromotion.addeditPromotion(content).then(res => {
          this.spinner.hide();
          try {
            if (res.type === true && res.status_code === 200) {
              console.log('res', res);
              try {
                if (res.data) {
                  if (res.data.error) {
                    this.toastr.error(res.data.error);
                  } else if (res.data.updated) {
                    this.router.navigate(['pages/promotion']);
                    this.toastr.success(res.data.updated);
                } else if (res.data.saved) {
                  this.router.navigate(['pages/promotion']);
                  this.toastr.success(res.data.saved);
              }
                }
              } catch (e) {
                console.log(e);
              }
            }
            if (res.type === false) {
              this.spinner.hide();
              console.log('res', res);
            }
          } catch (e) {
            console.log(e);
          }
        },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
      }
      // console.log('success');
      // this.toastr.success('Data Saved Successfully');
      // this.router.navigate(['pages/posts']);
    }
  }


  formattime(time) {
    var d = new Date(time),
      hr = '' + d.getHours(),
      mm = '' + d.getMinutes(),
      ss = '' + d.getSeconds(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
  
    if (hr.length < 2) hr = '0' + hr;
    if (mm.length < 2) mm = '0' + mm;
    if (ss.length < 2) ss = '0' + ss;
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
  
    var da = [year, month, day].join('-');
    var ti = [hr, mm, ss].join(':');
    return [da, ti].join(' ');
  }


  onToggle(value) {
    console.log('data', value)
    if(value === 1) {
      this.test = 2;
    } else{
      this.test = 1;
    }

  }


}

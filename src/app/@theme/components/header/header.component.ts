import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserData } from '../../../@core/data/users';
import { AnalyticsService } from '../../../@core/utils';
import { Router, ActivatedRoute } from '@angular/router';
import { LayoutService } from '../../../@core/utils';
import { EventsService } from 'angular4-events';
import { FcmService } from '../../../service/fcm/fcm.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  user: any;
  UD: any;
  pic: any ;
  userMenu = [{ title: 'Log out' }];
  notifBadge: any;
  notifData: any;
  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private userService: UserData,
    private analyticsService: AnalyticsService,
    private layoutService: LayoutService,
    private router: Router,
    private pubsub: EventsService,
    private fcmservice: FcmService,
    private ref: ChangeDetectorRef) {
      var data = this.fcmservice.getnotifbadge();
      this.notifBadge = data.notifBadge;
      this.notifData = data.notifData;
      this.UD = JSON.parse(localStorage.getItem('UserName'));
      this.pic = '../../../assets/images/user.png';
      this.user = { name: this.UD, picture: this.pic };
      console.log('this.user', this.user);
  }

  ngOnInit() {
    // this.userService.getUsers()
    //   .subscribe((users: any) => this.user = users.nick);
    this.pubsub.subscribe('login').subscribe((from) => {
      this.UD = JSON.parse(localStorage.getItem('UserDetails'));
      this.pic = '../../../assets/images/user.png';
      this.user = { name: this.UD , picture: this.pic };
      console.log('this.user', this.user);
    });

    this.pubsub.subscribe('notifiction').subscribe((from) => {
      var data = this.fcmservice.getnotifbadge();
      console.log('data', data);
      this.notifBadge = data.notifBadge;
      this.notifData = data.notifData;
      this.ref.detectChanges();
    });
    this.menuService.onItemClick()
      .subscribe((event) => {
        this.onContecxtItemSelection(event.item.title);
      });
  }

  goChangePassword() {
    // this.router.navigateByUrl([])
    this.router.navigate(['pages/changepassword'])

  }

  onContecxtItemSelection(title) {
    // console.log('click', title);
    if (title === 'Log out') {
      this.router.navigate(['/login']);
      localStorage.clear();
    }
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
  
  showNotifications(){
    this.fcmservice.clearbadge();
    this.pubsub.publish('notifiction');
  }
}

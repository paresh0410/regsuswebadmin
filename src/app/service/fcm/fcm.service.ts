import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ToastrService } from 'ngx-toastr';
import { EventsService } from 'angular4-events';

@Injectable({
  providedIn: 'root',
})
export class FcmService {

    baseUrl: any = webApi.baseUrl;
    deviceInfo: any;
    private headers = new HttpHeaders();
    saveAdminUserDeviceInfoUrl = this.baseUrl + webApi.apiUrl.saveAdminUserDeviceInfo;
    pushIds: any;
    notifBadge: any = 0;
    notifData: any = [];
    constructor(public http: HttpClient , private deviceService: DeviceDetectorService,
        private toastr: ToastrService, private pubsub: EventsService) {
        this.deviceInfo = this.deviceService.getDeviceInfo();
    }

    saveUserTokenInfo(userId, event, cb) {
        let act = 0;
        if (event === 'login') {
            act = userId === 0 ? 0 : 1;
        }
        const param: any = {
            userid: userId,
            appid: webApi.appId,
            browser: this.deviceInfo.browser,
            browser_version: this.deviceInfo.browser_version,
            device: this.deviceInfo.device,
            pushid: this.pushIds,
            os: this.deviceInfo.os,
            os_version: this.deviceInfo.os_version,
            userAgen : this.deviceInfo.userAgent,
            act : act,
        };

        return this.http.post(this.saveAdminUserDeviceInfoUrl, param, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response = response;
            })
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        return Promise.reject(error.message || error);
    }
    setPushIds(token) {
        this.pushIds = token;
    }

    getPushIds() {
        return this.pushIds;
    }
    async setnotifbadge(data) {
        this.notifBadge++;
        this.notifData.push(data);
        this.pubsub.publish('notifiction');
    }
    getnotifbadge() {
        return {'notifBadge': this.notifBadge, 'notifData' : this.notifData};
    }
    clearbadge() {
        this.notifBadge = 0;
        this.notifData = [];
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppSubscribersComponent } from './app-subscribers.component';

describe('AppSubscribersComponent', () => {
  let component: AppSubscribersComponent;
  let fixture: ComponentFixture<AppSubscribersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppSubscribersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppSubscribersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

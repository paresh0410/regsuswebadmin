import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { webApi } from '../../../config';
import { CommonService } from '../common.service';

@Injectable({
  providedIn: 'root'
})
export class SectionsService {
  sectionData: any
  constructor(private commonService: CommonService, private http: HttpClient) { }


  fetchSectionList() {
    const param = {
      bname: this.commonService.bowserInfo.name,
      bversion: this.commonService.bowserInfo.version,
      uid: this.commonService.userData.userid,
      uname: this.commonService.userData.subname
    }
    const url = webApi.baseUrl + webApi.apiUrl.fetchSection
    return new Promise(resolve => {
      this.http.post(url, param)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve('err');
          }
        );
    });

  }
}

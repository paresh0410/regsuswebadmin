import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class AddEditMsReportService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addEditMsReportUrl = this.baseUrl + webApi.apiUrl.addEditMsReport;
  constructor(public http: HttpClient) { }
  public addEditMsReports(msData: any): Promise<any> {

    return this.http.post(this.addEditMsReportUrl, msData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

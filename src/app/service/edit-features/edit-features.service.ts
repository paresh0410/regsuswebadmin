import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class EditFeaturesService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  editFeaturesUrl = this.baseUrl + webApi.apiUrl.editFeatures;
  constructor(public http: HttpClient) { }
  public editFeature(featureData: any): Promise<any> {

    return this.http.post(this.editFeaturesUrl, featureData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class AddEditPromotionService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addeditPromotionUrl = this.baseUrl + webApi.apiUrl.addEditPromotion;

  constructor(public http: HttpClient) { }

  public addeditPromotion(PromotionData: any): Promise<any> {

    return this.http.post(this.addeditPromotionUrl, PromotionData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

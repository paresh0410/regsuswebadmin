import { TestBed } from '@angular/core/testing';

import { EditFeaturesService } from './edit-features.service';

describe('EditFeaturesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditFeaturesService = TestBed.get(EditFeaturesService);
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { DatePipe } from '@angular/common';
import { Tree } from '@angular/router/src/utils/tree';
import { AddEditPollService } from '../../../service/add-edit-poll/add-edit-poll.service';
import { BrowserInfoService } from '../../../service/browser-info.service';


@Component({
  selector: 'ngx-add-edit-polls',
  templateUrl: './add-edit-polls.component.html',
  styleUrls: ['./add-edit-polls.component.scss']
})
export class AddEditPollsComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  selectRole: any;
  editUserData: any = [];
  userStatus: any;
  userFlag: any;
  nameVal: any = new RegExp('^[A-Za-z -]+$');
  spaceval: any = new RegExp(/^\S*$/);
  disableCompany: any = false;
  public name1: String = 'Choose image';
  public str1: String = '';
  message: string;
  public imagePath;
  public filePath;
  imgURL: any;
  isImg: any;
  isFile: any;
  PostData: any = [];
  minDate: any = new Date();
  newoption: any = false;
  optionsArray: any = [];
  inValidArray: any = [];
  isInvalid: any = false;
  pollsData: any = [];
  questionData: any = [];
  getIt: any = false;
  today: any = new Date();
  disableAll: any = false;
  checkstd: any = new Date();
  checkend: any = new Date();
  dropdownList: any = [];
  selectedItems: any = [];
  dropdownSettings: any = {
    singleSelection: false,
    idField: 'interestId',
    textField: 'intersetName',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };
  isCheck1: any;
  disableOption: any = false;
  stopAdd: any = false;
  userData :any;
  bowserInfo:any;
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private passSer: PassServService,
    private datePipe: DatePipe,
    private addEditPollSer: AddEditPollService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.pollsData = this.passSer.editPoll;
    this.dropdownList = this.passSer.interestList;
    console.log('this.pollsData' , this.pollsData);
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    // if ( new Date(this.pollsData[0].endDate) < this.today ) {
    //   this.disableAll = true;
    // }
    if (this.pollsData[0]) {
    this.questionData = this.pollsData[0].choicesLit;
    this.selectedItems = this.pollsData[0].intersetLit;
    for (let i = 0; i < this.selectedItems.length; i++) {
      this.selectedItems[i].interestId = parseInt(this.selectedItems[i].interestId);
    }
    if ( this.pollsData[0].pollsta === 3) {
      this.disableAll = true;
    }
    if ( this.pollsData[0].pollsta === 2) {
      this.disableOption = true;
    }
    this.registerForm = this.formBuilder.group({
      pname: [{value : this.pollsData[0].pollText, disabled : this.disableAll } , [Validators.required]],
      descri: [{value : this.pollsData[0].pollDescription , disabled : this.disableAll }, [Validators.required]],
      status: [{value : this.pollsData[0].isActive , disabled : this.disableAll }, [Validators.required]],
      startDt: [{value : new Date(this.pollsData[0].startDate)  , disabled : this.disableAll }, [Validators.required]],
      endDt: [ {value : new Date(this.pollsData[0].endDate)  , disabled : this.disableAll }, [Validators.required]],
      options:  this.formBuilder.array([this.formBuilder.group({element: {value : this.questionData[0].choiceText , disabled : this.disableAll || this.disableOption },
        cid: {value : this.questionData[0].pollchoiceId , disabled : this.disableAll || this.disableOption }})] , Validators.required),
    });
    this.getIt = true;
    this.addNewOption();
    this.minDate = new Date(this.pollsData[0].startDate);
  } else {
    this.registerForm = this.formBuilder.group({
      pname: ['' , [Validators.required]],
      descri: ['' , [Validators.required]],
      status: ['1', [Validators.required]],
      startDt: ['', [Validators.required]],
      endDt: ['', [Validators.required]],
      options:  this.formBuilder.array([this.formBuilder.group({element: '',cid:0})] , Validators.required),
    });
    this.addNewOption();
    this.checkstd = this.registerForm.value.startDt;
    this.checkend = this.registerForm.value.endDt;
  }
  console.log('disd',this.disableAll);
}
  get f() {
    return this.registerForm.controls;
  }
  get Qoptions() {
    return this.registerForm.get('options') as FormArray;
  }

  onItemSelect(item) {
    console.log('select', this.selectedItems);
    this.isCheck1 = '';
}
onSelectAll(items) {
  // console.log(items);
  if (this.selectedItems.length === this.dropdownList.length) {
    this.selectedItems = [];
    this.isCheck1 = 'Please select interests';
  } else {
    this.selectedItems = items;
    this.isCheck1 = '';
  }
  console.log('selectAll', this.selectedItems);
}
onItemDeSelect(des) {
  for (let i = 0; i < this.selectedItems.length; i++) {
    if (des.interestId === this.selectedItems[i].interestId) {
      this.selectedItems.splice(i, 1);
    }
  }
  if (this.selectedItems.length === 0) {
    this.isCheck1 = 'Please select interests';
  }
   console.log('deselect', this.selectedItems);
}

  addNewOption() {
    console.log('ISINVALID', this.isInvalid);
    if (this.Qoptions.length > 4) {
      this.stopAdd = true;
    } else {
      this.stopAdd = false;
    }
    if (this.getIt) {
      for ( let i = 0; i < this.questionData.length; i++) {
        if ( i > 0) {
          this.Qoptions.push(this.formBuilder.group({element: {value : this.questionData[i].choiceText , disabled : this.disableAll || this.disableOption } , 
            cid: {value : this.questionData[i].pollchoiceId , disabled : this.disableAll || this.disableOption }} , Validators.required ));
        }
        this.getIt = false;
      }
    } else {
      for ( let i = 0 ; i < 1 ; i ++) {
        this.Qoptions.push(this.formBuilder.group({element:'',cid:0} , Validators.required ));
      }
    }
  }
  deleteOption(index) {
    this.Qoptions.removeAt(index);
    this.stopAdd = false;
  }
  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value);
    this.inValidArray = this.registerForm.controls.options.value;
    if (this.selectedItems.length === 0) {
      this.isCheck1 = 'Please select interests';
    console.log('this.isCheck1', this.isCheck1);
      return;
    }
    for(let i = 0; i < this.inValidArray.length ; i ++) {
      if (!this.inValidArray[i].element) {
        this.isInvalid = true;
        return;
      }
      else {
        this.isInvalid = false;
      }
    }
    if (new Date(this.registerForm.value.startDt).setSeconds(0,0) ===
    new Date(this.registerForm.value.endDt).setSeconds(0,0)) {
      this.toastr.warning('End date and time should not be same as start date and time','', {
        timeOut: 6000
      });
      return;
    }
    if (new Date(this.registerForm.value.startDt) > new Date(this.registerForm.value.endDt)) {
      this.toastr.warning('End date and time should not be greater than start date and time','', {
        timeOut: 6000
      });
      return;
    }
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      if (this.selectedItems.length === 0) {
        this.isCheck1 = 'Please select interests';
    console.log('this.isCheck1', this.isCheck1);
      }
      return;
    } else {
      let tempChoice = [];
      for (let i = 0; i < this.inValidArray.length; i++) {
        let temOBj = this.inValidArray[i].cid + ',' + this.inValidArray[i].element + ',' + (i + 1);
        tempChoice.push(temOBj);
        console.log('tempChoice',tempChoice);
      }
      const intrId = [];
      for (let i = 0; i < this.selectedItems.length; i++) {
        intrId.push(this.selectedItems[i].interestId);
      }
      const content = {
        p_pollId : 0,
        p_pollTxt : this.registerForm.value.pname,
        p_pollDesc : this.registerForm.value.descri,
        p_staDate : this.formattime(this.registerForm.value.startDt),
        p_endDate : this.formattime(this.registerForm.value.endDt),
        p_act : this.registerForm.value.status,
        p_inters : intrId.join('|'),
        p_interslen : intrId.length,
        p_choices : tempChoice.join('|'),
        p_choiceslen : tempChoice.length, 
        p_pollsta : 1,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      }

      if (this.pollsData[0]) {
        if (this.pollsData[0].pollId) {
          content.p_pollId = this.pollsData[0].pollId;
          tempChoice = [];
          for (let i = 0; i < this.inValidArray.length; i++) {
            let temOBj = parseInt(this.inValidArray[i].cid)  + ',' + this.inValidArray[i].element + ',' + (i + 1);
            tempChoice.push(temOBj);
            console.log('tempChoice',tempChoice);
          }
          content.p_choices = tempChoice.join('|');
          content.p_choiceslen = tempChoice.length;
          content.p_pollsta = this.pollsData[0].pollsta;
        }
      }

      console.log('content',content);
      this.addEditPollSer.addEditPolls(content).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              if (res.data) {
                if (res.data.error) {
                  this.toastr.error(res.data.error);
                } else if (res.data.updated) {
                  this.router.navigate(['pages/polls']);
                  this.toastr.success(res.data.updated);
              } else if (res.data.saved) {
                this.router.navigate(['pages/polls']);
                this.toastr.success(res.data.saved);
            }
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        console.log(err);
        this.spinner.hide();
        this.toastr.error('Server Error');
      });
      // this.toastr.success('Data Saved Successfully');
      // this.router.navigate(['pages/polls']);
    }
  }
  back() {
    this.router.navigate(['pages/polls']);
  }
  addoption() {
    this.newoption = !this.newoption;
  }
  checkVal(event) {
    console.log('event', event);
    if (event.target.placeholder === 'Poll Name') {
      if (!event.target.value) {
        this.toastr.error('Poll Name is required');
      }
    }
    if (event.target.placeholder === 'Poll Description') {
      if (!event.target.value) {
        this.toastr.error('Poll Description is required');
      }
    }
    // if (event.target.placeholder === 'Start Date') {
    //   if (!event.target.value) {
    //     this.toastr.error('Start Date is required');
    //   }
    // }
    // if (event.target.placeholder === 'End Date') {
    //   if (!event.target.value) {
    //     this.toastr.error('End Date is required');
    //   }
    // }
    if (event.target.placeholder === 'Add option') {
      if (!event.target.value) {
        this.toastr.error('Option is required');
      }
    }
  }
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear(),
      time = d.getTime();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  formattime(time) {
		var d = new Date(time),
			hr = '' + d.getHours(),
      mm = '' + d.getMinutes(),
      ss = '' + d.getSeconds(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

		if (hr.length < 2) hr = '0' + hr;
    if (mm.length < 2) mm = '0' + mm;
		if (ss.length < 2) ss = '0' + ss;
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var da = [year, month, day].join('-');
    var ti = [hr, mm, ss].join(':');
		return [da, ti].join(' ');
	}
}
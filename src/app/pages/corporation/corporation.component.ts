import { Component, OnInit, ViewChild } from '@angular/core';
import { ExcelService } from '../../service/excel.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassServService } from '../../service/pass-serv.service';
import { ToastrService } from 'ngx-toastr';
import { FetchCorporationsService } from '../../service/fetch-corporations/fetch-corporations.service';
import { BrowserInfoService } from '../../service/browser-info.service';
@Component({
  selector: 'ngx-corporation',
  templateUrl: './corporation.component.html',
  styleUrls: ['./corporation.component.scss']
})

export class CorporationComponent implements OnInit {
  // @ViewChild('someVar') filteredItems;
  tableList: any = [
    // {
    //   corpid : 1,
    //   corpCode : 'COR1',
    //   corpName : 'ABCL Pvt Ltd',
    //   contactPer : 'Amitabh Bacchan',
    //   email : 'amitabh@abcl.com',
    //   gst : '27AAA1232321H2Z',
    //   phone : 9999999999,
    //   status : 1,
    // },
    // {
    //   corpid : 2,
    //   corpCode : 'COR2',
    //   corpName : 'ABCLM Pvt Ltd',
    //   contactPer : 'Aishwarya Rai Bacchan',
    //   email : 'aish@abclm.com',
    //   gst : '27AAA1232338K2Z',
    //   phone : 8989898899,
    //   status : 2,
    // },
    // {
    //   corpid : 3,
    //   corpCode : 'COR3',
    //   corpName : 'Byjus Corp',
    //   contactPer : 'Byju Raveendran',
    //   email : 'byju@byju.com',
    //   gst : '27AAA8932338L8Z',
    //   phone : 1234567890,
    //   status : 1,
    // },
    // {
    //   corpid : 4,
    //   corpCode : 'COR4',
    //   corpName : 'Hindalco Pvt Ltd',
    //   contactPer : 'Ravi Shastri',
    //   email : 'ravi@hindalco.com',
    //   gst : '27AAA5652338D2Z',
    //   phone : 1111111111,
    //   status : 1,
    // },
    // {
    //   corpid : 5,
    //   corpCode : 'COR5',
    //   corpName : 'Aditya Birla Group',
    //   contactPer : 'Aditya Birla',
    //   email : 'aditya@birla.com',
    //   gst : '27AAA1232354D1Z',
    //   phone : 8765498980,
    //   status : 1,
    // },
  ];
  p: number = 1;
  dataArray: any = [];
  dataObjforExcel: any = [];
  dataArrayforExcel: any = [];
  dataObj: any = {};
  isDesc: any = [];
  column: any = [];
  flag: any;
  length: any = 0;
  search = {
    corpCode: '',
    corpName: '',
    contactPer: '',
    email: '',
    gst: '',
    phone: '',
    createdDate: '',
    totalDeals: '',
    userCount: '',
    status: '',
    dealstatus: ''
  };
  display: any = 'none';
  showDiv: boolean = true;
  display1: any = 'none';
  showDiv1: boolean = true;
  arrow: any = false;
  featureLength: any = true;
  setveractive: any = false;
  userData: any;
  bowserInfo: any;
  constructor(
    private passSer: PassServService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService,
    private router: Router,
    private toastr: ToastrService,
    private fetchCorpSer: FetchCorporationsService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
  }

  ngOnInit() {
    this.spinner.show();
    this.fetchCorps();
    setTimeout(() => {
      /* spinner ends after 1 seconds */
      this.spinner.hide();
    }, 1000);
  }

  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;
    this.tableList.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  invert() {
    this.arrow = !this.arrow;
  }

  // clear button
  clearBtn() {
    this.search.corpCode = '';
    this.search.phone = '';
    this.search.corpName = '';
    this.search.email = '';
    this.search.contactPer = '';
    this.search.gst = '';
    this.search.userCount = '';
    this.search.createdDate = '';
    this.search.totalDeals = '';
    this.search.status = '';
    this.search.dealstatus = '';
  }

  myFunction(event) {
    this.search.status = event.target.value;
    console.log('value', event);
  }

  dealStatusEvt(event) {
    this.search.dealstatus = event.target.value;
    console.log('value', event);
  }
  exportExcel() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.tableList.length; i++) {
      if (this.tableList[i].status === 1) {
        this.flag = 'Active';
      }
      if (this.tableList[i].status === 2) {
        this.flag = 'Inactive';
      }
      this.dataObjforExcel = {
        'CORPORATION CODE': this.tableList[i].corpCode,
        'CORPORATION NAME': this.tableList[i].corpName,
        'CONTACT PERSON': this.tableList[i].contactPer,
        'EMAIL': this.tableList[i].email,
        'PHONE': this.tableList[i].phone,
        'GST': this.tableList[i].gst,
        'CREATED DATE': this.formatDateNewDash(this.tableList[i].createdDate),
        'NO. OF PERSONS': this.tableList[i].userCount,
        'TOTAL DEALS': this.tableList[i].totalDeals,
        'STATUS': this.flag,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSX();
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Corporation Details');
  }

  editFeature(editData) {
    this.passSer.editCorp[0] = (editData);
    this.router.navigate(['pages/corporations/add-edit-corporations']);
  }

  AddSub() {
    this.passSer.editCorp = [];
    this.router.navigate(['pages/corporations/add-edit-corporations']);
  }

  gotoDeals(corpData) {
    this.passSer.editCorp[0] = (corpData);
    this.router.navigate(['pages/corporations/deals']);
  }

  //   public generateStarsArray(level): Array<any> {
  //     let stars = [];
  //     //---> some logic
  //     console.log(level);
  //     console.log('tl',this.tableList.length);
  //     this.length = level;
  //     return stars;
  // }

  fetchCorps() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      };
      this.fetchCorpSer.fetchCorporation(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            console.log('res', res);
            try {
              // let userD = [];
              this.tableList = res.data[0];
              if (this.tableList.length === 0) {
                this.featureLength = false;
              } else if (this.tableList.length > 0) {
                this.featureLength = true;
              }
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.tableList = [];
          this.setveractive = true;
          this.toastr.error('Server Error');
        });
    }
  }
  formatDateNewDash(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  }
  checkLen(result) {
    console.log(result);
  }
}

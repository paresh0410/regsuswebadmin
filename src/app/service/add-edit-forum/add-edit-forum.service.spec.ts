import { TestBed } from '@angular/core/testing';

import { AddEditForumService } from './add-edit-forum.service';

describe('AddEditForumService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddEditForumService = TestBed.get(AddEditForumService);
    expect(service).toBeTruthy();
  });
});

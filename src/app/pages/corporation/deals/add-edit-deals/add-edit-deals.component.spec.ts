import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditDealsComponent } from './add-edit-deals.component';

describe('AddEditDealsComponent', () => {
  let component: AddEditDealsComponent;
  let fixture: ComponentFixture<AddEditDealsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditDealsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditDealsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

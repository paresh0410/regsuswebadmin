import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class UploadNewsOpinionsImageService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  uploadNewsOpinionImageUrl = this.baseUrl + webApi.apiUrl.uploadNewsOpinionImage;
  uploadMultiImageUrl = this.baseUrl + webApi.apiUrl.uploadMultipleImage;

  constructor(public http: HttpClient) { }
  public uploadNewsOpinionImg(imageData: any): Promise<any> {
    return this.http.post(this.uploadNewsOpinionImageUrl, imageData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public uploadMultiImg(imageData: any): Promise<any> {
    return this.http.post(this.uploadMultiImageUrl, imageData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

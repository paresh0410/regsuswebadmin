import { position } from 'html2canvas/dist/types/css/property-descriptors/position';
import { SeparatorsService } from './../../../service/separators/separators.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { DatePipe } from '@angular/common';
import { AddEditNewsOpinionsService } from '../../../service/add-edit-news-opinions/add-edit-news-opinions.service';
import { UploadNewsOpinionsImageService } from '../../../service/upload-news-opinions-image/upload-news-opinions-image.service';
import { NbDialogService } from '@nebular/theme';
import { BrowserInfoService } from '../../../service/browser-info.service';
import { CommonService } from '../../../service/common.service';
import { webApi } from '../../../../config';
import { Select2OptionData } from 'ng-select2';
import { ImageLibraryPopupComponent } from '../../image-library-popup/image-library-popup.component';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
@Component({
  selector: 'ngx-separatormanagementform',
  templateUrl: './separatormanagementform.component.html',
  styleUrls: ['./separatormanagementform.component.scss']
})
export class SeparatormanagementformComponent implements OnInit {
  progress: number = 0;
  registerForm: FormGroup;
  submitted = false;
  selectRole: any;
  editUserData: any = [];
  DynamicLink: any;

  bowserInfo: any;
  categoryType: any = [];
  userStatus: any;
  userFlag: any;
  dropdownList: any = [];
  selectedItems: any = [];
  dropdownSettings: any = {
    singleSelection: false,
    idField: 'interestId',
    textField: 'intersetName',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };
  nameVal: any = new RegExp('^[A-Za-z -]+$');
  spaceval: any = new RegExp(/^\S*$/);
  disableCompany: any = false;
  public name1: String = 'Choose image or video';
  public str1: String = '';
  message: string;
  public imagePath;
  public filePath;
  imgURL: any;
  minDate: any = new Date();
  isImg: any;
  isFile: any;
  PostData: any = [];
  isCheck: string;
  nsopImage: any;
  geturl = webApi.baseUrl + webApi.apiUrl.getNewsOpinionImage;
  isSpel: any;
  notispe: any;
  emailnotispe: any;
  // image preview
  displayVideo: boolean;
  displayImage: boolean;

  imgFlag: any;
  textBg: any
  imgBg: any
  selectedImgDetails: any;
  fruit: any
  public exampleData: Array<Select2OptionData>;
  public options: any;

  public _value: string[] = [];
  public current: string;

  public decodedHtml: SafeHtml;
  imgUrl = webApi.baseUrl + webApi.apiUrl.getImg
  imageplaceholder: any = 'assets/images/IamgePlaceholder.png'
  // public Editor = ClassicEditor;

  descriptionEditor: any = 'test'
  public config = {
    toolbar: ['heading', '|',
      'fontfamily', 'fontsize',
      'alignment',
      'fontColor', 'fontBackgroundColor', '|',
      'bold', 'italic', 'custombutton', 'strikethrough', 'underline', 'subscript', 'superscript', '|',
      'link', '|',
      'outdent', 'indent', '|',
      'bulletedList', 'numberedList', '|',
      'code', 'codeBlock', '|',
      'insertTable', '|',
      'imageUpload', 'onlyInsertImage', '|', 'blockQuote', '|',
      'undo', 'redo', '|',
      'youtube',
      'mediaEmbed'
    ],
    // plugins: [OnlyInsertImage]
  }
  mycontent: string;
  // @ViewChild('myckeditor') ckeditor: CKEditorComponent;
  ckeConfig: CKEDITOR.config;

  public minPublishDate = new Date()
  editorData: any = ''
  dataAction: any
  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private passSer: PassServService,
    private datePipe: DatePipe,
    private uploadImgSer: UploadNewsOpinionsImageService,
    private addEditNwOpSer: AddEditNewsOpinionsService,

    private browserInfoService: BrowserInfoService,
    private dialogService: NbDialogService,
    private commonService: CommonService,
    private sepratorsService:SeparatorsService,
    private sanitizer: DomSanitizer
  ) {
    this.ckeConfig = {
      allowedContent: false,
      extraPlugins: 'divarea',
      forcePasteAsPlainText: true,
      removePlugins: 'exportpdf',
      height: 100
    };
    this.ckeConfig.easyimage_styles = {
      full: {
        // Changes just the class name, icon label remains unchanged.
        attributes: {
          'class': 'my-custom-full-class'
        }
      },
    }

    this.ckeConfig.image_previewText = ''
    console.log('this.ckeConfig', this.ckeConfig)
    this.PostData = this.passSer.editPost;
    this.imgFlag = this.passSer.imageflag;
    this.dataAction = this.passSer.editAction
    console.log("this.PostData", this.PostData);
    this.bowserInfo = this.browserInfoService.get_browser();

  }


  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
    }, 1000);
    this.DynamicLink = null;

    this.registerForm = this.formBuilder.group({
      postion: ['', [Validators.required]],
      contentlink: ['', [Validators.required]],
      linktype: ['', [Validators.required]],
      status: ['1', [Validators.required]],
      // editorData:['',Validators.required]
    });
    this.PostData = this.sepratorsService.sepratordata;
    if (this.PostData) {
      this.prepareEditData(this.PostData);
    }
    this.spinner.hide();

  }


  prepareEditData(data: any) {
    this.registerForm.controls.postion.setValue(data.position)
    this.registerForm.controls.contentlink.setValue(data.link)
    this.editorData = data.html
    this.registerForm.controls.linktype.setValue(data.linkType? data.linkType:'')
    this.registerForm.controls.status.setValue(data.isActive)
  }

  onSubmit() {

    this.submitted = true;
    console.log(this.registerForm.value);
     if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      console.log('section', this.registerForm)
      if (this.registerForm.invalid) {
        this.submitted = true;
        // this.toastr.error('Please fill in all required fields.');
        return;
      } else {
        this.submitted = false;
        this.addeditsection();

      }
    }

  }


  back() {
    this.router.navigate(['pages/separatormanagement'])
    this.registerForm.reset()
    this.sepratorsService.sepratordata = ''
  }

  addeditsection(){
    const rawHtml = this.decodeHtml(this.editorData);
    const content = {
      id: this.PostData ? this.PostData.id : 0,
      position: this.registerForm.value.postion,
      html: rawHtml,
      link: this.registerForm.value.contentlink,
      linkType: this.registerForm.value.linktype,
      stat: this.registerForm.value.status,
    }
    const url = webApi.baseUrl + webApi.apiUrl.addeditSeparator
    this.commonService.httpPostRequest(url, content).then((res: any) => {
      console.log(res)
      try {
        if (res.type) {
          if (res.data.saved) {
            this.toastr.success(res.data.saved);
          } else if (res.data.updated) {
            this.toastr.success(res.data.updated);
          }
          this.back()
        } else {

        }
      } catch (error) {

      }
    })

  }

  private decodeHtml(html: string): string {
    const textArea = document.createElement('textarea');
    textArea.innerHTML = html;
    return textArea.value;
  }
  get f() {
    return this.registerForm.controls;
  }

  statusChange(event) {
    if (event.currentTarget.checked) {
      this.registerForm.controls.status.setValue(1)
    } else {
      this.registerForm.controls.status.setValue(2)
    }
    console.log(this.registerForm.controls.status.value)
  }
  }



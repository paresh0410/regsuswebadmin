import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class AddEditCategoriesService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  addEditCategoryUrl = this.baseUrl + webApi.apiUrl.addEditCategory;
  constructor(public http: HttpClient) { }
  public addEditCategories(catData: any): Promise<any> {

    return this.http.post(this.addEditCategoryUrl, catData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}

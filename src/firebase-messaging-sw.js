importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-messaging.js');
  firebase.initializeApp({
    apiKey: "AIzaSyAHkj_5kh6CODWDvXv88aTIQ3hFJlNH7dA",
    authDomain: "regsusapp.firebaseapp.com",
    databaseURL: "https://regsusapp.firebaseio.com",
    projectId: "regsusapp",
    storageBucket: "regsusapp.appspot.com",
    messagingSenderId: "695210995553",
    appId: "1:695210995553:web:92a8a6d254ed5252dfdf20",
    measurementId: "G-FY0BB6DWP4"
  });
  const messaging = firebase.messaging();

  messaging.setBackgroundMessageHandler(function(payload) {

    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    var data = payload.data;
    console.log(data.type);
  
    // Customize notification here
      const notificationTitle = 'Invitació a taller grupal';
      const notificationOptions = {
          body: 'http://localhost:4200/room/' + data.roomName,
          icon: 'logo.png',
    requireInteraction: true,
      };
  
      return self.registration.showNotification(notificationTitle,
          notificationOptions);
   
  });
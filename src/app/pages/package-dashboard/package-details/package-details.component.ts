import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../../../service/excel.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { DashboardService } from '../../../service/dashboard/dashboard.service';
import { PassServService } from '../../../service/pass-serv.service';

@Component({
  selector: 'ngx-package-details',
  templateUrl: './package-details.component.html',
  styleUrls: ['./package-details.component.scss']
})
export class PackageDetailsComponent implements OnInit {

  activityDetail: any = [];
  type: any;
  category: any;
  dataObjforExcel: any;
  dataArrayforExcel: any = [];

  searchManual = {
    subname: '',
    corpname: '',
    phoneno: '',
    baseAmt: '',
    totalAmt: '',
    fromDate: '',
    gracePeriod: '',
    status: '',
  };
  invert
  constructor(
    private excelService: ExcelService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private dashboard: DashboardService,
    private passService: PassServService
  ) { }

  ngOnInit() {

    const flag = this.passService.packId;
    const startDate = this.passService.startdate;
    const endDate = this.passService.enddate;
    this.category = this.passService.pkgName;
    this.fetchDashboardDetail(startDate, endDate, flag);

  }
  isDesc: any = [];
  column: any = [];


  sort(property) {
    this.isDesc = !this.isDesc; // change the direction
    this.column = property;
    const direction = this.isDesc ? 1 : -1;


    this.activityDetail.sort(function (a, b) {
      if (a[property] < b[property]) {
        return -1 * direction;
      } else if (a[property] > b[property]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }


  fetchDashboardDetail(start, end, id) {
    this.spinner.show()
    if (!window.navigator.onLine) {
      this.spinner.hide();
      this.toastr.error('Please check your internet connection');
    } else {
      const data = {
        FrmDate: start,
        ToDate: end,
        pacId: id
      };
      this.dashboard.getDashboardPackageDetails(data).then(res => {
        this.spinner.hide();
        try {
          if (res.type === true && res.status_code === 200) {
            // console.log('res', res);
            try {
              this.activityDetail = res.data[0];
              console.log('this.activityDetail', this.activityDetail);
            } catch (e) {
              console.log(e);
            }
          }
          if (res.type === false) {
            this.spinner.hide();
            console.log('res', res);
          }
        } catch (e) {
          console.log(e);
        }
      },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
    }

  }

  goBack() {

    // this.passService.actflag = 4;
    window.history.back();
  }

  clearBtnManual() {
    this.searchManual.subname = '';
    this.searchManual.corpname = '';
    this.searchManual.phoneno = '';
    this.searchManual.baseAmt = '';
    this.searchManual.totalAmt = '';
    this.searchManual.fromDate = '';
    this.searchManual.gracePeriod = '';
    this.searchManual.status = '';

  }

  exportExcelCorp() {
    this.dataArrayforExcel = [];
    for (let i = 0; i < this.activityDetail.length; i++) {
      this.dataObjforExcel = {
        'NAME': this.activityDetail[i].subname,
        'CORPORATION': this.activityDetail[i].corpname,
        'PHONE': this.activityDetail[i].phoneno,
        'CARD TYPE': this.category,
        'START DATE': this.activityDetail[i].fromDate,
        'END DATE': this.activityDetail[i].gracePeriod,
      }
      // console.log('this.dataArray',this.dataObjforExcel)

      this.dataArrayforExcel.push(this.dataObjforExcel);
      //  for export to excel END //
    }
    this.exportAsXLSXS();
  }

  exportAsXLSXS(): void {
    this.excelService.exportAsExcelFile(this.dataArrayforExcel, 'Activity User Details');
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PassServService } from '../../../service/pass-serv.service';
import { DatePipe } from '@angular/common';
import { UploadCategoryIconService } from '../../../service/upload-category-icon/upload-category-icon.service';
import { AddEditCategoriesService } from '../../../service/add-edit-categories/add-edit-categories.service';
import { BrowserInfoService } from '../../../service/browser-info.service';


@Component({
  selector: 'ngx-edit-categories',
  templateUrl: './edit-categories.component.html',
  styleUrls: ['./edit-categories.component.scss']
})
export class EditCategoriesComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  isNotification: any = 0;
  isDeleted: any = 0;
  isSuspended: any = 0;
  isPolicyAgreed: any = 0;
  isotpVerified: any;
  userData :any;
  bowserInfo:any;
  roleArray: any = [];
  selectRole: any;
  editCatgData: any = [];
  userStatus: any;
  userFlag: any;
  public maxDate = new Date();
  valdLbl: any = false;
  maxLength: any = false;
  ABmaxLength: any = false;
  icon: any;
  typeName: any;
  public imagePath;
  public filePath;
  imgURL: any;
  isImg: any;
  isFile: any;
  isCheck: any;
  isCheck1: any;
  public message: string;
  getEditArray: any = [];
  splitArray: any = [];
  refString: any;
  hideLabel: boolean = false;
  splitArray1: any = [];
  cicon: any;
  errorImsg: boolean = false;

  constructor(
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private passSer: PassServService,
    private datePipe: DatePipe,
    private uploadISer: UploadCategoryIconService,
    private addEditCatSer: AddEditCategoriesService,
    private browserInfoService: BrowserInfoService,
  ) {
    this.editCatgData = this.passSer.editCategory;
    this.userData = JSON.parse(localStorage.getItem('UserData'));
    this.bowserInfo = this.browserInfoService.get_browser();
    console.log('this.editCatgData', this.editCatgData);
    if (this.editCatgData[0]) {
      this.imgURL = this.editCatgData[0].catIcon;
    }
  }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /* spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    if(this.editCatgData[0]) {
      if(this.editCatgData[0].catIcon == null) {
        this.registerForm = this.formBuilder.group({
          cat: [this.editCatgData[0].cat, [Validators.required]],
          cDate: [this.editCatgData[0].cDate],
          status: [this.editCatgData[0].status, [Validators.required]],
        });
      } else {
        this.splitArray1 = this.editCatgData[0].catIcon.split('path=');
        this.cicon = this.splitArray1[this.splitArray1.length - 1];
        this.registerForm = this.formBuilder.group({
          cat: [this.editCatgData[0].cat, [Validators.required]],
          cDate: [this.editCatgData[0].cDate],
          status: [this.editCatgData[0].status, [Validators.required]],
        });
      }
     
    } else {
      this.registerForm = this.formBuilder.group({
        cat: ['', [Validators.required]],
        cDate: [''],
        status: ['1', [Validators.required]],
      });
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    let formData = new FormData();
    console.log(this.registerForm.value);
    // if (!this.imgURL) {
    //   this.isImg = 'Please select file';
    //   return;
    // }
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      const content = {
        c_id : 0,
        c_name : this.registerForm.value.cat,
        c_icon : this.imagePath == undefined ? null : this.cicon,
        c_isActive : this.registerForm.value.status,
        uid: this.userData.userid,
        uname: this.userData.subname,
        bname: this.bowserInfo.name,
        bversion: this.bowserInfo.version,
      }
      if (this.editCatgData[0]) {
        if (this.editCatgData[0].categoryId) {
          content.c_id = this.editCatgData[0].categoryId;
        }
      }
      if (this.imagePath) {
        formData.append('file', this.imagePath[0], this.imagePath[0].name);
      }

      // service for uploading category images
      if (this.imagePath) {
        this.uploadISer.uploadCatIcon(formData).then(res => {
          this.spinner.hide();
          try {
            if (res.error_code === 0) {
              console.log('res', res);
              content.c_icon = res.path;
  
                // service for add edit category
        this.addEditCatSer.addEditCategories(content).then(res => {
          this.spinner.hide();
          try {
            if (res.type === true && res.status_code === 200) {
              console.log('res', res);
              try {
                if (res.data) {
                  if (res.data.error) {
                    this.toastr.error(res.data.error);
                  } else if (res.data.updated) {
                    this.router.navigate(['pages/categories']);
                    this.toastr.success(res.data.updated);
                } else if (res.data.saved) {
                  this.router.navigate(['pages/categories']);
                  this.toastr.success(res.data.saved);
              }
                }
              } catch (e) {
                console.log(e);
              }
            }
            if (res.type === false) {
              this.spinner.hide();
              console.log('res', res);
            }
          } catch (e) {
            console.log(e);
          }
        },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
            }
            if (res.error_code === 1) {
              this.spinner.hide();
              console.log('res', res);
              this.toastr.error(res.err_desc);
            }
          } catch (e) {
            console.log(e);
          }
        },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
      } else {
        this.addEditCatSer.addEditCategories(content).then(res => {
          this.spinner.hide();
          try {
            if (res.type === true && res.status_code === 200) {
              console.log('res', res);
              try {
                if (res.data) {
                  if (res.data.error) {
                    this.toastr.error(res.data.error);
                  } else if (res.data.updated) {
                    this.router.navigate(['pages/categories']);
                    this.toastr.success(res.data.updated);
                } else if (res.data.saved) {
                  this.router.navigate(['pages/categories']);
                  this.toastr.success(res.data.saved);
              }
                }
              } catch (e) {
                console.log(e);
              }
            }
            if (res.type === false) {
              this.spinner.hide();
              console.log('res', res);
            }
          } catch (e) {
            console.log(e);
          }
        },
        err => {
          console.log(err);
          this.spinner.hide();
          this.toastr.error('Server Error');
        });
      }

      // console.log('success');
      // this.toastr.success('Data Saved Successfully');
      // this.router.navigate(['pages/categories']);
    }
  }
  back() {
    this.router.navigate(['pages/categories']);
  }
  checkVal(event) {
    console.log('event', event);
    if (event.target.placeholder === 'Category Name') {
      if (!event.target.value) {
        this.toastr.error('Category Name is required');
      }
    }
  }
  // image preview
  public name1: String = 'Choose image';
  public str1: String = '';

  preview(files) {
    this.name1 = this.str1;
    const str11: any[] = this.name1.split('\\');
    this.name1 = str11[str11.length - 1];
    var size = files[0].size;

    if (files.length === 0) {
      return;
    }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.name1 = 'Choose image';
      this.message = 'Only images are supported.';
      return;
    } else {
      if(size > 5266468){
        this.errorImsg = true;
        this.imgURL = '';
      } else {
        this.errorImsg = false;
        this.message = '';
        const reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
          this.imgURL = reader.result;
        };
        this.getEditArray.thumbnail = this.imgURL;
      }
    }
  }
  removeImage() {
    this.str1 = '';
    this.name1 = 'Choose Image';
    this.imgURL = '';
    this.getEditArray.thumbnail = '';
  }
}

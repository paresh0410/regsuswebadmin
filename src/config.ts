import { Injectable } from '@angular/core';

@Injectable()
export class Config {
}

export const webApi = {
    // baseUrl: 'http://13.232.192.230:9775/',            // New Regsus DEV
    // baseUrl: 'http://dev.regsus.com/',        // New Regsus DEV
    // baseUrl: 'http://test.regsus.com/',            // New Regsus TEST
    // baseUrl: 'http://mic.regsus.com/',        //  Regsus LIVE
    // baseUrl: 'https://mobeserv.com:9775/', // New Regsus DEV
      baseUrl: 'http://localhost:9775/',

    apiUrl: {
        /********************* Auth ********************/
        loginAdmin: 'api/webapi/adminLogin',
        loginForgot: 'api/webapi/forgotPassword',
        changePass: 'api/webapi/changePassword',
        fetchFeatures: 'api/webapi/fetchfeatures',
        editFeatures: 'api/webapi/editfeatures',
        fetchActivityReport: 'api/webapi/fetchDashboardActivity',
        fetchActivityDetail: 'api/webapi/fetchDashboardActivityDetail',
        fetchPackageDetail: 'api/webapi/fetchDashboardPackageDetail',
        fetchDashboardPackage: 'api/webapi/FetchReportPackageDashborad',
        fetchTrendPackage: 'api/webapi/FetchReportTrendDashborad',
        fetchDuration: 'api/webapi/fetchDurations',
        addEditDuration: 'api/webapi/addeditDurations',
        fetchRateCard: 'api/webapi/fetchRateCards',
        addEditRateCard: 'api/webapi/addeditRateCard',
        fetchCorporation: 'api/webapi/fetchCorporations',
        addEditCorporation: 'api/webapi/addeditCorporations',
        fetchSubscribers: 'api/webapi/fetchSubscribers',
        getTemplate: 'api/webapi/getSubscriberTemplate',
        fetchEmailTemplate: 'api/webapi/fetchEmailTemplates',
        editEmailTemplate: 'api/webapi/editEmailTemplates',
        addSubcribers: 'api/webapi/uploadAndSaveSubscriber',
        fetchCorporationDeals: 'api/webapi/fetchCorporationsDeals',
        addEditDeal: 'api/webapi/addeditCorporationDeals',
        fetchDealSubscribers: 'api/webapi/fetchSubscribersForDeals',
        fetchNormalSubscribers: 'api/webapi/fetchNormalSubscribers',
        editNormalSubscribers: 'api/webapi/editNormalSubscribers',
        logoutSubscribers: 'api/webapi/logoutSubscribers',
        fetchDealsOfCorp: 'api/webapi/fetchCorporationPenddingDeals',
        uploadSubDeals: 'api/webapi/uploadDealsForSubscriber',
        fetchInterests: 'api/webapi/fetchInterests',
        addEditInterests: 'api/webapi/addEditInterests',
        fetchCategories: 'api/webapi/fetchCategories',
        getCatImage: 'api/webapi/getCategoryImage?path=',
        uploadCatIcon: 'api/webapi/uploadCategoryImage',
        addEditCategory: 'api/webapi/addEditCategory',
        fetchNewsOpinions: 'api/webapi/fetchNewsOpinion',
        fetchNewsOpinionViewDetail: 'api/webapi/fetchNewsOpinionViewDetail',
        fetchFavViewDetail: 'api/webapi/fetchDetailOfNewsFavourite',

        getNewsOpinionImage: 'api/webapi/getNewsOpinionImage?path=',
        uploadNewsOpinionImage: 'api/webapi/uploadNewsOpinionImage',
        uploadMultipleImage: 'api/webapi/uploadNewsOpinionmultipleImages',
        uploadPromotionImage: 'api/webapi/uploadPromotionImage',
        addEditNewsOpinions: 'api/webapi/addeditNewsOpinion',
        searchNwOp: 'api/webapi/fetchSearchNewsOpinion',
        fetchpolls: 'api/webapi/fetchPoll',
        fetchPollsViewDetail: 'api/webapi/fetchPollViewDetail',

        serachPolls: 'api/webapi/fetchSearchPoll',
        addEditPoll: 'api/webapi/addeditPoll',
        fetchForum: 'api/webapi/fetchForum',
        fetchForumFavDet: 'api/webapi/fetchDetailOfForumFavourite',
        fetchForumFavView: 'api/webapi/fetchDetailOfForumView',
        searchForums: 'api/webapi/fetchSearchForum',
        addEditForum: 'api/webapi/addeditForum',
        fetchForumComments: 'api/webapi/fetchForumComments',
        saveForumComment: 'api/webapi/saveforumComment',
        saveForumCommentReply: 'api/webapi/saveforumCommentReply',
        fetchSubDeals: 'api/webapi/fetchSubscriberDeals',
        changePhoneNo: 'api/webapi/changePhoneNo',
        checkPhoneAvailaiblity: 'api/webapi/checkPhoneAvailaiblity',
        fetchPackage: 'api/webapi/fetchPackages',
        addEdiPackage: 'api/webapi/addEditPackages',
        fetchPromotion: 'api/webapi/fetchPromotion',
        addEditPromotion: 'api/webapi/addeditPromotion',
        fetchMSReport: 'api/webapi/fetchMSReport',
        getMsReportFile: 'api/webapi/getMsReportFile',
        uploadMsReportFile: 'api/webapi/uploadMsReportFile',
        addEditMsReport: 'api/webapi/addeditMSReport',
        downLoadMsReport: 'api/webapi/dowmloadMsReportFile',
        assignAllPackage: 'api/webapi/assignDefaultPackagetoUser',
        assignSpecialPackage: 'api/webapi/assignSpecialPackagetoUser',
        saveAdminUserDeviceInfo: 'api/webapi/saveAdminUserDeviceInfo',
        getDynamiclink: 'api/webapi/getNewsOpinionDynamiclink',

        fetchBroadcast: 'api/webapi/fetchBroadcast',
        addEditBroadcast: 'api/webapi/addEditBroadcast',
        fetchSection: 'api/webapi/fetchSection',
        addEditSection: 'api/webapi/addEditSection',
        sectionDropdown: 'api/webapi/sectionDropdown',
        setSectionOrder: 'api/webapi/setSectionOrder',
        newsopinionDropdown: 'api/webapi/newsopinionDropdown',
        addeditNewsOpinion_new: 'api/webapi/addeditNewsOpinion_new',
        fetchPackages_new: 'api/webapi/fetchPackages_new',
        addEditPackages_new: 'api/webapi/addEditPackages_new',
        packageDropdown: 'api/webapi/packageDropdown',
        fetchImageLibrary: 'api/webapi/fetchImageLibrary',
        addImages: 'api/webapi/addImages',
        uploadImages: 'api/webapi/addImages',
        getImg: 'api/webapi/getImagelibrary?path=',
        moveSubscribertoCorporation: 'api/webapi/moveSubscribertoCorporation',

        sendCorporateEmail: 'api/webapi/sendCorporateEmail',

        fetchLiveFeed:'api/webapi/fetchLiveFeeds',
        addEditLiveFeed:'api/webapi/addeditLiveFeed',

        addeditSeparator:'api/webapi/addeditSeparator',
        fetchSeparator:'api/webapi/fetchSeparators',

        saveEditSectionData: '',
        saveEditBroadcastData: '',
        fetchImageLibraryList: ''

    },
    appId: 'regsusWeb',
};

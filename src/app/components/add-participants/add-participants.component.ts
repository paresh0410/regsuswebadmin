import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-add-participants',
  templateUrl: './add-participants.component.html',
  styleUrls: ['./add-participants.component.scss']
})
export class AddParticipantsComponent implements OnInit {
  @Input() participants: any = [];
  @Input() image: any;
  @Input() interests: any = [];
  @Input() corporations: any = [];
  @Input() selectedList: any = [];
  selectedItems: any = [];
  settings = { 
    text:'Select Participants',
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    showCheckbox: true,
    badgeShowLimit: 4,
  };
  selectedIns: any = [];
  dropdownSettings: any = {
    singleSelection: false,
    idField: 'interestId',
    textField: 'iname',
    itemsShowLimit: 1,
    allowSearchFilter: true,
  };
  selectedCorps: any = [];
  dropdownCorpSettings: any = {
    singleSelection: false,
    idField: 'corpId',
    textField: 'corname',
    itemsShowLimit: 1,
    allowSearchFilter: true,
  };
  constructor() {
  }

  ngOnInit() {
    console.log('participants',this.participants);
    console.log('image',this.image);
    console.log('int',this.interests);
    console.log('corp',this.corporations);
    this.selectedItems = this.selectedList;
  }
  onItemSelect(item: any) {
    console.log("Event Select",item);
    console.log(this.selectedItems);
    //this.selectedItems=item;
  }
  OnItemDeSelect(item: any) {
    console.log("Event",item);
    console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onItemDeSelect(itemsall){
    
  }
}

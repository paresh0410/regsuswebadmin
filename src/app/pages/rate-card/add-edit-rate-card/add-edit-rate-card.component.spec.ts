import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditRateCardComponent } from './add-edit-rate-card.component';

describe('AddEditRateCardComponent', () => {
  let component: AddEditRateCardComponent;
  let fixture: ComponentFixture<AddEditRateCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditRateCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditRateCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

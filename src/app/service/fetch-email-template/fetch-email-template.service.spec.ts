import { TestBed } from '@angular/core/testing';

import { FetchEmailTemplateService } from './fetch-email-template.service';

describe('FetchEmailTemplateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchEmailTemplateService = TestBed.get(FetchEmailTemplateService);
    expect(service).toBeTruthy();
  });
});

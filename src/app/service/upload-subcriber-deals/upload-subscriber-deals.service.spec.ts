import { TestBed } from '@angular/core/testing';

import { UploadSubscriberDealsService } from './upload-subscriber-deals.service';

describe('UploadSubscriberDealsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadSubscriberDealsService = TestBed.get(UploadSubscriberDealsService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { AssignAlluserPackageService } from './assign-alluser-package.service';

describe('AssignAlluserPackageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssignAlluserPackageService = TestBed.get(AssignAlluserPackageService);
    expect(service).toBeTruthy();
  });
});

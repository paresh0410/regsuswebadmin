import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { webApi } from '../../../config';
import { CommonService } from '../../service/common.service';

@Component({
  selector: 'ngx-image-library',
  templateUrl: './image-library.component.html',
  styleUrls: ['./image-library.component.scss']
})
export class ImageLibraryComponent implements OnInit, OnDestroy {
  ddlList: any = {
    sortList: [
      { id: 1, name: 'Created ascending', selected: 0 },
      { id: 2, name: 'Created descending', selected: 0 },
      { id: 3, name: 'Sort by name ascending', selected: 0 },
      { id: 4, name: 'Sort by name descending', selected: 0 },
    ],
    categoryTypeList: []
  }
  imageList: any = [
    { id: 1, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image' },
    { id: 2, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image' },
    { id: 3, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Excel' },
    { id: 4, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image' },
    { id: 5, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Pdf' },
    { id: 1, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image' },
    { id: 2, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image' },
    { id: 3, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Excel' },
    { id: 4, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Image' },
    { id: 5, imgUrl: '../../../assets/images/Blue_bg.jpg', type: 'Pdf' }

  ]
  selectedImgDetails: any;
  previewImg: any
  @ViewChild('fileInput') myInputVariable: ElementRef;
  imageLibraryData: any = []
  imageplaceholder: any = 'assets/images/IamgePlaceholder.png'
  pdfplaceholder: any = 'assets/images/pdfPlaceholder.png'
  excelplaceholder: any = 'assets/images/excelPlaceholder.png'
  videoplaceholder: any = 'assets/images/excelPlaceholder.png'
  imgUrl = webApi.baseUrl + webApi.apiUrl.getImg
  count: any
  pageLimit: any = 15
  currentPage = 1
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  totalPages: any
  filetype: any = ''
  sortOrder: any = 2

  constructor(private commonService: CommonService, private toastr: ToastrService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.fetchImageData(this.currentPage)
    // this.imageLibraryData = this.commonService.fetchImageLibraryList()
    window.addEventListener('scroll', this.scroll, true);
  }

  scroll = (event: any): void => {
    // Here scroll is a variable holding the anonymous function 
    // this allows scroll to be assigned to the event during onInit
    // and removed onDestroy
    // To see what changed:
    // const number = event.srcElement.scrollTop;
    // console.log(event);
    if (this.currentPage != 0) {
      console.log(event.target.scrollHeight - event.target.scrollTop - event.target.clientHeight)
      if (event.target.scrollHeight - event.target.scrollTop - event.target.clientHeight < 5) {
        console.log('true')
        // if (!this.isworking) {
        this.currentPage++;
        if (this.currentPage <= this.totalPages) {
          this.fetchImageData(this.currentPage);
        }
        // }
      } else {
        console.log('false')
      }
    }

  };

  uploadImage(event) {
    console.log('uploadImage', event)
    const files = event.target.files;
    if (files.length === 0)
      return;
    // } else {
    //   for (let i = 0; i < files.length; i++) {
    //     if (files[i].type) {
    //       const fType = files[i].name.split('.')
    //       if (fType[1] == 'png' || fType[1] == 'pdf' || fType[1] == 'xlsx') {
    //         console.log('valid file')
    //       } else {
    //         console.log('invalid file')
    //       }
    //     }
    //   }
    // }
    if (files[0].name) {
      const nameSplit = files[0].name.split('.')
      if (nameSplit.length > 2) {
        this.toastr.error('Please upload file with proper name');
        return
      }
      const name = nameSplit[0]
      console.log(name)
      // this.checkWhiteSpace(name)

      const outputforSpace = this.hasWhiteSpace(name)
      console.log('outputforSpace', outputforSpace)
      if (outputforSpace) {
        this.toastr.error('Please upload file with proper name');
        return
      }

      // this.checkSpecialCharacters(name)
      const output = name.match(/[~!@#$%^&*(\\\/\-['`;=+\]),.?":{}|<>_]/g);
      console.log(output);
      if (output) {
        this.toastr.error('Please upload file with proper name');
        return
      }
    }

    console.log('ppp')

    const reader = new FileReader();
    this.selectedImgDetails = files
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.previewImg = reader.result;
    }
    // this.addFile()
    this.uploadImg()
  }

  checkWhiteSpace(name) {
    const outputforSpace = this.hasWhiteSpace(name)
    console.log('outputforSpace', outputforSpace)
    if (outputforSpace) {
      this.toastr.error('Please upload file with proper name');
      return
    }
  }

  checkSpecialCharacters(name) {
    const output = name.match(/[~!@#$%^&*(\\\/\-['`;=+\]),.?":{}|<>_]/g);
    console.log(output);
    if (output) {
      this.toastr.error('Please upload file with proper name');
      return
    }
  }

  hasWhiteSpace(s) {
    const whitespaceChars = [' ', '\t', '\n'];
    return whitespaceChars.some(char => s.includes(char));
  }

  fetchImageData(currentPage) {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      this.spinner.show();

      const param = {
        bname: this.commonService.bowserInfo.name,
        bversion: this.commonService.bowserInfo.version,
        uid: this.commonService.userData.userid,
        uname: this.commonService.userData.subname,
        pNo: currentPage,
        datalimit: this.pageLimit,
        searchStr: '',
        filetype: this.filetype ? (this.filetype) : null,
        orderby: this.sortOrder
      }
      console.log(param)
      const url = webApi.baseUrl + webApi.apiUrl.fetchImageLibrary
      this.commonService.httpPostRequest(url, param).then((res: any) => {
        console.log(res)
        try {
          if (res.type) {
            this.totalPages = res.totalcount[0].totalPages
            this.ddlList.categoryTypeList = res.filetypedropdown
            if (this.imageLibraryData.length > 0 && currentPage > 1) {
              this.imageLibraryData = this.imageLibraryData.concat(res.data);
            } else {
              this.imageLibraryData = res.data
            }
            this.spinner.hide();

            // this.imageLibraryData = res.data
            // this.myInputVariable.nativeElement.value = "";
          }
        } catch (error) {
          this.spinner.hide();

        }
      })
    }
  }

  uploadImg() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      if (this.selectedImgDetails) {
        this.spinner.show()
        // this.imgErrors.isValid = false
        // this.spinner.show();
        let formData: any = new FormData();
        // let filenames: any = []
        // for (let i = 0; i < this.selectedImgDetails.length; i++) {
        //   console.log(this.selectedImgDetails[i])
        //   if (this.selectedImgDetails[i].name) {
        //     filenames.push(this.selectedImgDetails[i].name)
        //   }
        // }
        // console.log(filenames)
        if (this.selectedImgDetails.length > 0) {
          for (let i = 0; i < this.selectedImgDetails.length; i++) {
            formData.append('file', this.selectedImgDetails[i], this.selectedImgDetails[i].name);
          }
        }
        // formData.append('file', this.selectedImgDetails[0], this.selectedImgDetails[0].name);
        formData.append('bname', this.commonService.bowserInfo.name);
        formData.append('bversion', this.commonService.bowserInfo.version);
        formData.append('uid', this.commonService.userData.userid);
        formData.append('uname', this.commonService.userData.subname);
        console.log(formData)
        const url: any = webApi.baseUrl + webApi.apiUrl.uploadImages;
        this.commonService.httpPostRequest(url, formData).then((res: any) => {
          console.log(res)
          try {
            if (res.type) {
              if (res.error_code == 0) {
                this.toastr.success(res.err_desc)
                this.imageLibraryData = []
                this.currentPage = 1
                this.fetchImageData(this.currentPage)
              } else {
                this.toastr.error(res.err_desc)
              }
              this.spinner.hide()
            }
          } catch (error) {
            this.spinner.hide()
            console.log(error)
          }
        })
      } else {
        // this.imgErrors.requiredText = "Please upload your AOF Form before processing."
        // this.imgErrors.isValid = true
      }
    }
  }
  addFile() {
    if (!window.navigator.onLine) {
      this.toastr.error('Please check your internet connection');
    } else {
      const param = {
        bname: this.commonService.bowserInfo.name,
        bversion: this.commonService.bowserInfo.version,
        uid: this.commonService.userData.userid,
        uname: this.commonService.userData.subname,
        fileName: this.selectedImgDetails.name,
        fileurl: null,
        mimetype: null,
        filesize: this.selectedImgDetails.size,
        filetype: this.selectedImgDetails.type
      }
      console.log(param)
      const url = webApi.baseUrl + webApi.apiUrl.fetchImageLibrary
      // this.commonService.httpPostRequest(url, param).then((res: any) => {
      //   console.log(res)
      //   try {
      //     if (res.type) {
      //       this.imageLibraryData = res.data
      //     }
      //   } catch (error) {

      //   }
      // })
    }
  }


  onSortChange(event) {
    console.log(event.target.value)
    this.sortOrder = event.target.value
    this.currentPage = 1
    this.imageLibraryData = []
    this.fetchImageData(this.currentPage)
  }

  onCategoryChange(event) {
    console.log(event.target.value)
    this.filetype = event.target.value
    this.currentPage = 1
    this.imageLibraryData = []
    this.fetchImageData(this.currentPage)
  }

  ngOnDestroy(): void {
    this.currentPage = 0
  }
}

import { TestBed } from '@angular/core/testing';

import { FetchFeaturesService } from './fetch-features.service';

describe('FetchFeaturesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchFeaturesService = TestBed.get(FetchFeaturesService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { webApi } from '../../../config';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  baseUrl: any = webApi.baseUrl;
  private headers = new HttpHeaders();
  webLoginUrl = this.baseUrl + webApi.apiUrl.loginAdmin;
  forgotUrl = this.baseUrl + webApi.apiUrl.loginForgot;
  changePassUrl = this.baseUrl + webApi.apiUrl.changePass;
  constructor(public http: HttpClient) { }
  public webLogin(loginData: any): Promise<any> {

    return this.http.post(this.webLoginUrl, loginData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public forgotLogin(loginData: any): Promise<any> {

    return this.http.post(this.forgotUrl, loginData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  public changePassword(loginData: any): Promise<any> {

    return this.http.post(this.changePassUrl, loginData, { headers: this.headers })
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
  }

  

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}
